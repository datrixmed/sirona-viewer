#ifndef DEVICEHANDLER_H
#define DEVICEHANDLER_H

#include <atomic>

#include <QObject>
#include <QFile>
#include <QMap>

#if defined(Q_OS_WIN)
#include <winsock2.h>
#endif

#include "sironadriver.h"
#include "interface.h"
#include "dataformat.h"
#include "ecgdata.h"

struct deviceInfo_t {
	friend bool operator <( const deviceInfo_t &lhs, const deviceInfo_t &rhs )
	{
		if ( lhs.devType != rhs.devType ) {
			return lhs.devType > rhs.devType;
		}

		return strcmp( lhs.devSerialNumber, rhs.devSerialNumber ) < 0;
	}

	sirona_handle handle = nullptr;
	char devSerialNumber[11] = {0};
	char devPortName[100] = {0};
	int  devType = 0;
};

class DeviceHandler : public QObject
{
	Q_OBJECT

public:
	explicit DeviceHandler( QObject *parent = 0 );
	void deviceStopLiveECG();
	void deviceStopWaitingForStorage();
	int device_key[4];
    bool manufacturing;
    bool generateResFile;
	void resetReconnectTimer();
	void stopReconnectTimer();

protected:
	void timerEvent( QTimerEvent *event );

private:
	int deviceEventGetHeader( quint32 eventNo, EventHeader &eventHeader );
	deviceInfo_t devInfo;
	ConfigParams devConfig;
	volatile bool m_TerminateStreaming;
	volatile bool m_TerminateLiveECG;
	int m_ConnectionTimer;
	std::atomic<bool> m_StopWaitingForStorage;
	QString zip_password;
	QString prefixForZip;

signals:
	void sDeviceConnectStatus( bool connected, QString devSerNum, int devType );
	void sDeviceHandlerMessage( bool silant, bool success, QString message );
	void sDeviceConfiguration( ConfigParams devConf );
	void sDeviceParameterListWritten ( bool parameterListWrittenWithSuccess );
	void sDeviceDataSaved ( bool eventSaveSuccess, bool holterSaveSuccess, QList<QString> httpFileList );
	void sDeviceDataDeleted ( bool deleteSuccess );
	void sDeviceEventList( QList<QString> eventList );
	void sDeviceBatteryStatus( quint32 batteryVoltage );
	void sDeviceProcedureStarted();
	void sDeviceProcedureStopped();
	void sDeviceDownloadProgress( quint32 current, quint32 total );
    void sDeviceThereIsDataRecorded( bool hasData );
	void sDeviceHolterDownloadRetry();
	void sDeviceEnumerateBtDevices( QList<deviceInfo_t> devices );
	void sDeviceStartLiveECG();
	void sDeviceLiveECG( uint32_t seq, uint32_t status, QByteArray data );
	void sDeviceStopLiveECG( int errorCode );
	void sDeviceWarning( QString description );
	void sDeviceComputerOutOfSpace( QString message );
    void requiredFields(uint8_t, uint8_t, uint8_t, uint8_t, uint8_t, uint8_t, uint8_t, uint8_t, uint8_t);
    void sDeviceStatus(quint8, quint8, quint8, quint8);
    void sDeviceSoftResetComplete();
    void showFloatingDisplay(QString);

public slots:
	void enumerateBtDevices();
	void connectDevice( deviceInfo_t device );
	void deviceDisconnect( bool silant, int retVal );
	void isDeviceConnected();
	void devicePopulateConfig( ConfigParams devConf );
	void deviceIsThereRecordedData();
	void deviceSaveData( bool saveEvents, QString eventPath, bool saveHolter, QString holterPath );
	void deviceDeleteData();
	void deviceParameterRead( QString parName );
	void deviceParameterListWrite( ConfigParams devConf, QList<QString> paramNamesList );
	void deviceParameterReadAll();
	void deviceStartLiveECG();
	void deviceGetBatteryStatus();
	void deviceStartProcedure();
	void deviceStopProcedure();
	void deviceSoftReset();
	void deviceSetTime();
	void markFilesSent( qint32 );
	void setZipPassword( QString zip_password );
	void getRequiredFields();
    void getStatus();
	void logDownload();
	int deviceEventCount();
	void setZipPrefix( QString prefix ) { devConfig.sitecode = prefix; }

private:
    int bulkReadAll( QByteArray * data );
    int holterDownload( const QString &path, quint32 procedureTime );
	int eventDownload( const QString &path, quint32 eventNo, EventHeader eventHeader, quint32 eventCount );
	int parameterCommit();
	int parameterWrite( const QString &parName );
	void populateConfigEntry( ConfigParams &devConf, void *name, quint32 valueSize, void *value );
	void createHeaFile( quint8 device_mode, QString datFileName, EventHeader *eventHeader );
	void createJsonFile( QString datFileName, EventHeader *eventHeader );
    void createResFile(char *serialNumber, uint32_t startTime, QFile *datFile);
	void createZipFile( QString destinationPath, QList<QString> paths , QString password );
	void zipEventFiles( QString datFileName );
};

#endif // DEVICEHANDLER_H
