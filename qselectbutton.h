#pragma once

#include <QWidget>
#include <QStringList>
#include <QVector>
#include <QColor>
#include <QPen>

class QSelectButton : public QWidget
{
	Q_OBJECT
	Q_PROPERTY( QStringList items READ items WRITE setItems NOTIFY itemsChanged DESIGNABLE true )
	Q_PROPERTY( int radius READ radius WRITE setRadius NOTIFY radiusChanged DESIGNABLE true )
	Q_PROPERTY( int margin READ margin WRITE setMargin NOTIFY marginChanged DESIGNABLE true )
	Q_PROPERTY( int outlineWidth READ outlineWidth WRITE setOutlineWidth NOTIFY outlineWidthChanged DESIGNABLE true )
public:
	explicit QSelectButton( QWidget *parent = 0 );
	void setItems( const QStringList items );
	QStringList items() const;
	void setRadius( int radius );
	int radius() const;
	void setMargin( int margin );
	int margin() const;
	void setOutlineWidth( int width );
	int outlineWidth() const;
	void setCurrentIndex( int index );
	int currentIndex() const;

signals:
	void itemsChanged();
	void radiusChanged();
	void marginChanged();
	void outlineWidthChanged();
	void indexChanged( int index );

public slots:

protected:
	void paintEvent( QPaintEvent *event );
	void resizeEvent( QResizeEvent *event );
	void mouseMoveEvent( QMouseEvent *event );
	void mousePressEvent( QMouseEvent *event );
	void leaveEvent( QEvent *event );

private:
	void updateVerticalBarPositions();
	QStringList m_items;
	QVector<int> m_verticalBarPositions;
	QColor m_selectedItemColor;
	QColor m_hoverItemColor;
	int m_margin = 1;
	int m_radius = 16;
	int m_selectedIndex = -1;
	int m_hoverIndex = -1;
	int m_outlineWidth = 2;
};
