#ifndef PRINTPREVIEWER_H
#define PRINTPREVIEWER_H

#include <QtGlobal>
#include <QtPrintSupport/QPrintPreviewWidget>
#include <QVBoxLayout>
#include <QWidget>

#define REPORT_FILENAME	"sirona_report"


class PdfPrinter : public QWidget
{
	Q_OBJECT

public:
	explicit PdfPrinter( QString pathname );
	~PdfPrinter();

	void init();
	void initPrinter();
	void zoomIn();
	void zoomOut();

	void setLayoutParent( QBoxLayout *layout )
	{
		layoutParent = layout;
	}

	QPrinter *getPrinter()
	{
		return pPrinter;
	}

private:
	QString pathnameReport;
	QPrinter *pPrinter;
	QPrintPreviewWidget *ppw;
	QBoxLayout *layoutParent;

	qreal zoomFactor;

signals:
	void paintRequested( QPrinter *thePrinter );

public slots:
	void doPrint()
	{
		if ( ppw ) {
			ppw->print();
		}
	}

};

#endif // PRINTPREVIEWER_H
