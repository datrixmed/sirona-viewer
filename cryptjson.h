#ifndef CRYPTJSON_H
#define CRYPTJSON_H

extern char * encryptJSON( char * map );
extern char * decryptJSON( char * mapEncrypted );

#endif // CRYPTJSON_H
