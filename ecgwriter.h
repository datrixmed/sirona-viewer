#ifndef __FILEFORMAT_H
#define __FILEFORMAT_H

#include <QFile>
#include <stdint.h>

#define BITPOSITION_EVENT	(0x40000000)
#define BITPOSITION_PACE	(0x80000000)

#define MASK_THESE_BITS(b,bits)	((uint32_t) ((b) & ((1 << (bits)) - 1)))


class EcgFormatWriter : public QFile
{
	Q_OBJECT

public:
	EcgFormatWriter();
	~EcgFormatWriter();
	void close();

	void setFormat( int theFormat )
	{
		mFormat = theFormat;
	}
	int getFormat()
	{
		return mFormat;
	}

	void setSampleRate( int theSampleRate )
	{
		sampleRate = theSampleRate;
	}
	int getSampleRate()
	{
		return sampleRate;
	}

	qint64 write( const char *data, qint64 maxSize );

	void writeEvent( long samplePos );

protected:

	int	mFormat;
	int sampleRate;
	QByteArray mEcgBuf;

signals:

};


#endif //__FILEFORMAT_H
