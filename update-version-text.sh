#!/bin/bash

REV=$(git describe | tail -c 8 )

cp -f installer/SironaViewer-linux.iss /tmp/SironaViewer-linux.iss
sed --in-place "s/define MyVersion.*/define MyVersion\t\t\"$REV\"/" /tmp/SironaViewer-linux.iss
diff installer/SironaViewer-linux.iss /tmp/SironaViewer-linux.iss
if [ $? -ne 0 ]
then
	mv -f /tmp/SironaViewer-linux.iss installer/SironaViewer-linux.iss
	echo Updated SironaViewer-linux.iss to Rev: $REV
fi

