/**
 * @file ecgdata.h
*/

#if !defined(ECGDATA_H)
#define ECGDATA_H

#include <QDataStream>
#include <QString>
#include <QObject>
#include <QHash>

#define MARKER_MISSING_DATA	(24999)

#define MAX_CHANNELS (12)

#define ECG_DEFAULT_FAKE_BYTE	(0)


/* {{{ class EcgData
   @brief	class to manage streams of ECG data
*/
class EcgData : public QObject
{
	Q_OBJECT

public:
	EcgData();
	~EcgData();

	void save( QString fileName );
	void restore( QString fileName );

	ulong size()
	{
		return datalen_secs * samps_per_chan_per_sec;    /* return samples per channel */
	}

	long *get( int channel_num, long start_time_samps, long duration_samps );
	long getRaw( int channel_num, long start_time_samps );

	void STORE_INTO_CHDATA( int ch, int i, long val );
	void STORE_RAW_INTO_CHDATA( int ch, int i, long val );

	double range_per_sample;
	double device_range_mV;
	int channel_count;
	int datalen_secs;
	int samps_per_chan_per_sec;
	int signal_format_specifier;
	float bytes_per_samp;
	quint32 utcStartTime;
	int samples_len;

	long *chData[MAX_CHANNELS];
	long sizeChData[MAX_CHANNELS];
	long *rawData[MAX_CHANNELS];

	quint32 cableType;

private:
	bool data_loading;

public slots:
	void cancel_data_loading();

signals:
	void load_size( int filesize );
	void data_loaded_so_far( int loaded );
	void loading_finished();
};
/* }}} */


QDataStream &operator<<( QDataStream &out, const EcgData &lEcgData );
QDataStream &operator>>( QDataStream &in, EcgData &lEcgData );

#endif
