#ifndef MYMESSAGEBOX_H
#define MYMESSAGEBOX_H

#include <QMessageBox>


class MyMessageBox : public QMessageBox
{
	uint32_t timeout = 1;
	bool autoClose = false;

public:
	void showEvent ( QShowEvent *event )
	{
		Q_UNUSED( event );

		if ( autoClose ) {
			this->startTimer( timeout * 1000 );
		}
	}

	void timerEvent( QTimerEvent *event )
	{
		Q_UNUSED( event );

		this->done( 0 );
	}

	void setAutoClose ( bool auto_close )
	{
		autoClose = auto_close;
	}

	void setTimeout ( uint32_t timeout_in_sec )
	{
		timeout = timeout_in_sec;
	}
};

#endif // MYMESSAGEBOX_H
