/**
 * @file        qpdfpixmap.cpp
*/
#include "qpdfpixmap.h"

QPDFPixmap::QPDFPixmap()
	: QPixmap()
	, m_DPI( 1200 )
{
}

QPDFPixmap::QPDFPixmap( QPlatformPixmap *data )
	: QPixmap( data )
	, m_DPI( 1200 )
{
}

QPDFPixmap::QPDFPixmap( int w, int h )
	: QPixmap( w, h )
	, m_DPI( 1200 )
{
}

QPDFPixmap::QPDFPixmap( const QSize &size )
	: QPixmap( size )
	, m_DPI( 1200 )
{
}

QPDFPixmap::QPDFPixmap( const QString &fileName, const char *format, Qt::ImageConversionFlags flags )
	: QPixmap( fileName, format, flags )
	, m_DPI( 1200 )
{
}

#ifndef QT_NO_IMAGEFORMAT_XPM
QPDFPixmap::QPDFPixmap( const char *const xpm[] )
	: QPixmap( xpm )
	, m_DPI( 1200 )
{
}
#endif

QPDFPixmap::QPDFPixmap( const QPixmap &other )
	: QPixmap( other )
	, m_DPI( 1200 )
{
}


void QPDFPixmap::setDPI( int dpi )
{
	m_DPI = dpi;
}

int QPDFPixmap::metric( PaintDeviceMetric metric ) const
{
	switch ( metric ) {
		case PdmDpiX:
		case PdmDpiY:
		case PdmPhysicalDpiX:
		case PdmPhysicalDpiY:
			return m_DPI;
		default:
			return QPixmap::metric( metric );
	}
}
