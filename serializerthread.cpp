#include "serializerthread.h"

// #define ENABLE_DATA_LOG

serializerThread::serializerThread()
{
}

void serializerThread::setATEngine( AtEngine *at )
{
	m_AtEngine = at;
}

void serializerThread::setMessages( Messages *msg )
{
	m_messages = msg;
}

void serializerThread::setSerial( Serial *ser )
{
	m_Serial = ser;
}

void serializerThread::setUI( Ui::SironaViewerUi *ui )
{
	m_ui = ui;
}

/** {{{ serializerThread::BluetoothAdapter_readyRead()
*/
void serializerThread::BluetoothAdapter_readyRead()
{
	QByteArray availableData = m_Serial->bluetoothAdapter()->readAll();

	if ( m_AtEngine->is_in_data_mode() ) {

		/* DISCONNECT message might be split between multiple readyRead() * calls.
		 * So we need to watch all successive data for a DISCONNECT string */
		int foundDISCONNECT = false;
		for ( int i = 0 ; i < availableData.size() ; i++ ) {
			if ( locatedDISCONNECT( availableData[i] ) ) {
				foundDISCONNECT = true;
				break;
			}
		}

		if ( foundDISCONNECT ) {
#ifdef ENABLE_DATA_LOG
			QQQ( "data.log" ) << QString( "BluetoothAdapter_readyRead() disconnectEvent emitted" );
#endif
			emit disconnectEvent( BT_DEVICE_NOT_RESPONDING );
		} else {
			emit ( parseSerialData( availableData ) );
		}

	} else {

#ifdef ENABLE_DATA_LOG
		QQQ( "data.log" ) << "void SironaViewerUi::SerialPortBluetoothAdapter_readyRead() found bluetooth data and pushing it into the Serial queue";
#endif

		for ( int i = 0 ; i < availableData.size() ; i++ ) {
			m_Serial->ser_push( availableData[i] );
		}
	}

	return;
}
/* }}} */

/** {{{ bool locatedDISCONNECT( char byte )
 */
bool serializerThread::locatedDISCONNECT( char byte )
{
#define STRING_DISCONNECT	"DISCONNECT,"
#define STRLEN_DISCONNECT	(11)

	static int searchPos = 0;

	if ( byte == STRING_DISCONNECT[searchPos] ) {
		searchPos += 1;
	} else {
		searchPos = 0;
	}
	return ( searchPos >= STRLEN_DISCONNECT );
}
/* }}} */


void serializerThread::run()
{
	qDebug () << "serializer thread started \n";
	exec();
}
