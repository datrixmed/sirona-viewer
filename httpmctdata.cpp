#include <math.h>
#include "httpmctdata.h"
#include "utilities.h"


HttpMctData::HttpMctData(QString boundary, QString atrFileName, QString datFileName, QString heaFileName, QString jsonFileName)
{
    QString preAtrStr = "--" + boundary +
                         "\r\nContent-Disposition: form-data; name=\"event_atr\"; filename=\"" + atrFileName +
                         "\"\r\nContent-Type: application/octet-stream\r\n\r\n";
    preAtrSize = preAtrStr.length();
    strncpy(preAtr, preAtrStr.toLatin1().data(), preAtrSize);

    QString preDatStr = "\r\n--" +
                         boundary +
                         "\r\nContent-Disposition: form-data; name=\"event_dat\"; filename=\"" + datFileName +
                         "\"\r\nContent-Type: application/octet-stream\r\n\r\n";
    preDatSize = preDatStr.length();
    strncpy(preDat, preDatStr.toLatin1().data(), preDatSize);

    QString preHeaStr = "\r\n--" +
                         boundary +
                         "\r\nContent-Disposition: form-data; name=\"event_hea\"; filename=\"" + heaFileName +
                         "\"\r\nContent-Type: application/octet-stream\r\n\r\n";
    preHeaSize = preHeaStr.length();
    strncpy(preHea, preHeaStr.toLatin1().data(), preHeaSize);

    QString preJsonStr = "\r\n--" +
                         boundary +
                         "\r\nContent-Disposition: form-data; name=\"event_json\"; filename=\"" + jsonFileName +
                         "\"\r\nContent-Type: application/json\r\n\r\n";
    preJsonSize = preJsonStr.length();
    strncpy(preJson, preJsonStr.toLatin1().data(), preJsonSize);

    QString postJsonStr = "\r\n--" + boundary + "--\r\n";
    postJsonSize = postJsonStr.length();
    strncpy(postJson, postJsonStr.toLatin1().data(), postJsonSize);

    this->atrFile = new QFile(atrFileName);
    this->datFile = new QFile(datFileName);
    this->heaFile = new QFile(heaFileName);
    this->jsonFile = new QFile(jsonFileName);
}

HttpMctData::~HttpMctData()
{
    delete heaFile;
    delete jsonFile;
    delete atrFile;
    delete datFile;
}

bool HttpMctData::open(OpenMode mode)
{
    if (mode & (WriteOnly|Append|Truncate)) {
        return false;       // modification of data is not supported
    }

    if (!atrFile->open(ReadOnly)) {
        LOGMSG("ATR file not found!");
        return false;
    }
    if (!datFile->open(ReadOnly)) {
        LOGMSG("DAT file not found!");
        return false;
    }
    if (!heaFile->open(ReadOnly)) {
        LOGMSG("HEA file not found!");
        return false;
    }
    if (!jsonFile->open(ReadOnly)) {
        LOGMSG("JSON file not found!");
        return false;
    }

    totalSize = preAtrSize + atrFile->size() +
                preDatSize + datFile->size() +
                preHeaSize + heaFile->size() +
                preJsonSize + jsonFile->size() + postJsonSize;

    setOpenMode(mode);
    return true;
}

void HttpMctData::close()
{
    totalSize = 0;
    heaFile->close();
    jsonFile->close();
    atrFile->close();
    datFile->close();
    setOpenMode(NotOpen);
}

bool HttpMctData::atEnd() const
{
    return pos() >= totalSize;
}

qint64 HttpMctData::bytesAvailable() const
{
    qint64 avail = (pos() < totalSize) ? totalSize - pos() : 0;
    return avail + QIODevice::bytesAvailable();
}

bool HttpMctData::isSequential() const
{
    return false;
}

qint64 HttpMctData::size() const
{
    return totalSize;
}

qint64 HttpMctData::readData(char* data, qint64 maxSize)
{
    qint64 absolutePosition = pos();
    qint64 relativePosition = absolutePosition;         // relative to the start of each section
    if (absolutePosition >= totalSize) {
        return 0;
    }
    if (maxSize > (totalSize - absolutePosition)) {
        maxSize = totalSize - absolutePosition;
    }
    qint64 returnSize = maxSize;

    // Pre-ATR
    if ((relativePosition < preAtrSize) && (maxSize > 0)) {
        qint64 size = (maxSize < (preAtrSize-relativePosition)) ? maxSize : preAtrSize-relativePosition;
        strncpy(data, &preAtr[relativePosition], size);
        data += size;
        absolutePosition += size;
        relativePosition += size;
        maxSize -= size;
    }
    relativePosition -= preAtrSize;

    // ATR File
    if ((relativePosition < atrFile->size()) && (maxSize > 0)) {
        qint64 size = (maxSize < (atrFile->size()-relativePosition)) ? maxSize : atrFile->size()-relativePosition;
        size = (size > atrFile->bytesAvailable()) ? atrFile->bytesAvailable() : size;
        atrFile->seek(relativePosition);
        memcpy(data, atrFile->read(size), size);
        data += size;
        absolutePosition += size;
        relativePosition += size;
        maxSize -= size;
    }
    relativePosition -= atrFile->size();

    // Pre-DAT
    if ((relativePosition < preDatSize) && (maxSize > 0)) {
        qint64 size = (maxSize < (preDatSize-relativePosition)) ? maxSize : preDatSize-relativePosition;
        strncpy(data, &preDat[relativePosition], size);
        data += size;
        absolutePosition += size;
        relativePosition += size;
        maxSize -= size;
    }
    relativePosition -= preDatSize;

    // DAT File
    if ((relativePosition < datFile->size()) && (maxSize > 0)) {
        qint64 size = (maxSize < (datFile->size()-relativePosition)) ? maxSize : datFile->size()-relativePosition;
        size = (size > datFile->bytesAvailable()) ? datFile->bytesAvailable() : size;
        datFile->seek(relativePosition);
        memcpy(data, datFile->read(size), size);
        data += size;
        absolutePosition += size;
        relativePosition += size;
        maxSize -= size;
    }
    relativePosition -= datFile->size();

    // Pre-HEA
    if ((relativePosition < preHeaSize) && (maxSize > 0)) {
        qint64 size = (maxSize < (preHeaSize-relativePosition)) ? maxSize : preHeaSize-relativePosition;
        strncpy(data, &preHea[relativePosition], size);
        data += size;
        absolutePosition += size;
        relativePosition += size;
        maxSize -= size;
    }
    relativePosition -= preHeaSize;

    // HEA File
    if ((relativePosition < heaFile->size()) && (maxSize > 0)) {
        qint64 size = (maxSize < (heaFile->size()-relativePosition)) ? maxSize : heaFile->size()-relativePosition;
        size = (size > heaFile->bytesAvailable()) ? heaFile->bytesAvailable() : size;
        heaFile->seek(relativePosition);
        memcpy(data, heaFile->read(size), size);
        data += size;
        absolutePosition += size;
        relativePosition += size;
        maxSize -= size;
    }
    relativePosition -= heaFile->size();

    // Pre-JSON
    if ((relativePosition < preJsonSize) && (maxSize > 0)) {
        qint64 size = (maxSize < (preJsonSize-relativePosition)) ? maxSize : preJsonSize-relativePosition;
        strncpy(data, &preJson[relativePosition], size);
        data += size;
        absolutePosition += size;
        relativePosition += size;
        maxSize -= size;
    }
    relativePosition -= preJsonSize;

    // JSON File
    if ((relativePosition < jsonFile->size()) && (maxSize > 0)) {
        qint64 size = (maxSize < (jsonFile->size()-relativePosition)) ? maxSize : jsonFile->size()-relativePosition;
        size = (size > jsonFile->bytesAvailable()) ? jsonFile->bytesAvailable() : size;
        jsonFile->seek(relativePosition);
        memcpy(data, jsonFile->read(size), size);
        data += size;
        absolutePosition += size;
        relativePosition += size;
        maxSize -= size;
    }
    relativePosition -= jsonFile->size();

    // Post-JSON
    if ((relativePosition < postJsonSize) && (maxSize > 0)) {
        qint64 size = (maxSize < (postJsonSize-relativePosition)) ? maxSize : postJsonSize-relativePosition;
        strncpy(data, &postJson[relativePosition], size);
        data += size;
        absolutePosition += size;
        relativePosition += size;
        maxSize -= size;
    }
    relativePosition -= postJsonSize;

    qint64 currentPercentage = lroundf((float)absolutePosition / (float)totalSize * 100.);
    if (currentPercentage != lastReportedPercentage) {
        lastReportedPercentage = currentPercentage;
        emit percentageChanged(lastReportedPercentage);
    }
    emit reloadTimer();

    return returnSize;
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
qint64 HttpMctData::writeData(const char *data, qint64 len)
{
    // writing is not supported
    return 0;
}
#pragma GCC diagnostic pop
