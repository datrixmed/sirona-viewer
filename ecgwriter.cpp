
#include <QDebug>

#include <QDataStream>
#include "ecgwriter.h"



/** {{{ EcgFormatWriter::EcgFormatWriter() : QFile() , mFormat(311)
 */
EcgFormatWriter::EcgFormatWriter()
	: QFile()
	, mFormat( 311 )
	, sampleRate( 0 )
{
}
/* }}} */


/** {{{ EcgFormatWriter::~EcgFormatWriter()
 */
EcgFormatWriter::~EcgFormatWriter()
{
	close();
}
/* }}} */



/** {{{ void EcgFormatWriter::close()
 */
void EcgFormatWriter::close()
{
	qDebug() << "EcgFormatWriter::close()";
	int leftover = mEcgBuf.count();

	if ( leftover ) {
		QIODevice::write( mEcgBuf, leftover );
		mEcgBuf = mEcgBuf.remove( 0, leftover );
	}

	QFile::close();
}
/* }}} */


/** {{{ qint64 EcgFormatWriter::write( const char * data, qint64 maxSize )
 *
 * @note	This assumes that maxSize is always an even multiple of 4 and that
 *			data is always 311 format.
 *
 */
qint64 EcgFormatWriter::write( const char *data, qint64 maxSize )
{
	qint64 retval = 0;


	switch ( getFormat() ) {

		/* VX3 VNL format */
		case 'v': {
			static qint16 sampleHistory[3][21];
			static int sampleHistoryCounter = 0;
			static int decimationCounter = 0;
			int decimationAmount = getSampleRate() / 128;
			int eventIsHere = false;

			for ( int i = 4 - 1 ; i < maxSize ; i += 4 ) {
				qint16 samp[3] = { 0 };

				samp[0] |= MASK_THESE_BITS( data[i - 3], 8 );
				samp[0] |= MASK_THESE_BITS( data[i - 2], 2 ) << 8;
				samp[1] |= MASK_THESE_BITS( data[i - 2] >> 2, 6 );
				samp[1] |= MASK_THESE_BITS( data[i - 1], 4 ) << 6;
				samp[2] |= MASK_THESE_BITS( data[i - 1] >> 4, 4 );
				samp[2] |= MASK_THESE_BITS( data[i - 0], 6 ) << 4;

				/* Turn 10 bit signed to 16 bit signed and lose 2 bits of precision and put the baseline at 0x80. */
				for ( int eachChannel = 0 ; eachChannel < 3 ; eachChannel++ ) {
					samp[eachChannel] <<= ( 16 - 10 );
					samp[eachChannel] >>= ( 16 - 10 );
					samp[eachChannel] >>= 1;	/* turn 20mV 10 bit data into 5mV 8 bit data */
					sampleHistory[eachChannel][sampleHistoryCounter] += ( int ) samp[eachChannel];
				}
				decimationCounter++;


				/* a possibly averaged sample is now ready to be stored */
				if ( decimationCounter >= decimationAmount ) {
					decimationCounter = 0;

					for ( int eachChannel = 0 ; eachChannel < 3 ; eachChannel++ ) {
						sampleHistory[eachChannel][sampleHistoryCounter] = ( sampleHistory[eachChannel][sampleHistoryCounter] / decimationAmount + 0x80 ) & 0xFF;
					}

					sampleHistoryCounter++;

					if ( sampleHistoryCounter >= 21 ) {
						sampleHistoryCounter = 0;

						for ( int eachChannel = 0 ; eachChannel < 3 ; eachChannel++ ) {
							mEcgBuf.append( ( char ) 0x0A );	/* add Sync Byte */
							if ( eventIsHere ) {
								mEcgBuf.append( ( char ) 0x09 );	/* add Event Byte */
							}
							for ( int eachSample = 0 ; eachSample < 21 ; eachSample++ ) {
								mEcgBuf.append( ( char ) sampleHistory[eachChannel][eachSample] );
								sampleHistory[eachChannel][eachSample] = 0;
							}
						}
					}
				}

				/*
									if ( mEcgBuf.count() >= 512 ) {
										retval = QIODevice::write( mEcgBuf, 512 );
										mEcgBuf = mEcgBuf.remove(0, 512);
									}
				*/

			}
			retval = maxSize;	/* TODO: trick the calling function into believing we were successful, because we probably will be... */
		}
		break;

		case 80: {
			static qint16 sampleHistory[3];
			static int decimationCounter = 0;
			int decimationAmount = getSampleRate() / 128;

			for ( int i = 4 - 1 ; i < maxSize ; i += 4 ) {
				qint16 samp[3] = { 0 };

				samp[0] |= MASK_THESE_BITS( data[i - 3], 8 );
				samp[0] |= MASK_THESE_BITS( data[i - 2], 2 ) << 8;
				samp[1] |= MASK_THESE_BITS( data[i - 2] >> 2, 6 );
				samp[1] |= MASK_THESE_BITS( data[i - 1], 4 ) << 6;
				samp[2] |= MASK_THESE_BITS( data[i - 1] >> 4, 4 );
				samp[2] |= MASK_THESE_BITS( data[i - 0], 6 ) << 4;

				/* Turn 10 bit signed to 16 bit signed and lose 2 bits of precision and put the baseline at 0x80. */
				for ( int eachSample = 0 ; eachSample < 3 ; eachSample++ ) {
					samp[eachSample] <<= ( 16 - 10 );
					samp[eachSample] >>= ( 16 - 10 );
					samp[eachSample] >>= 1;	/* turn 20mV 10 bit data into 5mV 8 bit data */
					sampleHistory[eachSample] += ( int ) samp[eachSample];
				}
				decimationCounter++;


				/* a possibly averaged sample is now ready to be stored */
				if ( decimationCounter >= decimationAmount ) {
					decimationCounter = 0;

					for ( int eachSample = 0 ; eachSample < 3 ; eachSample++ ) {
						mEcgBuf.append( ( char ) ( ( sampleHistory[eachSample] / decimationAmount + 0x80 ) & 0xFF ) );
						sampleHistory[eachSample] = 0;
					}
				}

				if ( mEcgBuf.count() >= 512 ) {
					retval = QIODevice::write( mEcgBuf, 512 );
					mEcgBuf = mEcgBuf.remove( 0, 512 );
				}

			}
			retval = maxSize;	/* TODO: trick the calling function into believing we were successful, because we probably will be... */
		}
		break;

		case 311: {
			retval = QIODevice::write( data, maxSize );
		}
		break;

		case 1311: {
			qint32 *raw32 = ( qint32 * ) data;

			for ( int i = 0 ; i < maxSize / 4 ; i++ ) {
				// raw32[i] = raw32[i] & 0x3FFFFFFFl;		/* clear the top two bits because VX3+ uses this as pacemaker and event bits */
				raw32[i] = raw32[i] & ~( BITPOSITION_EVENT | BITPOSITION_PACE );		/* clear the top two bits because VX3+ uses this as pacemaker and event bits */
			}

			retval = QIODevice::write( data, maxSize );
		}
		break;

		default:
			break;
	}


	return retval;
}
/* }}} */



/** {{{ void EcgFormatWriter::writeEvent( long secondOffset )
 *
 *
 */
void EcgFormatWriter::writeEvent( long secondOffset )
{
	switch ( getFormat() ) {

		case 'v': {
			long samplePos = ( long )( ( ( float )secondOffset + 0.5 ) * 128 * 3 * 22 / 21 );

			if ( samplePos < mEcgBuf.count() ) {
				for ( int s = samplePos ; s < samplePos + 21 * 3 ; s++ ) {
					if ( mEcgBuf[s] == ( char )0x0a ) {
						mEcgBuf.insert( s, ( char )0x09 );
						break;
					}
				}
			}
		}
		break;

		case 80:
			break;

		case 311: {
			/*
					  int minutes = samplePos / getSampleRate() / 60;
					  float seconds = (samplePos - (minutes * getSampleRate()));
					  QString("%1:%2%3%4%5%6%7")
					  .arg( minutes, 5 )
					  .arg( seconds, 6 )
					  .arg( samplePos, 9 )
					  .arg("N", 6 )
					  .arg("0", 5 )
					  .arg("0", 5 )
					  .arg("0", 5 )
			*/
		}
		break;

		case 1311: {
			qint32 dataSample;

			long samplePos = ( long )( ( ( float )secondOffset + 0.5 ) * getSampleRate() );

			seek( samplePos * sizeof( qint32 ) );	/* all of the 3 channels are packed in 32 bits, so the byte position is the samplePos multiplied by 4 bytes. */
			readData( ( char * ) &dataSample, sizeof( qint32 ) );

			dataSample |= BITPOSITION_EVENT;

			seek( samplePos * sizeof( qint32 ) );	/* all of the 3 channels are packed in 32 bits, so the byte position is the samplePos multiplied by 4 bytes. */
			writeData( ( char * ) &dataSample, sizeof( qint32 ) );

			/*
			qDebug() << QString("writeEvent(%1)     data = %2    data[] = %3 %4 %5 %6 ")
				.arg(samplePos * 4)
			    .arg((quint32) *((quint32 *)data),8,16)
				.arg((int)data[0] & 0xFF,2,16)
				.arg((int)data[1] & 0xFF,2,16)
				.arg((int)data[2] & 0xFF,2,16)
				.arg((int)data[3] & 0xFF,2,16)
				;
			*/
		}
		break;

		default:
			break;
	}
}
/* }}} */


