
TEMPLATE = app

CONFIG += qt \
        thread \
        c++11

CONFIG(debug, debug|release) {
}

# * Enable if you want to erase the data from the device after downloading */
DEFINES += ERASE_ALL_EVENTS_UPON_REQUEST

# Enables downloading of Events from Holter device
# * Comment out if you like to disable this feature */
DEFINES += DOWNLOAD_EVENTS_FROM_HOLTER_DEVICE

# Check if the connected device has compatible config version */
#DEFINES += RESTRICT_WHICH_FIRMWARE_CAN_BE_USED

equals(QMAKE_CXX, g++):GIT_VERSION = $$system(git --git-dir ../.git --work-tree ../ describe --always --tags)
!equals(QMAKE_CXX, g++):GIT_VERSION = $$system(git --git-dir ./.git --work-tree ../ describe --always --tags)
DEFINES += GIT_VERSION=\\\"$$GIT_VERSION\\\"

CONFIG(release, debug|release) {
	DEFINES += QT_NO_DEBUG_OUTPUT
	DEFINES += QT_NO_DEBUG
}

QT += network \
      widgets \
      printsupport

INCLUDEPATH +=	../osea20-gcc \
                ../osea20-gcc/wfdb \
                ../usb-dongle_interface/USB-dongle-interface \
                .

win32:INCLUDEPATH +=  ../sironadriver
!win32:INCLUDEPATH +=  ../sironadriver-qt

TRANSLATIONS    = languages/SironaViewer_mk.ts \
                  languages/SironaViewer_cs.ts \
                  languages/SironaViewer_da.ts \
                  languages/SironaViewer_de.ts \
                  languages/SironaViewer_es.ts \
                  languages/SironaViewer_fi.ts \
                  languages/SironaViewer_fr.ts \
                  languages/SironaViewer_hu.ts \
                  languages/SironaViewer_it.ts \
                  languages/SironaViewer_nl.ts \
                  languages/SironaViewer_no.ts \
                  languages/SironaViewer_pl.ts \
                  languages/SironaViewer_pt.ts \
                  languages/SironaViewer_ro.ts \
                  languages/SironaViewer_sv.ts \
                  languages/SironaViewer_ja.ts \
                  languages/SironaViewer_zh_CN.ts \
                  languages/SironaViewer_zh_HK.ts \
                  languages/SironaViewer_zh_TW.ts \
                  languages/SironaViewer_ko.ts \
                  languages/SironaViewer_hi.ts


CONFIG(debug, debug|release) {
        TARGET = SironaViewer.debug
        RCC_DIR = debug/rcc
        MOC_DIR = debug/moc
        UI_DIR = debug/ui
        QT += testlib
}

CONFIG(release, debug|release) {
        TARGET = SironaViewer
        RCC_DIR = release/rcc
        MOC_DIR = release/moc
        UI_DIR = release/ui
}

win32:equals(QMAKE_CXX, g++):DESTDIR = $${_PRO_FILE_PWD_}/../bin
win32:equals(QMAKE_CXX, g++):LIBS += -L$${_PRO_FILE_PWD_}/../bin
!equals(QMAKE_CXX, g++):DESTDIR = $${_PRO_FILE_PWD_}/out
!equals(QMAKE_CXX, g++):LIBS += -L$${_PRO_FILE_PWD_}/out

# For compiling with libcurl
INCLUDEPATH += curl/include
win32:LIBS += -L$${_PRO_FILE_PWD_}/curl/lib
win32:LIBS += -lcurldll
unix:LIBS += -lcurl

# For creating the shortcut
win32:LIBS += -lole32 -luuid

DEFINES += LOG_USB_DONGLE_COMUNICATIONS

# Define how to create version.h
win32 {
	!equals(QMAKE_CXX, g++):installer.path = installer
	!equals(QMAKE_CXX, g++):installer.commands = ./update-version-text.sh ; docker run --rm -i -v "$$PWD:/work" amake/innosetup installer/*-linux.iss
	!equals(QMAKE_CXX, g++):INSTALLS += installer
}



#choose correct customer prl file
include(Sironaviewer.prl)

CONFIG(debug, debug|release) {
	win32:LIBS += -lsironadriverd
	!win32:LIBS += -lsironadriverd-qt
}

CONFIG(release, debug|release) {
	win32:LIBS += -lsironadriver
	!win32:LIBS += -lsironadriver
}

INCLUDEPATH += minizip
win32:LIBS += -L$${_PRO_FILE_PWD_}/minizip
win32:LIBS += -lminizip

win32:INCLUDEPATH += zlib128-dll\include
win32:LIBS += -L$${_PRO_FILE_PWD_}/zlib128-dll
win32:LIBS += -lzlib
!win32:LIBS += -lz -lusb-1.0

win32:LIBS += -L../sironadriver -lsetupapi -lws2_32 -lbthprops -ladvapi32
!win32:LIBS += -L../sironadriver-qt

# Uncomment the following lines to be able to debug the release version
#QMAKE_CXXFLAGS_RELEASE += -g
#QMAKE_CFLAGS_RELEASE += -g
#QMAKE_LFLAGS_RELEASE =

# Address sanitizer(currently not working on win)
#QMAKE_CXXFLAGS += -fsanitize=address
#QMAKE_LFLAGS += -lasan

FORMS += \

HEADERS += \

SOURCES +=
