
#include <QtGui>

#include "floatingdisplay.h"
#include "ui_floatingdisplay.h"




FloatingDisplay::FloatingDisplay( QWidget *parent ) :
	QWidget( parent, Qt::Window ), pixmap( NULL ),
	ui( new Ui::FloatingDisplay )
{
	ui->setupUi( this );

	show();
}



FloatingDisplay::~FloatingDisplay()
{
	delete ui;
}



void FloatingDisplay::changeEvent( QEvent *e )
{
	QWidget::changeEvent( e );
	switch ( e->type() ) {
		case QEvent::LanguageChange:
			ui->retranslateUi( this );
			break;
		default:
			break;
	}
}


void FloatingDisplay::wheelEvent( QWheelEvent *event )
{
	if ( event->modifiers() == Qt::ControlModifier ) {
		setWindowOpacity( ( qreal ) windowOpacity() + ( ( event->delta() < 0 ) ? -0.05 : 0.05 ) );
		if ( windowOpacity() < 0.1 ) {
			setWindowOpacity( 0.1 );
		}
		if ( windowOpacity() > 1.0 ) {
			setWindowOpacity( 1.0 );
		}
	}
}


void FloatingDisplay::keyPressEvent( QKeyEvent *event )
{
	if ( event->type() == QEvent::KeyPress ) {
		switch ( event->key() ) {
			case Qt::Key_Q:
			case Qt::Key_Escape:
				close();
				break;
		}
	}
}



QString FloatingDisplay::getBackgroundImage()
{
	return "";
	// return "images/background.png";
}




void FloatingDisplay::closeEvent( QCloseEvent *event )
{
	Q_UNUSED(event);
	QSettings settings( QSettings::IniFormat, QSettings::UserScope, "Peregrine", "LighthouseViewer" );
	settings.setValue( "FloatingDisplay/geometry", pos() );
}




/* {{{ transparent bitmapped background functions */

void FloatingDisplay::paintEvent( QPaintEvent *event )
{
	Q_UNUSED( event );

#ifdef ANTIQUE
	if ( ! pixmap ) {
		pixmap = new QPixmap;
		pixmap->load( getBackgroundImage() );
	}
#endif

	if ( pixmap && ! pixmap->isNull() ) {
		*pixmap = pixmap->scaled( width(), height(), Qt::IgnoreAspectRatio, Qt::SmoothTransformation );
		setMask( pixmap->mask() );
		QPainter painter( this );
		painter.drawPixmap( 0, 0, *pixmap );
	}
}

/* }}} */


void FloatingDisplay::resizeEvent( QResizeEvent * /* event */ )
{
	if ( pixmap && ! pixmap->isNull() ) {
		*pixmap = pixmap->scaled( width(), height(), Qt::IgnoreAspectRatio, Qt::SmoothTransformation );
		setMask( pixmap->mask() );
	}
	setWindowOpacity( 0.90 );
}



void FloatingDisplay::mousePressEvent( QMouseEvent *event )
{
	isDragging = false;
	if ( event->button() == Qt::RightButton ) {
		/* only move the widget if grabbing the top part of the widget */
		if ( event->pos().y() < frameGeometry().height() * 1 / 3 ) {
			dragPosition = event->globalPos() - frameGeometry().topLeft();
			isDragging = true;
			event->accept();
		}
	}
}



void FloatingDisplay::mouseMoveEvent( QMouseEvent *event )
{
	if ( event->buttons() & Qt::RightButton ) {
		/* only move the widget is grabbing the top half of the widget */
		if ( isDragging ) {
			move( event->globalPos() - dragPosition );
			event->accept();
		}
	}
}



void FloatingDisplay::append( QString str )
{
	ui->textDisplay->append( str );
}


void FloatingDisplay::append( QStringList strList )
{
	foreach ( QString str, strList ) {
		ui->textDisplay->append( str );
	}
}


