#include <QDesktopServices>
#include <QDesktopWidget>
#include <QStorageInfo>
#include <QSettings>
#include <QDateTime>
#include <QMessageBox>
#include <QFileDialog>
#include <QInputDialog>
#include <QLocale>
#include <QMenu>
#include <QDir>
#include <QThread>
#include <QTimer>
#include <QTranslator>
#include <QJsonDocument>
#include <QJsonObject>
#include <math.h>
#include <time.h>
using std::sort;

#include "Sironaviewerui.h"
#include "ui_Sironaviewerui.h"
#include "showsignalstreaming.h"
#include "floatingdisplay.h"
#include "string.h"
#include "printpreviewer.h"
#include "utilities.h"
#include "mymessagebox.h"
#include "httppostdata.h"
#include "sironadriver.h"
#include "httpmctdata.h"
#include "cryptjson.h"
#include "dbglog.h"

#include "project_macros.h"
#include "version.h"


#define BLUETOOTH_IDLE_TIMEOUT	(300000)
#define ACTIVATION_TIMEOUT		(30000)
#define VERSION_HAS_FIXED_SAMPLE_RATE		( ((m_configParams.fw_rev & 0xFF) >= 2) && ((m_configParams.fw_rev >> 8) >= 1) )

//Communication packes BluetoothAdapter

batteryCharge_t battery_state[] = {
	/*voltage *//*% */
	{4180, 100},
	{4170, 100},
	{4150, 100},
	{4140, 98},
	{4130, 96},
	{4120, 95},
	{4100, 93},
	{4090, 92},
	{4080, 90},
	{4070, 89},
	{4060, 87},
	{4050, 87},
	{4040, 85},
	{4020, 83},
	{3990, 77},
	{3960, 74},
	{3950, 73},
	{3930, 71},
	{3920, 68},
	{3910, 65},
	{3890, 63},
	{3880, 60},
	{3860, 58},
	{3820, 52},
	{3810, 50},
	{3800, 46},
	{3790, 41},
	{3780, 37},
	{3770, 31},
	{3760, 25},
	{3750, 20},
	{3740, 18},
	{3720, 15},
	{3700, 13},
	{3690, 10},
	{3680, 7},
	{3670, 5},
	{3660, 4},
	{3640, 3},
	{3600, 2},
	{3530, 1},
	{3000, 0}
};


bool glbUsbImmediateSelection = true;
QString glbCustomCode;




SironaViewerUi::SironaViewerUi( QWidget *parent, CustomerInfo *pCustomer )
	: QWidget( parent )
	, m_ui( new Ui::SironaViewerUi )
	, m_configParams()
{
	QSettings settings( QSettings::IniFormat, QSettings::UserScope, MANUFACTURER_NAME, APPLICATION_NAME );
	dbgTesting = settings.value("dbgTesting",false).toBool();

	m_ui->setupUi( this );

	customer = pCustomer;

	glbCustomCode = customer->customCode;

    QString windowTitle = tr( "Sirona Viewer %1" ).arg( REVISION_REV );
    LOGMSG("%s     %s     using %s\n", windowTitle.toLatin1().data(), glbCustomCode.toLatin1().data(), settings.fileName().toLatin1().data() );

	m_configParams = ConfigParams();
	m_ui->patientDOB_dateEdit->setDate( QDate::currentDate() );
    setWindowTitle(windowTitle);

	QChar decimapPoint = QLocale::system().decimalPoint();
	if ( decimapPoint != ',' ) {
		int total = m_ui->pageProcedureMain_PauseComboBox->count();

		for ( int c = 0; c < total; c++ ) {
			QString itemText = m_ui->pageProcedureMain_PauseComboBox->itemText( c );

			itemText = itemText.replace( ',', decimapPoint );

			m_ui->pageProcedureMain_PauseComboBox->setItemText( c, itemText );
		}
	}

	/* Hide pacemaker settings(for now) */
	m_ui->pageProcedureMain_PacemakerFrame->setVisible( false );

	/* set fixed size to the SironaViewerUi widget */
	this->setFixedSize( this->size() );

	this->setGeometry(
		QStyle::alignedRect(
			Qt::LeftToRight,
			Qt::AlignCenter,
			this->size(),
			QGuiApplication::primaryScreen()->geometry()
		)
	);

	QFile file( "stylesheet.css" );
	if ( file.open( QIODevice::ReadOnly | QIODevice::Text ) ) {
		QString text = ( QString::fromUtf8( file.readAll() ) );
		preprocess_stylesheet( &text );
		setStyleSheet( text );
	}

    QString errorMsg = Utilities::createDirectories(!customer->server_enabled);
    if (!errorMsg.isNull()) {
        QMessageBox::critical(this, tr("Directory error"), errorMsg);
    }

	connect( m_ui->contentStackedWidget, SIGNAL ( currentChanged( int ) ), this, SLOT( contentStackedWidgetPageChanged( int ) ) );

	deviceConnectionStatus = NO_DEVICE_CONNECTED;
	deviceCommunicationStatus = DEVICE_IDLE;

	m_EcgData = nullptr;
	myProcess = NULL;

	if ( ( int )( ( float )m_ui->pageWelcome_LogoLabel->width() / ( float ) customer->logo.width() * ( float )customer->logo.height() ) < 65 ) {
		m_ui->pageWelcome_LogoLabel->setFixedHeight( ( int )( ( float )m_ui->pageWelcome_LogoLabel->width() / ( float ) customer->logo.width() * ( float )customer->logo.height() ) );
		m_ui->pageConnection_LogoLabel->setFixedHeight( ( int )( ( float )m_ui->pageConnection_LogoLabel->width() / ( float ) customer->logo.width() * ( float )customer->logo.height() ) );
		m_ui->pageCable_LogoLabel->setFixedHeight( ( int )( ( float )m_ui->pageCable_LogoLabel->width() / ( float ) customer->logo.width() * ( float )customer->logo.height() ) );
		m_ui->pagePatient_LogoLabel->setFixedHeight( ( int )( ( float )m_ui->pagePatient_LogoLabel->width() / ( float ) customer->logo.width() * ( float )customer->logo.height() ) );
		m_ui->pageProcedure_LogoLabel->setFixedHeight( ( int )( ( float )m_ui->pageProcedure_LogoLabel->width() / ( float ) customer->logo.width() * ( float )customer->logo.height() ) );
		m_ui->pageUSBSaved_LogoLabel->setFixedHeight( ( int )( ( float )m_ui->pageUSBSaved_LogoLabel->width() / ( float ) customer->logo.width() * ( float )customer->logo.height() ) );
		m_ui->pageHookup_LogoLabel->setFixedHeight( ( int )( ( float )m_ui->pageHookup_LogoLabel->width() / ( float ) customer->logo.width() * ( float )customer->logo.height() ) );
		m_ui->pageECG_LogoLabel->setFixedHeight( ( int )( ( float )m_ui->pageECG_LogoLabel->width() / ( float ) customer->logo.width() * ( float )customer->logo.height() ) );
		m_ui->pageProcedureActive_LogoLabel->setFixedHeight( ( int )( ( float )m_ui->pageProcedureActive_LogoLabel->width() / ( float ) customer->logo.width() * ( float )customer->logo.height() ) );
        m_ui->pageDownloadOptions_LogoLabel->setFixedHeight( ( int )( ( float )m_ui->pageDownloadOptions_LogoLabel->width() / ( float ) customer->logo.width() * ( float )customer->logo.height() ) );
        m_ui->pageErase_LogoLabel->setFixedHeight( ( int )( ( float )m_ui->pageErase_LogoLabel->width() / ( float ) customer->logo.width() * ( float )customer->logo.height() ) );
        m_ui->pageDownload_LogoLabel->setFixedHeight( ( int )( ( float )m_ui->pageDownload_LogoLabel->width() / ( float ) customer->logo.width() * ( float )customer->logo.height() ) );
		m_ui->pageComplete_LogoLabel->setFixedHeight( ( int )( ( float )m_ui->pageComplete_LogoLabel->width() / ( float ) customer->logo.width() * ( float )customer->logo.height() ) );
		m_ui->pageDevice_LogoLabel->setFixedHeight( ( int )( ( float )m_ui->pageDevice_LogoLabel->width() / ( float ) customer->logo.width() * ( float )customer->logo.height() ) );
	} else {
		m_ui->pageWelcome_LogoLabel->setFixedWidth( customer->logo_width );
		m_ui->pageConnection_LogoLabel->setFixedWidth( customer->logo_width );
		m_ui->pageCable_LogoLabel->setFixedWidth( customer->logo_width );
		m_ui->pagePatient_LogoLabel->setFixedWidth( customer->logo_width );
		m_ui->pageProcedure_LogoLabel->setFixedWidth( customer->logo_width );
		m_ui->pageUSBSaved_LogoLabel->setFixedWidth( customer->logo_width );
		m_ui->pageHookup_LogoLabel->setFixedWidth( customer->logo_width );
		m_ui->pageECG_LogoLabel->setFixedWidth( customer->logo_width );
		m_ui->pageProcedureActive_LogoLabel->setFixedWidth( customer->logo_width );
        m_ui->pageDownloadOptions_LogoLabel->setFixedWidth( customer->logo_width );
        m_ui->pageErase_LogoLabel->setFixedWidth( customer->logo_width );
        m_ui->pageDownload_LogoLabel->setFixedWidth( customer->logo_width );
		m_ui->pageComplete_LogoLabel->setFixedWidth( customer->logo_width );
		m_ui->pageDevice_LogoLabel->setFixedWidth( customer->logo_width );

	}

	m_ui->pageWelcome_LogoLabel->setPixmap( customer->logo );
	m_ui->pageWelcome_SupportLabel->setText( customer->phone_number );
	m_ui->pageWelcome_TitleLabel->setText( customer->product_name );
	m_ui->pageConnection_LogoLabel->setPixmap( customer->logo );
	m_ui->pageConnection_SupportLabel->setText( customer->phone_number );
	m_ui->pageConnection_MainLabel->setText( tr( "Choose a " ) + customer->product_name + tr( " device" ) );
	m_ui->pageCable_LogoLabel->setPixmap( customer->logo );
	m_ui->pageCable_SupportLabel->setText( customer->phone_number );
	m_ui->pagePatient_LogoLabel->setPixmap( customer->logo );
	m_ui->pagePatient_SupportLabel->setText( customer->phone_number );
	m_ui->pageProcedure_LogoLabel->setPixmap( customer->logo );
	m_ui->pageProcedure_SupportLabel->setText( customer->phone_number );
	m_ui->pageUSBSaved_LogoLabel->setPixmap( customer->logo );
	m_ui->pageUSBSaved_SupportLabel->setText( customer->phone_number );
	m_ui->pageHookup_LogoLabel->setPixmap( customer->logo );
	m_ui->pageHookup_SupportLabel->setText( customer->phone_number );
	m_ui->pageECG_LogoLabel->setPixmap( customer->logo );
	m_ui->pageECG_SupportLabel->setText( customer->phone_number );
	m_ui->pageProcedureActive_LogoLabel->setPixmap( customer->logo );
	m_ui->pageProcedureActive_SupportLabel->setText( customer->phone_number );
    m_ui->pageDownloadOptions_LogoLabel->setPixmap( customer->logo );
    m_ui->pageDownloadOptions_SupportLabel->setText( customer->phone_number );
    m_ui->pageErase_LogoLabel->setPixmap( customer->logo );
    m_ui->pageErase_SupportLabel->setText( customer->phone_number );
    m_ui->pageDownload_LogoLabel->setPixmap( customer->logo );
	m_ui->pageDownload_SupportLabel->setText( customer->phone_number );
	m_ui->pageComplete_LogoLabel->setPixmap( customer->logo );
	m_ui->pageComplete_SupportLabel->setText( customer->phone_number );
	m_ui->pageCompany_LogoLabel->setFixedHeight( ( int )( ( float )m_ui->pageCompany_LogoLabel->width() / ( float ) customer->logo.width() * ( float )customer->logo.height() ) );
	m_ui->pageCompany_LogoLabel->setPixmap( customer->logo );
	m_ui->pageDevice_LogoLabel->setPixmap( customer->logo );
	m_ui->pageDevice_SupportLabel->setText( customer->phone_number );
	m_ui->pageCompany_SupportLabel->setText( customer->phone_number );
	m_ui->pageCompany_InfoLabel->setText( customer->description );
	m_ui->webAddressLabel->setText( customer->website );
	m_ui->pageDevice_ApplicationDIInfoLabel->setText ( customer->di_number );

	curl_sender = new curl_client( this, customer );
	connect( curl_sender, SIGNAL( percentageChanged( qint64 ) ), this, SLOT( onUploadPercentageChange( qint64 ) ) );
	connect( curl_sender, SIGNAL( reloadTimer() ), this, SLOT( reloadUploadTimer() ) );

	/*
	 * Device Handler object and thread initialization
	 */
	deviceHandler = new DeviceHandler();
	deviceHandler->moveToThread( &deviceHandlerThread );

	connect( &deviceHandlerThread, SIGNAL( finished() ), deviceHandler, SLOT( deleteLater() ) );
//	connect( deviceHandler, SIGNAL( sDeviceMap( QMap<QString, int> devMap ) ), this, SLOT( editDeviceList ( QMap<QString, int> devMap ) ) );
	connect( deviceHandler, SIGNAL( sDeviceConnectStatus( bool, QString, int ) ), this, SLOT( pwmDeviceConnectStatus( bool, QString, int ) ) );
	connect( deviceHandler, SIGNAL( sDeviceHandlerMessage( bool , bool , QString ) ), this, SLOT( pwmDeviceHandlerMessage( bool , bool , QString ) ) );
	connect( deviceHandler, SIGNAL( sDeviceConfiguration( ConfigParams ) ), this, SLOT( pwmDeviceConfiguration( ConfigParams ) ) );
	connect( deviceHandler, SIGNAL( sDeviceParameterListWritten ( bool ) ), this, SLOT( pwmDeviceParameterListWritten( bool ) ) );
    connect( deviceHandler, SIGNAL( sDeviceThereIsDataRecorded( bool ) ), this, SLOT( pwmDeviceThereIsDataRecorded( bool ) ) );
	connect( deviceHandler, SIGNAL( sDeviceDataSaved ( bool, bool, QList<QString> ) ), this, SLOT( pwmDeviceDataSaved ( bool, bool, QList<QString> ) ) );
	connect( deviceHandler, SIGNAL( sDeviceDataDeleted ( bool ) ), this, SLOT( pwmDeviceDataDeleted ( bool ) ) );
	connect( deviceHandler, SIGNAL( sDeviceBatteryStatus( quint32 ) ), this, SLOT( pwmDeviceBatteryStatus( quint32 ) ) );
	connect( deviceHandler, SIGNAL( sDeviceProcedureStopped() ), this, SLOT( pwmDeviceProcedureStopped() ) );
	connect( deviceHandler, SIGNAL( sDeviceDownloadProgress( quint32, quint32 ) ), this, SLOT( pwmDeviceDownloadProgress( quint32, quint32 ) ) );
	connect( deviceHandler, SIGNAL( sDeviceComputerOutOfSpace( QString ) ), this, SLOT( pwmDeviceComputerOutOfSpace( QString ) ) );
	connect( deviceHandler, SIGNAL( sDeviceEnumerateBtDevices( QList<deviceInfo_t> ) ), this, SLOT( pwmDeviceEnumerateBtDevices( QList<deviceInfo_t> ) ) );
	connect( deviceHandler, SIGNAL( sDeviceStopLiveECG( int ) ), this, SLOT( pwmDeviceStopLiveECG( int ) ) );
	connect( deviceHandler, SIGNAL( sDeviceLiveECG( uint32_t, uint32_t, QByteArray ) ), this, SLOT( pwmDeviceLiveECG( uint32_t, uint32_t, QByteArray ) ) );
	connect( deviceHandler, SIGNAL( sDeviceWarning( QString ) ), this, SLOT( pwmDeviceWarning( QString ) ) );
    connect( deviceHandler, SIGNAL( requiredFields( uint8_t, uint8_t, uint8_t, uint8_t, uint8_t, uint8_t, uint8_t, uint8_t, uint8_t ) ), this, SLOT( requiredFields( uint8_t, uint8_t, uint8_t, uint8_t, uint8_t, uint8_t, uint8_t, uint8_t, uint8_t ) ) );
    connect( deviceHandler, SIGNAL( sDeviceStatus(quint8, quint8, quint8, quint8)), this, SLOT( statusReceived(quint8, quint8, quint8, quint8)));
    connect( deviceHandler, SIGNAL( sDeviceSoftResetComplete()), this, SLOT( pwmSoftResetComplete()));
    connect( deviceHandler, SIGNAL( showFloatingDisplay(QString)), this, SLOT( showFloatingDisplay(QString) ));

	connect( this, SIGNAL( connectPwmDevice( deviceInfo_t ) ), deviceHandler, SLOT( connectDevice( deviceInfo_t ) ) );
	connect( this, SIGNAL( disconnectPwmDevice( bool, int ) ), deviceHandler, SLOT( deviceDisconnect( bool, int ) ) );
	connect( this, SIGNAL( isDeviceConnected() ), deviceHandler, SLOT( isDeviceConnected() ) );
	connect( this, SIGNAL( isThereRecordedData() ), deviceHandler, SLOT( deviceIsThereRecordedData() ) );
	connect( this, SIGNAL( saveDataOnDevice( bool , QString, bool, QString ) ), deviceHandler, SLOT( deviceSaveData( bool , QString, bool, QString ) ) );
	connect( this, SIGNAL( deleteDataOnDevice() ), deviceHandler, SLOT( deviceDeleteData() ) );
	connect( this, SIGNAL( sendDeviceConfig( ConfigParams ) ), deviceHandler, SLOT( devicePopulateConfig( ConfigParams ) ) );
	connect( this, SIGNAL( parameterRead( QString ) ), deviceHandler, SLOT( deviceParameterRead( QString ) ) );
	connect( this, SIGNAL( parameterListWrite( ConfigParams, QList<QString> ) ), deviceHandler, SLOT( deviceParameterListWrite( ConfigParams, QList<QString> ) ) );
	connect( this, SIGNAL( parameterReadAll() ), deviceHandler, SLOT( deviceParameterReadAll() ) );
	connect( this, SIGNAL( getBatteryStatus() ), deviceHandler, SLOT( deviceGetBatteryStatus() ) );
	connect( this, SIGNAL( startProcedure() ), deviceHandler, SLOT( deviceStartProcedure() ) );
	connect( this, SIGNAL( stopProcedure() ), deviceHandler, SLOT( deviceStopProcedure() ) );
	connect( this, SIGNAL( softReset() ), deviceHandler, SLOT( deviceSoftReset() ) );
	connect( this, SIGNAL( setDeviceTime() ), deviceHandler, SLOT( deviceSetTime() ) );
	connect( this, SIGNAL( enumerateBtDevices() ), deviceHandler, SLOT( enumerateBtDevices() ) );
	connect( this, SIGNAL( deviceStartLiveECG() ), deviceHandler, SLOT( deviceStartLiveECG() ) );
	connect( this, SIGNAL( markFilesSent( qint32 ) ), deviceHandler, SLOT( markFilesSent( qint32 ) ) );
	connect( this, SIGNAL( setZipPassword( QString ) ), deviceHandler, SLOT( setZipPassword( QString ) ) );
	connect( this, SIGNAL( getRequiredFields() ), deviceHandler, SLOT( getRequiredFields() ) );
    connect( this, SIGNAL( getStatus()), deviceHandler, SLOT( getStatus()));
    connect( this, SIGNAL( logDownload()), deviceHandler, SLOT( logDownload() ));


	deviceHandlerThread.start();

	ConfigParams defaultConfig;
	populateDefaultConfig( &defaultConfig );
	emit sendDeviceConfig( defaultConfig );

	deviceHandler->device_key[0] = customer->device_key[0];
	deviceHandler->device_key[1] = customer->device_key[1];
	deviceHandler->device_key[2] = customer->device_key[2];
	deviceHandler->device_key[3] = customer->device_key[3];
    deviceHandler->setZipPassword(customer->zip_password);
    deviceHandler->manufacturing = customer->manufacturing;
    deviceHandler->generateResFile = customer->generateResFile;

	network_access_manager = new QNetworkAccessManager ( this );
	connect ( network_access_manager, SIGNAL ( finished ( QNetworkReply * ) ), this, SLOT ( uploadFinished( QNetworkReply * ) ) );

	/*
	 * UI variables and elements initialization
	 */
	previousPage = PAGE_INITIAL_VALUE;
	procedurePageAdveancedOptionsEnabled = false;

	m_ui->pageConnection_StatusFrame->setVisible( false );
	m_ui->pageCable_StatusFrame->setVisible( false );
	m_ui->pagePatient_StatusFrame->setVisible( false );
	m_ui->pageProcedure_StatusFrame->setVisible( false );
	m_ui->pageUSBSaved_StatusFrame->setVisible( false );
	m_ui->pageHookup_StatusFrame->setVisible( true );
	m_ui->pageECG_StatusFrame->setVisible( true );
	m_ui->pageProcedureActive_StatusFrame->setVisible( false );
    m_ui->pageDownloadOptions_StatusFrame->setVisible( false );
    m_ui->pageErase_StatusFrame->setVisible( false );
    m_ui->pageDownload_StatusFrame->setVisible( false );
	m_ui->pageComplete_StatusFrame->setVisible( false );
	m_ui->pageDevice_StatusFrame->setVisible( false );

	/* For the PC application these two frames should not exist, that is not to be visable */
	m_ui->pageDevice_InternetStatusFrame->setVisible( false );
	m_ui->pageDevice_InternetConnectButtonFrame->setVisible( false );

	m_ui->pageDevice_BatteryInfoProgressBar->setStyleSheet( "QProgressBar:horizontal { border: 0px solid gray; text-align: right; margin-right: 16ex;} QProgressBar:chunk { background: green;}" );
	m_ui->pageDevice_MonitorSignalProgressBar->setStyleSheet( "QProgressBar:horizontal { border: 0px solid gray; text-align: right; margin-right: 16ex;} QProgressBar:chunk { background: green;}" );

LOGMSG( "SironaViewerUi::SironaViewerUi() 001   going to   PAGE_WELCOME_SCREEN ..." );
	m_ui->contentStackedWidget->setCurrentIndex( PAGE_WELCOME_SCREEN );

#ifdef WARN_LOW_DISK_SPACE
	{
		QStorageInfo storageInfo( Utilities::getLocalStoragePath( "Event" ) );

		qint64 MBAvailable = storageInfo.bytesAvailable() / 1024 / 1024; // Convert bytes to MB

		if ( MBAvailable < 100 ) { // 100MB
			QMessageBox::warning( this, tr( "Low disk space" ), tr( "There is low disk space(%1MB). Please close this app. Free some disk space, and restart the app again." ).arg( MBAvailable ) );

			lowDiskSpace = true;
		}
	}
#endif

	initGetTextCompleters();

	m_ui->frameSiteCode->hide();
	m_ui->grpDiaryEntries->hide();

	if ( glbCustomCode == "ACT1" ) {
		m_ui->grpDiaryEntries->show();
		m_ui->frameSiteCode->show();
		connect( m_ui->tableDiaryEntries, SIGNAL(itemClicked(QTableWidgetItem*)), this, SLOT(tableItemClicked(QTableWidgetItem*)) );
		connect( m_ui->edtSiteCode, SIGNAL( textChanged(QString) ), this, SLOT( patientFieldChanged() ) );
		patientFieldChanged();
		on_edtSiteCode_textChanged("");
	}

	if ( ! pCustomer->hasConfig() ) {
		getNewConfiguration();
	}
}

SironaViewerUi::~SironaViewerUi()
{
	LOGMSG( "~SironaViewerUi()" );

	deviceHandlerThread.requestInterruption();
	deviceHandlerThread.exit();
	deviceHandlerThread.wait();

	if ( m_EcgData ) {
		delete m_EcgData;
		m_EcgData = nullptr;
	}

	if ( m_ui ) {
		delete m_ui;
		m_ui = nullptr;
	}

}


bool SironaViewerUi::isLowDiskSpace() const
{
	return lowDiskSpace;
}

bool SironaViewerUi::nativeEvent( const QByteArray &eventType, void *message, long *result )
{
#ifdef Q_OS_WIN
	MSG *msg = reinterpret_cast<MSG *>( message );
	if ( msg->message == WM_DEVICECHANGE ) {
		LOGMSG( "WM_DEVICECHANGE" );

		if ( ( deviceConnectionStatus == NO_DEVICE_CONNECTED ) && ( m_ui->contentStackedWidget->currentIndex() == PAGE_CONNECTIONS_SCREEN ) ) {
			enumPwmDevices();
		} else  if ( deviceConnectionStatus == USB_DEVICE_CONNECTED ) {
			emit isDeviceConnected();
		}
	}
#endif // Q_OS_WIN

	return QWidget::nativeEvent( eventType, message, result );
}

qint8 getPercentBatteryCharge( qint32 BatteryVoltage )
{
	LOGMSG( "getPercentBatteryCharge" );
	int elements = ( sizeof( battery_state ) / sizeof( battery_state[0] ) );
	batteryCharge_t *item = battery_state;

	for ( int i = 0; i < elements; i++ ) {
		if ( item->voltage < BatteryVoltage ) {
			return item->percent;
		}

		item++;
	}

	return 0;
}

void SironaViewerUi::changeEvent( QEvent *e )
{
	// LOGMSG( "changeEvent" );
	QWidget::changeEvent( e );
	switch ( e->type() ) {
		case QEvent::LanguageChange:
			m_ui->retranslateUi( this );
			break;

		case QEvent::ActivationChange:
			if ( deviceConnectionStatus == BT_DEVICE_CONNECTED && !isActiveWindow() ) {
				m_activationTimer = 0;
				m_activationTimer = startTimer( ACTIVATION_TIMEOUT );
				LOGMSG( "changeEvent() ActivationChange Create m_activationTimer = %d", m_activationTimer );

				if ( m_bluetoothTimer ) {
					restartBluetoothTimer();
				}
			} else if ( isActiveWindow() && m_activationTimer ) {
				killTimer( m_activationTimer );
				m_activationTimer = 0;
				if ( m_bluetoothTimer ) {
					restartBluetoothTimer();
				}
			}
			break;

		case QEvent::WindowStateChange:
			if ( deviceConnectionStatus == BT_DEVICE_CONNECTED && isMinimized() ) {
				m_activationTimer = 0;
				m_activationTimer = startTimer( ACTIVATION_TIMEOUT );
				LOGMSG( "changeEvent() WindowStateChange Create m_activationTimer = %d", m_activationTimer );

				if ( m_bluetoothTimer ) {
					restartBluetoothTimer();
				}
			} else if ( !isMinimized() && m_activationTimer ) {
				killTimer( m_activationTimer );
				m_activationTimer = 0;
				if ( m_bluetoothTimer ) {
					restartBluetoothTimer();
				}
			}
			break;

		default:
			break;
	}
}

void SironaViewerUi::timerEvent( QTimerEvent *event )
{
	if ( event->timerId() == m_enumerateTimer ) {
		emit enumerateBtDevices();
		stopEnumerateTimer();
	} else if ( event->timerId() == m_leadoffTimer ) {
		m_showLead = !m_showLead;
		updateElectrodesWidget();
	} else if ( event->timerId() == m_liveEcgTimer ) {
		qint64 currentTime = QDateTime::currentMSecsSinceEpoch();
		while ( ( !m_futureSamples.isEmpty() ) && ( m_futureSamples.first().msSinceEpox < currentTime ) ) {
			QFutureLiveECGSample sample = m_futureSamples.takeFirst();
			m_ui->pageECG_MainWidget->addSample( sample.ch1, sample.ch2, sample.ch3 );
		}
	} else if ( event->timerId() == m_bluetoothTimer ) {
        if ( ( ( m_configParams.procedure_state == PROCSTATE_STARTED ) || ( m_configParams.procedure_state == PROCSTATE_DONE || ( isMinimized() ) ) ) && ( m_ui->contentStackedWidget->currentIndex() != PAGE_DOWNLOAD_SCREEN ) ) {
			stopBluetoothTimer();
			futurePage = PAGE_INITIAL_VALUE;
			deviceHandler->deviceStopLiveECG();
			emit disconnectPwmDevice( true, SIRONA_DRIVER_NO_ERROR );
		} else {
			restartBluetoothTimer();
		}
	} else if ( event->timerId() == m_activationTimer ) {
        if ( ( ( m_configParams.procedure_state == PROCSTATE_STARTED ) || ( m_configParams.procedure_state == PROCSTATE_DONE ) || ( isMinimized() ) ) && ( m_ui->contentStackedWidget->currentIndex() != PAGE_DOWNLOAD_SCREEN ) ) {
            deviceHandler->deviceStopLiveECG();
			emit disconnectPwmDevice( true, SIRONA_DRIVER_NO_ERROR );
		}
		killTimer ( m_activationTimer );
		m_activationTimer = 0;
	} else if ( event->timerId() == statusMessageTimer ) {
		killTimer ( statusMessageTimer );
		statusMessageTimer = 0;
		removeStatusMessage();
	} else if ( event->timerId() == m_upload_timeout ) {
        LOGMSG("Server request timed out.");
		m_ui->pageDownload_Server_Message->setText( tr( "Server request timed out." ) );
		cancelUpload();
	} else if ( event->timerId() == m_statusTimer ) {
        emit getStatus();
	}
}

void SironaViewerUi::keyPressEvent( QKeyEvent *event )
{
	LOGMSG( "keyPressEvent" );
	if ( event->type() == QEvent::KeyPress ) {

		switch ( event->key() ) {

			case Qt::Key_C:
				if ( (event->modifiers() & (Qt::ControlModifier | Qt::AltModifier)) == (Qt::ControlModifier | Qt::AltModifier) ) {
					QMessageBox qBox;

					qBox.setText( tr( "Are you sure you would like to rebrand this app (You must restart the app to reconfigure)?" ) );
					qBox.setWindowIcon( QPixmap( customer->sv_icon ) );
					qBox.setStandardButtons( QMessageBox::Yes | QMessageBox::No );

					if ( qBox.exec() == QMessageBox::Yes ) {
						QSettings settings( QSettings::IniFormat, QSettings::UserScope, MANUFACTURER_NAME, APPLICATION_NAME );
						settings.setValue("serial_number_prefix","");
						settings.setValue( "config_string", "" );
						qApp->quit();
					}
					event->accept();
				}
				break;

			case Qt::Key_L:
				if ( (event->modifiers() & (Qt::ControlModifier | Qt::AltModifier)) == (Qt::ControlModifier | Qt::AltModifier) ) {
					emit logDownload();
				}
				break;

			case Qt::Key_Q:
				if ( event->modifiers() == (Qt::ControlModifier | Qt::AltModifier) ) {
					qApp->quit();
					break;
				}
				if ( event->modifiers() == Qt::ControlModifier ) {
					QMessageBox qBox;

					qBox.setText( tr( "Are you sure you would like to exit?" ) );
					qBox.setWindowIcon( QPixmap( customer->sv_icon ) );
					qBox.setStandardButtons( QMessageBox::Yes | QMessageBox::No );

					if ( qBox.exec() == QMessageBox::Yes ) {
						qApp->quit();
					}
					event->accept();
				}
				break;
		}
		if ( m_bluetoothTimer ) {
			restartBluetoothTimer();
		}
	}
}

void SironaViewerUi::mousePressEvent( QMouseEvent *event )
{
	if ( event->type() == QEvent::MouseButtonPress ) {

		if ( m_bluetoothTimer ) {
			restartBluetoothTimer();
		}
	}
}


void SironaViewerUi::showFloatingDisplay( QString str )
{
	FloatingDisplay *infobox = new FloatingDisplay(NULL);
	infobox->append(str);
}


bool SironaViewerUi::readConfigFromDevice()
{
	LOGMSG( "readConfigFromDevice" );
	deviceCommunicationStatus = DEVICE_READ_ALL_PARAMETERS;
	emit parameterReadAll();

	return true;
}

void SironaViewerUi::populateDefaultConfig( ConfigParams *defaultConfig )
{
	LOGMSG( "populateDefaultConfig" );
	// Setting the default values other then 0
	defaultConfig->pre_trigger = customer->default_pre_trigger;
	defaultConfig->post_trigger = customer->default_post_trigger;
	defaultConfig->time_zone = ( quint32 ) DEFAULT_TIME_ZONE;
	defaultConfig->recording_len = customer->default_recording_duration;
	defaultConfig->automatic_event_limit = customer->default_auto_event_limit;
	defaultConfig->manual_event_limit = customer->default_manual_event_limit;
	defaultConfig->audiable_on = ( qint8 ) DEFAULT_AUDIABLE;
	defaultConfig->ttm_enabled = ( quint8 ) DEFAULT_TTM_ENABLED;
	defaultConfig->ttm_speedup_on = customer->default_ttm_speed;
	defaultConfig->analyzer_tachy_on = ( qint8 ) DEFAULT_TACHY_ANALYZER_STATE;
	defaultConfig->analyzer_brady_on = ( qint8 ) DEFAULT_BRADY_ANALYZER_STATE;
	defaultConfig->analyzer_pause_on = ( qint8 ) DEFAULT_PAUSE_ANALYZER_STATE;
	defaultConfig->analyzer_tachy_threshold = customer->default_tachy_threshold;
	defaultConfig->analyzer_brady_threshold = customer->default_brady_threshold;
	defaultConfig->analyzer_pause_threshold = customer->default_pause_threshold;
	defaultConfig->analyzer_af_on = ( qint8 ) customer->default_afib_setting;
    defaultConfig->device_mode = PROCTYPE_NOT_SET;
	defaultConfig->sample_rate = customer->default_sample_rate;
	/* do not overwrite the existing device Id, class Id and firmware revision */
	defaultConfig->device_id = m_configParams.device_id;
	defaultConfig->class_id = m_configParams.class_id;
	defaultConfig->fw_rev = m_configParams.fw_rev;
}


char *strProcType( int mode )
{
	switch ( mode ) {
		case PROCTYPE_NOT_SET:	return (char*) "Not Set";
		case PROCTYPE_EVENT:	return (char*) "Event";
		case PROCTYPE_HOLTER:	return (char*) "Holter";
		case PROCTYPE_POST:		return (char*) "Post Event";
		case PROCTYPE_MCT:		return (char*) "MCT";
		default:	return (char*) "Invalid Device Mode";
	}
}

char *strProcState( int state )
{
	switch ( state ) {
		case PROCSTATE_IDLE:	return (char*) "PROCSTATE_IDLE";
		case PROCSTATE_STARTED:	return (char*) "PROCSTATE_STARTED";
		case PROCSTATE_DONE:	return (char*) "PROCSTATE_DONE";
		default:	return (char*) "Invalid ProcState";
	}
}


void SironaViewerUi::evaluateConfigData( ConfigParams *config )
{
	LOGMSG( "evaluateConfigData" );
	QString errStr;

// #define THROW_ERROR_FOR_ALL_BAD_PARAMS

	// device mode
	switch ( config->device_mode ) {
        case PROCTYPE_NOT_SET:
        case PROCTYPE_EVENT:
        case PROCTYPE_HOLTER:
        case PROCTYPE_POST:
        case PROCTYPE_MCT:
			LOGMSG( "evaluateConfigData()   Procedure Type: %s", strProcType(config->device_mode) );
			break;

		default:
			LOGMSG( "evaluateConfigData()   Procedure Type: %s", "Invalid Device Mode" );
			errStr.append( "The device has invalid device mode." );
			throw errStr;
	}

	// pre-trigger
	switch ( config->pre_trigger ) {
		case PRE_TRIGGER_TIME_0_SEC:
		case PRE_TRIGGER_TIME_15_SEC:
		case PRE_TRIGGER_TIME_30_SEC:
		case PRE_TRIGGER_TIME_45_SEC:
		case PRE_TRIGGER_TIME_60_SEC:
		case PRE_TRIGGER_TIME_90_SEC:
		case PRE_TRIGGER_TIME_120_SEC:
		case PRE_TRIGGER_TIME_180_SEC:
		case PRE_TRIGGER_TIME_300_SEC:
			break;

		default:
			errStr.append( QString("Invalid pre-trigger value set.   (%1)").arg(config->pre_trigger) );
#ifdef THROW_ERROR_FOR_ALL_BAD_PARAMS
			throw errStr;
#endif
	}

	// post-trigger
	switch ( config->post_trigger ) {
		case POST_TRIGGER_TIME_15_SEC:
		case POST_TRIGGER_TIME_30_SEC:
		case POST_TRIGGER_TIME_45_SEC:
		case POST_TRIGGER_TIME_60_SEC:
		case POST_TRIGGER_TIME_90_SEC:
		case POST_TRIGGER_TIME_120_SEC:
		case POST_TRIGGER_TIME_180_SEC:
		case POST_TRIGGER_TIME_300_SEC:
			break;

		default:
			errStr.append( QString("Invalid post-trigger value set.   (%1)").arg(config->post_trigger) );
#ifdef THROW_ERROR_FOR_ALL_BAD_PARAMS
			throw errStr;
#endif
	}

	// recording length
#ifdef ANTIQUE
    if ( ( config->recording_len % 24 ) || ((( config->recording_len / 24 ) > 14 ) && config->device_mode == PROCTYPE_HOLTER ) || ((( config->recording_len / 24 ) > 30 ) && config->device_mode == PROCTYPE_MCT )) {
		errStr.append( "Invalid recording length value set." );
#ifdef THROW_ERROR_FOR_ALL_BAD_PARAMS
		throw errStr;
#endif
	}
#else
#endif

	//Sample rate
	switch ( config->sample_rate ) {
		case SAMPLE_RATE_128:
		case SAMPLE_RATE_256:
			break;

		default:
			errStr.append( QString("Invalid sample rate value set.   (%1)").arg(config->sample_rate) );
#ifdef THROW_ERROR_FOR_ALL_BAD_PARAMS
			throw errStr;
#endif
	}
}

QString SironaViewerUi::getPatientInfoSummaryString()
{
	/* Use the patient id, if available... */
	if ( ! m_configParams.patient_id.isEmpty() ) {
		return m_configParams.patient_id;
	} else {
		/* ... otherwise, if no last name, leave blank... */
		if ( m_configParams.patient_last_name.isEmpty() ) {
			return "";
		} else {
			/* ... otherwise if the first name is empty, use just the last name... */
			if ( m_configParams.patient_first_name.isEmpty() ) {
				return m_configParams.patient_last_name;
			} else {
				/* ... or else use both last and first name with a comma. */
				return QString("%1, %2").arg(m_configParams.patient_last_name).arg(m_configParams.patient_first_name);
			}
		}
	}

	return "";	/* blank, by default, unless some good info is available. */
}

void SironaViewerUi::updateDeviceScreenWidget()
{
	m_ui->pageDevice_DetailedStatusFrame->show();
	m_ui->pageDevice_StatusFrame->show();

	if ( m_percentage < 20 ) {
		m_ui->pageDevice_StatusLabel->setText( tr( "Low Battery" ) );
		m_ui->pageDevice_StatusLabel->setStyleSheet( QStringLiteral( "background: red" ) );
		m_ui->pageDevice_DetailedStatusLabel->setText( tr( "The monitor’s battery is low. Connect the charger." ) );
		return;
	}

    if ( ( deviceConnectionStatus == BT_DEVICE_CONNECTED ) && ( m_cableId == CABLETYPE_UNKNOWN ) ) {

        if ( m_configParams.device_mode == PROCTYPE_POST ) {
			m_ui->pageDevice_StatusLabel->setText( tr( "Sensor Disconnected" ) );
			m_ui->pageDevice_StatusLabel->setStyleSheet( QStringLiteral( "background: red" ) );
			m_ui->pageDevice_DetailedStatusLabel->setText( tr( "The sensor is disconnected. Connect the sensor." ) );
		} else {
			m_ui->pageDevice_StatusLabel->setText( tr( "Cable Disconnected" ) );
			m_ui->pageDevice_StatusLabel->setStyleSheet( QStringLiteral( "background: red" ) );
			m_ui->pageDevice_DetailedStatusLabel->setText( tr( "The patient cable is disconnected. Connect the cable." ) );
		}
		return;

		m_ui->pageDevice_StatusLabel->setText( tr( "Cable Disconnected" ) );
		m_ui->pageDevice_StatusLabel->setStyleSheet( QStringLiteral( "background: red" ) );
		return;
		m_ui->pageDevice_DetailedStatusLabel->setText( tr( "The patient cable is disconnected. Connect the cable." ) );
	}

	if ( m_leadOff ) {
		m_ui->pageDevice_StatusLabel->setText( tr( "Ensure electrodes are connected" ) );
		m_ui->pageDevice_StatusLabel->setStyleSheet( QStringLiteral( "background: red" ) );
		m_ui->pageDevice_DetailedStatusLabel->setText( tr( "One or more electrodes may not be properly attached to the patient." ) );
		return;
	}


	m_ui->pageDevice_StatusLabel->setText( tr( "Monitor Functioning Properly" ) );
	m_ui->pageDevice_StatusLabel->setStyleSheet( QStringLiteral( "" ) );
	m_ui->pageDevice_DetailedStatusFrame->hide();
}

void SironaViewerUi::updateElectrodesWidget()
{
	QPixmap pixmap( m_torsoImage );
	QPainter painter( &pixmap );

	QPixmap sirona_per( m_sirona_PER_image );
	QPixmap patch_image ( m_patch_image );

	auto set_status_label = [this] ( bool correct ) {
		if ( correct ) {
            if ( m_configParams.device_mode == PROCTYPE_POST ) {
				m_ui->pageHookup_StatusLabel->setText( tr( "Sensor connected" ) );
			} else {
				m_ui->pageHookup_StatusLabel->setText( tr( "All electrodes connected" ) );
			}
			m_ui->pageHookup_StatusLabel->setStyleSheet( QStringLiteral( "" ) );

		} else {
            if ( m_configParams.device_mode == PROCTYPE_POST ) {
				m_ui->pageHookup_StatusLabel->setText( tr( "Connect sensor" ) );
			} else {
				m_ui->pageHookup_StatusLabel->setText( tr( "Ensure electrodes are connected" ) );
			}
			m_ui->pageHookup_StatusLabel->setText( tr( "All electrodes connected" ) );
			m_ui->pageHookup_StatusLabel->setStyleSheet( QStringLiteral( "" ) );

		}
		m_ui->pageHookup_NextButton->setEnabled( true );
	};

	switch ( m_cableId ) {
        case CABLETYPE_I_CHAN_2_WIRE:
        case CABLETYPE_I_CHAN_3_WIRE:
			if ( ( m_showLead ) || ( ( m_leadOff & SIRONA_LEAD_WHITE ) == 0 ) ) {
				paintElectrodeLead( painter, 199, 232, m_leadOff, SIRONA_LEAD_WHITE );
			}
			if ( ( m_showLead ) || ( ( m_leadOff & SIRONA_LEAD_RED ) == 0 ) ) {
				paintElectrodeLead( painter, 331, 373, m_leadOff, SIRONA_LEAD_RED );
			}
			set_status_label(
				!( m_leadOff & SIRONA_LEAD_WHITE )	&&
				!( m_leadOff & SIRONA_LEAD_RED )
			);
			break;
        case CABLETYPE_II_CHAN_3_WIRE:
			if ( ( m_showLead ) || ( ( m_leadOff & SIRONA_LEAD_WHITE ) == 0 ) ) {
				paintElectrodeLead( painter, 229, 235, m_leadOff, SIRONA_LEAD_WHITE );
			}
			if ( ( m_showLead ) || ( ( m_leadOff & SIRONA_LEAD_BLACK ) == 0 ) ) {
				paintElectrodeLead( painter, 283, 235, m_leadOff, SIRONA_LEAD_BLACK );
			}
			if ( ( m_showLead ) || ( ( m_leadOff & SIRONA_LEAD_RED ) == 0 ) ) {
				paintElectrodeLead( painter, 292, 336, m_leadOff, SIRONA_LEAD_RED );
			}
			set_status_label(
				!( m_leadOff & SIRONA_LEAD_WHITE )	&&
				!( m_leadOff & SIRONA_LEAD_RED )	&&
				!( m_leadOff & SIRONA_LEAD_BLACK )
			);
			break;
        case CABLETYPE_II_CHAN_5_WIRE:
			if ( ( m_showLead ) || ( ( m_leadOff & SIRONA_LEAD_WHITE ) == 0 ) ) {
				paintElectrodeLead( painter, 253, 193, m_leadOff, SIRONA_LEAD_WHITE );
			}
			if ( ( m_showLead ) || ( ( m_leadOff & SIRONA_LEAD_RED ) == 0 ) ) {
				paintElectrodeLead( painter, 295, 360, m_leadOff, SIRONA_LEAD_RED );
			}
			if ( ( m_showLead ) || ( ( m_leadOff & SIRONA_LEAD_BLACK ) == 0 ) ) {
				paintElectrodeLead( painter, 223, 233, m_leadOff, SIRONA_LEAD_BLACK );
			}
			if ( ( m_showLead ) || ( ( m_leadOff & SIRONA_LEAD_GREEN ) == 0 ) ) {
				paintElectrodeLead( painter, 169, 375, m_leadOff, SIRONA_LEAD_GREEN );
			}
			if ( ( m_showLead ) || ( ( m_leadOff & SIRONA_LEAD_BROWN ) == 0 ) ) {
				paintElectrodeLead( painter, 327, 379, m_leadOff, SIRONA_LEAD_BROWN );
			}
			set_status_label(
				!( m_leadOff & SIRONA_LEAD_WHITE )	&&
				!( m_leadOff & SIRONA_LEAD_RED )	&&
				!( m_leadOff & SIRONA_LEAD_BLACK )	&&
				!( m_leadOff & SIRONA_LEAD_GREEN )	&&
				!( m_leadOff & SIRONA_LEAD_BROWN )
			);
			break;
        case CABLETYPE_III_CHAN_5_WIRE:
			if ( ( m_showLead ) || ( ( m_leadOff & SIRONA_LEAD_WHITE ) == 0 ) ) {
				paintElectrodeLead( painter, 198, 373, m_leadOff, SIRONA_LEAD_WHITE );
			}
			if ( ( m_showLead ) || ( ( m_leadOff & SIRONA_LEAD_RED ) == 0 ) ) {
				paintElectrodeLead( painter, 253, 207, m_leadOff, SIRONA_LEAD_RED );
			}
			if ( ( m_showLead ) || ( ( m_leadOff & SIRONA_LEAD_BLACK ) == 0 ) ) {
				paintElectrodeLead( painter, 337, 399, m_leadOff, SIRONA_LEAD_BLACK );
			}
			if ( ( m_showLead ) || ( ( m_leadOff & SIRONA_LEAD_GREEN ) == 0 ) ) {
				paintElectrodeLead( painter, 176, 392, m_leadOff, SIRONA_LEAD_GREEN );
			}
			if ( ( m_showLead ) || ( ( m_leadOff & SIRONA_LEAD_BROWN ) == 0 ) ) {
				paintElectrodeLead( painter, 302, 358, m_leadOff, SIRONA_LEAD_BROWN );
			}
			set_status_label(
				!( m_leadOff & SIRONA_LEAD_WHITE )	&&
				!( m_leadOff & SIRONA_LEAD_RED )	&&
				!( m_leadOff & SIRONA_LEAD_BLACK )	&&
				!( m_leadOff & SIRONA_LEAD_GREEN )	&&
				!( m_leadOff & SIRONA_LEAD_BROWN )
			);
			break;
        case CABLETYPE_III_CHAN_7_WIRE:
			if ( ( m_showLead ) || ( ( m_leadOff & SIRONA_LEAD_WHITE ) == 0 ) ) {
				paintElectrodeLead( painter, 236, 194, m_leadOff, SIRONA_LEAD_WHITE );
			}
			if ( ( m_showLead ) || ( ( m_leadOff & SIRONA_LEAD_RED ) == 0 ) ) {
				paintElectrodeLead( painter, 297, 345, m_leadOff, SIRONA_LEAD_RED );
			}
			if ( ( m_showLead ) || ( ( m_leadOff & SIRONA_LEAD_BLACK ) == 0 ) ) {
				paintElectrodeLead( painter, 224, 237, m_leadOff, SIRONA_LEAD_BLACK );
			}
			if ( ( m_showLead ) || ( ( m_leadOff & SIRONA_LEAD_GREEN ) == 0 ) ) {
				paintElectrodeLead( painter, 177, 377, m_leadOff, SIRONA_LEAD_GREEN );
			}
			if ( ( m_showLead ) || ( ( m_leadOff & SIRONA_LEAD_BROWN ) == 0 ) ) {
				paintElectrodeLead( painter, 324, 375, m_leadOff, SIRONA_LEAD_BROWN );
			}
			if ( ( m_showLead ) || ( ( m_leadOff & SIRONA_LEAD_BLUE ) == 0 ) ) {
				paintElectrodeLead( painter, 277, 247, m_leadOff, SIRONA_LEAD_BLUE );
			}
			if ( ( m_showLead ) || ( ( m_leadOff & SIRONA_LEAD_ORANGE ) == 0 ) ) {
				paintElectrodeLead( painter, 200, 356, m_leadOff, SIRONA_LEAD_ORANGE );
			}
			set_status_label(
				!( m_leadOff & SIRONA_LEAD_WHITE )	&&
				!( m_leadOff & SIRONA_LEAD_RED )	&&
				!( m_leadOff & SIRONA_LEAD_BLACK )	&&
				!( m_leadOff & SIRONA_LEAD_GREEN )	&&
				!( m_leadOff & SIRONA_LEAD_BROWN )	&&
				!( m_leadOff & SIRONA_LEAD_BLUE )	&&
				!( m_leadOff & SIRONA_LEAD_ORANGE )
			);
			break;
        case CABLETYPE_II_CHAN_PATCH:
			painter.drawPixmap(  m_ui->pageHookup_MainFrame->width() / 2 - 31, m_ui->pageHookup_MainFrame->height() / 2 , patch_image );
			set_status_label(
				!( m_leadOff & SIRONA_LEAD_WHITE )	&&
				!( m_leadOff & SIRONA_LEAD_RED )	&&
				!( m_leadOff & SIRONA_LEAD_BLACK )
			);
			break;
        case CABLETYPE_I_CHAN_POST:
			set_status_label( true );
			painter.drawPixmap(  m_ui->pageHookup_MainFrame->width() / 2 + sirona_per.width() / 4, m_ui->pageHookup_MainFrame->height() / 2 , sirona_per );
			break;
        case CABLETYPE_INVALID:
			//Incorrect cable connected.\n\nYou cannot change cables once setup has started.
			m_ui->pageHookup_StatusLabel->setText( tr( "Incorrect cable connected." ) );
			m_ui->pageHookup_StatusLabel->setStyleSheet( QStringLiteral( "background: red" ) );
			m_ui->pageHookup_NextButton->setEnabled( false );
			break;
		default:
            if ( m_configParams.device_mode == PROCTYPE_POST ) {
				m_ui->pageHookup_StatusLabel->setText( tr( "Connect sensor." ) );
				painter.drawPixmap(  m_ui->pageHookup_MainFrame->width() / 2 + sirona_per.width() / 4, m_ui->pageHookup_MainFrame->height() / 2 , sirona_per );
			} else {
				m_ui->pageHookup_StatusLabel->setText( tr( "Connect patient cable." ) );
			}
			m_ui->pageHookup_StatusLabel->setStyleSheet( QStringLiteral( "background: red" ) );
			m_ui->pageHookup_NextButton->setEnabled( false );
			break;
	}

	m_ui->pageHookup_MainLabel->setPixmap( pixmap );
}

void SironaViewerUi::paintElectrodeLead( QPainter &painter, int x, int y, quint8 leadOff, quint8 leadId )
{
	QPoint point( x, y );
	QBrush brush( Qt::SolidPattern );
	QPen pen;

	switch ( leadId ) {
		case SIRONA_LEAD_WHITE:
			pen.setColor( Qt::black );
			brush.setColor( Qt::white );
			break;
		case SIRONA_LEAD_RED:
			pen.setColor( Qt::black );
			brush.setColor( Qt::red );
			break;
		case SIRONA_LEAD_BLACK:
			pen.setColor( Qt::white );
			brush.setColor( Qt::black );
			break;
		case SIRONA_LEAD_GREEN:
			pen.setColor( Qt::black );
			brush.setColor( Qt::green );
			break;
		case SIRONA_LEAD_BROWN:
			pen.setColor( Qt::black );
			brush.setColor( QColor( 0xb8, 0x86, 0x0b ) ); // Brown
			break;
		case SIRONA_LEAD_BLUE:
			pen.setColor( Qt::black );
			brush.setColor( Qt::blue );
			break;
		case SIRONA_LEAD_ORANGE:
			pen.setColor( Qt::black );
			brush.setColor( QColor( 0xff, 0xa5, 0x00 ) ); // Orange
			break;
		default:
			break;
	}

	pen.setWidth( 1 );

	painter.setPen( pen );
	painter.setBrush( brush );
	painter.drawEllipse( point, 10, 10 );

	if ( leadOff & leadId ) {
		pen.setColor( Qt::green );
		pen.setWidth( 2 );

		painter.setPen( pen );

		painter.drawLine( x - 10, y - 10, x + 10, y + 10 );
		painter.drawLine( x - 10, y + 10, x + 10, y - 10 );
	}
}

void SironaViewerUi::restartBluetoothTimer()
{
	stopBluetoothTimer();
	m_bluetoothTimer = startTimer( BLUETOOTH_IDLE_TIMEOUT );
	LOGMSG( "restartBluetoothTimer Create m_bluetoothTimer = %d", m_bluetoothTimer );
}

void SironaViewerUi::stopBluetoothTimer()
{
	if ( m_bluetoothTimer ) {
		killTimer( m_bluetoothTimer );
		m_bluetoothTimer = 0;
	}
}

void SironaViewerUi::pwmDeviceConnectStatus(  bool connected, QString devSerNum, int devType )
{
	LOGMSG( "pwmDeviceConnectStatus" );
	QApplication::restoreOverrideCursor();

	/* If device got connected over the USB direct connect */
	if ( connected ) {
		switch ( devType ) {
			case SIRONA_DRIVER_DEVICE_BLUETOOTH:
				deviceConnectionStatus = BT_DEVICE_CONNECTED;
				restartBluetoothTimer();
				break;
			case SIRONA_DRIVER_DEVICE_USB:
				deviceConnectionStatus = USB_DEVICE_CONNECTED;
				break;
			default:
				LOGMSG( "Unknown connection type." );
				break;
		}

		deviceCommunicationStatus = DEVICE_SET_SYSTEM_TIME;
		connectedDeviceSN = devSerNum.toUpper();

		emit setDeviceTime();

		/* If device got disconnected from the USB direct connect */
	} else {

		// Kill all timers.
        stopEnumerateTimer();
		if ( m_leadoffTimer ) {
			killTimer( m_leadoffTimer );
			m_leadoffTimer = 0;
		}
		if ( m_liveEcgTimer ) {
			killTimer( m_liveEcgTimer );
			m_liveEcgTimer = 0;
		}
        stopBluetoothTimer();
		if ( m_activationTimer ) {
			killTimer( m_activationTimer );
			m_activationTimer = 0;
		}
		if ( statusMessageTimer ) {
			killTimer( statusMessageTimer );
			statusMessageTimer = 0;
		}
        stopStatusTimer();


		if ( deviceCommunicationStatus != DEVICE_IDLE ) {
			//@TODO: Inform the user that the device got disconnected while ...
			switch ( deviceCommunicationStatus ) {
				case DEVICE_READ_ALL_PARAMETERS:
					break;

				case DEVICE_WRITE_LIST_OF_PARAMETERS:
					break;

				case DEVICE_IS_THERE_RECORDED_DATA:
					break;

				case DEVICE_START_DEVICE_PROCEDURE:
					break;

				case DEVICE_STOP_DEVICE_PROCEDURE:
					break;

				case DEVICE_SAVE_RECORDED_DATA:
					break;

				case DEVICE_DELETE_RECORDED_DATA:
					break;

				case DEVICE_SOFT_RESET:
					break;

				case DEVICE_GET_BATTERY_STATUS:
					break;

				case DEVICE_SET_SYSTEM_TIME:
					break;

				default:
					break;
			}

			deviceCommunicationStatus = DEVICE_IDLE;
		}

		deviceConnectionStatus = NO_DEVICE_CONNECTED;
		connectedDeviceSN.clear();

		m_ui->pageConnection_ChooseDevicelistWidget->clear();

		m_ui->pageConnection_NextButton->setEnabled( false );

LOGMSG( "pwmDeviceConnectStatus() 153   going to   PAGE_CONNECTIONS_SCREEN ..." );
		m_ui->contentStackedWidget->setCurrentIndex( PAGE_CONNECTIONS_SCREEN );

		m_ui->pageConnection_StatusFrame->setVisible( true );
		m_ui->pageConnection_StatusLabel->setText( QObject::tr( "Sirona Disconnected!" ) );
		m_ui->pageConnection_StatusLabel->setStyleSheet( "background: #e33838" );

		statusMessageTimer = startTimer( 1500 ); // 1.5 seconds

		procedurePageAdveancedOptionsEnabled = false;
		pageProcedure_setupUI( false );
		LOGMSG( "Create statusMessageTimer = %d", statusMessageTimer );

		startEnumerateTimer();
		m_configParams = ConfigParams();
        m_cableId = 0;
	}
}

void SironaViewerUi::removeStatusMessage()
{
	LOGMSG( "removeStatusMessage" );
	switch ( m_ui->contentStackedWidget->currentIndex() ) {

		case PAGE_CONNECTIONS_SCREEN:
			m_ui->pageConnection_StatusFrame->setVisible( false );
			m_ui->pageConnection_StatusLabel->setText( "" );
			break;

		case PAGE_PROCEDURE_SCREEN:
			m_ui->pageProcedure_StatusFrame->setVisible( false );
			m_ui->pageProcedure_StatusLabel->setText( "" );
			break;
	}
}

void SironaViewerUi::pwmDeviceHandlerMessage( bool silent, bool success, QString message )
{
	LOGMSG( "pwmDeviceHandlerMessage - %s", message.toLatin1().data() );
	if ( silent ) {
	} else {
		if ( success ) {
			/* Put this information into appropriate text label */
		} else {
			/* Put this information into appropriate text label */
			QMessageBox::critical( this, tr( "Communication error." ), message );
		}
	}
}

void SironaViewerUi::pwmDeviceConfiguration( ConfigParams devConf )
{
	LOGMSG( "pwmDeviceConfiguration()" );
	try {
		evaluateConfigData( &devConf );
	} catch ( const QString &errorString ) {
		/* Put this information into appropriate text label */
		LOGMSG( "evaluateConfigData: %s", errorString.toLatin1().data() );
		if ( ( deviceConnectionStatus == USB_DEVICE_CONNECTED ) || ( deviceConnectionStatus == BT_DEVICE_CONNECTED ) ) {
			LOGMSG( "Device will be disconnected." );
		}
		deviceCommunicationStatus = DEVICE_IDLE;
		stopBluetoothTimer();
		deviceHandler->deviceStopLiveECG();
		emit disconnectPwmDevice( true, SIRONA_DRIVER_NO_ERROR );
		return;
	}

	m_configParams = devConf;

	switch ( m_ui->contentStackedWidget->currentIndex() ) {
		case PAGE_CONNECTIONS_SCREEN:
			if ( deviceCommunicationStatus == DEVICE_SET_SYSTEM_TIME ) {
				deviceCommunicationStatus = DEVICE_GET_BATTERY_STATUS;
				emit getBatteryStatus();

			} else if ( deviceCommunicationStatus == DEVICE_READ_ALL_PARAMETERS ) {

				switch ( m_configParams.procedure_state ) {
                    case PROCSTATE_IDLE:
						deviceCommunicationStatus = DEVICE_IDLE;
						previousPage = PAGE_CONNECTIONS_SCREEN;
						m_ui->pagePatient_BackButton->setVisible( false );
                        if ((m_configParams.device_mode == PROCTYPE_NOT_SET) || (m_configParams.cable_id == CABLETYPE_II_CHAN_PATCH) || (m_configParams.cable_id == CABLETYPE_II_CHAN_3_WIRE)) {
LOGMSG( "pwmDeviceConfiguration() 154   going to   PAGE_CABLE_SCREEN ..." );
							m_ui->contentStackedWidget->setCurrentIndex( PAGE_CABLE_SCREEN );
						} else {
LOGMSG( "pwmDeviceConfiguration() 155   going to   PAGE_PATIENT_SCREEN..." );
                            m_ui->contentStackedWidget->setCurrentIndex(PAGE_PATIENT_SCREEN);
						}
						break;
                    case PROCSTATE_STARTED:
						deviceCommunicationStatus = DEVICE_IDLE;
						previousPage = PAGE_CONNECTIONS_SCREEN;
LOGMSG( "pwmDeviceConfiguration() 156   going to   PAGE_PROCEDURE_ACTIVE_SCREEN ..." );
						m_ui->contentStackedWidget->setCurrentIndex( PAGE_PROCEDURE_ACTIVE_SCREEN );
						break;
                    case PROCSTATE_DONE:
						deviceCommunicationStatus = DEVICE_IS_THERE_RECORDED_DATA;
						emit isThereRecordedData();
						break;
				}
			}
			break;
		case PAGE_DEVICE_SCREEN:
		case PAGE_COMPANY_SCREEN:
			if ( previousPage == PAGE_CONNECTIONS_SCREEN ) {
                if ( m_configParams.procedure_state == PROCSTATE_DONE ) {
					deviceCommunicationStatus = DEVICE_IS_THERE_RECORDED_DATA;
					emit isThereRecordedData();
				} else {
					deviceCommunicationStatus = DEVICE_IDLE;
					previousPage = PAGE_PROCEDURE_ACTIVE_SCREEN;
				}

			} else if ( previousPage == PAGE_COMPLETE_SCREEN ) {
				deviceCommunicationStatus = DEVICE_IDLE;
				previousPage = PAGE_CABLE_SCREEN;
			}
			break;
		case PAGE_ECG_SCREEN:
			m_ui->pageECG_MainWidget->setCableId( devConf.cable_id );
			m_ui->pageECG_MainWidget->setDeviceMode ( devConf.device_mode );

			m_futureSamples.clear();
			liveEcgStartmsSinceEpox = 0;
			liveEcgSampleCount = 0;

			if ( m_liveEcgTimer ) {
				killTimer( m_liveEcgTimer );
			}
			m_liveEcgTimer = startTimer( 16, Qt::PreciseTimer );

			LOGMSG( "Create m_liveEcgTimer = %d", m_liveEcgTimer );
			m_ui->pageECG_StatusLabel->setText( "" );
			m_ui->pageECG_StatusLabel->setStyleSheet( "" );

			deviceCommunicationStatus = DEVICE_LIVE_ECG;
			emit deviceStartLiveECG();
			break;
		case PAGE_CABLE_SCREEN:
			cable_setupUI();
			break;
		default:
			break;
	}
}

void SironaViewerUi::pwmDeviceParameterListWritten( bool parameterListWrittenWithSuccess )
{
	LOGMSG( "pwmDeviceParameterListWritten" );
	Q_UNUSED( parameterListWrittenWithSuccess )

	if ( deviceCommunicationStatus == DEVICE_WRITE_LIST_OF_PARAMETERS ) {
		deviceCommunicationStatus = DEVICE_IDLE;
	}
    m_ui->pageProcedure_NextButton->setEnabled(true);
    m_ui->pageUSBSaved_TitleLabel->setVisible(true);
    m_ui->pageUSBSaved_MainLabel->setVisible(true);
}

void SironaViewerUi::pwmDeviceComputerOutOfSpace( QString message )
{
	LOGMSG( "pwmDeviceComputerOutOfSpace" );
	m_ui->pageDownload_MainMessageLabel->setText( message );
	m_ui->pageDownload_MainPercentageLabel->setText( "" );
	m_ui->pageDownload_NextFrame->setVisible( deviceConnectionStatus == BT_DEVICE_CONNECTED );
}

void SironaViewerUi::pwmDeviceThereIsDataRecorded( bool hasData )
{
    LOGMSG( "pwmDeviceThereIsDataRecorded(hasData:%d)    Event Count: %d", hasData, deviceHandler->deviceEventCount() );

    auto const eventPath = Utilities::getLocalStoragePath( "Event" );
	auto const holterPath = Utilities::getLocalStoragePath( "Holter" );
	auto const postPath = Utilities::getLocalStoragePath( "Post" );
	auto const mctPath = Utilities::getLocalStoragePath( "MCT" );

	if ( customer->review_patient_before_download ) {
		m_ui->pageDownloadOptions_ReviewFrame->show();
		m_ui->pageDownloadOptions_MainLabel->show();
	} else {
		m_ui->pageDownloadOptions_ReviewFrame->hide();
		m_ui->pageDownloadOptions_MainLabel->hide();
	}

	m_ui->pageDownloadOptions_PatientInfo->setText( getPatientInfoSummaryString() );
	m_ui->pageErase_PatientInfo->setText( getPatientInfoSummaryString() );

	m_ui->pageDownloadOptions_TitleLabel->setText(tr("Monitoring Complete"));


    if ( ( deviceConnectionStatus == BT_DEVICE_CONNECTED ) && ( (m_procType == PROCTYPE_HOLTER) || (m_procType == PROCTYPE_MCT)) ) {
LOGMSG( "pwmDeviceThereIsDataRecorded() 157   going to   PAGE_CONNECTIONS_SCREEN ..." );
		m_ui->pageDownload_MainStackedWidget->setCurrentIndex( PAGE_CONNECTIONS_SCREEN ); //disconnect message for BT with holter data
		previousPage = PAGE_PROCEDURE_ACTIVE_SCREEN;
		m_ui->pageDownload_NextFrame->setVisible( true );
		m_ui->pageDownload_MainPercentageLabel->setText( "" );
LOGMSG( "pwmDeviceThereIsDataRecorded() 158   going to   PAGE_DOWNLOAD_SCREEN ..." );
		m_ui->contentStackedWidget->setCurrentIndex( PAGE_DOWNLOAD_SCREEN );
		LOGMSG( "pwmDeviceThereIsDataRecorded()  RETURN because deviceConnectionStatus == %d  && m_procType == %d", deviceConnectionStatus, m_procType );
		return;
	}

	m_ui->pageDownload_NextFrame->setVisible( false );
LOGMSG( "pwmDeviceThereIsDataRecorded() 159   going to   PAGE_WELCOME_SCREEN ..." );
	m_ui->pageDownload_MainStackedWidget->setCurrentIndex ( PAGE_WELCOME_SCREEN ); //normal download message and status

	if ( deviceCommunicationStatus == DEVICE_IS_THERE_RECORDED_DATA ) {
		deviceCommunicationStatus = DEVICE_IDLE;
	}

    if ( hasData ) {

        if ((m_ui->contentStackedWidget->currentIndex() == PAGE_DEVICE_SCREEN) || (m_ui->contentStackedWidget->currentIndex() == PAGE_COMPANY_SCREEN)) {
            if ( ! downloadOptionsActive ) {
                downloadOptionsActive = true;
                previousPage = PAGE_DOWNLOAD_OPTIONS_SCREEN;
				LOGMSG( "pwmDeviceThereIsDataRecorded()  RETURN because we'll come back later to kick off the download as screen/page is Company or Device  and downloadOptionsActive == %d ", downloadOptionsActive );
                return;     // we'll come back later to kick off the download
            } else {
                downloadOptionsActive = false;
                previousPage = PAGE_DOWNLOAD_SCREEN;
            }
        } else {
			if ( (m_configParams.device_mode == PROCTYPE_EVENT) && (deviceHandler->deviceEventCount() <= 0) ) {
LOGMSG( "pwmDeviceThereIsDataRecorded() Event mode - Active but no data" );
                deviceCommunicationStatus = DEVICE_SOFT_RESET;
                m_ui->pageComplete_NewProcedureButton->setDisabled(true);
                emit softReset();
				QMessageBox::information( this, "Device Empty", "Procedure was started. But no events recorded." );
				m_ui->pageComplete_DownloadCompleteLabel->hide();
				m_ui->contentStackedWidget->setCurrentIndex(PAGE_COMPLETE_SCREEN);
				return;
			}
            if ( ! downloadOptionsActive ) {
                downloadOptionsActive = true;
                previousPage = (pageNames_t)m_ui->contentStackedWidget->currentIndex();
LOGMSG( "pwmDeviceThereIsDataRecorded() 160   going to   PAGE_DOWNLOAD_OPTIONS_SCREEN..." );
                m_ui->contentStackedWidget->setCurrentIndex(PAGE_DOWNLOAD_OPTIONS_SCREEN);
				LOGMSG( "pwmDeviceThereIsDataRecorded()  RETURN because we'll come back later to kick off the download as screen/page is NOT Company nor Device  and downloadOptionsActive == %d ", downloadOptionsActive );
                return;     // we'll come back later to kick off the download
            } else {
                downloadOptionsActive = false;
                previousPage = (pageNames_t)m_ui->contentStackedWidget->currentIndex();
                m_ui->pageDownload_MainPercentageLabel->setText( "" );
LOGMSG( "pwmDeviceThereIsDataRecorded() 161   going to   PAGE_DOWNLOAD_SCREEN..." );
                m_ui->contentStackedWidget->setCurrentIndex(PAGE_DOWNLOAD_SCREEN);
            }
        }

        // now that the UI is showing the download, actually kick it off

		m_ui->pageDownloadOptions_TitleLabel->setText(tr("Downloading"));

		deviceCommunicationStatus = DEVICE_SAVE_RECORDED_DATA;
        if ( m_configParams.device_mode == PROCTYPE_EVENT ) {
			LOGMSG( "pwmDeviceThereIsDataRecorded()  Downloading Events..." );
            emit saveDataOnDevice( true, eventPath, false, holterPath );
        } else if (m_configParams.device_mode == PROCTYPE_HOLTER ) {
			LOGMSG( "pwmDeviceThereIsDataRecorded()  Downloading Holter..." );
            emit saveDataOnDevice( false, eventPath, true, holterPath );
        } else if ( m_configParams.device_mode == PROCTYPE_POST ) {
			LOGMSG( "pwmDeviceThereIsDataRecorded()  Downloading Post Events..." );
            emit saveDataOnDevice( true, postPath, false, holterPath );
        } else if ( m_configParams.device_mode == PROCTYPE_MCT ) {
			LOGMSG( "pwmDeviceThereIsDataRecorded()  Downloading Events and Holter..." );
            emit saveDataOnDevice( true, eventPath, true, mctPath );
		}

	} else {
		LOGMSG( "pwmDeviceThereIsDataRecorded(NO DATA)   currently at screen: %d ", m_ui->contentStackedWidget->currentIndex() );
		m_ui->pageComplete_DownloadCompleteLabel->hide();
        switch ( m_ui->contentStackedWidget->currentIndex() ) {
            case PAGE_CONNECTIONS_SCREEN:
                previousPage = PAGE_CONNECTIONS_SCREEN;
				if ( (m_configParams.device_mode == PROCTYPE_EVENT) && (deviceHandler->deviceEventCount() <= 0) ) {
LOGMSG( "pwmDeviceThereIsDataRecorded() 162   going to   PAGE_COMPLETE_SCREEN..." );
					m_ui->contentStackedWidget->setCurrentIndex(PAGE_COMPLETE_SCREEN);
				} else {
LOGMSG( "pwmDeviceThereIsDataRecorded() 163   going to   PAGE_CABLE_SCREEN..." );
					m_ui->contentStackedWidget->setCurrentIndex(PAGE_CABLE_SCREEN);
				}
                break;
            case PAGE_DEVICE_SCREEN:
            case PAGE_COMPANY_SCREEN:
                if (previousPage == PAGE_CONNECTIONS_SCREEN) {
                    previousPage = PAGE_CABLE_SCREEN;
                }
                break;
            case PAGE_PROCEDURE_ACTIVE_SCREEN:
            case PAGE_DOWNLOAD_OPTIONS_SCREEN:
            case PAGE_ERASE_SCREEN:
            case PAGE_DOWNLOAD_SCREEN:
                previousPage = PAGE_CONNECTIONS_SCREEN;
				if ( (m_configParams.device_mode == PROCTYPE_EVENT) && (deviceHandler->deviceEventCount() <= 0) ) {
					m_ui->pageComplete_DownloadCompleteLabel->hide();
				} else {
					m_ui->pageComplete_DownloadCompleteLabel->show();
				}
LOGMSG( "pwmDeviceThereIsDataRecorded() 164   going to   PAGE_COMPLETE_SCREEN..." );
				m_ui->contentStackedWidget->setCurrentIndex(PAGE_COMPLETE_SCREEN);
                break;
		}
	}
}

void SironaViewerUi::pwmDeviceDataSaved( bool eventSaveSuccess, bool holterSaveSuccess, QList<QString> fileList )
{
	LOGMSG( "pwmDeviceDataSaved" );
	if ( deviceCommunicationStatus == DEVICE_SAVE_RECORDED_DATA ) {
		deviceCommunicationStatus = DEVICE_IDLE;
	}

    if ( ( eventSaveSuccess && ( m_configParams.device_mode == PROCTYPE_EVENT || m_configParams.device_mode == PROCTYPE_POST ) ) ||
            ( holterSaveSuccess && ( m_configParams.device_mode == PROCTYPE_HOLTER || m_configParams.device_mode == PROCTYPE_MCT ) ) ) {

		if ( customer->server_enabled ) {
			uploadFileList = fileList;

LOGMSG( "pwmDeviceDataSaved() 165   going to   PAGE_CABLE_SCREEN ..." );
			m_ui->pageDownload_MainStackedWidget->setCurrentIndex( PAGE_CABLE_SCREEN );
			m_ui->pageDownload_Server_Message->setText( "" );

			upload_cancelled = false;
			m_files_to_send = uploadFileList.count();
			if ( m_files_to_send == 0 ) {
                // erase the procedure
                deviceCommunicationStatus = DEVICE_SOFT_RESET;
                m_ui->pageComplete_NewProcedureButton->setDisabled(true);
                emit softReset();

                if ( m_ui->contentStackedWidget->currentIndex() == PAGE_DOWNLOAD_SCREEN ) {
					previousPage = PAGE_DOWNLOAD_SCREEN;
LOGMSG( "pwmDeviceDataSaved() 101   going to PAGE_COMPLETE_SCREEN..." );
					m_ui->contentStackedWidget->setCurrentIndex( PAGE_COMPLETE_SCREEN );

				} else if ( ( m_ui->contentStackedWidget->currentIndex() == PAGE_DEVICE_SCREEN ) || ( m_ui->contentStackedWidget->currentIndex() == PAGE_COMPANY_SCREEN ) ) {
					if ( previousPage == PAGE_DOWNLOAD_SCREEN ) {
						previousPage = PAGE_COMPLETE_SCREEN;
					}
				}
			} else {
				deviceHandler->stopReconnectTimer();
				m_files_sent = 0;
				onUploadPercentageChange( 0 );
                if (m_configParams.device_mode == PROCTYPE_EVENT || m_configParams.device_mode == PROCTYPE_POST || m_configParams.device_mode == PROCTYPE_MCT) {

					customer_url = customer->network_url_event;

					customer_ftp_url_info.scheme = customer->server_scheme2;
					customer_ftp_url_info.host = customer->server_host2;
					customer_ftp_url_info.port = QString::number( customer->server_port2 );
					customer_ftp_url_info.security = customer->server_tls2;
                    customer_ftp_url_info.directory = customer->server_directory2;
					customer_ftp_url_info.username = customer->server_user_name2;
                    customer_ftp_url_info.password = customer->server_password2;

                } else if ( m_configParams.device_mode == PROCTYPE_HOLTER ) {

					customer_url = customer->network_url_holter;

					customer_ftp_url_info.scheme = customer->server_scheme;
					customer_ftp_url_info.host = customer->server_host;
					customer_ftp_url_info.port = QString::number( customer->server_port );
					customer_ftp_url_info.security = customer->server_tls;
                    customer_ftp_url_info.directory = customer->server_directory;
					customer_ftp_url_info.username = customer->server_user_name;
                    customer_ftp_url_info.password = customer->server_password;

				} else {
					LOGMSG( "pwmDeviceThereIsDataRecorded()  Unexpected device_mode when setting request_type!" );
                    m_ui->pageDownload_Server_Message->setText(tr("Unexpected device mode."));
					cancelUpload();
					return;
				}
				file_handler();
			}

		} else {
            // Files are not being uploaded to a server. Move them to SavePath.
			for ( qint32 i = 0; i < fileList.count(); i ++ ) {
                Utilities::moveProcedureToSavePath(fileList.value(i), customer->generateResFile);
                emit markFilesSent( i );
			}

            // erase the procedure
            deviceCommunicationStatus = DEVICE_SOFT_RESET;
            m_ui->pageComplete_NewProcedureButton->setDisabled(true);
LOGMSG( "pwmDeviceDataSaved() calling softReset()  RESET 10" );
            emit softReset();

            if ( m_ui->contentStackedWidget->currentIndex() == PAGE_DOWNLOAD_SCREEN ) {
				previousPage = PAGE_DOWNLOAD_SCREEN;
				if ( (m_configParams.device_mode == PROCTYPE_EVENT) && (deviceHandler->deviceEventCount() <= 0) ) {
					m_ui->pageComplete_DownloadCompleteLabel->hide();
				} else {
					m_ui->pageComplete_DownloadCompleteLabel->show();
				}
LOGMSG( "pwmDeviceDataSaved() 102   going to  PAGE_COMPLETE_SCREEN ..." );
				m_ui->contentStackedWidget->setCurrentIndex( PAGE_COMPLETE_SCREEN );

			} else if ( ( m_ui->contentStackedWidget->currentIndex() == PAGE_DEVICE_SCREEN ) || ( m_ui->contentStackedWidget->currentIndex() == PAGE_COMPANY_SCREEN ) ) {
				if ( previousPage == PAGE_DOWNLOAD_SCREEN ) {
					previousPage = PAGE_COMPLETE_SCREEN;
				}
			}
		}
	} else {
LOGMSG( "pwmDeviceDataSaved() 103   going to   PAGE_PATIENT_SCREEN  ..." );
		m_ui->pageDownload_MainStackedWidget->setCurrentIndex( PAGE_PATIENT_SCREEN ); //fail screen
		m_ui->pageDownload_NextFrame->setVisible( true );
	}

	if ( m_bluetoothTimer ) {
		restartBluetoothTimer();
	}

	if ( m_activationTimer ) {
		m_activationTimer = 0;
		m_activationTimer = startTimer( ACTIVATION_TIMEOUT );
		LOGMSG( "pwmDeviceDataSaved Create m_activationTimer = %d", m_activationTimer );
	}
}

void SironaViewerUi::file_handler()
{
    uploadData = nullptr;
	if ( customer_url.scheme().toLower().contains( "http" ) ) {
		sendReportViaHttp( uploadFileList.first() );
	} else if ( customer_url.scheme().toLower().contains( "ftp" ) ) {
		sendReportViaFtp( uploadFileList.first() );
	} else {
		LOGMSG( "Server scheme not set. Could not send data to server." );
        m_ui->pageDownload_Server_Message->setText(tr("Server scheme not set."));
		cancelUpload();
	}
}



void SironaViewerUi::getNewConfiguration()
{
	QString pwd = QInputDialog::getText(this, tr("Configuration Password"), tr("Sirona Viewer not configured yet.\n\nPlease enter your configuration password"), QLineEdit::Password );

	QNetworkAccessManager * mgr = new QNetworkAccessManager(this);
	connect( mgr, SIGNAL(finished(QNetworkReply*)), this, SLOT(onQueryConfigFinish(QNetworkReply*)) );
	connect( mgr, SIGNAL(finished(QNetworkReply*)), mgr, SLOT(deleteLater()) );

	QUrl url = QUrl(QString("https://sironaecg.com/Public/config/index.php?es=%1").arg(pwd));
	QNetworkRequest request(url);

	QSslConfiguration sslConfiguration = request.sslConfiguration();
	sslConfiguration.setPeerVerifyMode(QSslSocket::VerifyNone);
	sslConfiguration.setProtocol(QSsl::AnyProtocol);
	request.setSslConfiguration(sslConfiguration);

	request.setAttribute( QNetworkRequest::FollowRedirectsAttribute, true );
	mgr->get(request);
}

void SironaViewerUi::onQueryConfigFinish( QNetworkReply *reply )
{
	QString restartMessage = "Configuration Failed.  Please restart and try again.";
	QMessageBox msgBox;

	if ( reply ) {
		QString encryptedString = (QString) reply->readAll();

		if ( customer->hasConfig(encryptedString) ) {
			QSettings settings( QSettings::IniFormat, QSettings::UserScope, MANUFACTURER_NAME, APPLICATION_NAME );
			settings.setValue( "config_string", encryptedString );
			customer->load();

			updateShortcut( customer->sv_icon );

			restartMessage = "Configuration Saved.  Please restart now.";
		}
	}

	msgBox.setText(restartMessage);
	msgBox.exec();
	qApp->quit();

	return;
}

void SironaViewerUi::updateShortcut( QString iconName )
{
	QFile script(":/mkicon.py");
	QString tmpFilename = QDir::toNativeSeparators( QDir::tempPath() + "/" + "mkicon.py");

	QFile::remove(tmpFilename);
	script.copy(tmpFilename);

	QString myPath = QDir::toNativeSeparators( QDir::currentPath() );
	QString program = QDir::toNativeSeparators( QDir::cleanPath(myPath+"/python-3.9.2/python.exe") );
	QStringList args;

	args << tmpFilename;
	args << iconName;
	args << QDir::cleanPath( myPath );

#ifdef DEBUG
	QQQ("C:/temp/sv.log") << program << " " << args.join(" ");
#endif

	if ( myProcess ) {
		delete myProcess;
	}
	myProcess = new QProcess(this);
	myProcess->start( program, args );
	myProcess->waitForStarted();
	myProcess->waitForFinished();
	QFile::remove(tmpFilename);
}


void SironaViewerUi::onUploadPercentageChange( qint64 percentage )
{
	m_ui->pageDownload_HTTPPercentageLabel->setText( QString( "%1%" ).arg( ( percentage + m_files_sent * 100 ) / m_files_to_send ) );
	m_ui->pageDownload_HTTPPercentageLabel->repaint();
	QCoreApplication::processEvents( QEventLoop::AllEvents );
}

void SironaViewerUi::reloadUploadTimer()
{
	if ( m_upload_timeout ) {
        killTimer(m_upload_timeout);
        m_upload_timeout = startTimer(1000 * 60);

        // once a second, ping the device to keep it connected
        static QDateTime lastDevicePing = QDateTime::currentDateTime();
        if (lastDevicePing.msecsTo(QDateTime::currentDateTime()) > 1000) {
            lastDevicePing = QDateTime::currentDateTime();
            isDeviceConnected();
        }
	}
}

void SironaViewerUi::sendReportViaHttp( QString datFileName )
{
	if ( dbgTesting ) {
		QMessageBox::information( this, "Debug Testing", "Not uploading to the customers HTTP site as this is just a local test" );
		QNetworkReply *reply = nullptr;
		uploadFinished( reply );
	} else {
		// in the remote chance that the boundary actually matches the file, qrand() will prevent the problem from repeating if the user tries again
		QString boundary = "--FormBoundary" + QString::number( qrand() % 10000 );

		const char *requestType;
		if (m_configParams.device_mode == PROCTYPE_EVENT || m_configParams.device_mode == PROCTYPE_POST || m_configParams.device_mode == PROCTYPE_MCT) {
			requestType = "event";
		} else if ( m_configParams.device_mode == PROCTYPE_HOLTER ) {
			requestType = "holter";
		} else {
			LOGMSG( "Unexpected device_mode when setting request_type!" );
			requestType = "unknown";
		}

		if (customer->mct_server) {
			customer_url.setQuery("device_serial=" + connectedDeviceSN);
		}
		QNetworkRequest request( customer_url );

		if (customer->mct_server) {
			QString atrFileName = Utilities::changeExtenstion( datFileName, "atr" );
			QString heaFileName = Utilities::changeExtenstion( datFileName, "hea" );
			QString jsonFileName = Utilities::changeExtenstion( datFileName, "json" );
			uploadData = new HttpMctData(boundary, atrFileName, datFileName, heaFileName, jsonFileName);
		}
		else {
			QString zipFileName = Utilities::changeExtenstion( datFileName, "zip" );
			uploadData = new HttpPostData( connectedDeviceSN, requestType, boundary, zipFileName );
		}

		if ( !uploadData->open( QIODevice::ReadOnly ) ) {
			LOGMSG("sendReportViaHttp can't open data file");
			m_ui->pageDownload_Server_Message->setText(tr("Could not open data file."));
			cancelUpload();
			return;
		}
		connect( uploadData, SIGNAL( percentageChanged( qint64 ) ), this, SLOT( onUploadPercentageChange( qint64 ) ) );
		connect( uploadData, SIGNAL( reloadTimer() ), this, SLOT( reloadUploadTimer() ) );

		if ( customer->network_url_holter.scheme().toLower() == "https" ) {
			request.setSslConfiguration( QSslConfiguration::defaultConfiguration() );
		}
		request.setRawHeader( "Content-Type", "multipart/form-data; boundary=" + boundary.toLatin1() );
		request.setAttribute( QNetworkRequest::FollowRedirectsAttribute, true );

		LOGMSG("sendReportViaHttp()  %s to %s  ",
				datFileName.toLatin1().data(), customer->network_url_holter.host().toLatin1().data() );

		m_upload_timeout = startTimer( 1000 * 30 );
		network_reply = network_access_manager->post( request, uploadData );
	}
}

void SironaViewerUi::sendReportViaFtp( QString datFileName )
{
	QByteArray file_name = Utilities::changeExtenstion( datFileName, "zip" ).toLatin1();

	LOGMSG("sendReportViaFtp()  %s to %s ", file_name.data(), customer_ftp_url_info.host.toLatin1().data() );

	if ( dbgTesting ) {
		QMessageBox::information( this, "Debug Testing", "Not uploading to the customers FTP site as this is just a local test" );
		QNetworkReply *reply = nullptr;
		uploadFinished( reply );
	} else {
		if ( curl_sender->sendFile( file_name, customer_ftp_url_info ) ) {
			const char *errStr = curl_sender->getErrorString();
			if (errStr == NULL) {
				errStr = "Unknown failure";
			}
			LOGMSG("sendReportViaFtp Communication Error: %s", errStr);
			m_ui->pageDownload_Server_Message->setText(tr( "\nCommunication Error:\n" ) + errStr);
			cancelUpload();
		} else {
			QNetworkReply *reply = nullptr;
			uploadFinished( reply );
		}
	}
}

void SironaViewerUi::uploadFinished( QNetworkReply *reply )
{
	if ( uploadData ) {
		delete uploadData;
	}

	QString upload_successful = "failure";
	QString server_message = tr( "\nUnknown server error." );

	if ( dbgTesting ) {
		upload_successful = "success";
	} else {
		if ( reply ) {
			QString response = ( QString )reply->readAll();
			QJsonDocument json_response = QJsonDocument::fromJson( response.toUtf8() );
			QJsonObject object = json_response.object();
			// support either the old or new (MCT) json reply
			upload_successful = object.value( "Result" ).toString() + object.value( "result" ).toString();
			QString message = object.value( "Message" ).toString() + object.value( "error" ).toString();
			if ( message != "" ) {
				server_message = tr( "\nServer Message:\n" ) + message;
			} else {
				QVariant status_code = reply->attribute( QNetworkRequest::HttpStatusCodeAttribute );
				if ( status_code.isValid() ) {
					if ( status_code.toInt() == 200 ) {
						// the server replied with 200/OK but didn't give a valid JSON reply
						server_message = tr( "\nServer error message missing" );
					} else {
						server_message = tr( "\nServer Message:\n" ) + status_code.toString();
						QVariant status_msg = reply->attribute( QNetworkRequest::HttpReasonPhraseAttribute );
						if ( status_msg.isValid() ) {
							server_message += " " + status_msg.toString();
						}
					}
				} else if ( reply->error() != QNetworkReply::NoError ) {
					server_message = tr( "\nCommunication Error:\n" ) + reply->errorString();
				}
			}
		}
	}

    if (upload_cancelled) {
        // pageDownload_Server_Message was already set before the upload was canceled
    } else if (upload_successful.toLower() == "success" || customer_url.scheme().contains("ftp")) {
        if (uploadFileList.count() > 0) {
            Utilities::removeAllProcedureFiles(uploadFileList.first());
            uploadFileList.removeFirst();
        }

		emit markFilesSent( ( qint32 )m_files_sent );
		m_files_sent++;
		onUploadPercentageChange( 0 );
		if ( m_files_sent == m_files_to_send ) {
			killTimer( m_upload_timeout );
            m_upload_timeout = 0;

            // erase the procedure
            deviceCommunicationStatus = DEVICE_SOFT_RESET;
            m_ui->pageComplete_NewProcedureButton->setDisabled(true);
LOGMSG( "uploadFinished() calling softReset()  RESET 11" );
            emit softReset();

            if ( m_ui->contentStackedWidget->currentIndex() == PAGE_DOWNLOAD_SCREEN ) {
				previousPage = PAGE_DOWNLOAD_SCREEN;
LOGMSG( "uploadFinished() 104   going to   PAGE_COMPLETE_SCREEN ..." );
				m_ui->contentStackedWidget->setCurrentIndex( PAGE_COMPLETE_SCREEN );

			} else if ( ( m_ui->contentStackedWidget->currentIndex() == PAGE_DEVICE_SCREEN ) || ( m_ui->contentStackedWidget->currentIndex() == PAGE_COMPANY_SCREEN ) ) {
				if ( previousPage == PAGE_DOWNLOAD_SCREEN ) {
					previousPage = PAGE_COMPLETE_SCREEN;
				}
			}
			deviceHandler->resetReconnectTimer();
		} else {
			file_handler();
		}
	} else {
        LOGMSG("uploadFinished failure: %s", server_message.toLatin1().data());
		cancelUpload();
		m_ui->pageDownload_Server_Message->setText( server_message );
	}
	if ( m_bluetoothTimer ) {
		restartBluetoothTimer();
	}
	if ( m_activationTimer ) {
		m_activationTimer = 0;
		m_activationTimer = startTimer( ACTIVATION_TIMEOUT );
	}
	if ( reply ) {
		reply->deleteLater();
	}
}

void SironaViewerUi::cancelUpload()
{
	if ( !upload_cancelled ) {
		upload_cancelled = true;
LOGMSG( "cancelUpload() 105   going to   PAGE_PATIENT_SCREEN ..." );
		m_ui->pageDownload_MainStackedWidget->setCurrentIndex( PAGE_PATIENT_SCREEN ); //fail screen
		m_ui->pageDownload_NextFrame->setVisible( true );

		if ( network_reply ) {
			network_reply->abort();
			network_reply->deleteLater();
		}

		for ( int i = 0; i < uploadFileList.count(); i++ ) {
            Utilities::removeAllProcedureFiles(uploadFileList.at(i));
		}
		uploadFileList.clear();

		if ( m_upload_timeout ) {
			killTimer( m_upload_timeout );
			m_upload_timeout = 0;
		}
	}
}

void SironaViewerUi::pwmDeviceDataDeleted ( bool deleteSuccess )
{
	Q_UNUSED( deleteSuccess );
	LOGMSG( "pwmDeviceDataDeleted" );
	if ( deviceCommunicationStatus == DEVICE_DELETE_RECORDED_DATA ) {
		deviceCommunicationStatus = DEVICE_IDLE;
	}
}

void SironaViewerUi::pwmDeviceBatteryStatus( quint32 batteryVoltage )
{
	LOGMSG( "pwmDeviceBatteryStatus" );
	if ( m_ui->contentStackedWidget->currentIndex() == PAGE_CONNECTIONS_SCREEN ) {
		if ( deviceCommunicationStatus == DEVICE_GET_BATTERY_STATUS ) {
			deviceCommunicationStatus = DEVICE_READ_ALL_PARAMETERS;
			emit parameterReadAll();
		}

	} else {

		if ( deviceCommunicationStatus == DEVICE_GET_BATTERY_STATUS ) {
			deviceCommunicationStatus = DEVICE_IDLE;
		}

		m_percentage = getPercentBatteryCharge( batteryVoltage );

		m_ui->pageProcedureMain_BatteryProgressBar->setValue( m_percentage );
		m_ui->pageDevice_BatteryInfoProgressBar->setValue( m_percentage );

		if ( m_percentage <= 20 ) {
			m_ui->pageProcedureMain_BatteryProgressBar->setStyleSheet( "QProgressBar:horizontal { border: 0px solid gray; text-align: right; margin-right: 16ex} QProgressBar::chunk { background: red;}" );
			m_ui->pageDevice_BatteryInfoProgressBar->setStyleSheet( "QProgressBar:horizontal { border: 0px solid gray; text-align: right; margin-right: 16ex} QProgressBar::chunk { background: red;}" );
		} else {
			m_ui->pageProcedureMain_BatteryProgressBar->setStyleSheet( "QProgressBar:horizontal { border: 0px solid gray; text-align: right; margin-right: 16ex} QProgressBar::chunk { background: green;}" );
			m_ui->pageDevice_BatteryInfoProgressBar->setStyleSheet( "QProgressBar:horizontal { border: 0px solid gray; text-align: right; margin-right: 16ex} QProgressBar::chunk { background: green;}" );
		}
		m_ui->pageProcedureMain_BatteryProgressBar->update();
		m_ui->pageDevice_BatteryInfoProgressBar->update();
	}
}

void SironaViewerUi::pwmDeviceProcedureStopped()
{
	LOGMSG( "pwmDeviceProcedureStopped" );
	emit parameterRead( "PROC_STATE" );

	deviceCommunicationStatus = DEVICE_IS_THERE_RECORDED_DATA;
	emit isThereRecordedData();
}

void SironaViewerUi::pwmDeviceDownloadProgress( quint32 current, quint32 total )
{
	quint32 percentage = 0;
	QString text;

	m_ui->pageDownload_MainMessageLabel->setText( tr( "Downloading data…" ) );

	if ( total > 0 ) {
		percentage = qRound( ( ( qreal )current * 100 ) / total );
	}

	text = QString( "%1%" ).arg( percentage );

    LOGMSG("pwmDeviceDownloadProgress %s", text.toLatin1().data());
	m_ui->pageDownload_MainPercentageLabel->setText( text );
}


void SironaViewerUi::pwmDeviceEnumerateBtDevices( QList<deviceInfo_t> devices )
{
    LOGMSG( "pwmDeviceEnumerateBtDevices()" );
#ifdef OBSOLETE
	qSort( devices );
#else
	std::sort(devices.begin(), devices.end(), [] ( const deviceInfo_t& lhs, const deviceInfo_t& rhs ) {
		if ( lhs.devType != rhs.devType ) {
			return lhs.devType > rhs.devType;
		}

		return strcmp( lhs.devSerialNumber, rhs.devSerialNumber ) < 0;
	});
#endif

	for ( deviceInfo_t device : devices ) {
		QList<QListWidgetItem *> found_items;
		QListWidgetItem *item = nullptr;
		QByteArray devicePortName;

		QString sn_prefix = ( QString ) device.devSerialNumber[0] + ( QString ) device.devSerialNumber[1] + ( QString ) device.devSerialNumber[2] + ( QString ) device.devSerialNumber[3];
        if ((!customer->serial_number_prefix.contains(sn_prefix.toUpper())) && (!customer->manufacturing)) {
LOGMSG( "pwmDeviceEnumerateBtDevices()   Found foreign device: %s", device.devSerialNumber );
			continue;
		}
LOGMSG( "pwmDeviceEnumerateBtDevices()   Found PROPER  device: %s", device.devSerialNumber );

		// has this device been already listed
		found_items = m_ui->pageConnection_ChooseDevicelistWidget->findItems( device.devSerialNumber, Qt::MatchFixedString );

		switch ( found_items.count() ) {
			//the device was not listed before
			case 0:
				item = new QListWidgetItem();
				item->setText( QString( device.devSerialNumber ).toUpper() );
				item->setIcon( m_bt_image );
				devicePortName.resize( 6 );
				memcpy( devicePortName.data(), device.devPortName, 6 );
				item->setData( Qt::UserRole, devicePortName );
				item->setData( Qt::UserRole + 1, SIRONA_DRIVER_DEVICE_BLUETOOTH );
				item->setData( Qt::UserRole + 2, QDateTime::currentDateTime() );
				item->setTextAlignment( Qt::AlignHCenter );
				m_ui->pageConnection_ChooseDevicelistWidget->addItem( item );
				break;
			//the device was listed before
			case 1:
				item = found_items.first();
				if ( item->data( Qt::UserRole + 1 ) == SIRONA_DRIVER_DEVICE_BLUETOOTH ) {
					item->setData( Qt::UserRole + 2, QDateTime::currentDateTime() );
				}
				break;
		}
	}

	// Remove bluetooth devices not found in last 10 seconds
	for ( int c = 0; c < m_ui->pageConnection_ChooseDevicelistWidget->count(); c++ ) {
		QListWidgetItem *item = m_ui->pageConnection_ChooseDevicelistWidget->item( c );
		QDateTime timestamp = item->data( Qt::UserRole + 2 ).toDateTime();
		int itemType = item->data( Qt::UserRole + 1 ).toInt();
		item = nullptr;

		timestamp = timestamp.addSecs( 10 );

		if ( ( itemType == SIRONA_DRIVER_DEVICE_BLUETOOTH ) && ( timestamp < QDateTime::currentDateTime() ) ) {
			delete m_ui->pageConnection_ChooseDevicelistWidget->takeItem( c );
			c--;
		}
	}

	if ( deviceConnectionStatus == NO_DEVICE_CONNECTED ) {
		startEnumerateTimer();
	}
}

void SironaViewerUi::statusReceived( quint8 leadOff, quint8 cableId, quint8 procState, quint8 procType)
{
	/* DEBUG */
	{
		static time_t lastStatusReported = 0;
		static quint8 glbLeadOff, glbCableId, glbProcState, glbProcType;

		if (	(glbLeadOff != leadOff) ||
				(cableId != glbCableId) ||
				(procState != glbProcState) ||
				(procType  != glbProcType) ||
				(time(NULL) > lastStatusReported + 30) ) {
			LOGMSG("statusReceived()  leadOff = %d, cableId = %d, procState = %s, procType = %s)", leadOff, cableId, strProcState(procState), strProcType(procType));
			glbLeadOff = leadOff;
			glbCableId = cableId;
			glbProcState = procState;
			glbProcType = procType;
			time(&lastStatusReported);
		}
	}
	/* DEBUG */

    m_configParams.procedure_state = procState;
    m_leadOff = leadOff;
    m_cableId = cableId;
    m_procType = procType;
    m_procState = procState;

	/* only show diary entry stuff for A.C.T. when ready to download the data */
	if ( procState == PROCSTATE_IDLE ) {
		m_ui->grpDiaryEntries->hide();
	} else {
		if ( glbCustomCode == "ACT1" ) {
			m_ui->grpDiaryEntries->show();
		}
	}

	switch (m_ui->contentStackedWidget->currentIndex() ) {
		case PAGE_CABLE_SCREEN:
			if (cableId != CABLETYPE_UNKNOWN) {
				if (m_procType == PROCTYPE_NOT_SET) {
					cable_setupUI();
				} else {
					previousPage = PAGE_CABLE_SCREEN;
					futurePage = PAGE_PATIENT_SCREEN;
					emit parameterRead("PROC_STATE");
				}
				emit parameterReadAll();
			}
			break;
		case PAGE_PATIENT_SCREEN:
		case PAGE_PROCEDURE_SCREEN:
			if (procState == PROCSTATE_STARTED) {
LOGMSG( "statusReceived() 106   going to   PAGE_PROCEDURE_ACTIVE_SCREEN ..." );
				m_ui->contentStackedWidget->setCurrentIndex( PAGE_PROCEDURE_ACTIVE_SCREEN );
			} else if ( (procState == PROCSTATE_DONE) && (cableId != CABLETYPE_UNKNOWN) ) {
				if ( (m_configParams.device_mode == PROCTYPE_EVENT) && (deviceHandler->deviceEventCount() <= 0) ) {
					m_ui->pageComplete_DownloadCompleteLabel->hide();
				} else {
					m_ui->pageComplete_DownloadCompleteLabel->show();
				}
#ifdef ANTIQUE
LOGMSG( "statusReceived() 107   going to   PAGE_COMPLETE_SCREEN ..." );
				m_ui->contentStackedWidget->setCurrentIndex( PAGE_COMPLETE_SCREEN );
#else
#endif
			}
			break;
		case PAGE_DEVICE_SCREEN:
			updateDeviceScreenWidget();
			break;
		case PAGE_HOOKUP_SCREEN:
			if (procState == PROCSTATE_STARTED) {
LOGMSG( "statusReceived() 108   going to   PAGE_PROCEDURE_ACTIVE_SCREEN ..." );
				m_ui->contentStackedWidget->setCurrentIndex( PAGE_PROCEDURE_ACTIVE_SCREEN );
			} else if (procState == PROCSTATE_DONE) {
LOGMSG( "statusReceived() 109   going to   PAGE_COMPLETE_SCREEN ..." );
				m_ui->contentStackedWidget->setCurrentIndex( PAGE_COMPLETE_SCREEN );
			}
			if ( (leadOff != 0) && (m_leadoffTimer == 0) ) {
				m_leadoffTimer = startTimer(200);
			} else if ( ( leadOff == 0) && ( m_leadoffTimer != 0) ) {
				killTimer( m_leadoffTimer );
				m_leadoffTimer = 0;
			}
			updateElectrodesWidget();
			break;
		case PAGE_ECG_SCREEN:
			if ( m_cableId != CABLETYPE_UNKNOWN ) {
				m_ui->pageECG_StatusLabel->setText( "" );
				m_ui->pageECG_StatusLabel->setStyleSheet( "" );
				pageEcg_setupUI();
			}
			break;
		default:
			break;
	}
}

void SironaViewerUi::pwmDeviceStopLiveECG( int errorCode )
{
	deviceCommunicationStatus = DEVICE_IDLE;

	m_futureSamples.clear();
	liveEcgStartmsSinceEpox = 0;
	liveEcgSampleCount = 0;
	killTimer( m_liveEcgTimer );
	m_liveEcgTimer = 0;

	m_ui->pageECG_StatusLabel->setText( "" );
	m_ui->pageECG_StatusLabel->setStyleSheet( "" );

	switch ( m_ui->contentStackedWidget->currentIndex() ) {
		case PAGE_ECG_SCREEN:
            if ( errorCode == SIRONA_DRIVER_STREAM_STOPPED ) {
                if ( m_configParams.device_mode == PROCTYPE_POST ) {
					m_ui->pageECG_StatusLabel->setText( tr( "Connect sensor" ) );
				} else {
					m_ui->pageECG_StatusLabel->setText( tr( "Connect patient cable" ) );
				}
				m_ui->pageECG_StatusLabel->setStyleSheet( "background: red" );
				m_ui->pageECG_NextButton->setEnabled( false );
                startStatusTimer();
				return;
			}
			switch ( m_exitScreenOperation ) {
				case QPanelExitOperationNoOperation:
					break;
				case QPanelExitOperationNextScreen:
					previousPage = PAGE_ECG_SCREEN;
					deviceHandler->deviceStopLiveECG();
					emit disconnectPwmDevice( true, SIRONA_DRIVER_NO_ERROR );
					break;
				case QPanelExitOperationPreviousScreen:
					previousPage = PAGE_ECG_SCREEN;
LOGMSG( "pwmDeviceStopLiveECG() 110   going to   PAGE_HOOKUP_SCREEN ..." );
					m_ui->contentStackedWidget->setCurrentIndex( PAGE_HOOKUP_SCREEN );
					break;
				case QPanelExitOperationSettingsScreen:
					previousPage = PAGE_ECG_SCREEN;
LOGMSG( "pwmDeviceStopLiveECG() 111   going to   PAGE_DEVICE_SCREEN ..." );
					m_ui->contentStackedWidget->setCurrentIndex( PAGE_DEVICE_SCREEN );
					break;
				case QPanelExitOperationLogoScreen:
					previousPage = PAGE_ECG_SCREEN;
LOGMSG( "pwmDeviceStopLiveECG() 112   going to   PAGE_COMPANY_SCREEN ..." );
					m_ui->contentStackedWidget->setCurrentIndex( PAGE_COMPANY_SCREEN );
					break;
				default:
					break;
			}
			break;
		default:
			break;
	}
}

void SironaViewerUi::pwmDeviceLiveECG( uint32_t seq, uint32_t status, QByteArray data )
{
	const int PAYLOAD_SIZE = 54;
	// LOGMSG( "LiveECG - seq: %u, status: %u, data: %s", seq, status, data.toHex().data() );

	int noChannels = 0;
	int noSamples = 0;

	if ( data.length() != PAYLOAD_SIZE ) {
		LOGMSG( "Invalid LiveECG packet size." );
		return;
	}

	noChannels = m_ui->pageECG_MainWidget->channelCount();

	if ( ( noChannels < 1 ) || ( noChannels > 3 ) ) {
		LOGMSG( "Invalid number of channels." );
		return;
	}

	noSamples = PAYLOAD_SIZE / noChannels;

	if ( liveEcgStartmsSinceEpox == 0 ) {
		liveEcgStartmsSinceEpox = QDateTime::currentMSecsSinceEpoch() + 1000;
	}

	// Add missing samples(if any)
	while ( m_ecgSeq != seq ) {
		for ( int c = 0; c < noSamples; c++ ) {
			m_futureSamples.push_back( QFutureLiveECGSample( liveEcgStartmsSinceEpox + ( ( liveEcgSampleCount * SEC_TO_MSEC ) / 128 ), INT_MAX, INT_MAX, INT_MAX ) );
			liveEcgSampleCount++;
		}

		m_ecgSeq = ( m_ecgSeq + 1 ) % 256;
	}

	// Add samples from this packet.
	for ( int sample = 0; sample < noSamples; sample++ ) {
		switch ( noChannels ) {
			case 1:
				m_futureSamples.push_back( QFutureLiveECGSample( liveEcgStartmsSinceEpox + ( ( liveEcgSampleCount * SEC_TO_MSEC ) / 128 ), data.at( ( sample * 1 ) + 0 ), 0, 0 ) );
				liveEcgSampleCount++;
				break;
			case 2:
				m_futureSamples.push_back( QFutureLiveECGSample( liveEcgStartmsSinceEpox + ( ( liveEcgSampleCount * SEC_TO_MSEC ) / 128 ), data.at( ( sample * 2 ) + 0 ), data.at( ( sample * 2 ) + 1 ), 0 ) );
				liveEcgSampleCount++;
				break;
			case 3:
				m_futureSamples.push_back( QFutureLiveECGSample( liveEcgStartmsSinceEpox + ( ( liveEcgSampleCount * SEC_TO_MSEC ) / 128 ), data.at( ( sample * 3 ) + 0 ), data.at( ( sample * 3 ) + 1 ), data.at( ( sample * 3 ) + 2 ) ) );
				liveEcgSampleCount++;
				break;
		}
	}

	m_ecgSeq = ( m_ecgSeq + 1 ) % 256;

	if ( status ) {
        if ( m_configParams.device_mode == PROCTYPE_POST ) {
			m_ui->pageECG_StatusLabel->setText( tr( "Ensure sensor is connected" ) );
		} else {
			m_ui->pageECG_StatusLabel->setText( tr( "Ensure electrodes are connected" ) );
		}
		m_ui->pageECG_StatusLabel->setStyleSheet( "background: red" );
	} else {
        if ( m_configParams.device_mode == PROCTYPE_POST ) {
			m_ui->pageECG_StatusLabel->setText( tr( "Sensor is connected" ) );
		} else {
			m_ui->pageECG_StatusLabel->setText( tr( "All electrodes connected" ) );;
		}

		m_ui->pageECG_StatusLabel->setStyleSheet( "" );
	}

	m_ui->pageECG_NextButton->setEnabled( true );
}

void SironaViewerUi::requiredFields(uint8_t first_name, uint8_t last_name, uint8_t dob, uint8_t physician, uint8_t patient_id, uint8_t middle_name, uint8_t comment, uint8_t technician, uint8_t facility)
{
    if (first_name == FIELD_STATE_REQUIRED) {
		connect( m_ui->patientFirstName_LineEdit, SIGNAL( textChanged( QString ) ), this, SLOT( patientFieldChanged() ) );
		m_configParams.first_name_required = true;
	} else {
		m_configParams.first_name_required = false;
        if (customer->hide_first_name) {
            m_ui->patientFirstName_LineEdit->hide();
            QGridLayout *layout = (QGridLayout*)m_ui->patientName_LineEditFrame->layout();
            layout->removeItem(m_ui->pagePatient_horizontalSpacer_3);
            layout->removeItem(m_ui->pagePatient_horizontalSpacer_4);
            layout->setRowMinimumHeight(0, 0);
            layout->setRowStretch(0, 0);
        }
	}
    if (last_name == FIELD_STATE_REQUIRED) {
		connect( m_ui->patientLastName_LineEdit, SIGNAL( textChanged( QString ) ), this, SLOT( patientFieldChanged() ) );
		m_configParams.last_name_required = true;
	} else {
		m_configParams.last_name_required = false;
        if (customer->hide_last_name) {
            m_ui->patientLastName_LineEdit->hide();
            QGridLayout *layout = (QGridLayout*)m_ui->patientName_LineEditFrame->layout();
            layout->removeItem(m_ui->pagePatient_horizontalSpacer_7);
            layout->removeItem(m_ui->pagePatient_horizontalSpacer_8);
            layout->setRowMinimumHeight(2, 0);
            layout->setRowStretch(2, 0);
        }
    }
    if (dob == FIELD_STATE_REQUIRED) {
		connect( m_ui->patientDOB_dateEdit , SIGNAL( dateChanged( QDate ) ), this, SLOT( patientFieldChanged() ) );
		m_configParams.dob_required = true;
	} else {
		m_configParams.dob_required = false;
        if (customer->hide_dob) {
            m_ui->dob_LabelFrame->hide();
            m_ui->dob_LineEditFrame->hide();
        }
    }
    if (physician == FIELD_STATE_REQUIRED) {
		connect ( m_ui->physicianName_LineEdit, SIGNAL( textChanged( QString ) ), this, SLOT ( patientFieldChanged() ) );
		m_configParams.physician_name_required = true;
	} else {
		m_configParams.physician_name_required = false;
        if (customer->hide_physician) {
            m_ui->physicianName_LabelFrame->hide();
            m_ui->physicianName_LineEditFrame->hide();
        }
    }
    if (patient_id == FIELD_STATE_REQUIRED) {
		connect ( m_ui->patientID_LineEdit, SIGNAL( textChanged( QString ) ), this, SLOT ( patientFieldChanged() ) );
		m_configParams.patient_id_required = true;
	} else {
		m_configParams.patient_id_required = false;
        if (customer->hide_patient_id) {
            m_ui->patientID_LabelFrame->hide();
            m_ui->patientID_LineEditFrame->hide();
        }
    }
    if (middle_name == FIELD_STATE_REQUIRED) {
        connect ( m_ui->patientMiddleInitial_LineEdit, SIGNAL( textChanged( QString ) ), this, SLOT ( patientFieldChanged() ) );
        m_configParams.middle_name_required = true;
    } else {
        m_configParams.middle_name_required = false;
        if (customer->hide_middle_name) {
            m_ui->patientMiddleInitial_LineEdit->hide();
            QGridLayout *layout = (QGridLayout*)m_ui->patientName_LineEditFrame->layout();
            layout->removeItem(m_ui->pagePatient_horizontalSpacer_5);
            layout->removeItem(m_ui->pagePatient_horizontalSpacer_6);
            layout->setRowMinimumHeight(1, 0);
            layout->setRowStretch(1, 0);
        }
    }
	if (comment == FIELD_STATE_REQUIRED) {
		connect ( m_ui->pagePatientMain_CommentTextEdit, SIGNAL( textChanged() ), this, SLOT ( patientFieldChanged() ) );
		m_configParams.comment_required = true;
	} else {
		m_configParams.comment_required = false;
		if (customer->hide_comment) {
			m_ui->comment_LabelFrame->hide();
			m_ui->pagePatientMain_CommentFrame->hide();
		}
	}
    if ((technician == FIELD_STATE_NOT_SUPPORTED) || (customer->hide_technician)) {
        m_ui->technicianName_LabelFrame->hide();
        m_ui->technicianName_LineEditFrame->hide();
        m_configParams.technician_name_required = false;
    } else if (technician == FIELD_STATE_REQUIRED) {
        connect ( m_ui->technicianName_LineEdit, SIGNAL( textChanged( QString ) ), this, SLOT ( patientFieldChanged() ) );
        m_configParams.technician_name_required = true;
    } else {
        m_configParams.technician_name_required = false;
    }
    if ((facility == FIELD_STATE_NOT_SUPPORTED) || (customer->hide_facility)) {
        m_ui->facility_LabelFrame->hide();
        m_ui->facility_LineEditFrame->hide();
        m_configParams.facility_required = false;
    } else if (facility == FIELD_STATE_REQUIRED) {
        connect ( m_ui->facility_LineEdit, SIGNAL( textChanged( QString ) ), this, SLOT ( patientFieldChanged() ) );
        m_configParams.facility_required = true;
    } else {
        m_configParams.facility_required = false;
    }

	patientFieldChanged();
}

void SironaViewerUi::patientFieldChanged()
{
    bool field_req_met = true;

	// LOGMSG( "patientFieldChanged()" );

	if ( m_configParams.first_name_required ) {
        if (m_ui->patientFirstName_LineEdit->text() != "") {
			m_ui->patientFirstName_LineEdit->setStyleSheet( "border: 1px solid " + customer->color_main + "; color: " + customer->color_main + ";"  );
		} else {
			m_ui->patientFirstName_LineEdit->setStyleSheet( "border: 1px solid red; background: #e8ccd1; color: " + customer->color_main + ";"  );
            field_req_met = false;
		}
	}
	if ( m_configParams.last_name_required ) {
        if (m_ui->patientLastName_LineEdit->text() != "") {
			m_ui->patientLastName_LineEdit->setStyleSheet( "border: 1px solid " + customer->color_main + "; color: " + customer->color_main + ";"  );
		} else {
			m_ui->patientLastName_LineEdit->setStyleSheet( "border: 1px solid red; background: #e8ccd1; color: " + customer->color_main + ";" );
            field_req_met = false;
        }
	}
	if ( m_configParams.dob_required ) {
        if (QDate( 1900, 1, 1 ) < m_ui->patientDOB_dateEdit->date()) {
			m_ui->patientDOB_dateEdit->setStyleSheet( "border: 1px solid " + customer->color_main + "; color: " + customer->color_main + ";"  );

		} else {
			m_ui->patientDOB_dateEdit->setStyleSheet( "border: 1px solid red; background: #e8ccd1; color: " + customer->color_main + ";" );
            field_req_met = false;
        }
	}
	if ( m_configParams.physician_name_required ) {
        if (m_ui->physicianName_LineEdit->text() != "") {
			m_ui->physicianName_LineEdit->setStyleSheet( "border: 1px solid " + customer->color_main + "; color: " + customer->color_main + ";"  );
		} else {
			m_ui->physicianName_LineEdit->setStyleSheet( "border: 1px solid red; background: #e8ccd1; color: " + customer->color_main + ";" );
            field_req_met = false;
        }
	}
	if ( m_configParams.patient_id_required ) {
        if (m_ui->patientID_LineEdit->text() != "") {
			m_ui->patientID_LineEdit->setStyleSheet( "border: 1px solid " + customer->color_main + "; color: " + customer->color_main + ";"  );
		} else {
			m_ui->patientID_LineEdit->setStyleSheet( "border: 1px solid red; background: #e8ccd1; color: " + customer->color_main + ";" );
            field_req_met = false;
        }
	}
    if ( m_configParams.middle_name_required ) {
        if (m_ui->patientMiddleInitial_LineEdit->text() != "") {
            m_ui->patientMiddleInitial_LineEdit->setStyleSheet( "border: 1px solid " + customer->color_main + "; color: " + customer->color_main + ";"  );
        } else {
            m_ui->patientMiddleInitial_LineEdit->setStyleSheet( "border: 1px solid red; background: #e8ccd1; color: " + customer->color_main + ";" );
            field_req_met = false;
        }
    }
    if (m_configParams.comment_required) {
        if (m_ui->pagePatientMain_CommentTextEdit->document()->toPlainText() != "") {
            m_ui->pagePatientMain_CommentTextEdit->setStyleSheet( "border: 1px solid " + customer->color_main + "; color: " + customer->color_main + ";"  );
        } else {
            m_ui->pagePatientMain_CommentTextEdit->setStyleSheet( "border: 1px solid red; background: #e8ccd1; color: " + customer->color_main + ";" );
            field_req_met = false;
        }
    }
    if (m_configParams.technician_name_required) {
        if (m_ui->technicianName_LineEdit->text() != "") {
            m_ui->technicianName_LineEdit->setStyleSheet( "border: 1px solid " + customer->color_main + "; color: " + customer->color_main + ";"  );
        } else {
            m_ui->technicianName_LineEdit->setStyleSheet( "border: 1px solid red; background: #e8ccd1; color: " + customer->color_main + ";" );
            field_req_met = false;
        }
    }
    if (m_configParams.facility_required) {
        if (m_ui->facility_LineEdit->text() != "") {
            m_ui->facility_LineEdit->setStyleSheet( "border: 1px solid " + customer->color_main + "; color: " + customer->color_main + ";"  );
        } else {
            m_ui->facility_LineEdit->setStyleSheet( "border: 1px solid red; background: #e8ccd1; color: " + customer->color_main + ";" );
            field_req_met = false;
        }
    }

	/* site code is always required, for ACT */
	if ( glbCustomCode == "ACT1" ) {
		if (m_ui->edtSiteCode->text() != "") {
			m_ui->edtSiteCode->setStyleSheet( "border: 1px solid " + customer->color_main + "; color: " + customer->color_main + ";"  );
		} else {
			m_ui->edtSiteCode->setStyleSheet( "border: 1px solid red; background: #e8ccd1; color: " + customer->color_main + ";" );
		}

		m_ui->cbNoDiaryEventsNoted->setEnabled( m_ui->tableDiaryEntries->rowCount() <= 0 );

		/* only check diary stuff if the procedure is done */
		if ( m_procState == PROCSTATE_DONE ) {
			/* is the diary table not populated AND the "no diary events noted" isn't checked? */
			if ( (m_ui->tableDiaryEntries->rowCount() <= 0) && ! m_ui->cbNoDiaryEventsNoted->isChecked() ) {
				field_req_met = false;
			}
		}
	}


    m_ui->pagePatient_NextButton->setEnabled(field_req_met);
}

void SironaViewerUi::pwmDeviceWarning( QString warning )
{
	QMessageBox::warning( this, "Device warning", warning );
}

void SironaViewerUi::contentStackedWidgetPageChanged( int index )
{
	LOGMSG( "contentStackedWidgetPageChanged" );
	if ( m_bluetoothTimer ) {
		restartBluetoothTimer();
	}

	switch ( index ) {
		case PAGE_CONNECTIONS_SCREEN:
            pageConnections_setupUI();
			break;

		case PAGE_PATIENT_SCREEN:
			pagePatient_setupUI();
			break;

		case PAGE_PROCEDURE_SCREEN:
			pageProcedure_setupUI( true );
			break;

		case PAGE_PROCEDURE_ACTIVE_SCREEN:
			pageProcedureActive_setupUI();
			break;

        case PAGE_DOWNLOAD_OPTIONS_SCREEN:
            pageDownloadOptions_setupUI();
            break;

        case PAGE_ERASE_SCREEN:
            pageErase_setupUI();
            break;

        case PAGE_DEVICE_SCREEN:
			pageDevice_setupUI();
			break;

		case PAGE_WELCOME_SCREEN:
			break;

		case PAGE_CABLE_SCREEN:
			m_exitScreenOperation = QPanelExitOperationNoOperation;
			cable_setupUI();
			break;

		case PAGE_USB_SAVED_SCREEN:
			break;

		case PAGE_HOOKUP_SCREEN:
			pageHookup_setupUI();
			break;

		case PAGE_ECG_SCREEN:
			pageEcg_setupUI();
			break;

		case PAGE_COMPLETE_SCREEN:
            startStatusTimer();
			break;

		default:
			break;
	}
}

void SironaViewerUi::on_pageWelcome_NextButton_clicked()
{
	LOGMSG( "on_pageWelcome_NextButton_clicked" );
	previousPage = PAGE_WELCOME_SCREEN;
LOGMSG( "on_pageWelcome_NextButton_clicked() 113   going to   PAGE_CONNECTIONS_SCREEN ..." );
	m_ui->contentStackedWidget->setCurrentIndex( PAGE_CONNECTIONS_SCREEN );

}

void SironaViewerUi::on_pageConnection_LogoLabel_clicked()
{
	LOGMSG( "on_pageConnection_LogoLabel_clicked" );
	previousPage = PAGE_CONNECTIONS_SCREEN;
LOGMSG( "on_pageConnection_LogoLabel_clicked() 114   going to   PAGE_COMPANY_SCREEN ..." );
	m_ui->contentStackedWidget->setCurrentIndex( PAGE_COMPANY_SCREEN );
}

void SironaViewerUi::on_pageConnection_NextButton_clicked()
{
	QListWidgetItem *currentItem = nullptr;

	LOGMSG( "on_pageConnection_NextButton_clicked" );
	currentItem = m_ui->pageConnection_ChooseDevicelistWidget->currentItem();

	if ( currentItem ) {
		deviceInfo_t device;
		memset( &device, 0, sizeof( device ) );

		strncpy( device.devSerialNumber, currentItem->text().toLatin1().constData(), sizeof( device.devSerialNumber ) );
		memcpy( device.devPortName, currentItem->data( Qt::UserRole ).toByteArray().constData(), currentItem->data( Qt::UserRole ).toByteArray().length() );
		device.devType = currentItem->data( Qt::UserRole + 1 ).toInt();

		deviceConnectionStatus = DEVICE_CONNECTING;

		emit connectPwmDevice( device );

		m_ui->pageConnection_NextButton->setEnabled( false );

		QApplication::setOverrideCursor( Qt::WaitCursor );
	}
}

void SironaViewerUi::on_pageCable_LogoLabel_clicked()
{
	LOGMSG( "on_pageCable_LogoLabel_clicked" );
    previousPage = PAGE_CABLE_SCREEN;
LOGMSG( "on_pageCable_LogoLabel_clicked() 115   going to   PAGE_COMPANY_SCREEN ..." );
    m_ui->contentStackedWidget->setCurrentIndex( PAGE_COMPANY_SCREEN );
}

void SironaViewerUi::on_pageCable_GearButton_clicked()
{
	LOGMSG( "on_pageCable_GearButton_clicked" );
    previousPage = PAGE_CABLE_SCREEN;
LOGMSG( "on_pageCable_GearButton_clicked() 116   going to   PAGE_DEVICE_SCREEN ..." );
    m_ui->contentStackedWidget->setCurrentIndex( PAGE_DEVICE_SCREEN );
}

void SironaViewerUi::on_pageCable_Button_clicked( quint8 devMode )
{
    ConfigParams confParams = m_configParams;
    confParams.device_mode = devMode;

    deviceCommunicationStatus = DEVICE_COMMIT_PARAMETERS;

    QList<QString> paramNamesList;
    paramNamesList.append( "DEV_MODE" );
    deviceCommunicationStatus = DEVICE_WRITE_LIST_OF_PARAMETERS;
    emit parameterListWrite( confParams, paramNamesList );

    m_ui->pagePatient_BackButton->setVisible( true );

    m_exitScreenOperation = QPanelExitOperationNextScreen;
    previousPage = PAGE_CABLE_SCREEN;
    futurePage = PAGE_PATIENT_SCREEN;
    emit parameterRead( "PROC_STATE" );
    emit parameterReadAll();
LOGMSG( "on_pageCable_Button_clicked() 117   going to   futurePage = %d ...", futurePage );
    m_ui->contentStackedWidget->setCurrentIndex(futurePage);
}

void SironaViewerUi::on_pageCable_HolterButton_clicked()
{
	LOGMSG( "on_pageCable_HolterButton_clicked" );
    on_pageCable_Button_clicked(PROCTYPE_HOLTER);
}

void SironaViewerUi::on_pageCable_EventButton_clicked()
{
	LOGMSG( "on_pageCable_EventButton_clicked" );
    on_pageCable_Button_clicked(PROCTYPE_EVENT);
}

void SironaViewerUi::on_pageCable_PostButton_clicked()
{
	LOGMSG( "on_pageCable_PostButton_clicked" );
    on_pageCable_Button_clicked(PROCTYPE_POST);
}

void SironaViewerUi::on_pageCable_MCTButton_clicked()
{
	LOGMSG( "on_pageCable_MCTButton_clicked" );
    on_pageCable_Button_clicked(PROCTYPE_MCT);
}

void SironaViewerUi::on_pagePatient_LogoLabel_clicked()
{
	LOGMSG( "on_pagePatient_LogoLabel_clicked" );
	previousPage = PAGE_PATIENT_SCREEN;
	futurePage = PAGE_COMPANY_SCREEN;
	savePatientInfo();
}

void SironaViewerUi::on_pagePatient_GearButton_clicked()
{
	LOGMSG( "on_pagePatient_GearButton_clicked" );
	previousPage = PAGE_PATIENT_SCREEN;
	futurePage = PAGE_DEVICE_SCREEN;
	savePatientInfo();
}

void SironaViewerUi::on_pagePatient_BackButton_clicked()
{
	LOGMSG( "on_pagePatient_BackButton_clicked" );
    if (downloadOptionsActive) {        // if the patient screen was shown before downloading
        downloadOptionsActive = false;  // display Download Options again
        futurePage = PAGE_INITIAL_VALUE;
        previousPage = PAGE_PATIENT_SCREEN;
        savePatientInfo();
        deviceCommunicationStatus = DEVICE_IS_THERE_RECORDED_DATA;
        emit isThereRecordedData();
    } else {
        futurePage = previousPage;
        previousPage = PAGE_PATIENT_SCREEN;
        savePatientInfo();
    }
}

void SironaViewerUi::on_pagePatient_NextButton_clicked()
{
	LOGMSG( "on_pagePatient_NextButton_clicked" );
    previousPage = PAGE_PATIENT_SCREEN;
    futurePage = PAGE_INITIAL_VALUE;
    savePatientInfo();
    if (downloadOptionsActive) {        // if the patient screen was shown before downloading
        deviceCommunicationStatus = DEVICE_IS_THERE_RECORDED_DATA;
        emit isThereRecordedData();
    } else {
LOGMSG( "on_pagePatient_NextButton_clicked() 118   going to   PAGE_PROCEDURE_SCREEN..." );
        m_ui->contentStackedWidget->setCurrentIndex(PAGE_PROCEDURE_SCREEN);
    }
}

void SironaViewerUi::savePatientInfo()
{
	ConfigParams confParams = m_configParams;
	QDateTime epoch( QDate( 1900, 1, 1 ), QTime ( 0, 0, 0 ) );

	confParams.patient_id = m_ui->patientID_LineEdit->text();
	confParams.patient_first_name =  m_ui->patientFirstName_LineEdit->text();
	confParams.patient_middle_initial = m_ui->patientMiddleInitial_LineEdit->text();
	confParams.patient_last_name = m_ui->patientLastName_LineEdit->text();
	confParams.patient_date_of_birth = ( epoch.daysTo( m_ui->patientDOB_dateEdit->dateTime() ) );
	confParams.physician_name = m_ui->physicianName_LineEdit->text();
	confParams.comment = m_ui->pagePatientMain_CommentTextEdit->toPlainText();
    confParams.technician_name = m_ui->technicianName_LineEdit->text();
    confParams.facility = m_ui->facility_LineEdit->text();
    confParams.diagnosis = m_ui->edtDiagnosis->text();
    confParams.medication_notes = m_ui->txtEditMedicationNotes->toPlainText();
    confParams.diary_entries = getDiaryEntries();

LOGMSG( "savePatientInfo() 119   going to   futurePage = %d ...", futurePage );
    m_ui->contentStackedWidget->setCurrentIndex( futurePage );
    m_ui->pageProcedure_NextButton->setEnabled(false);

	QList<QString> paramNamesList;
	paramNamesList.append( "PAT_ID" );
	paramNamesList.append( "PAT_NAME" );
	paramNamesList.append( "PAT_MIDDLE" );
	paramNamesList.append( "PAT_LAST" );
	paramNamesList.append( "PAT_DOB" );
	paramNamesList.append( "PHY_NAME" );
	paramNamesList.append( "COMMENT" );
    paramNamesList.append( "TECH_NAME" );
    paramNamesList.append( "FACILITY" );
    paramNamesList.append( "MEDICATION_NOTES" );
    paramNamesList.append( "DIAGNOSIS" );
    paramNamesList.append( "DIARY_ENTRIES" );
    deviceCommunicationStatus = DEVICE_WRITE_LIST_OF_PARAMETERS;
	emit parameterListWrite( confParams, paramNamesList );
}

#define PATIENT_COMMENT_MAX_BYTES (500-1)   // 1 allows for a terminating NULL

void SironaViewerUi::on_pagePatientMain_CommentTextEdit_textChanged()
{
	LOGMSG( "on_pagePatientMain_CommentTextEdit_textChanged" );
	static QString prevStr;
	static bool revertText = false;

    int lengthBytes = m_ui->pagePatientMain_CommentTextEdit->document()->toPlainText().toUtf8().length();

    if (lengthBytes <= PATIENT_COMMENT_MAX_BYTES) {
		prevStr = m_ui->pagePatientMain_CommentTextEdit->toPlainText();
		if ( revertText ) {
			QTextCursor cursor = m_ui->pagePatientMain_CommentTextEdit->textCursor();
			cursor.movePosition( QTextCursor::End );
			m_ui->pagePatientMain_CommentTextEdit->setTextCursor( cursor );
			revertText = false;
		}
	} else {
		revertText = true;
		m_ui->pagePatientMain_CommentTextEdit->setPlainText( prevStr );
	}

    patientFieldChanged();
}

void SironaViewerUi::cable_setupUI()
{
	LOGMSG("cable_setupUI()   single_device_mode = %d", customer->single_device_mode );

#ifdef ANTIQUE
	if ( deviceConnectionStatus == USB_DEVICE_CONNECTED ) {
		/* start by default with no cable connected */
		QList<QString> paramNamesList;
		paramNamesList.append( "CABLE_ID" );
		m_configParams.cable_id = CABLETYPE_UNKNOWN;
		deviceCommunicationStatus = DEVICE_WRITE_LIST_OF_PARAMETERS;
		emit parameterListWrite( m_configParams, paramNamesList );
	}
#endif

	switch ( customer->single_device_mode ) {
        case PROCTYPE_HOLTER:
			on_pageCable_HolterButton_clicked();
			m_ui->pagePatient_BackButton->setVisible( false );
			return;
        case PROCTYPE_EVENT:
			on_pageCable_EventButton_clicked();
			m_ui->pagePatient_BackButton->setVisible( false );
			return;
        case PROCTYPE_POST:
			on_pageCable_PostButton_clicked();
			m_ui->pagePatient_BackButton->setVisible( false );
			return;
        case PROCTYPE_MCT:
			on_pageCable_MCTButton_clicked();
			m_ui->pagePatient_BackButton->setVisible( false );
			return;
	}

    if ( m_cableId == CABLETYPE_I_CHAN_POST ) {
		on_pageCable_PostButton_clicked();
		m_ui->pagePatient_BackButton->setVisible( false );
		return;
	}
	if ( deviceConnectionStatus == USB_DEVICE_CONNECTED ) {
		m_ui->pageCable_MainLabel->setText( tr( "Select mode" ) );
		m_ui->pageCable_HolterFrame->setVisible( true );
		m_ui->pageCable_EventFrame->setVisible( true );
		m_ui->pageCable_PostFrame->setVisible( true );
		if ( m_configParams.mct_enabled ) {
		m_ui->pageCable_MCTFrame->setVisible( true );
		} else {
			m_ui->pageCable_MCTFrame->setVisible( false );
		}

    } else if ( deviceConnectionStatus == BT_DEVICE_CONNECTED && m_cableId == CABLETYPE_UNKNOWN ) {
		m_ui->pageCable_MainLabel->setText( tr( "Insert cable." ) );
		m_ui->pageCable_HolterFrame->setVisible( false );
		m_ui->pageCable_EventFrame->setVisible( false );
		m_ui->pageCable_PostFrame->setVisible( false );
		m_ui->pageCable_MCTFrame->setVisible( false );
        startStatusTimer();
	} else {
        switch ( m_configParams.operation_mode ) {
			case NO_CABLE:
				//wait for parameter read to finish
				break;

			case CHANNEL_1_EVENT:
			case CHANNEL_2_EVENT:
				m_ui->pageCable_MainLabel->setText( tr( "Select mode" ) );
				m_ui->pageCable_HolterFrame->setVisible( false );
				m_ui->pageCable_EventFrame->setVisible( true );
				m_ui->pageCable_PostFrame->setVisible( false );
				if ( m_configParams.mct_enabled ) {
				m_ui->pageCable_MCTFrame->setVisible( true );
				} else {
					m_ui->pageCable_MCTFrame->setVisible( false );
				}
				break;

			case CHANNEL_1_HOLTER:
			case CHANNEL_2_HOLTER:
				m_ui->pageCable_MainLabel->setText( tr( "Select mode" ) );
				m_ui->pageCable_HolterFrame->setVisible( true );
				m_ui->pageCable_EventFrame->setVisible( false );
				m_ui->pageCable_PostFrame->setVisible( false );
				if ( m_configParams.mct_enabled ) {
				m_ui->pageCable_MCTFrame->setVisible( true );
				} else {
					m_ui->pageCable_MCTFrame->setVisible( false );
				}
				break;

			case CHANNEL_2_PATCH:
				m_ui->pageCable_MainLabel->setText( tr( "Select mode" ) );
				m_ui->pageCable_HolterFrame->setVisible( true );
				m_ui->pageCable_EventFrame->setVisible( true );
				m_ui->pageCable_PostFrame->setVisible( false );
				if ( m_configParams.mct_enabled ) {
				m_ui->pageCable_MCTFrame->setVisible( true );
				} else {
					m_ui->pageCable_MCTFrame->setVisible( false );
				}
				break;

			case CHANNEL_3_EVENT:
				on_pageCable_EventButton_clicked();
				m_ui->pagePatient_BackButton->setVisible( false );
				break;

			case CHANNEL_3_HOLTER:
				on_pageCable_HolterButton_clicked();
				m_ui->pagePatient_BackButton->setVisible( false );
				break;


		}
	}
}

void SironaViewerUi::pageConnections_setupUI()
{
    LOGMSG( "pageConnections_setupUI" );
    downloadOptionsActive = false;
    startEnumerateTimer();
    m_ui->pageConnection_NextButton->setEnabled( false );
    m_ui->pageConnection_ChooseDevicelistWidget->clear();
    enumPwmDevices();
}

void SironaViewerUi::pagePatient_setupUI()
{
	LOGMSG( "pagePatient_setupUI" );
	QDate epoch( 1900, 1, 1 );

    if (customer->review_patient_before_download) {
        // since this screen is shown in two different contexts, be more explicit with the title
        if (downloadOptionsActive) {
            m_ui->pagePatient_TitleLabel->setText(tr("Downloading"));
        } else {
            m_ui->pagePatient_TitleLabel->setText(tr("New Patient"));
        }
    } // else use the default title since the screen is only shown in one situation
	m_ui->patientID_LineEdit->setText( m_configParams.patient_id );
	m_ui->patientFirstName_LineEdit->setText( m_configParams.patient_first_name );
	m_ui->patientMiddleInitial_LineEdit->setText( m_configParams.patient_middle_initial );
	m_ui->patientLastName_LineEdit->setText( m_configParams.patient_last_name );
	m_ui->patientDOB_dateEdit->setDate( epoch.addDays( m_configParams.patient_date_of_birth ) );
	m_ui->physicianName_LineEdit->setText( m_configParams.physician_name );
	m_ui->pagePatientMain_CommentTextEdit->setPlainText( m_configParams.comment );
    m_ui->technicianName_LineEdit->setText( m_configParams.technician_name );
    m_ui->facility_LineEdit->setText( m_configParams.facility );
    if ( customer->indications ) {
		m_ui->comment_Label->setText( tr( "Indications:" ) );
	}
	/* clear any diary entries in table for new patient */
	while ( m_ui->tableDiaryEntries->rowCount() > 0 ) {
		m_ui->tableDiaryEntries->removeRow( 0 );
	}

    // in manufacturing, make sure every field has text in case the firmware requires them to be filled out
    if (customer->manufacturing) {
        if (m_configParams.patient_id.length() == 0) {
            m_ui->patientID_LineEdit->setText( "TestID" );
        }
        if (m_configParams.patient_first_name.length() == 0) {
            m_ui->patientFirstName_LineEdit->setText( "TestFirst" );
        }
        if (m_configParams.patient_middle_initial.length() == 0) {
            m_ui->patientMiddleInitial_LineEdit->setText( "T" );
        }
        if (m_configParams.patient_last_name.length() == 0) {
            m_ui->patientLastName_LineEdit->setText( "TestLast" );
        }
        if (m_configParams.patient_date_of_birth == 0) {
            m_ui->patientDOB_dateEdit->setDate( epoch.addDays( 366 ) );
        }
        if (m_configParams.physician_name.length() == 0) {
            m_ui->physicianName_LineEdit->setText( "TestPhysician" );
        }
        if (m_configParams.comment.length() == 0) {
            m_ui->pagePatientMain_CommentTextEdit->setPlainText( "TestComment" );
        }
    }

	emit getRequiredFields();
    startStatusTimer();
}

void SironaViewerUi::on_pageProcedure_LogoLabel_clicked()
{
	LOGMSG( "on_pageProcedure_LogoLabel_clicked" );
	previousPage = PAGE_PROCEDURE_SCREEN;
	futurePage = PAGE_COMPANY_SCREEN;
	saveProcedureInfo();
LOGMSG( "on_pageProcedure_LogoLabel_clicked() 120   going to   PAGE_COMPANY_SCREEN..." );
    m_ui->contentStackedWidget->setCurrentIndex(PAGE_COMPANY_SCREEN);
}

void SironaViewerUi::on_pageProcedureMain_AdvancedOptionsLabel_clicked()
{
	LOGMSG( "on_pageProcedureMain_AdvancedOptionsLabel_clicked" );
	procedurePageAdveancedOptionsEnabled = !procedurePageAdveancedOptionsEnabled;
	pageProcedure_setupUI( false );
}

void SironaViewerUi::on_pageProcedure_GearButton_clicked()
{
	LOGMSG( "on_pageProcedure_GearButton_clicked" );
	previousPage = PAGE_PROCEDURE_SCREEN;
	futurePage = PAGE_DEVICE_SCREEN;
	saveProcedureInfo();
LOGMSG( "on_pageProcedure_GearButton_clicked() 121   going to   PAGE_DEVICE_SCREEN..." );
    m_ui->contentStackedWidget->setCurrentIndex(PAGE_DEVICE_SCREEN);
}

void SironaViewerUi::on_pageProcedure_BackButton_clicked()
{
	LOGMSG( "on_pageProcedure_BackButton_clicked" );
	previousPage = PAGE_PROCEDURE_SCREEN;
	futurePage = PAGE_PATIENT_SCREEN;
	saveProcedureInfo();
LOGMSG( "on_pageProcedure_BackButton_clicked() 122   going to   PAGE_PATIENT_SCREEN..." );
    m_ui->contentStackedWidget->setCurrentIndex(PAGE_PATIENT_SCREEN);
}

void SironaViewerUi::on_pageProcedure_NextButton_clicked()
{
	LOGMSG( "on_pageProcedure_NextButton_clicked" );
	saveProcedureInfo();
	/* Go to the next screen */
	previousPage = PAGE_PROCEDURE_SCREEN;
	if ( deviceConnectionStatus == USB_DEVICE_CONNECTED ) {
LOGMSG( "on_pageProcedure_NextButton_clicked() 123   going to   PAGE_USB_SAVED_SCREEN ..." );
		m_ui->contentStackedWidget->setCurrentIndex( PAGE_USB_SAVED_SCREEN );
	} else if ( deviceConnectionStatus == BT_DEVICE_CONNECTED ) {
LOGMSG( "on_pageProcedure_NextButton_clicked() 124   going to   PAGE_HOOKUP_SCREEN ..." );
		m_ui->contentStackedWidget->setCurrentIndex( PAGE_HOOKUP_SCREEN );
	}
}

void SironaViewerUi::saveProcedureInfo()
{
	/*
	 * Populate the config struct entries from the Procedure page entries
	 */
	ConfigParams confParams = m_configParams;
    if ( m_configParams.device_mode == PROCTYPE_EVENT ) {

		//Pre-trigger
		if ( m_ui->pageProcedureMain_PretriggerComboBox->currentIndex() == 0 ) {
			confParams.pre_trigger = ( qint16 ) PRE_TRIGGER_TIME_15_SEC;
		} else if ( m_ui->pageProcedureMain_PretriggerComboBox->currentIndex() == 1 ) {
			confParams.pre_trigger = ( qint16 ) PRE_TRIGGER_TIME_30_SEC;
		} else if ( m_ui->pageProcedureMain_PretriggerComboBox->currentIndex() == 2 ) {
			confParams.pre_trigger = ( qint16 ) PRE_TRIGGER_TIME_45_SEC;
		} else if ( m_ui->pageProcedureMain_PretriggerComboBox->currentIndex() == 3 ) {
			confParams.pre_trigger = ( qint16 ) PRE_TRIGGER_TIME_60_SEC;
		} else if ( m_ui->pageProcedureMain_PretriggerComboBox->currentIndex() == 4 ) {
			confParams.pre_trigger = ( qint16 ) PRE_TRIGGER_TIME_90_SEC;
		} else if ( m_ui->pageProcedureMain_PretriggerComboBox->currentIndex() == 5 ) {
			confParams.pre_trigger = ( qint16 ) PRE_TRIGGER_TIME_120_SEC;
		} else if ( m_ui->pageProcedureMain_PretriggerComboBox->currentIndex() == 6 ) {
			confParams.pre_trigger = ( qint16 ) PRE_TRIGGER_TIME_180_SEC;
		} else if ( m_ui->pageProcedureMain_PretriggerComboBox->currentIndex() == 7 ) {
			confParams.pre_trigger = ( qint16 ) PRE_TRIGGER_TIME_300_SEC;
		} else {
			confParams.pre_trigger = ( qint16 ) DEFAULT_PRE_TRIGGER_TIME;
			LOGMSG( "Invalid device configuration - pre_trigger" );
		}

		//Automatic event limit
		if ( m_ui->pageProcedureMain_AutoEvenetLimitComboBox->currentIndex() == 0 ) {
            confParams.automatic_event_limit = ( qint16 ) EVENT_LIMIT_0;
		} else if ( m_ui->pageProcedureMain_AutoEvenetLimitComboBox->currentIndex() == 1 ) {
            confParams.automatic_event_limit = ( qint16 ) EVENT_LIMIT_1;
		} else if ( m_ui->pageProcedureMain_AutoEvenetLimitComboBox->currentIndex() == 2 ) {
            confParams.automatic_event_limit = ( qint16 ) EVENT_LIMIT_2;
		} else if ( m_ui->pageProcedureMain_AutoEvenetLimitComboBox->currentIndex() == 3 ) {
            confParams.automatic_event_limit = ( qint16 ) EVENT_LIMIT_3;
		} else if ( m_ui->pageProcedureMain_AutoEvenetLimitComboBox->currentIndex() == 4 ) {
            confParams.automatic_event_limit = ( qint16 ) EVENT_LIMIT_4;
		} else if ( m_ui->pageProcedureMain_AutoEvenetLimitComboBox->currentIndex() == 5 ) {
            confParams.automatic_event_limit = ( qint16 ) EVENT_LIMIT_5;
		} else if ( m_ui->pageProcedureMain_AutoEvenetLimitComboBox->currentIndex() == 6 ) {
            confParams.automatic_event_limit = ( qint16 ) EVENT_LIMIT_6;
		} else if ( m_ui->pageProcedureMain_AutoEvenetLimitComboBox->currentIndex() == 7 ) {
            confParams.automatic_event_limit = ( qint16 ) EVENT_LIMIT_7;
		} else if ( m_ui->pageProcedureMain_AutoEvenetLimitComboBox->currentIndex() == 8 ) {
            confParams.automatic_event_limit = ( qint16 ) EVENT_LIMIT_8;
		} else if ( m_ui->pageProcedureMain_AutoEvenetLimitComboBox->currentIndex() == 9 ) {
            confParams.automatic_event_limit = ( qint16 ) EVENT_LIMIT_9;
		} else if ( m_ui->pageProcedureMain_AutoEvenetLimitComboBox->currentIndex() == 10 ) {
            confParams.automatic_event_limit = ( qint16 ) EVENT_LIMIT_10;
		} else if ( m_ui->pageProcedureMain_AutoEvenetLimitComboBox->currentIndex() == 11 ) {
            confParams.automatic_event_limit = ( qint16 ) EVENT_LIMIT_11;
		} else if ( m_ui->pageProcedureMain_AutoEvenetLimitComboBox->currentIndex() == 12 ) {
            confParams.automatic_event_limit = ( qint16 ) EVENT_LIMIT_12;
		} else if ( m_ui->pageProcedureMain_AutoEvenetLimitComboBox->currentIndex() == 13 ) {
            confParams.automatic_event_limit = ( qint16 ) EVENT_LIMIT_13;
		} else if ( m_ui->pageProcedureMain_AutoEvenetLimitComboBox->currentIndex() == 14 ) {
            confParams.automatic_event_limit = ( qint16 ) EVENT_LIMIT_14;
		} else if ( m_ui->pageProcedureMain_AutoEvenetLimitComboBox->currentIndex() == 15 ) {
            confParams.automatic_event_limit = ( qint16 ) EVENT_LIMIT_15;
		} else if ( m_ui->pageProcedureMain_AutoEvenetLimitComboBox->currentIndex() == 16 ) {
            confParams.automatic_event_limit = ( qint16 ) EVENT_LIMIT_16;
		} else if ( m_ui->pageProcedureMain_AutoEvenetLimitComboBox->currentIndex() == 17 ) {
            confParams.automatic_event_limit = ( qint16 ) EVENT_LIMIT_17;
		} else if ( m_ui->pageProcedureMain_AutoEvenetLimitComboBox->currentIndex() == 18 ) {
            confParams.automatic_event_limit = ( qint16 ) EVENT_LIMIT_18;
		} else if ( m_ui->pageProcedureMain_AutoEvenetLimitComboBox->currentIndex() == 19 ) {
            confParams.automatic_event_limit = ( qint16 ) EVENT_LIMIT_19;
		} else if ( m_ui->pageProcedureMain_AutoEvenetLimitComboBox->currentIndex() == 20 ) {
            confParams.automatic_event_limit = ( qint16 ) EVENT_LIMIT_20;
		} else if ( m_ui->pageProcedureMain_AutoEvenetLimitComboBox->currentIndex() == 21 ) {
            confParams.automatic_event_limit = ( qint16 ) EVENT_LIMIT_50;
        } else if ( m_ui->pageProcedureMain_AutoEvenetLimitComboBox->currentIndex() == 22 ) {
            confParams.automatic_event_limit = ( qint16 ) EVENT_LIMIT_100;
        } else if ( m_ui->pageProcedureMain_AutoEvenetLimitComboBox->currentIndex() == 23 ) {
			confParams.automatic_event_limit = ( qint16 ) EVENT_LIMIT_1000;
        } else {
			confParams.automatic_event_limit = ( qint16 ) DEFAULT_AUTOMATIC_EVENT_LIMIT;
			LOGMSG( "Invalid device configuration - automatic_event_limit" );
		}

		//Tachy
		confParams.analyzer_tachy_on = ( qint8 ) 1;
		if ( m_ui->pageProcedureMain_TachyComboBox->currentIndex() == 0 ) {
			confParams.analyzer_tachy_on = ( qint8 ) 0;
		} else if ( m_ui->pageProcedureMain_TachyComboBox->currentIndex() == 1 ) {
			confParams.analyzer_tachy_threshold = ( qint16 ) TACHY_THRESHOLD_120;
		} else if ( m_ui->pageProcedureMain_TachyComboBox->currentIndex() == 2 ) {
			confParams.analyzer_tachy_threshold = ( qint16 ) TACHY_THRESHOLD_125;
		} else if ( m_ui->pageProcedureMain_TachyComboBox->currentIndex() == 3 ) {
			confParams.analyzer_tachy_threshold = ( qint16 ) TACHY_THRESHOLD_130;
		} else if ( m_ui->pageProcedureMain_TachyComboBox->currentIndex() == 4 ) {
			confParams.analyzer_tachy_threshold = ( qint16 ) TACHY_THRESHOLD_135;
		} else if ( m_ui->pageProcedureMain_TachyComboBox->currentIndex() == 5 ) {
			confParams.analyzer_tachy_threshold = ( qint16 ) TACHY_THRESHOLD_140;
		} else if ( m_ui->pageProcedureMain_TachyComboBox->currentIndex() == 6 ) {
			confParams.analyzer_tachy_threshold = ( qint16 ) TACHY_THRESHOLD_145;
		} else if ( m_ui->pageProcedureMain_TachyComboBox->currentIndex() == 7 ) {
			confParams.analyzer_tachy_threshold = ( qint16 ) TACHY_THRESHOLD_150;
		} else if ( m_ui->pageProcedureMain_TachyComboBox->currentIndex() == 8 ) {
			confParams.analyzer_tachy_threshold = ( qint16 ) TACHY_THRESHOLD_155;
		} else if ( m_ui->pageProcedureMain_TachyComboBox->currentIndex() == 9 ) {
			confParams.analyzer_tachy_threshold = ( qint16 ) TACHY_THRESHOLD_160;
		} else if ( m_ui->pageProcedureMain_TachyComboBox->currentIndex() == 10 ) {
			confParams.analyzer_tachy_threshold = ( qint16 ) TACHY_THRESHOLD_165;
		} else if ( m_ui->pageProcedureMain_TachyComboBox->currentIndex() == 11 ) {
			confParams.analyzer_tachy_threshold = ( qint16 ) TACHY_THRESHOLD_170;
		} else if ( m_ui->pageProcedureMain_TachyComboBox->currentIndex() == 12 ) {
			confParams.analyzer_tachy_threshold = ( qint16 ) TACHY_THRESHOLD_175;
		} else if ( m_ui->pageProcedureMain_TachyComboBox->currentIndex() == 13 ) {
			confParams.analyzer_tachy_threshold = ( qint16 ) TACHY_THRESHOLD_180;
		} else if ( m_ui->pageProcedureMain_TachyComboBox->currentIndex() == 14 ) {
			confParams.analyzer_tachy_threshold = ( qint16 ) TACHY_THRESHOLD_185;
		} else if ( m_ui->pageProcedureMain_TachyComboBox->currentIndex() == 15 ) {
			confParams.analyzer_tachy_threshold = ( qint16 ) TACHY_THRESHOLD_190;
		} else if ( m_ui->pageProcedureMain_TachyComboBox->currentIndex() == 16 ) {
			confParams.analyzer_tachy_threshold = ( qint16 ) TACHY_THRESHOLD_195;
		} else if ( m_ui->pageProcedureMain_TachyComboBox->currentIndex() == 17 ) {
			confParams.analyzer_tachy_threshold = ( qint16 ) TACHY_THRESHOLD_200;
		} else if ( m_ui->pageProcedureMain_TachyComboBox->currentIndex() == 18 ) {
			confParams.analyzer_tachy_threshold = ( qint16 ) TACHY_THRESHOLD_205;
		} else if ( m_ui->pageProcedureMain_TachyComboBox->currentIndex() == 19 ) {
			confParams.analyzer_tachy_threshold = ( qint16 ) TACHY_THRESHOLD_210;
		} else if ( m_ui->pageProcedureMain_TachyComboBox->currentIndex() == 20 ) {
			confParams.analyzer_tachy_threshold = ( qint16 ) TACHY_THRESHOLD_215;
		} else if ( m_ui->pageProcedureMain_TachyComboBox->currentIndex() == 21 ) {
			confParams.analyzer_tachy_threshold = ( qint16 ) TACHY_THRESHOLD_220;
		} else if ( m_ui->pageProcedureMain_TachyComboBox->currentIndex() == 22 ) {
			confParams.analyzer_tachy_threshold = ( qint16 ) TACHY_THRESHOLD_225;
		} else if ( m_ui->pageProcedureMain_TachyComboBox->currentIndex() == 23 ) {
			confParams.analyzer_tachy_threshold = ( qint16 ) TACHY_THRESHOLD_230;
		} else if ( m_ui->pageProcedureMain_TachyComboBox->currentIndex() == 24 ) {
			confParams.analyzer_tachy_threshold = ( qint16 ) TACHY_THRESHOLD_235;
		} else if ( m_ui->pageProcedureMain_TachyComboBox->currentIndex() == 25 ) {
			confParams.analyzer_tachy_threshold = ( qint16 ) TACHY_THRESHOLD_240;
		} else {
			confParams.analyzer_tachy_threshold = ( qint16 ) DEFAULT_TACHY_THRESHOLD;
			LOGMSG( "Invalid device configuration - analyzer_tachy_threshold" );
		}

		//Brady
		confParams.analyzer_brady_on = ( qint8 ) 1;
		if ( m_ui->pageProcedureMain_BradyComboBox->currentIndex() == 0 ) {
			confParams.analyzer_brady_on = ( qint8 ) 0;
		} else if ( m_ui->pageProcedureMain_BradyComboBox->currentIndex() == 1 ) {
			confParams.analyzer_brady_threshold = ( qint16 ) BRADY_THRESHOLD_15;
		} else if ( m_ui->pageProcedureMain_BradyComboBox->currentIndex() == 2 ) {
			confParams.analyzer_brady_threshold = ( qint16 ) BRADY_THRESHOLD_20;
		} else if ( m_ui->pageProcedureMain_BradyComboBox->currentIndex() == 3 ) {
			confParams.analyzer_brady_threshold = ( qint16 ) BRADY_THRESHOLD_25;
		} else if ( m_ui->pageProcedureMain_BradyComboBox->currentIndex() == 4 ) {
			confParams.analyzer_brady_threshold = ( qint16 ) BRADY_THRESHOLD_30;
		} else if ( m_ui->pageProcedureMain_BradyComboBox->currentIndex() == 5 ) {
			confParams.analyzer_brady_threshold = ( qint16 ) BRADY_THRESHOLD_35;
		} else if ( m_ui->pageProcedureMain_BradyComboBox->currentIndex() == 6 ) {
			confParams.analyzer_brady_threshold = ( qint16 ) BRADY_THRESHOLD_40;
		} else if ( m_ui->pageProcedureMain_BradyComboBox->currentIndex() == 7 ) {
			confParams.analyzer_brady_threshold = ( qint16 ) BRADY_THRESHOLD_45;
		} else if ( m_ui->pageProcedureMain_BradyComboBox->currentIndex() == 8 ) {
			confParams.analyzer_brady_threshold = ( qint16 ) BRADY_THRESHOLD_50;
		} else if ( m_ui->pageProcedureMain_BradyComboBox->currentIndex() == 9 ) {
			confParams.analyzer_brady_threshold = ( qint16 ) BRADY_THRESHOLD_55;
		} else if ( m_ui->pageProcedureMain_BradyComboBox->currentIndex() == 10 ) {
			confParams.analyzer_brady_threshold = ( qint16 ) BRADY_THRESHOLD_60;
		} else {
			confParams.analyzer_brady_threshold = ( qint16 ) DEFAULT_BRADY_THRESHOLD;
			LOGMSG( "Invalid device configuration - analyzer_brady_threshold" );
		}

		//Pause
		confParams.analyzer_pause_on = ( qint8 ) 1;
		if ( m_ui->pageProcedureMain_PauseComboBox->currentIndex() == 0 ) {
			confParams.analyzer_pause_on = ( qint8 ) 0;
		} else if ( m_ui->pageProcedureMain_PauseComboBox->currentIndex() == 1 ) {
			confParams.analyzer_pause_threshold = ( qint16 ) PAUSE_THRESHOLD_1_5;
		} else if ( m_ui->pageProcedureMain_PauseComboBox->currentIndex() == 2 ) {
			confParams.analyzer_pause_threshold = ( qint16 ) PAUSE_THRESHOLD_2_0;
		} else if ( m_ui->pageProcedureMain_PauseComboBox->currentIndex() == 3 ) {
			confParams.analyzer_pause_threshold = ( qint16 ) PAUSE_THRESHOLD_2_5;
		} else if ( m_ui->pageProcedureMain_PauseComboBox->currentIndex() == 4 ) {
			confParams.analyzer_pause_threshold = ( qint16 ) PAUSE_THRESHOLD_3_0;
		} else if ( m_ui->pageProcedureMain_PauseComboBox->currentIndex() == 5 ) {
			confParams.analyzer_pause_threshold = ( qint16 ) PAUSE_THRESHOLD_3_5;
		} else if ( m_ui->pageProcedureMain_PauseComboBox->currentIndex() == 6 ) {
			confParams.analyzer_pause_threshold = ( qint16 ) PAUSE_THRESHOLD_4_0;
		} else if ( m_ui->pageProcedureMain_PauseComboBox->currentIndex() == 7 ) {
			confParams.analyzer_pause_threshold = ( qint16 ) PAUSE_THRESHOLD_4_5;
		} else if ( m_ui->pageProcedureMain_PauseComboBox->currentIndex() == 8 ) {
			confParams.analyzer_pause_threshold = ( qint16 ) PAUSE_THRESHOLD_5_0;
		} else if ( m_ui->pageProcedureMain_PauseComboBox->currentIndex() == 9 ) {
			confParams.analyzer_pause_threshold = ( qint16 ) PAUSE_THRESHOLD_5_5;
		} else if ( m_ui->pageProcedureMain_PauseComboBox->currentIndex() == 10 ) {
			confParams.analyzer_pause_threshold = ( qint16 ) PAUSE_THRESHOLD_6_0;
		} else if ( m_ui->pageProcedureMain_PauseComboBox->currentIndex() == 11 ) {
			confParams.analyzer_pause_threshold = ( qint16 ) PAUSE_THRESHOLD_6_5;
		} else if ( m_ui->pageProcedureMain_PauseComboBox->currentIndex() == 12 ) {
			confParams.analyzer_pause_threshold = ( qint16 ) PAUSE_THRESHOLD_7_0;
		} else if ( m_ui->pageProcedureMain_PauseComboBox->currentIndex() == 13 ) {
			confParams.analyzer_pause_threshold = ( qint16 ) PAUSE_THRESHOLD_7_5;
		} else if ( m_ui->pageProcedureMain_PauseComboBox->currentIndex() == 14 ) {
			confParams.analyzer_pause_threshold = ( qint16 ) PAUSE_THRESHOLD_8_0;
		} else if ( m_ui->pageProcedureMain_PauseComboBox->currentIndex() == 15 ) {
			confParams.analyzer_pause_threshold = ( qint16 ) PAUSE_THRESHOLD_8_5;
		} else if ( m_ui->pageProcedureMain_PauseComboBox->currentIndex() == 16 ) {
			confParams.analyzer_pause_threshold = ( qint16 ) PAUSE_THRESHOLD_9_0;
		} else if ( m_ui->pageProcedureMain_PauseComboBox->currentIndex() == 17 ) {
			confParams.analyzer_pause_threshold = ( qint16 ) PAUSE_THRESHOLD_9_5;
		} else if ( m_ui->pageProcedureMain_PauseComboBox->currentIndex() == 18 ) {
			confParams.analyzer_pause_threshold = ( qint16 ) PAUSE_THRESHOLD_10_0;
		} else {
			confParams.analyzer_pause_threshold = ( qint16 ) DEFAULT_TACHY_THRESHOLD;
			LOGMSG( "Invalid device configuration - analyzer_pause_threshold" );
		}

		// Afib
		confParams.analyzer_af_on = m_ui->pageProcedureMain_AfibComboBox->currentIndex();

	}

    if ( m_configParams.device_mode == PROCTYPE_EVENT || m_configParams.device_mode == PROCTYPE_POST ) {

		//TTM speed
		confParams.ttm_speedup_on = m_ui->pageProcedureMain_TTMSpeedComboBox->currentIndex();

		//Manual event limit
		if ( m_ui->pageProcedureMain_ManualEvenetLimitComboBox->currentIndex() == 0 ) {
            confParams.manual_event_limit = ( qint16 ) EVENT_LIMIT_0;
		} else if ( m_ui->pageProcedureMain_ManualEvenetLimitComboBox->currentIndex() == 1 ) {
            confParams.manual_event_limit = ( qint16 ) EVENT_LIMIT_1;
		} else if ( m_ui->pageProcedureMain_ManualEvenetLimitComboBox->currentIndex() == 2 ) {
            confParams.manual_event_limit = ( qint16 ) EVENT_LIMIT_2;
		} else if ( m_ui->pageProcedureMain_ManualEvenetLimitComboBox->currentIndex() == 3 ) {
            confParams.manual_event_limit = ( qint16 ) EVENT_LIMIT_3;
		} else if ( m_ui->pageProcedureMain_ManualEvenetLimitComboBox->currentIndex() == 4 ) {
            confParams.manual_event_limit = ( qint16 ) EVENT_LIMIT_4;
		} else if ( m_ui->pageProcedureMain_ManualEvenetLimitComboBox->currentIndex() == 5 ) {
            confParams.manual_event_limit = ( qint16 ) EVENT_LIMIT_5;
		} else if ( m_ui->pageProcedureMain_ManualEvenetLimitComboBox->currentIndex() == 6 ) {
            confParams.manual_event_limit = ( qint16 ) EVENT_LIMIT_6;
		} else if ( m_ui->pageProcedureMain_ManualEvenetLimitComboBox->currentIndex() == 7 ) {
            confParams.manual_event_limit = ( qint16 ) EVENT_LIMIT_7;
		} else if ( m_ui->pageProcedureMain_ManualEvenetLimitComboBox->currentIndex() == 8 ) {
            confParams.manual_event_limit = ( qint16 ) EVENT_LIMIT_8;
		} else if ( m_ui->pageProcedureMain_ManualEvenetLimitComboBox->currentIndex() == 9 ) {
            confParams.manual_event_limit = ( qint16 ) EVENT_LIMIT_9;
		} else if ( m_ui->pageProcedureMain_ManualEvenetLimitComboBox->currentIndex() == 10 ) {
            confParams.manual_event_limit = ( qint16 ) EVENT_LIMIT_10;
		} else if ( m_ui->pageProcedureMain_ManualEvenetLimitComboBox->currentIndex() == 11 ) {
            confParams.manual_event_limit = ( qint16 ) EVENT_LIMIT_11;
		} else if ( m_ui->pageProcedureMain_ManualEvenetLimitComboBox->currentIndex() == 12 ) {
            confParams.manual_event_limit = ( qint16 ) EVENT_LIMIT_12;
		} else if ( m_ui->pageProcedureMain_ManualEvenetLimitComboBox->currentIndex() == 13 ) {
            confParams.manual_event_limit = ( qint16 ) EVENT_LIMIT_13;
		} else if ( m_ui->pageProcedureMain_ManualEvenetLimitComboBox->currentIndex() == 14 ) {
            confParams.manual_event_limit = ( qint16 ) EVENT_LIMIT_14;
		} else if ( m_ui->pageProcedureMain_ManualEvenetLimitComboBox->currentIndex() == 15 ) {
            confParams.manual_event_limit = ( qint16 ) EVENT_LIMIT_15;
		} else if ( m_ui->pageProcedureMain_ManualEvenetLimitComboBox->currentIndex() == 16 ) {
            confParams.manual_event_limit = ( qint16 ) EVENT_LIMIT_16;
		} else if ( m_ui->pageProcedureMain_ManualEvenetLimitComboBox->currentIndex() == 17 ) {
            confParams.manual_event_limit = ( qint16 ) EVENT_LIMIT_17;
		} else if ( m_ui->pageProcedureMain_ManualEvenetLimitComboBox->currentIndex() == 18 ) {
            confParams.manual_event_limit = ( qint16 ) EVENT_LIMIT_18;
		} else if ( m_ui->pageProcedureMain_ManualEvenetLimitComboBox->currentIndex() == 19 ) {
            confParams.manual_event_limit = ( qint16 ) EVENT_LIMIT_19;
		} else if ( m_ui->pageProcedureMain_ManualEvenetLimitComboBox->currentIndex() == 20 ) {
            confParams.manual_event_limit = ( qint16 ) EVENT_LIMIT_20;
		} else if ( m_ui->pageProcedureMain_ManualEvenetLimitComboBox->currentIndex() == 21 ) {
            confParams.manual_event_limit = ( qint16 ) EVENT_LIMIT_50;
        } else if ( m_ui->pageProcedureMain_ManualEvenetLimitComboBox->currentIndex() == 22 ) {
            confParams.manual_event_limit = ( qint16 ) EVENT_LIMIT_100;
        } else if ( m_ui->pageProcedureMain_ManualEvenetLimitComboBox->currentIndex() == 23 ) {
            confParams.manual_event_limit = ( qint16 ) EVENT_LIMIT_1000;
		} else {
			confParams.manual_event_limit = ( qint16 ) DEFAULT_MANUAL_EVENT_LIMIT;
			LOGMSG( "Invalid device configuration - manual_event_limit" );
		}

		//Post-trigger
		if ( m_ui->pageProcedureMain_PosttriggerComboBox->currentIndex() == 0 ) {
			confParams.post_trigger = ( qint16 ) POST_TRIGGER_TIME_15_SEC;
		} else if ( m_ui->pageProcedureMain_PosttriggerComboBox->currentIndex() == 1 ) {
			confParams.post_trigger = ( qint16 ) POST_TRIGGER_TIME_30_SEC;
		} else if ( m_ui->pageProcedureMain_PosttriggerComboBox->currentIndex() == 2 ) {
			confParams.post_trigger = ( qint16 ) POST_TRIGGER_TIME_45_SEC;
		} else if ( m_ui->pageProcedureMain_PosttriggerComboBox->currentIndex() == 3 ) {
			confParams.post_trigger = ( qint16 ) POST_TRIGGER_TIME_60_SEC;
		} else if ( m_ui->pageProcedureMain_PosttriggerComboBox->currentIndex() == 4 ) {
			confParams.post_trigger = ( qint16 ) POST_TRIGGER_TIME_90_SEC;
		} else if ( m_ui->pageProcedureMain_PosttriggerComboBox->currentIndex() == 5 ) {
			confParams.post_trigger = ( qint16 ) POST_TRIGGER_TIME_120_SEC;
		} else if ( m_ui->pageProcedureMain_PosttriggerComboBox->currentIndex() == 6 ) {
			confParams.post_trigger = ( qint16 ) POST_TRIGGER_TIME_180_SEC;
		} else if ( m_ui->pageProcedureMain_PosttriggerComboBox->currentIndex() == 7 ) {
			confParams.post_trigger = ( qint16 ) POST_TRIGGER_TIME_300_SEC;
		} else {
			confParams.post_trigger = ( qint16 ) DEFAULT_POST_TRIGGER_TIME;
			LOGMSG( "Invalid device configuration - post_trigger" );
		}


    } else if ( m_configParams.device_mode == PROCTYPE_HOLTER ) {

		confParams.recording_len = m_ui->pageProcedureMain_ManualRecordingLengthSpinBox->value() * 24;

		//Sample rate
		if  ( customer->show_128_sps ) {
			if ( m_ui->pageProcedureMain_SampleRateComboBox->currentIndex() == 0 ) {
				confParams.sample_rate = ( qint16 ) SAMPLE_RATE_128;
			} else if ( m_ui->pageProcedureMain_SampleRateComboBox->currentIndex() == 1 ) {
				confParams.sample_rate = ( qint16 ) SAMPLE_RATE_256;
			} else {
				confParams.sample_rate = ( qint16 ) DEFAULT_SAMPLE_RATE;
				LOGMSG( "Invalid device configuration - sample_rate" );
			}
		} else {
			confParams.sample_rate = ( qint16 ) SAMPLE_RATE_256;
		}
		if ( confParams.recording_len >= 24 * 21 ) {
			confParams.sample_rate = ( qint16 ) SAMPLE_RATE_128;
		}

		// Pacemaker
		if ( m_ui->pageProcedureMain_PacemakerComboBox->currentIndex() == 0 ) {
			confParams.analyzer_pace_on = 0;
		} else {
			confParams.analyzer_pace_on = 1;
		}


    } else if ( m_configParams.device_mode == PROCTYPE_MCT ) {
		//Pre-trigger
		if ( m_ui->pageProcedureMain_PretriggerComboBox->currentIndex() == 0 ) {
			confParams.pre_trigger = ( qint16 ) PRE_TRIGGER_TIME_15_SEC;
		} else if ( m_ui->pageProcedureMain_PretriggerComboBox->currentIndex() == 1 ) {
			confParams.pre_trigger = ( qint16 ) PRE_TRIGGER_TIME_30_SEC;
		} else if ( m_ui->pageProcedureMain_PretriggerComboBox->currentIndex() == 2 ) {
			confParams.pre_trigger = ( qint16 ) PRE_TRIGGER_TIME_45_SEC;
		} else if ( m_ui->pageProcedureMain_PretriggerComboBox->currentIndex() == 3 ) {
			confParams.pre_trigger = ( qint16 ) PRE_TRIGGER_TIME_60_SEC;
		} else if ( m_ui->pageProcedureMain_PretriggerComboBox->currentIndex() == 4 ) {
			confParams.pre_trigger = ( qint16 ) PRE_TRIGGER_TIME_90_SEC;
		} else if ( m_ui->pageProcedureMain_PretriggerComboBox->currentIndex() == 5 ) {
			confParams.pre_trigger = ( qint16 ) PRE_TRIGGER_TIME_120_SEC;
		} else if ( m_ui->pageProcedureMain_PretriggerComboBox->currentIndex() == 6 ) {
			confParams.pre_trigger = ( qint16 ) PRE_TRIGGER_TIME_180_SEC;
		} else if ( m_ui->pageProcedureMain_PretriggerComboBox->currentIndex() == 7 ) {
			confParams.pre_trigger = ( qint16 ) PRE_TRIGGER_TIME_300_SEC;
		} else {
			confParams.pre_trigger = ( qint16 ) DEFAULT_PRE_TRIGGER_TIME;
			qDebug() << "Invalid device configuration";
		}

		//Post-trigger
		if ( m_ui->pageProcedureMain_PosttriggerComboBox->currentIndex() == 0 ) {
			confParams.post_trigger = ( qint16 ) POST_TRIGGER_TIME_15_SEC;
		} else if ( m_ui->pageProcedureMain_PosttriggerComboBox->currentIndex() == 1 ) {
			confParams.post_trigger = ( qint16 ) POST_TRIGGER_TIME_30_SEC;
		} else if ( m_ui->pageProcedureMain_PosttriggerComboBox->currentIndex() == 2 ) {
			confParams.post_trigger = ( qint16 ) POST_TRIGGER_TIME_45_SEC;
		} else if ( m_ui->pageProcedureMain_PosttriggerComboBox->currentIndex() == 3 ) {
			confParams.post_trigger = ( qint16 ) POST_TRIGGER_TIME_60_SEC;
		} else if ( m_ui->pageProcedureMain_PosttriggerComboBox->currentIndex() == 4 ) {
			confParams.post_trigger = ( qint16 ) POST_TRIGGER_TIME_90_SEC;
		} else if ( m_ui->pageProcedureMain_PosttriggerComboBox->currentIndex() == 5 ) {
			confParams.post_trigger = ( qint16 ) POST_TRIGGER_TIME_120_SEC;
		} else if ( m_ui->pageProcedureMain_PosttriggerComboBox->currentIndex() == 6 ) {
			confParams.post_trigger = ( qint16 ) POST_TRIGGER_TIME_180_SEC;
		} else if ( m_ui->pageProcedureMain_PosttriggerComboBox->currentIndex() == 7 ) {
			confParams.post_trigger = ( qint16 ) POST_TRIGGER_TIME_300_SEC;
		} else {
			confParams.post_trigger = ( qint16 ) DEFAULT_POST_TRIGGER_TIME;
			qDebug() << "Invalid device configuration";
		}

		//Record duration
        confParams.recording_len = m_ui->pageProcedureMain_ManualRecordingLengthSpinBox->value() * 24;

		//Sample Rate
		confParams.sample_rate = ( qint16 ) SAMPLE_RATE_256;
		if ( confParams.recording_len >= 24 * 21 ) {
			confParams.sample_rate = ( qint16 ) SAMPLE_RATE_128;
		}

		//TTM speed
		confParams.ttm_speedup_on = m_ui->pageProcedureMain_TTMSpeedComboBox->currentIndex();

		//Tachy
		confParams.analyzer_tachy_on = ( qint8 ) 1;
		if ( m_ui->pageProcedureMain_TachyComboBox->currentIndex() == 0 ) {
			confParams.analyzer_tachy_on = ( qint8 ) 0;
		} else if ( m_ui->pageProcedureMain_TachyComboBox->currentIndex() == 1 ) {
			confParams.analyzer_tachy_threshold = ( qint16 ) TACHY_THRESHOLD_120;
		} else if ( m_ui->pageProcedureMain_TachyComboBox->currentIndex() == 2 ) {
			confParams.analyzer_tachy_threshold = ( qint16 ) TACHY_THRESHOLD_125;
		} else if ( m_ui->pageProcedureMain_TachyComboBox->currentIndex() == 3 ) {
			confParams.analyzer_tachy_threshold = ( qint16 ) TACHY_THRESHOLD_130;
		} else if ( m_ui->pageProcedureMain_TachyComboBox->currentIndex() == 4 ) {
			confParams.analyzer_tachy_threshold = ( qint16 ) TACHY_THRESHOLD_135;
		} else if ( m_ui->pageProcedureMain_TachyComboBox->currentIndex() == 5 ) {
			confParams.analyzer_tachy_threshold = ( qint16 ) TACHY_THRESHOLD_140;
		} else if ( m_ui->pageProcedureMain_TachyComboBox->currentIndex() == 6 ) {
			confParams.analyzer_tachy_threshold = ( qint16 ) TACHY_THRESHOLD_145;
		} else if ( m_ui->pageProcedureMain_TachyComboBox->currentIndex() == 7 ) {
			confParams.analyzer_tachy_threshold = ( qint16 ) TACHY_THRESHOLD_150;
		} else if ( m_ui->pageProcedureMain_TachyComboBox->currentIndex() == 8 ) {
			confParams.analyzer_tachy_threshold = ( qint16 ) TACHY_THRESHOLD_155;
		} else if ( m_ui->pageProcedureMain_TachyComboBox->currentIndex() == 9 ) {
			confParams.analyzer_tachy_threshold = ( qint16 ) TACHY_THRESHOLD_160;
		} else if ( m_ui->pageProcedureMain_TachyComboBox->currentIndex() == 10 ) {
			confParams.analyzer_tachy_threshold = ( qint16 ) TACHY_THRESHOLD_165;
		} else if ( m_ui->pageProcedureMain_TachyComboBox->currentIndex() == 11 ) {
			confParams.analyzer_tachy_threshold = ( qint16 ) TACHY_THRESHOLD_170;
		} else if ( m_ui->pageProcedureMain_TachyComboBox->currentIndex() == 12 ) {
			confParams.analyzer_tachy_threshold = ( qint16 ) TACHY_THRESHOLD_175;
		} else if ( m_ui->pageProcedureMain_TachyComboBox->currentIndex() == 13 ) {
			confParams.analyzer_tachy_threshold = ( qint16 ) TACHY_THRESHOLD_180;
		} else if ( m_ui->pageProcedureMain_TachyComboBox->currentIndex() == 14 ) {
			confParams.analyzer_tachy_threshold = ( qint16 ) TACHY_THRESHOLD_185;
		} else if ( m_ui->pageProcedureMain_TachyComboBox->currentIndex() == 15 ) {
			confParams.analyzer_tachy_threshold = ( qint16 ) TACHY_THRESHOLD_190;
		} else if ( m_ui->pageProcedureMain_TachyComboBox->currentIndex() == 16 ) {
			confParams.analyzer_tachy_threshold = ( qint16 ) TACHY_THRESHOLD_195;
		} else if ( m_ui->pageProcedureMain_TachyComboBox->currentIndex() == 17 ) {
			confParams.analyzer_tachy_threshold = ( qint16 ) TACHY_THRESHOLD_200;
		} else if ( m_ui->pageProcedureMain_TachyComboBox->currentIndex() == 18 ) {
			confParams.analyzer_tachy_threshold = ( qint16 ) TACHY_THRESHOLD_205;
		} else if ( m_ui->pageProcedureMain_TachyComboBox->currentIndex() == 19 ) {
			confParams.analyzer_tachy_threshold = ( qint16 ) TACHY_THRESHOLD_210;
		} else if ( m_ui->pageProcedureMain_TachyComboBox->currentIndex() == 20 ) {
			confParams.analyzer_tachy_threshold = ( qint16 ) TACHY_THRESHOLD_215;
		} else if ( m_ui->pageProcedureMain_TachyComboBox->currentIndex() == 21 ) {
			confParams.analyzer_tachy_threshold = ( qint16 ) TACHY_THRESHOLD_220;
		} else if ( m_ui->pageProcedureMain_TachyComboBox->currentIndex() == 22 ) {
			confParams.analyzer_tachy_threshold = ( qint16 ) TACHY_THRESHOLD_225;
		} else if ( m_ui->pageProcedureMain_TachyComboBox->currentIndex() == 23 ) {
			confParams.analyzer_tachy_threshold = ( qint16 ) TACHY_THRESHOLD_230;
		} else if ( m_ui->pageProcedureMain_TachyComboBox->currentIndex() == 24 ) {
			confParams.analyzer_tachy_threshold = ( qint16 ) TACHY_THRESHOLD_235;
		} else if ( m_ui->pageProcedureMain_TachyComboBox->currentIndex() == 25 ) {
			confParams.analyzer_tachy_threshold = ( qint16 ) TACHY_THRESHOLD_240;
		} else {
			confParams.analyzer_tachy_threshold = ( qint16 ) DEFAULT_TACHY_THRESHOLD;
			qDebug() << "Invalid device configuration";
		}

		//Brady
		confParams.analyzer_brady_on = ( qint8 ) 1;
		if ( m_ui->pageProcedureMain_BradyComboBox->currentIndex() == 0 ) {
			confParams.analyzer_brady_on = ( qint8 ) 0;
		} else if ( m_ui->pageProcedureMain_BradyComboBox->currentIndex() == 1 ) {
			confParams.analyzer_brady_threshold = ( qint16 ) BRADY_THRESHOLD_15;
		} else if ( m_ui->pageProcedureMain_BradyComboBox->currentIndex() == 2 ) {
			confParams.analyzer_brady_threshold = ( qint16 ) BRADY_THRESHOLD_20;
		} else if ( m_ui->pageProcedureMain_BradyComboBox->currentIndex() == 3 ) {
			confParams.analyzer_brady_threshold = ( qint16 ) BRADY_THRESHOLD_25;
		} else if ( m_ui->pageProcedureMain_BradyComboBox->currentIndex() == 4 ) {
			confParams.analyzer_brady_threshold = ( qint16 ) BRADY_THRESHOLD_30;
		} else if ( m_ui->pageProcedureMain_BradyComboBox->currentIndex() == 5 ) {
			confParams.analyzer_brady_threshold = ( qint16 ) BRADY_THRESHOLD_35;
		} else if ( m_ui->pageProcedureMain_BradyComboBox->currentIndex() == 6 ) {
			confParams.analyzer_brady_threshold = ( qint16 ) BRADY_THRESHOLD_40;
		} else if ( m_ui->pageProcedureMain_BradyComboBox->currentIndex() == 7 ) {
			confParams.analyzer_brady_threshold = ( qint16 ) BRADY_THRESHOLD_45;
		} else if ( m_ui->pageProcedureMain_BradyComboBox->currentIndex() == 8 ) {
			confParams.analyzer_brady_threshold = ( qint16 ) BRADY_THRESHOLD_50;
		} else if ( m_ui->pageProcedureMain_BradyComboBox->currentIndex() == 9 ) {
			confParams.analyzer_brady_threshold = ( qint16 ) BRADY_THRESHOLD_55;
		} else if ( m_ui->pageProcedureMain_BradyComboBox->currentIndex() == 10 ) {
			confParams.analyzer_brady_threshold = ( qint16 ) BRADY_THRESHOLD_60;
		} else {
			confParams.analyzer_brady_threshold = ( qint16 ) DEFAULT_BRADY_THRESHOLD;
			qDebug() << "Invalid device configuration";
		}

		//Pause
		confParams.analyzer_pause_on = ( qint8 ) 1;
		if ( m_ui->pageProcedureMain_PauseComboBox->currentIndex() == 0 ) {
			confParams.analyzer_pause_on = ( qint8 ) 0;
		} else if ( m_ui->pageProcedureMain_PauseComboBox->currentIndex() == 1 ) {
			confParams.analyzer_pause_threshold = ( qint16 ) PAUSE_THRESHOLD_1_5;
		} else if ( m_ui->pageProcedureMain_PauseComboBox->currentIndex() == 2 ) {
			confParams.analyzer_pause_threshold = ( qint16 ) PAUSE_THRESHOLD_2_0;
		} else if ( m_ui->pageProcedureMain_PauseComboBox->currentIndex() == 3 ) {
			confParams.analyzer_pause_threshold = ( qint16 ) PAUSE_THRESHOLD_2_5;
		} else if ( m_ui->pageProcedureMain_PauseComboBox->currentIndex() == 4 ) {
			confParams.analyzer_pause_threshold = ( qint16 ) PAUSE_THRESHOLD_3_0;
		} else if ( m_ui->pageProcedureMain_PauseComboBox->currentIndex() == 5 ) {
			confParams.analyzer_pause_threshold = ( qint16 ) PAUSE_THRESHOLD_3_5;
		} else if ( m_ui->pageProcedureMain_PauseComboBox->currentIndex() == 6 ) {
			confParams.analyzer_pause_threshold = ( qint16 ) PAUSE_THRESHOLD_4_0;
		} else if ( m_ui->pageProcedureMain_PauseComboBox->currentIndex() == 7 ) {
			confParams.analyzer_pause_threshold = ( qint16 ) PAUSE_THRESHOLD_4_5;
		} else if ( m_ui->pageProcedureMain_PauseComboBox->currentIndex() == 8 ) {
			confParams.analyzer_pause_threshold = ( qint16 ) PAUSE_THRESHOLD_5_0;
		} else if ( m_ui->pageProcedureMain_PauseComboBox->currentIndex() == 9 ) {
			confParams.analyzer_pause_threshold = ( qint16 ) PAUSE_THRESHOLD_5_5;
		} else if ( m_ui->pageProcedureMain_PauseComboBox->currentIndex() == 10 ) {
			confParams.analyzer_pause_threshold = ( qint16 ) PAUSE_THRESHOLD_6_0;
		} else if ( m_ui->pageProcedureMain_PauseComboBox->currentIndex() == 11 ) {
			confParams.analyzer_pause_threshold = ( qint16 ) PAUSE_THRESHOLD_6_5;
		} else if ( m_ui->pageProcedureMain_PauseComboBox->currentIndex() == 12 ) {
			confParams.analyzer_pause_threshold = ( qint16 ) PAUSE_THRESHOLD_7_0;
		} else if ( m_ui->pageProcedureMain_PauseComboBox->currentIndex() == 13 ) {
			confParams.analyzer_pause_threshold = ( qint16 ) PAUSE_THRESHOLD_7_5;
		} else if ( m_ui->pageProcedureMain_PauseComboBox->currentIndex() == 14 ) {
			confParams.analyzer_pause_threshold = ( qint16 ) PAUSE_THRESHOLD_8_0;
		} else if ( m_ui->pageProcedureMain_PauseComboBox->currentIndex() == 15 ) {
			confParams.analyzer_pause_threshold = ( qint16 ) PAUSE_THRESHOLD_8_5;
		} else if ( m_ui->pageProcedureMain_PauseComboBox->currentIndex() == 16 ) {
			confParams.analyzer_pause_threshold = ( qint16 ) PAUSE_THRESHOLD_9_0;
		} else if ( m_ui->pageProcedureMain_PauseComboBox->currentIndex() == 17 ) {
			confParams.analyzer_pause_threshold = ( qint16 ) PAUSE_THRESHOLD_9_5;
		} else if ( m_ui->pageProcedureMain_PauseComboBox->currentIndex() == 18 ) {
			confParams.analyzer_pause_threshold = ( qint16 ) PAUSE_THRESHOLD_10_0;
		} else {
			confParams.analyzer_pause_threshold = ( qint16 ) DEFAULT_TACHY_THRESHOLD;
			qDebug() << "Invalid device configuration";
		}

		// Afib
		confParams.analyzer_af_on = m_ui->pageProcedureMain_AfibComboBox->currentIndex();
	}

	/*
	 * Send the config struct to the devicehandler,
	 * write the parameters to the device and
	 * commit parameters
	 */
	QList<QString> paramNamesList;

    if ( m_configParams.device_mode == PROCTYPE_EVENT ) {
		paramNamesList.append( "PRE_TRIGGER" );
		paramNamesList.append( "POST_TRIGGER" );
		paramNamesList.append( "AUTO_EV_LIMIT" );
		paramNamesList.append( "MAN_EV_LIMIT" );
		paramNamesList.append( "TTM_EN_FLG" );
		paramNamesList.append( "TTM_3X_FLG" );
		paramNamesList.append( "TACHY_ON_FLG" );
		paramNamesList.append( "TACHY_THRESH" );
		paramNamesList.append( "BRADY_ON_FLG" );
		paramNamesList.append( "BRADY_THRESH" );
		paramNamesList.append( "PAUSE_ON_FLG" );
		paramNamesList.append( "PAUSE_THRESH" );
		paramNamesList.append( "AF_ON_FLG" );

    } else if ( m_configParams.device_mode == PROCTYPE_HOLTER ) {
		if ( confParams.sample_rate <= 128 ) {
			paramNamesList.append( "SAMP_RATE" );
			paramNamesList.append( "REC_LENGTH" );
		} else {
			paramNamesList.append( "REC_LENGTH" );
			paramNamesList.append( "SAMP_RATE" );
		}
    } else if ( m_configParams.device_mode == PROCTYPE_POST ) {
		paramNamesList.append( "POST_TRIGGER" );
		paramNamesList.append( "TTM_3X_FLG" );
		paramNamesList.append( "MAN_EV_LIMIT" );
    } else if ( m_configParams.device_mode == PROCTYPE_MCT ) {
		paramNamesList.append( "PRE_TRIGGER" );
		paramNamesList.append( "POST_TRIGGER" );
		paramNamesList.append( "SAMP_RATE" );
		paramNamesList.append( "TTM_EN_FLG" );
		paramNamesList.append( "TTM_3X_FLG" );
		paramNamesList.append( "TACHY_ON_FLG" );
		paramNamesList.append( "TACHY_THRESH" );
		paramNamesList.append( "BRADY_ON_FLG" );
		paramNamesList.append( "BRADY_THRESH" );
		paramNamesList.append( "PAUSE_ON_FLG" );
		paramNamesList.append( "PAUSE_THRESH" );
		paramNamesList.append( "AF_ON_FLG" );

	}

    m_ui->pageUSBSaved_TitleLabel->setVisible(false);
    m_ui->pageUSBSaved_MainLabel->setVisible(false);
    deviceCommunicationStatus = DEVICE_WRITE_LIST_OF_PARAMETERS;
	emit parameterListWrite( confParams, paramNamesList );
}

void SironaViewerUi::pageProcedure_setupUI( bool fullSetup )
{
	LOGMSG( "pageProcedure_setupUI(%d)    device_mode = %d", (int)fullSetup, m_configParams.device_mode );
	switch ( m_configParams.device_mode ) {
		case PROCTYPE_HOLTER:
			m_ui->pageProcedureMain_PretriggerFrame->setVisible( false );
			m_ui->pageProcedureMain_PosttriggerFrame->setVisible( false );
			m_ui->pageProcedureMain_RecordingLengthFrame->setVisible( true );
			m_ui->pageProcedureMain_Tmp->setVisible( true );
			m_ui->pageProcedureMain_AutoEvenetLimitFrame->setVisible( false );
			m_ui->pageProcedureMain_ManualEvenetLimitFrame->setVisible( false );
			m_ui->pageProcedureMain_TTMSpeedFrame->setVisible( false );
			m_ui->pageProcedureMain_TachyFrame->setVisible( false );
			m_ui->pageProcedureMain_BradyFrame->setVisible( false );
			m_ui->pageProcedureMain_PauseFrame->setVisible( false );
			m_ui->pageProcedureMain_AfibFrame->setVisible( false );

			if ( customer->show_128_sps && ! VERSION_HAS_FIXED_SAMPLE_RATE ) {
				m_ui->pageProcedureMain_AdvancedOptionsFrame->setVisible( true );
				if ( procedurePageAdveancedOptionsEnabled ) {
					m_ui->pageProcedureMain_AdvancedOptionsLabel->setText( tr( "Advanced options %1" ).arg( QString::fromWCharArray( L"\u25B2" ) ) );
					m_ui->pageProcedureMain_SampleRateFrame->setVisible( true );
				} else {
					m_ui->pageProcedureMain_AdvancedOptionsLabel->setText( tr( "Advanced options %1" ).arg( QString::fromWCharArray( L"\u25BC" ) ) );
					m_ui->pageProcedureMain_SampleRateFrame->setVisible( false );
				}
			} else {
				m_ui->pageProcedureMain_AdvancedOptionsFrame->setVisible( false );
				m_ui->pageProcedureMain_SampleRateFrame->setVisible( false );
			}
			break;

		case PROCTYPE_EVENT:
		case PROCTYPE_NOT_SET:	/* also set appropriate defaults when using USB to configure things, since procType is sometimes NotSet while a USB cable is connected during configuration */
			m_ui->pageProcedureMain_PretriggerFrame->setVisible( true );
			m_ui->pageProcedureMain_PosttriggerFrame->setVisible( true );
			m_ui->pageProcedureMain_PosttriggerLabel->setText( tr( "Post-trigger" ) );
			m_ui->pageProcedureMain_RecordingLengthFrame->setVisible( false );
			m_ui->pageProcedureMain_Tmp->setVisible( false );
			m_ui->pageProcedureMain_SampleRateFrame->setVisible( false );
			m_ui->pageProcedureMain_AdvancedOptionsFrame->setVisible( true );

			if ( procedurePageAdveancedOptionsEnabled ) {
				m_ui->pageProcedureMain_AdvancedOptionsLabel->setText( tr( "Advanced options %1" ).arg( QString::fromWCharArray( L"\u25B2" ) ) );
				m_ui->pageProcedureMain_AutoEvenetLimitFrame->setVisible( true );
				m_ui->pageProcedureMain_ManualEvenetLimitFrame->setVisible( true );
				m_ui->pageProcedureMain_TTMSpeedFrame->setVisible( true );
				m_ui->pageProcedureMain_TachyFrame->setVisible( true );
				m_ui->pageProcedureMain_BradyFrame->setVisible( true );
				m_ui->pageProcedureMain_PauseFrame->setVisible( true );
				m_ui->pageProcedureMain_AfibFrame->setVisible( true );
			} else {
				m_ui->pageProcedureMain_AdvancedOptionsLabel->setText( tr( "Advanced options %1" ).arg( QString::fromWCharArray( L"\u25BC" ) ) );
				m_ui->pageProcedureMain_AutoEvenetLimitFrame->setVisible( false );
				m_ui->pageProcedureMain_ManualEvenetLimitFrame->setVisible( false );
				m_ui->pageProcedureMain_TTMSpeedFrame->setVisible( false );
				m_ui->pageProcedureMain_TachyFrame->setVisible( false );
				m_ui->pageProcedureMain_BradyFrame->setVisible( false );
				m_ui->pageProcedureMain_PauseFrame->setVisible( false );
				m_ui->pageProcedureMain_AfibFrame->setVisible( false );
			}
			break;

		case PROCTYPE_POST:
			m_ui->pageProcedureMain_PretriggerFrame->setVisible( false );
			m_ui->pageProcedureMain_PosttriggerFrame->setVisible( true );
			m_ui->pageProcedureMain_PosttriggerLabel->setText( tr( "Record length" ) );
			m_ui->pageProcedureMain_RecordingLengthFrame->setVisible( false );
			m_ui->pageProcedureMain_Tmp->setVisible( false );
			m_ui->pageProcedureMain_SampleRateFrame->setVisible( false );
			m_ui->pageProcedureMain_TachyFrame->setVisible( false );
			m_ui->pageProcedureMain_BradyFrame->setVisible( false );
			m_ui->pageProcedureMain_PauseFrame->setVisible( false );
			m_ui->pageProcedureMain_AfibFrame->setVisible( false );
			m_ui->pageProcedureMain_AutoEvenetLimitFrame->setVisible( false );
			m_ui->pageProcedureMain_AdvancedOptionsFrame->setVisible( true );
			if ( procedurePageAdveancedOptionsEnabled ) {
				m_ui->pageProcedureMain_AdvancedOptionsLabel->setText( tr( "Advanced options %1" ).arg( QString::fromWCharArray( L"\u25B2" ) ) );
				m_ui->pageProcedureMain_ManualEvenetLimitFrame->setVisible( true );
				m_ui->pageProcedureMain_TTMSpeedFrame->setVisible( true );
			} else {
				m_ui->pageProcedureMain_AdvancedOptionsLabel->setText( tr( "Advanced options %1" ).arg( QString::fromWCharArray( L"\u25BC" ) ) );
				m_ui->pageProcedureMain_ManualEvenetLimitFrame->setVisible( false );
				m_ui->pageProcedureMain_TTMSpeedFrame->setVisible( false );
			}
			break;

		case PROCTYPE_MCT:
			m_ui->pageProcedureMain_PretriggerFrame->setVisible( true );
			m_ui->pageProcedureMain_PosttriggerFrame->setVisible( true );
			m_ui->pageProcedureMain_PosttriggerLabel->setText( tr( "Post-trigger" ) );
			m_ui->pageProcedureMain_RecordingLengthFrame->setVisible( true );
			m_ui->pageProcedureMain_Tmp->setVisible( true );
			m_ui->pageProcedureMain_AdvancedOptionsFrame->setVisible( true );

			if ( procedurePageAdveancedOptionsEnabled ) {
				m_ui->pageProcedureMain_AdvancedOptionsLabel->setText( tr( "Advanced options %1" ).arg( QString::fromWCharArray( L"\u25B2" ) ) );
				m_ui->pageProcedureMain_AutoEvenetLimitFrame->setVisible( false );
				m_ui->pageProcedureMain_ManualEvenetLimitFrame->setVisible( false );
				m_ui->pageProcedureMain_TTMSpeedFrame->setVisible( true );
				m_ui->pageProcedureMain_TachyFrame->setVisible( true );
				m_ui->pageProcedureMain_BradyFrame->setVisible( true );
				m_ui->pageProcedureMain_PauseFrame->setVisible( true );
				m_ui->pageProcedureMain_AfibFrame->setVisible( true );
				m_ui->pageProcedureMain_SampleRateFrame->setVisible( false );
			} else {
				m_ui->pageProcedureMain_AdvancedOptionsLabel->setText( tr( "Advanced options %1" ).arg( QString::fromWCharArray( L"\u25BC" ) ) );
				m_ui->pageProcedureMain_AutoEvenetLimitFrame->setVisible( false );
				m_ui->pageProcedureMain_ManualEvenetLimitFrame->setVisible( false );
				m_ui->pageProcedureMain_TTMSpeedFrame->setVisible( false );
				m_ui->pageProcedureMain_TachyFrame->setVisible( false );
				m_ui->pageProcedureMain_BradyFrame->setVisible( false );
				m_ui->pageProcedureMain_PauseFrame->setVisible( false );
				m_ui->pageProcedureMain_AfibFrame->setVisible( false );
				m_ui->pageProcedureMain_SampleRateFrame->setVisible( false );
			}

			if ( m_ui->pageProcedureMain_PretriggerComboBox->findText( "300", Qt::MatchContains ) < 0 ) {
				m_ui->pageProcedureMain_PretriggerComboBox->insertItem( m_ui->pageProcedureMain_PretriggerComboBox->count(), tr( "300 sec" ) ); //add 300 second pre-trigger
			}

			break;

		default:
			break;
	}

	/*
	 * Populate this page's entries with the data from the m_ConfigParams
	 */

	if ( fullSetup ) {
		deviceCommunicationStatus = DEVICE_GET_BATTERY_STATUS;
		emit getBatteryStatus();

		//Pre-trigger
		switch ( m_configParams.pre_trigger ) {
			case PRE_TRIGGER_TIME_15_SEC:	m_ui->pageProcedureMain_PretriggerComboBox->setCurrentIndex( 0 ); break;
			case PRE_TRIGGER_TIME_30_SEC:	m_ui->pageProcedureMain_PretriggerComboBox->setCurrentIndex( 1 ); break;
			case PRE_TRIGGER_TIME_45_SEC:	m_ui->pageProcedureMain_PretriggerComboBox->setCurrentIndex( 2 ); break;
			case PRE_TRIGGER_TIME_60_SEC:	m_ui->pageProcedureMain_PretriggerComboBox->setCurrentIndex( 3 ); break;
			case PRE_TRIGGER_TIME_90_SEC:	m_ui->pageProcedureMain_PretriggerComboBox->setCurrentIndex( 4 ); break;
			case PRE_TRIGGER_TIME_120_SEC:	m_ui->pageProcedureMain_PretriggerComboBox->setCurrentIndex( 5 ); break;
			case PRE_TRIGGER_TIME_180_SEC:	m_ui->pageProcedureMain_PretriggerComboBox->setCurrentIndex( 6 ); break;
			case PRE_TRIGGER_TIME_300_SEC:	m_ui->pageProcedureMain_PretriggerComboBox->setCurrentIndex( 7 ); break;
			default:
				break;
		}

		//Post-trigger
		switch ( m_configParams.post_trigger ) {
			case POST_TRIGGER_TIME_15_SEC:	m_ui->pageProcedureMain_PosttriggerComboBox->setCurrentIndex( 0 ); break;
			case POST_TRIGGER_TIME_30_SEC:	m_ui->pageProcedureMain_PosttriggerComboBox->setCurrentIndex( 1 ); break;
			case POST_TRIGGER_TIME_45_SEC:	m_ui->pageProcedureMain_PosttriggerComboBox->setCurrentIndex( 2 ); break;
			case POST_TRIGGER_TIME_60_SEC:	m_ui->pageProcedureMain_PosttriggerComboBox->setCurrentIndex( 3 ); break;
			case POST_TRIGGER_TIME_90_SEC:	m_ui->pageProcedureMain_PosttriggerComboBox->setCurrentIndex( 4 ); break;
			case POST_TRIGGER_TIME_120_SEC:	m_ui->pageProcedureMain_PosttriggerComboBox->setCurrentIndex( 5 ); break;
			case POST_TRIGGER_TIME_180_SEC:	m_ui->pageProcedureMain_PosttriggerComboBox->setCurrentIndex( 6 ); break;
			case POST_TRIGGER_TIME_300_SEC:	m_ui->pageProcedureMain_PosttriggerComboBox->setCurrentIndex( 7 ); break;
			default:
				break;
		}

		//Recording length
        if ( m_configParams.device_mode == PROCTYPE_HOLTER ) {
			m_ui->pageProcedureMain_RecordingLengthSelector->setItems( customer->recording_durations );
			m_ui->pageProcedureMain_ManualRecordingLengthSpinBox->setMaximum( customer->recording_durations.last().toInt() );
		on_pageProcedureMain_ManualRecordingLengthSpinBox_valueChanged( m_configParams.recording_len / 24 );
        } else if ( m_configParams.device_mode == PROCTYPE_MCT ) {
			m_ui->pageProcedureMain_RecordingLengthSelector->setItems( customer->mct_recording_durations );
			m_ui->pageProcedureMain_ManualRecordingLengthSpinBox->setMaximum( customer->mct_recording_durations.last().toInt() );
            on_pageProcedureMain_ManualRecordingLengthSpinBox_valueChanged( m_configParams.recording_len / 24 );
		}

	}

	//Automatic event limit
	switch ( m_configParams.automatic_event_limit ) {
        case EVENT_LIMIT_0:	m_ui->pageProcedureMain_AutoEvenetLimitComboBox->setCurrentIndex( 0 ); break;
        case EVENT_LIMIT_1:	m_ui->pageProcedureMain_AutoEvenetLimitComboBox->setCurrentIndex( 1 ); break;
        case EVENT_LIMIT_2:	m_ui->pageProcedureMain_AutoEvenetLimitComboBox->setCurrentIndex( 2 ); break;
        case EVENT_LIMIT_3:	m_ui->pageProcedureMain_AutoEvenetLimitComboBox->setCurrentIndex( 3 ); break;
        case EVENT_LIMIT_4:	m_ui->pageProcedureMain_AutoEvenetLimitComboBox->setCurrentIndex( 4 ); break;
        case EVENT_LIMIT_5:	m_ui->pageProcedureMain_AutoEvenetLimitComboBox->setCurrentIndex( 5 ); break;
        case EVENT_LIMIT_6:	m_ui->pageProcedureMain_AutoEvenetLimitComboBox->setCurrentIndex( 6 ); break;
        case EVENT_LIMIT_7:	m_ui->pageProcedureMain_AutoEvenetLimitComboBox->setCurrentIndex( 7 ); break;
        case EVENT_LIMIT_8:	m_ui->pageProcedureMain_AutoEvenetLimitComboBox->setCurrentIndex( 8 ); break;
        case EVENT_LIMIT_9:	m_ui->pageProcedureMain_AutoEvenetLimitComboBox->setCurrentIndex( 9 ); break;
        case EVENT_LIMIT_10:	m_ui->pageProcedureMain_AutoEvenetLimitComboBox->setCurrentIndex( 10 ); break;
        case EVENT_LIMIT_11:	m_ui->pageProcedureMain_AutoEvenetLimitComboBox->setCurrentIndex( 11 ); break;
        case EVENT_LIMIT_12:	m_ui->pageProcedureMain_AutoEvenetLimitComboBox->setCurrentIndex( 12 ); break;
        case EVENT_LIMIT_13:	m_ui->pageProcedureMain_AutoEvenetLimitComboBox->setCurrentIndex( 13 ); break;
        case EVENT_LIMIT_14:	m_ui->pageProcedureMain_AutoEvenetLimitComboBox->setCurrentIndex( 14 ); break;
        case EVENT_LIMIT_15:	m_ui->pageProcedureMain_AutoEvenetLimitComboBox->setCurrentIndex( 15 ); break;
        case EVENT_LIMIT_16:	m_ui->pageProcedureMain_AutoEvenetLimitComboBox->setCurrentIndex( 16 ); break;
        case EVENT_LIMIT_17:	m_ui->pageProcedureMain_AutoEvenetLimitComboBox->setCurrentIndex( 17 ); break;
        case EVENT_LIMIT_18:	m_ui->pageProcedureMain_AutoEvenetLimitComboBox->setCurrentIndex( 18 ); break;
        case EVENT_LIMIT_19:	m_ui->pageProcedureMain_AutoEvenetLimitComboBox->setCurrentIndex( 19 ); break;
        case EVENT_LIMIT_20:	m_ui->pageProcedureMain_AutoEvenetLimitComboBox->setCurrentIndex( 20 ); break;
        case EVENT_LIMIT_50:	m_ui->pageProcedureMain_AutoEvenetLimitComboBox->setCurrentIndex( 21 ); break;
        case EVENT_LIMIT_100:	m_ui->pageProcedureMain_AutoEvenetLimitComboBox->setCurrentIndex( 22 ); break;
        case EVENT_LIMIT_1000:	m_ui->pageProcedureMain_AutoEvenetLimitComboBox->setCurrentIndex( 23 ); break;
        default:
			break;
	}

	//Manual event limit
	switch ( m_configParams.manual_event_limit ) {
        case EVENT_LIMIT_0:	m_ui->pageProcedureMain_ManualEvenetLimitComboBox->setCurrentIndex( 0 ); break;
        case EVENT_LIMIT_1:	m_ui->pageProcedureMain_ManualEvenetLimitComboBox->setCurrentIndex( 1 ); break;
        case EVENT_LIMIT_2:	m_ui->pageProcedureMain_ManualEvenetLimitComboBox->setCurrentIndex( 2 ); break;
        case EVENT_LIMIT_3:	m_ui->pageProcedureMain_ManualEvenetLimitComboBox->setCurrentIndex( 3 ); break;
        case EVENT_LIMIT_4:	m_ui->pageProcedureMain_ManualEvenetLimitComboBox->setCurrentIndex( 4 ); break;
        case EVENT_LIMIT_5:	m_ui->pageProcedureMain_ManualEvenetLimitComboBox->setCurrentIndex( 5 ); break;
        case EVENT_LIMIT_6:	m_ui->pageProcedureMain_ManualEvenetLimitComboBox->setCurrentIndex( 6 ); break;
        case EVENT_LIMIT_7:	m_ui->pageProcedureMain_ManualEvenetLimitComboBox->setCurrentIndex( 7 ); break;
        case EVENT_LIMIT_8:	m_ui->pageProcedureMain_ManualEvenetLimitComboBox->setCurrentIndex( 8 ); break;
        case EVENT_LIMIT_9:	m_ui->pageProcedureMain_ManualEvenetLimitComboBox->setCurrentIndex( 9 ); break;
        case EVENT_LIMIT_10:	m_ui->pageProcedureMain_ManualEvenetLimitComboBox->setCurrentIndex( 10 ); break;
        case EVENT_LIMIT_11:	m_ui->pageProcedureMain_ManualEvenetLimitComboBox->setCurrentIndex( 11 ); break;
        case EVENT_LIMIT_12:	m_ui->pageProcedureMain_ManualEvenetLimitComboBox->setCurrentIndex( 12 ); break;
        case EVENT_LIMIT_13:	m_ui->pageProcedureMain_ManualEvenetLimitComboBox->setCurrentIndex( 13 ); break;
        case EVENT_LIMIT_14:	m_ui->pageProcedureMain_ManualEvenetLimitComboBox->setCurrentIndex( 14 ); break;
        case EVENT_LIMIT_15:	m_ui->pageProcedureMain_ManualEvenetLimitComboBox->setCurrentIndex( 15 ); break;
        case EVENT_LIMIT_16:	m_ui->pageProcedureMain_ManualEvenetLimitComboBox->setCurrentIndex( 16 ); break;
        case EVENT_LIMIT_17:	m_ui->pageProcedureMain_ManualEvenetLimitComboBox->setCurrentIndex( 17 ); break;
        case EVENT_LIMIT_18:	m_ui->pageProcedureMain_ManualEvenetLimitComboBox->setCurrentIndex( 18 ); break;
        case EVENT_LIMIT_19:	m_ui->pageProcedureMain_ManualEvenetLimitComboBox->setCurrentIndex( 19 ); break;
        case EVENT_LIMIT_20:	m_ui->pageProcedureMain_ManualEvenetLimitComboBox->setCurrentIndex( 20 ); break;
        case EVENT_LIMIT_50:	m_ui->pageProcedureMain_ManualEvenetLimitComboBox->setCurrentIndex( 21 ); break;
        case EVENT_LIMIT_100:	m_ui->pageProcedureMain_ManualEvenetLimitComboBox->setCurrentIndex( 22 ); break;
        case EVENT_LIMIT_1000:	m_ui->pageProcedureMain_ManualEvenetLimitComboBox->setCurrentIndex( 23 ); break;
        default:
			break;
	}

	//TTM speed
	m_ui->pageProcedureMain_TTMSpeedComboBox->setCurrentIndex( m_configParams.ttm_speedup_on );

	//Tachy
	if ( m_configParams.analyzer_tachy_on ) {
		switch ( m_configParams.analyzer_tachy_threshold ) {
            case TACHY_THRESHOLD_110:   // round up to 120 (only Zywie uses tachy down to 110)
            case TACHY_THRESHOLD_115:
            case TACHY_THRESHOLD_120:	m_ui->pageProcedureMain_TachyComboBox->setCurrentIndex( 1 ); break;
			case TACHY_THRESHOLD_125:	m_ui->pageProcedureMain_TachyComboBox->setCurrentIndex( 2 ); break;
			case TACHY_THRESHOLD_130:	m_ui->pageProcedureMain_TachyComboBox->setCurrentIndex( 3 ); break;
			case TACHY_THRESHOLD_135:	m_ui->pageProcedureMain_TachyComboBox->setCurrentIndex( 4 ); break;
			case TACHY_THRESHOLD_140:	m_ui->pageProcedureMain_TachyComboBox->setCurrentIndex( 5 ); break;
			case TACHY_THRESHOLD_145:	m_ui->pageProcedureMain_TachyComboBox->setCurrentIndex( 6 ); break;
			case TACHY_THRESHOLD_150:	m_ui->pageProcedureMain_TachyComboBox->setCurrentIndex( 7 ); break;
			case TACHY_THRESHOLD_155:	m_ui->pageProcedureMain_TachyComboBox->setCurrentIndex( 8 ); break;
			case TACHY_THRESHOLD_160:	m_ui->pageProcedureMain_TachyComboBox->setCurrentIndex( 9 ); break;
			case TACHY_THRESHOLD_165:	m_ui->pageProcedureMain_TachyComboBox->setCurrentIndex( 10 ); break;
			case TACHY_THRESHOLD_170:	m_ui->pageProcedureMain_TachyComboBox->setCurrentIndex( 11 ); break;
			case TACHY_THRESHOLD_175:	m_ui->pageProcedureMain_TachyComboBox->setCurrentIndex( 12 ); break;
			case TACHY_THRESHOLD_180:	m_ui->pageProcedureMain_TachyComboBox->setCurrentIndex( 13 ); break;
			case TACHY_THRESHOLD_185:	m_ui->pageProcedureMain_TachyComboBox->setCurrentIndex( 14 ); break;
			case TACHY_THRESHOLD_190:	m_ui->pageProcedureMain_TachyComboBox->setCurrentIndex( 15 ); break;
			case TACHY_THRESHOLD_195:	m_ui->pageProcedureMain_TachyComboBox->setCurrentIndex( 16 ); break;
			case TACHY_THRESHOLD_200:	m_ui->pageProcedureMain_TachyComboBox->setCurrentIndex( 17 ); break;
			case TACHY_THRESHOLD_205:	m_ui->pageProcedureMain_TachyComboBox->setCurrentIndex( 18 ); break;
			case TACHY_THRESHOLD_210:	m_ui->pageProcedureMain_TachyComboBox->setCurrentIndex( 19 ); break;
			case TACHY_THRESHOLD_215:	m_ui->pageProcedureMain_TachyComboBox->setCurrentIndex( 20 ); break;
			case TACHY_THRESHOLD_220:	m_ui->pageProcedureMain_TachyComboBox->setCurrentIndex( 21 ); break;
			case TACHY_THRESHOLD_225:	m_ui->pageProcedureMain_TachyComboBox->setCurrentIndex( 22 ); break;
			case TACHY_THRESHOLD_230:	m_ui->pageProcedureMain_TachyComboBox->setCurrentIndex( 23 ); break;
			case TACHY_THRESHOLD_235:	m_ui->pageProcedureMain_TachyComboBox->setCurrentIndex( 24 ); break;
			case TACHY_THRESHOLD_240:	m_ui->pageProcedureMain_TachyComboBox->setCurrentIndex( 25 ); break;
			default:
				break;
		}
	} else {
		m_ui->pageProcedureMain_TachyComboBox->setCurrentIndex( 0 );
	}

	//Brady
	if ( m_configParams.analyzer_brady_on ) {
		switch ( m_configParams.analyzer_brady_threshold ) {
			case BRADY_THRESHOLD_15:	m_ui->pageProcedureMain_BradyComboBox->setCurrentIndex( 1 ); break;
			case BRADY_THRESHOLD_20:	m_ui->pageProcedureMain_BradyComboBox->setCurrentIndex( 2 ); break;
			case BRADY_THRESHOLD_25:	m_ui->pageProcedureMain_BradyComboBox->setCurrentIndex( 3 ); break;
			case BRADY_THRESHOLD_30:	m_ui->pageProcedureMain_BradyComboBox->setCurrentIndex( 4 ); break;
			case BRADY_THRESHOLD_35:	m_ui->pageProcedureMain_BradyComboBox->setCurrentIndex( 5 ); break;
			case BRADY_THRESHOLD_40:	m_ui->pageProcedureMain_BradyComboBox->setCurrentIndex( 6 ); break;
			case BRADY_THRESHOLD_45:	m_ui->pageProcedureMain_BradyComboBox->setCurrentIndex( 7 ); break;
			case BRADY_THRESHOLD_50:	m_ui->pageProcedureMain_BradyComboBox->setCurrentIndex( 8 ); break;
			case BRADY_THRESHOLD_55:	m_ui->pageProcedureMain_BradyComboBox->setCurrentIndex( 9 ); break;
			case BRADY_THRESHOLD_60:	m_ui->pageProcedureMain_BradyComboBox->setCurrentIndex( 10 ); break;
			default:
				break;
		}
	} else {
		m_ui->pageProcedureMain_BradyComboBox->setCurrentIndex( 0 );
	}

	//Pause
	if ( m_configParams.analyzer_pause_on ) {
		switch ( m_configParams.analyzer_pause_threshold ) {
			case PAUSE_THRESHOLD_1_5:	m_ui->pageProcedureMain_PauseComboBox->setCurrentIndex( 1 ); break;
			case PAUSE_THRESHOLD_2_0:	m_ui->pageProcedureMain_PauseComboBox->setCurrentIndex( 2 ); break;
			case PAUSE_THRESHOLD_2_5:	m_ui->pageProcedureMain_PauseComboBox->setCurrentIndex( 3 ); break;
			case PAUSE_THRESHOLD_3_0:	m_ui->pageProcedureMain_PauseComboBox->setCurrentIndex( 4 ); break;
			case PAUSE_THRESHOLD_3_5:	m_ui->pageProcedureMain_PauseComboBox->setCurrentIndex( 5 ); break;
			case PAUSE_THRESHOLD_4_0:	m_ui->pageProcedureMain_PauseComboBox->setCurrentIndex( 6 ); break;
			case PAUSE_THRESHOLD_4_5:	m_ui->pageProcedureMain_PauseComboBox->setCurrentIndex( 7 ); break;
			case PAUSE_THRESHOLD_5_0:	m_ui->pageProcedureMain_PauseComboBox->setCurrentIndex( 8 ); break;
			case PAUSE_THRESHOLD_5_5:	m_ui->pageProcedureMain_PauseComboBox->setCurrentIndex( 9 ); break;
			case PAUSE_THRESHOLD_6_0:	m_ui->pageProcedureMain_PauseComboBox->setCurrentIndex( 10 ); break;
			case PAUSE_THRESHOLD_6_5:	m_ui->pageProcedureMain_PauseComboBox->setCurrentIndex( 11 ); break;
			case PAUSE_THRESHOLD_7_0:	m_ui->pageProcedureMain_PauseComboBox->setCurrentIndex( 12 ); break;
			case PAUSE_THRESHOLD_7_5:	m_ui->pageProcedureMain_PauseComboBox->setCurrentIndex( 13 ); break;
			case PAUSE_THRESHOLD_8_0:	m_ui->pageProcedureMain_PauseComboBox->setCurrentIndex( 14 ); break;
			case PAUSE_THRESHOLD_8_5:	m_ui->pageProcedureMain_PauseComboBox->setCurrentIndex( 15 ); break;
			case PAUSE_THRESHOLD_9_0:	m_ui->pageProcedureMain_PauseComboBox->setCurrentIndex( 16 ); break;
			case PAUSE_THRESHOLD_9_5:	m_ui->pageProcedureMain_PauseComboBox->setCurrentIndex( 17 ); break;
			case PAUSE_THRESHOLD_10_0:	m_ui->pageProcedureMain_PauseComboBox->setCurrentIndex( 18 ); break;
			default:
				break;
		}
	} else {
		m_ui->pageProcedureMain_PauseComboBox->setCurrentIndex( 0 );
	}

	// Afib
	switch ( m_configParams.analyzer_af_on ) {
		case AF_OFF:
		case AF_START_ON:
			m_ui->pageProcedureMain_AfibComboBox->setCurrentIndex( m_configParams.analyzer_af_on );
			break;
		default:
			LOGMSG( "Unknown Afib setting(%u). Valid values are: 0, 1", m_configParams.analyzer_af_on );
			m_ui->pageProcedureMain_AfibComboBox->setCurrentIndex( DEFAULT_AF );
			break;
	}

	//Sample rate
	switch ( m_configParams.sample_rate ) {
		case SAMPLE_RATE_128:	m_ui->pageProcedureMain_SampleRateComboBox->setCurrentIndex( 0 ); break;
		case SAMPLE_RATE_256:	m_ui->pageProcedureMain_SampleRateComboBox->setCurrentIndex( 1 ); break;
		default:
			break;
	}

	// Pacemaker
	if ( m_configParams.analyzer_pace_on == 0 ) {
		m_ui->pageProcedureMain_PacemakerComboBox->setCurrentIndex( 0 );
	} else {
		m_ui->pageProcedureMain_PacemakerComboBox->setCurrentIndex( 1 );
	}

	if ( fullSetup ) {
        startStatusTimer();
	}
}

void SironaViewerUi::on_pageUSBSaved_LogoLabel_clicked()
{
	LOGMSG( "on_pageUSBSaved_LogoLabel_clicked" );
	previousPage = PAGE_USB_SAVED_SCREEN;
LOGMSG( "on_pageUSBSaved_LogoLabel_clicked() 126   going to   PAGE_COMPANY_SCREEN ..." );
	m_ui->contentStackedWidget->setCurrentIndex( PAGE_COMPANY_SCREEN );
}

void SironaViewerUi::on_pageUSBSaved_GearButton_clicked()
{
	LOGMSG( "on_pageUSBSaved_GearButton_clicked" );
	previousPage = PAGE_USB_SAVED_SCREEN;
LOGMSG( "on_pageUSBSaved_GearButton_clicked() 127   going to   PAGE_DEVICE_SCREEN ..." );
	m_ui->contentStackedWidget->setCurrentIndex( PAGE_DEVICE_SCREEN );
}

void SironaViewerUi::on_pageUSBSaved_BackButton_clicked()
{
	LOGMSG( "on_pageUSBSaved_BackButton_clicked" );
	previousPage = PAGE_USB_SAVED_SCREEN;
LOGMSG( "on_pageUSBSaved_BackButton_clicked() 128   going to   PAGE_PROCEDURE_SCREEN..." );
    m_ui->contentStackedWidget->setCurrentIndex(PAGE_PROCEDURE_SCREEN);
}

void SironaViewerUi::on_pageHookup_LogoLabel_clicked()
{
	LOGMSG( "on_pageHookup_LogoLabel_clicked" );
    previousPage = PAGE_HOOKUP_SCREEN;
LOGMSG( "on_pageHookup_LogoLabel_clicked() 129   going to   PAGE_COMPANY_SCREEN ..." );
    m_ui->contentStackedWidget->setCurrentIndex( PAGE_COMPANY_SCREEN );
}

void SironaViewerUi::on_pageHookup_GearButton_clicked()
{
	LOGMSG( "on_pageHookup_GearButton_clicked" );
    previousPage = PAGE_HOOKUP_SCREEN;
LOGMSG( "on_pageHookup_GearButton_clicked() 130   going to   PAGE_DEVICE_SCREEN ..." );
    m_ui->contentStackedWidget->setCurrentIndex( PAGE_DEVICE_SCREEN );
}

void SironaViewerUi::on_pageHookup_BackButton_clicked()
{
	LOGMSG( "on_pageHookup_BackButton_clicked" );
    previousPage = PAGE_HOOKUP_SCREEN;
    futurePage = PAGE_PROCEDURE_SCREEN;
    emit parameterRead( "PROC_STATE" );
}

void SironaViewerUi::on_pageHookup_NextButton_clicked()
{
	LOGMSG( "on_pageHookup_NextButton_clicked" );
    previousPage = PAGE_HOOKUP_SCREEN;
LOGMSG( "on_pageHookup_NextButton_clicked() 131   going to   PAGE_ECG_SCREEN ..." );
    m_ui->contentStackedWidget->setCurrentIndex( PAGE_ECG_SCREEN );
}

void SironaViewerUi::on_pageECG_LogoLabel_clicked()
{
	LOGMSG( "on_pageECG_LogoLabel_clicked" );
	m_exitScreenOperation = QPanelExitOperationLogoScreen;

	deviceHandler->deviceStopLiveECG();
}

void SironaViewerUi::on_pageECG_GearButton_clicked()
{
	LOGMSG( "on_pageECG_GearButton_clicked" );
	m_exitScreenOperation = QPanelExitOperationSettingsScreen;

	deviceHandler->deviceStopLiveECG();
}

void SironaViewerUi::on_pageECG_BackButton_clicked()
{
	LOGMSG( "on_pageECG_BackButton_clicked" );
	m_exitScreenOperation = QPanelExitOperationPreviousScreen;

	deviceHandler->deviceStopLiveECG();
}

void SironaViewerUi::on_pageECG_NextButton_clicked()
{
	LOGMSG( "on_pageECG_NextButton_clicked" );
	deviceHandler->deviceStopLiveECG();

	emit startProcedure();

LOGMSG( "on_pageECG_NextButton_clicked() 132   going to   PAGE_PROCEDURE_ACTIVE_SCREEN ..." );
	m_ui->contentStackedWidget->setCurrentIndex( PAGE_PROCEDURE_ACTIVE_SCREEN );
}

void SironaViewerUi::on_pageProcedureActive_LogoLabel_clicked()
{
	LOGMSG( "on_pageProcedureActive_LogoLabel_clicked" );
	previousPage = PAGE_PROCEDURE_ACTIVE_SCREEN;
LOGMSG( "on_pageProcedureActive_LogoLabel_clicked() 133   going to   PAGE_COMPANY_SCREEN ..." );
    m_ui->contentStackedWidget->setCurrentIndex( PAGE_COMPANY_SCREEN );
}

void SironaViewerUi::on_pageProcedureActive_GearButton_clicked()
{
	LOGMSG( "on_pageProcedureActive_GearButton_clicked" );
	previousPage = PAGE_PROCEDURE_ACTIVE_SCREEN;
LOGMSG( "on_pageProcedureActive_GearButton_clicked() 134   going to   PAGE_DEVICE_SCREEN ..." );
    m_ui->contentStackedWidget->setCurrentIndex( PAGE_DEVICE_SCREEN );
}

void SironaViewerUi::on_pageErase_GearButton_clicked()
{
	LOGMSG( "on_pageErase_GearButton_clicked" );
    previousPage = PAGE_ERASE_SCREEN;
    futurePage = PAGE_DEVICE_SCREEN;
}

void SironaViewerUi::on_pageProcedureActive_DisconnectButton_clicked()
{
	LOGMSG( "on_pageProcedureActive_DisconnectButton_clicked" );
	previousPage = PAGE_PROCEDURE_ACTIVE_SCREEN;
LOGMSG( "on_pageProcedureActive_DisconnectButton_clicked() 135   going to   PAGE_CONNECTIONS_SCREEN ..." );
    m_ui->contentStackedWidget->setCurrentIndex( PAGE_CONNECTIONS_SCREEN );
    deviceHandler->deviceStopLiveECG();
	emit disconnectPwmDevice( true, SIRONA_DRIVER_NO_ERROR );
}

void SironaViewerUi::on_pageProcedureActive_EndProcedureButton_clicked()
{
	LOGMSG( "on_pageProcedureActive_EndProcedureButton_clicked" );
	deviceCommunicationStatus = DEVICE_STOP_DEVICE_PROCEDURE;
LOGMSG( "on_pageProcedureActive_EndProcedureButton_clicked() 136   going to   PAGE_INITIAL_VALUE ..." );
    m_ui->contentStackedWidget->setCurrentIndex( PAGE_INITIAL_VALUE );
    emit stopProcedure();
}

void SironaViewerUi::on_pageDownloadOptions_LogoLabel_clicked()
{
    LOGMSG("on_pageDownloadOptions_LogoLabel_clicked");
    previousPage = PAGE_DOWNLOAD_OPTIONS_SCREEN;
    futurePage = PAGE_COMPANY_SCREEN;
}

void SironaViewerUi::on_pageDownloadOptions_GearButton_clicked()
{
    LOGMSG("on_pageDownloadOptions_GearButton_clicked");
    previousPage = PAGE_DOWNLOAD_OPTIONS_SCREEN;
    futurePage = PAGE_DEVICE_SCREEN;
}

void SironaViewerUi::on_pageDownloadOptions_ReviewButton_clicked()
{
    LOGMSG("on_pageDownloadOptions_ReviewButton_clicked");
    previousPage = PAGE_DOWNLOAD_OPTIONS_SCREEN;
    futurePage = PAGE_PATIENT_SCREEN;
	m_ui->contentStackedWidget->setCurrentIndex( PAGE_PATIENT_SCREEN );
}

void SironaViewerUi::on_pageDownloadOptions_DownloadButton_clicked()
{
    LOGMSG("on_pageDownloadOptions_DownloadButton_clicked");
    previousPage = PAGE_DOWNLOAD_OPTIONS_SCREEN;
    futurePage = PAGE_INITIAL_VALUE;        // isThereRecordedData will handle the page transition
    emit isThereRecordedData();
}

void SironaViewerUi::on_pageDownloadOptions_EraseButton_clicked()
{
    LOGMSG("on_pageDownloadOptions_EraseButton_clicked");
    previousPage = PAGE_DOWNLOAD_OPTIONS_SCREEN;
LOGMSG( "on_pageDownloadOptions_EraseButton_clicked() 137   going to   PAGE_ERASE_SCREEN..." );
    m_ui->contentStackedWidget->setCurrentIndex(PAGE_ERASE_SCREEN);
}

void SironaViewerUi::on_pageDownload_LogoLabel_clicked()
{
	LOGMSG( "on_pageDownload_LogoLabel_clicked" );
	previousPage = PAGE_DOWNLOAD_SCREEN;
LOGMSG( "on_pageDownload_LogoLabel_clicked() 138   going to   PAGE_COMPANY_SCREEN ..." );
	m_ui->contentStackedWidget->setCurrentIndex( PAGE_COMPANY_SCREEN );
}

void SironaViewerUi::on_pageDownload_GearButton_clicked()
{
	LOGMSG( "on_pageDownload_GearButton_clicked" );
	previousPage = PAGE_DOWNLOAD_SCREEN;
LOGMSG( "on_pageDownload_GearButton_clicked() 139   going to   PAGE_DEVICE_SCREEN ..." );
	m_ui->contentStackedWidget->setCurrentIndex( PAGE_DEVICE_SCREEN );
}

void SironaViewerUi::on_pageDownload_DisconnectButton_clicked()
{
	LOGMSG( "on_pageDownload_DisconnectButton_clicked" );
	previousPage = PAGE_DOWNLOAD_SCREEN;
LOGMSG( "on_pageDownload_DisconnectButton_clicked() 140   going to   PAGE_CONNECTIONS_SCREEN ..." );
	m_ui->contentStackedWidget->setCurrentIndex( PAGE_CONNECTIONS_SCREEN );
	deviceHandler->deviceStopWaitingForStorage();
    deviceHandler->deviceStopLiveECG();
	m_ui->pageDownload_Server_Message->setText( "" );
	emit disconnectPwmDevice( true, SIRONA_DRIVER_NO_ERROR );
}

void SironaViewerUi::on_pageErase_EraseButton_clicked()
{
    LOGMSG("on_pageErase_EraseButton_clicked");
    previousPage = PAGE_CONNECTIONS_SCREEN;
    futurePage = PAGE_COMPLETE_SCREEN;

	/* erase all data here */
	deviceCommunicationStatus = DEVICE_SOFT_RESET;
    m_ui->pageComplete_NewProcedureButton->setDisabled(true);
    m_ui->pageComplete_DownloadCompleteLabel->hide();       // we didn't download
    emit softReset();
    previousPage = PAGE_ERASE_SCREEN;
LOGMSG( "on_pageErase_EraseButton_clicked() 141   going to   PAGE_COMPLETE_SCREEN..." );
    m_ui->contentStackedWidget->setCurrentIndex(PAGE_COMPLETE_SCREEN);
}

void SironaViewerUi::on_pageErase_BackButton_clicked()
{
    LOGMSG("on_pageErase_BackButton_clicked");
LOGMSG( "on_pageErase_BackButton_clicked() 142   going to   previousPage..." );
    m_ui->contentStackedWidget->setCurrentIndex(previousPage);
}

void SironaViewerUi::on_pageComplete_LogoLabel_clicked()
{
	LOGMSG( "on_pageComplete_LogoLabel_clicked" );
    previousPage = PAGE_COMPLETE_SCREEN;
LOGMSG( "on_pageComplete_LogoLabel_clicked() 143   going to   PAGE_COMPANY_SCREEN ..." );
    m_ui->contentStackedWidget->setCurrentIndex( PAGE_COMPANY_SCREEN );
}

void SironaViewerUi::on_pageComplete_GearButton_clicked()
{
	LOGMSG( "on_pageComplete_GearButton_clicked" );
    previousPage = PAGE_COMPLETE_SCREEN;
LOGMSG( "on_pageComplete_GearButton_clicked() 144   going to   PAGE_DEVICE_SCREEN ..." );
    m_ui->contentStackedWidget->setCurrentIndex( PAGE_DEVICE_SCREEN );
}

void SironaViewerUi::pwmSoftResetComplete()
{
    LOGMSG( "pwmSoftResetComplete()   deviceSoftReset completed");
    downloadOptionsActive = false;
    m_ui->pageComplete_NewProcedureButton->setEnabled(true);

	if ( (m_configParams.procedure_state == PROCSTATE_IDLE) && (m_configParams.device_mode == PROCTYPE_NOT_SET) ) {
		m_ui->contentStackedWidget->setCurrentIndex( PAGE_CABLE_SCREEN );
	} else {
		m_ui->contentStackedWidget->setCurrentIndex( PAGE_PROCEDURE_SCREEN );
	}
}

void SironaViewerUi::on_pageComplete_NewProcedureButton_clicked()
{
	LOGMSG( "on_pageComplete_NewProcedureButton_clicked" );
	m_exitScreenOperation = QPanelExitOperationNextScreen;

    previousPage = PAGE_COMPLETE_SCREEN;
LOGMSG( "on_pageComplete_NewProcedureButton_clicked() 145   going to   PAGE_CABLE_SCREEN..." );
    m_ui->contentStackedWidget->setCurrentIndex(PAGE_CABLE_SCREEN);
    m_ui->pageComplete_DownloadCompleteLabel->show();     // reenabled for next time, in case it was hidden
}

void SironaViewerUi::on_pageDevice_LogoLabel_clicked()
{
	LOGMSG( "on_pageDevice_LogoLabel_clicked" );
LOGMSG( "on_pageDevice_LogoLabel_clicked() 146   going to   PAGE_COMPANY_SCREEN ..." );
    m_ui->contentStackedWidget->setCurrentIndex( PAGE_COMPANY_SCREEN );
}

void SironaViewerUi::on_pageDevice_GearButton_clicked()
{
	LOGMSG( "on_pageDevice_GearButton_clicked" );
LOGMSG( "on_pageDevice_GearButton_clicked() 147   going to   previousPage ..." );
    m_ui->contentStackedWidget->setCurrentIndex( previousPage );
}

void SironaViewerUi::on_pageDevice_BackButton_clicked()
{
	LOGMSG( "on_pageDevice_BackButton_clicked" );
LOGMSG( "on_pageDevice_BackButton_clicked() 148   going to   previousPage ..." );
    m_ui->contentStackedWidget->setCurrentIndex( previousPage );
}

void SironaViewerUi::on_pageDevice_NextButton_clicked()
{
	LOGMSG( "on_pageDevice_NextButton_clicked" );
LOGMSG( "on_pageDevice_NextButton_clicked() 149   going to   previousPage ..." );
    m_ui->contentStackedWidget->setCurrentIndex( previousPage );
}

void SironaViewerUi::on_pageDevice_DisconnectButton_clicked()
{
	LOGMSG( "on_pageDevice_DisconnectButton_clicked" );
	stopBluetoothTimer();
    deviceHandler->deviceStopLiveECG();
	emit disconnectPwmDevice( true, SIRONA_DRIVER_NO_ERROR );
}

void SironaViewerUi::on_pageDevice_MuteCheckBox_toggled( bool checked )
{
	LOGMSG( "on_pageDevice_MuteCheckBox_toggled" );
	ConfigParams confParams = m_configParams;

	if ( checked ) {
		confParams.audiable_on = AUDIABLE_OFF;
	} else {
		confParams.audiable_on = AUDIABLE_ON;
	}

	QList<QString> paramNamesList;
	paramNamesList.append( "AUDIO_MUTE" );
	deviceCommunicationStatus = DEVICE_WRITE_LIST_OF_PARAMETERS;
	emit parameterListWrite( confParams, paramNamesList );
}

void SironaViewerUi::pageProcedureActive_setupUI()
{
	if ( deviceConnectionStatus == USB_DEVICE_CONNECTED ) {
		m_ui->pageProcedureActive_DisconnectFrame->setVisible( false );
	} else if ( deviceConnectionStatus == BT_DEVICE_CONNECTED ) {
		m_ui->pageProcedureActive_DisconnectFrame->setVisible( true );
	}

    startStatusTimer();
}

void SironaViewerUi::pageDownloadOptions_setupUI()
{
    startStatusTimer();
}

void SironaViewerUi::pageErase_setupUI()
{
    startStatusTimer();
}

void SironaViewerUi::pageDevice_setupUI()
{
	LOGMSG( "pageDevice_setupUI" );
	m_ui->pageDevice_StatusFrame->setStyleSheet( "background-color: " + customer->color_contrast + ";" );
	m_ui->pageDevice_StatusLabel->setText( tr( "Monitor Functioning Properly" ) );
	m_ui->pageDevice_StatusFrame->hide();
	m_ui->pageDevice_DetailedStatusFrame->hide();

	if ( deviceConnectionStatus == USB_DEVICE_CONNECTED ) {
		m_ui->pageDevice_MonitorSignalFrame->setVisible( false );
	} else if ( deviceConnectionStatus == BT_DEVICE_CONNECTED ) {
		m_ui->pageDevice_MonitorSignalFrame->setVisible( false );
	}
	if ( m_ui->pageDevice_MonitorSignalProgressBar->value() <= 20 ) {
		m_ui->pageDevice_MonitorSignalProgressBar->setStyleSheet( "QProgressBar:horizontal { border: 0px solid gray; text-align: right; margin-right: 16ex} QProgressBar::chunk { background: red;}" );
	} else {
		m_ui->pageDevice_MonitorSignalProgressBar->setStyleSheet( "QProgressBar:horizontal { border: 0px solid gray; text-align: right; margin-right: 16ex} QProgressBar::chunk { background: green;}" );
	}
	m_ui->pageDevice_MonitorSignalProgressBar->update();


	if ( m_configParams.audiable_on == AUDIABLE_OFF ) {
		m_ui->pageDevice_MuteCheckBox->setChecked( true );
	} else if ( m_configParams.audiable_on == AUDIABLE_ON ) {
		m_ui->pageDevice_MuteCheckBox->setChecked( false );
	}

	m_ui->pageDevice_MonitorSNInfoLabel->setText( connectedDeviceSN );
	m_ui->pageDevice_MonitorModelNumberInfoLabel->setText( m_configParams.model_number );
	m_ui->pageDevice_MonitorFWVersionInfoLabel->setText( QString( "%1.%2.%3" ).arg( m_configParams.fw_rev & 0xFF ).arg( m_configParams.fw_rev >> 8 ).arg( m_configParams.svn_rev ) );
	m_ui->pageDevice_ApplicationVersionInfoLabel->setText( tr("Sirona Viewer %1").arg(REVISION_REV) );

	deviceCommunicationStatus = DEVICE_GET_BATTERY_STATUS;
	emit getBatteryStatus();
    startStatusTimer();
}

void SironaViewerUi::pageHookup_setupUI()
{
    if ( m_configParams.device_mode == PROCTYPE_POST ) {
		m_ui->pageHookup_TitleLabel->setText( tr( "Monitor Placement" ) );
	} else {
		m_ui->pageHookup_TitleLabel->setText( tr( "Attach Electrodes" ) );
	}

    startStatusTimer();
}

void SironaViewerUi::pageEcg_setupUI()
{
	m_ecgSeq = 0;

	m_ui->pageECG_MainWidget->clearData();

	m_ui->pageECG_NextButton->setEnabled( false );

	emit parameterRead( "CABLE_ID" );
}

void SironaViewerUi::pageComplete_setupUI()
{
    downloadOptionsActive = false;
    startStatusTimer();
}

void SironaViewerUi::on_pageCompany_NextButton_clicked()
{
	LOGMSG( "on_pageCompany_NextButton_clicked" );
LOGMSG( "on_pageCompany_NextButton_clicked() 150   going to   previousPage = %d ...", previousPage );
	m_ui->contentStackedWidget->setCurrentIndex( previousPage );

}

void SironaViewerUi::on_pageProcedureMain_SampleRateComboBox_activated( const QString &text )
{
	LOGMSG( "on_pageProcedureMain_SampleRateComboBox_activated()" );
	Q_UNUSED(text);
}

void SironaViewerUi::on_pageWelcome_LogoLabel_clicked()
{
	previousPage = PAGE_WELCOME_SCREEN;
LOGMSG( "on_pageWelcome_LogoLabel_clicked() 151   going to   PAGE_COMPANY_SCREEN ..." );
	m_ui->contentStackedWidget->setCurrentIndex( PAGE_COMPANY_SCREEN );
}

void SironaViewerUi::on_pageConnection_ChooseDevicelistWidget_itemDoubleClicked( QListWidgetItem *item )
{
	Q_UNUSED( item );

	on_pageConnection_NextButton_clicked();
}

void SironaViewerUi::on_pageConnection_ChooseDevicelistWidget_itemSelectionChanged()
{
	if ( m_ui->pageConnection_ChooseDevicelistWidget->selectedItems().count() == 0 ) {
		m_ui->pageConnection_NextButton->setEnabled( false );
	} else {
		m_ui->pageConnection_NextButton->setEnabled( true );
		if ( glbUsbImmediateSelection ) {
#ifdef ANTIQUE
			FloatingDisplay *infobox = new FloatingDisplay(NULL);
			infobox->append("Auto-Connecting...");
#else
#endif
			on_pageConnection_NextButton_clicked();
		}
	}
}

void SironaViewerUi::startEnumerateTimer()
{
	stopEnumerateTimer();

	m_enumerateTimer = startTimer( 50 );
    //LOGMSG( "Create m_enumerateTimer = %d", m_enumerateTimer );
}

void SironaViewerUi::stopEnumerateTimer()
{
	if ( m_enumerateTimer ) {
		killTimer( m_enumerateTimer );
		m_enumerateTimer = 0;
	}
}

void SironaViewerUi::startStatusTimer()
{
    stopStatusTimer();
    emit getStatus();
    m_statusTimer = startTimer( 500 );
}

void SironaViewerUi::stopStatusTimer()
{
    if (m_statusTimer) {
        killTimer( m_statusTimer );
        m_statusTimer = 0;
    }
}

void SironaViewerUi::enumPwmDevices()
{
	int retVal = SIRONA_DRIVER_NO_ERROR;
	sirona_enum_handle enum_handle = nullptr;
	QList<deviceInfo_t> devices;

	LOGMSG("enumPwmDevices()");

	retVal = sironadriver_enum_open( &enum_handle, SIRONA_DRIVER_DEVICE_USB );

	if ( retVal == SIRONA_DRIVER_NO_ERROR ) {

		deviceInfo_t device;

		while ( ( retVal = sironadriver_enum_next( enum_handle, device.devSerialNumber, device.devPortName, &device.devType ) ) == SIRONA_DRIVER_NO_ERROR ) {
			devices.append( device );
			LOGMSG("enumPwmDevices()   found device: %s", device.devSerialNumber );
		}

		sironadriver_enum_close( enum_handle );
	}

	// Delete current USB devices.
	for ( int c = m_ui->pageConnection_ChooseDevicelistWidget->count() - 1; c >= 0; c-- ) {
		QListWidgetItem *item = nullptr;

		item = m_ui->pageConnection_ChooseDevicelistWidget->item( c );

		if ( item->data( Qt::UserRole + 1 ) == SIRONA_DRIVER_DEVICE_USB ) {
			m_ui->pageConnection_ChooseDevicelistWidget->removeItemWidget( item );
			delete item;
			item = nullptr;
		} else {
			for ( auto device : devices ) {
				if ( item->text().compare( QString::fromLatin1( device.devSerialNumber ), Qt::CaseInsensitive ) == 0 ) {
					m_ui->pageConnection_ChooseDevicelistWidget->removeItemWidget( item );
					delete item;
					item = nullptr;
					break;
				}
			}
		}
	}

	// Add detected USB devices.
	for ( auto device : devices ) {

		QString sn_prefix = ( QString ) device.devSerialNumber[0] + ( QString ) device.devSerialNumber[1] + ( QString ) device.devSerialNumber[2] + ( QString ) device.devSerialNumber[3];
		QListWidgetItem *item = nullptr;

		item = new QListWidgetItem();
		item->setText( QString( device.devSerialNumber ).toUpper() );
        if ((customer->serial_number_prefix.contains(sn_prefix.toUpper())) || (customer->manufacturing)) {
			item->setIcon( m_usb_image );
		} else {
			item->setIcon( m_caution_symbol );
		}
		item->setData( Qt::UserRole, device.devPortName );
		item->setData( Qt::UserRole + 1, SIRONA_DRIVER_DEVICE_USB );
		item->setTextAlignment( Qt::AlignHCenter );

		item->setData( Qt::UserRole + 2, QDateTime::currentDateTime() );
		m_ui->pageConnection_ChooseDevicelistWidget->insertItem( 0, item );

		if ( glbUsbImmediateSelection ) {
			m_ui->pageConnection_ChooseDevicelistWidget->setCurrentItem( item );
			break;
		}
	}
}

void SironaViewerUi::on_pageProcedureMain_RecordingLengthSelector_indexChanged( int index )
{
	int value = m_ui->pageProcedureMain_RecordingLengthSelector->items().at( index ).toInt();

	m_ui->pageProcedureMain_ManualRecordingLengthSpinBox->setValue( value );
}

void SironaViewerUi::on_pageProcedureMain_ManualRecordingLengthSpinBox_valueChanged( int value )
{
	int index = -1;

	if ( value == 1 ) {
		m_ui->pageProcedureMain_ManualRecordingLengthSpinBox->setSuffix( tr( " day" ) );
	} else {
		m_ui->pageProcedureMain_ManualRecordingLengthSpinBox->setSuffix( tr( " days" ) );
	}

	const QStringList items = m_ui->pageProcedureMain_RecordingLengthSelector->items();

	for ( int c = 0; c < items.count(); c++ ) {
		if ( value == items.at( c ).toInt() ) {
			index = c;

			break;
		}
	}

	m_ui->pageProcedureMain_RecordingLengthSelector->setCurrentIndex( index );
	m_ui->pageProcedureMain_ManualRecordingLengthSpinBox->setValue( value );

	if ( value == 21 ) {
		m_ui->pageProcedureMain_SampleRateComboBox->setCurrentIndex(0);
		m_configParams.sample_rate = SAMPLE_RATE_128;
	}
}

void SironaViewerUi::on_webAddressLabel_linkActivated( const QString &link )
{
	QDesktopServices::openUrl( link );
}

void SironaViewerUi::preprocess_stylesheet( QString *text )
{
	text->replace( "@main-color", customer->color_main );
	text->replace( "@accent1", customer->color_accent1 );
	text->replace( "@accent2", customer->color_accent2 );
	text->replace( "@contrast-color", customer->color_contrast );
	text->replace( "@caution-color", customer->color_caution );
	text->replace( "@disabled-color", customer->color_disabled );

}


void SironaViewerUi::on_edtSiteCode_editingFinished()
{
	// qDebug() << "on_edtSiteCode_editingFinished()";
	getTextCompleterThing( m_ui->edtSiteCode, "sitecode" );
}

void SironaViewerUi::on_edtSiteCode_textChanged( const QString &str )
{
	// qDebug() << QString("on_edtSiteCode_textChanged(%1)").arg(str);
	m_configParams.sitecode = m_ui->edtSiteCode->text();
	deviceHandler->setZipPrefix( m_configParams.sitecode );
	m_ui->pageWelcome_NextButton->setEnabled( (str.count() > 0) );
}

void SironaViewerUi::on_edtDiagnosis_editingFinished()
{
	getTextCompleterThing( m_ui->edtDiagnosis, "diagnosis" );
}

void SironaViewerUi::on_edtSymptoms_editingFinished()
{
	getTextCompleterThing( m_ui->edtSymptoms, "symptoms" );
}

void SironaViewerUi::on_edtActivity_editingFinished()
{
	getTextCompleterThing( m_ui->edtActivity, "activities" );
}


void SironaViewerUi::on_btnAddDiaryEntry_clicked()
{
	// qDebug() << "on_btnAddDiaryEntry_clicked()";

	int row = m_ui->tableDiaryEntries->rowCount();

	QString theDate = m_ui->dateTimeEditDiaryEntry->date().toString("MMM dd, yyyy");
	QString theTime = m_ui->dateTimeEditDiaryEntry->time().toString("h:mm a");
	QTableWidgetItem *itemDate = new QTableWidgetItem(tr("%1").arg( theDate ));
	QTableWidgetItem *itemTime = new QTableWidgetItem(tr("%1").arg( theTime ));
	QTableWidgetItem *itemSymptom = new QTableWidgetItem( m_ui->edtSymptoms->text() );
	QTableWidgetItem *itemActivity = new QTableWidgetItem( m_ui->edtActivity->text() );
	QTableWidgetItem *itemDelete = new QTableWidgetItem( "Delete" );

	itemDate->setData( Qt::UserRole, (m_ui->dateTimeEditDiaryEntry->dateTime()) );

	QFont fontDelete("Times", 11, QFont::Bold, false );
	fontDelete.setUnderline(true);
	itemDelete->setFont( fontDelete );
	itemDelete->setForeground((Qt::darkRed));


	m_ui->tableDiaryEntries->setRowCount( row + 1 );

	m_ui->tableDiaryEntries->setItem( row, 0, itemDate );
	m_ui->tableDiaryEntries->setItem( row, 1, itemTime );
	m_ui->tableDiaryEntries->setItem( row, 2, itemSymptom );
	m_ui->tableDiaryEntries->setItem( row, 3, itemActivity );
	m_ui->tableDiaryEntries->setItem( row, 4, itemDelete );

	for ( int col = 0 ; col < m_ui->tableDiaryEntries->columnCount() ; col++ ) {
		m_ui->tableDiaryEntries->item( row , col )->setTextAlignment(Qt::AlignCenter);
	}

	QTimer::singleShot( 10, m_ui->tableDiaryEntries, SLOT(scrollToBottom()) );

    patientFieldChanged();

	// qDebug() << "on_btnAddDiaryEntry_clicked()   patient table: " << getDiaryEntries();
}


void SironaViewerUi::tableItemClicked( QTableWidgetItem *item )
{
	QString colName = m_ui->tableDiaryEntries->horizontalHeaderItem( item->column() )->text();
	// qDebug() << tr("tableItemClicked( QTableWidgetItem *item ) is on row %1    for column: %2    item->text: %3").arg(item->row()).arg(colName).arg(item->text());
	if ( item->text() == "Delete" /* colName.isEmpty() */ ) {
		m_ui->tableDiaryEntries->removeRow( item->row() );
	}

    patientFieldChanged();
}



QList< QHash<QString,QString> > SironaViewerUi::getDiaryEntries()
{
	QList< QHash<QString,QString> > lstHashDiaryEntries;

	for ( int row = 0 ; row < m_ui->tableDiaryEntries->rowCount() ; row++ ) {

		QHash<QString,QString> entry;

		QDateTime dt = m_ui->tableDiaryEntries->item(row,0)->data(Qt::UserRole).toDateTime();

		entry["Date"] = dt.toString("ddd MMM d yyyy");
		entry["Event"] = m_ui->tableDiaryEntries->item(row,2)->text();
		entry["Symptoms"] = m_ui->tableDiaryEntries->item(row,3)->text();
		entry["Time"] = dt.toString("HH:mm:00");

		lstHashDiaryEntries.append( entry );
	}

	// qDebug() << "getDiaryEntries()" << lstHashDiaryEntries;

	return lstHashDiaryEntries;
}


void SironaViewerUi::on_cbNoDiaryEventsNoted_toggled(bool checked)
{
	LOGMSG("on_cbNoDiaryEventsNoted_toggled(%s)", checked ? "true" : "false" );

	if ( m_ui->tableDiaryEntries->rowCount() <= 0 ) {
		m_ui->btnAddDiaryEntry->setEnabled( ! checked );
		if ( checked ) {
			m_ui->btnAddDiaryEntry->setStyleSheet( " background: lightgrey; color: darkgrey; border-color: darkgrey"  );
		} else {
			m_ui->btnAddDiaryEntry->setStyleSheet( " background: white; color: " + customer->color_caution + ";" + " border-color: " + customer->color_caution + ";" );
		}
	}

    patientFieldChanged();
}


