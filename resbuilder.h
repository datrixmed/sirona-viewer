#ifndef RESBUILDER_H
#define RESBUILDER_H

bool buildRes(char *serialNumber, unsigned channelCount, unsigned sampleRate, uint32_t startTime, QFile *datFile);

#endif // RESBUILDER_H
