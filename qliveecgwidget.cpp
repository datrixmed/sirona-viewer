#include "qliveecgwidget.h"
#include <QPaintEvent>
#include <QPainterPath>
#include <QDateTime>
#include <QPainter>

#define LIVE_ECG_SPS 128
#define ECG_BUFFER_SIZE ( LIVE_ECG_SPS * 5 )
#define SAMPLES_ON_WIDGET ( LIVE_ECG_SPS * 4 )
#define SAMPLE_GAP ( ( LIVE_ECG_SPS * 2 ) / 10 )

#define Qt__brown QRgb( 0x835c3b )
#define Qt__orange QRgb( 0xffa500 )

QLiveECGWidget::QLiveECGWidget( QWidget *parent )
	: QWidget( parent )
	, m_lastSampleWasMissing( false )
	, m_startTime( 0 )
	, m_currentXPosition( 0 )
	, m_channelCount( 0 )
    , m_CableId( CABLETYPE_UNKNOWN )
	, m_xScale( 1.0 )
	, m_yScale( 1.0 )
	, m_backgroundColor( 249, 252, 255 )
	, m_missingEcgPen( QColor( 49, 222, 195, 128 ) )
	, m_ecgPen( QColor( 49, 222, 195 ) )
	, m_gridPen( QColor( 152, 198, 224 ) )
{
	m_lastGoodSmpleYPos[0] = 0;
	m_lastGoodSmpleYPos[1] = 0;
	m_lastGoodSmpleYPos[2] = 0;

	m_ecgPen.setWidth( 2.5f );
	m_missingEcgPen.setWidth( 2.5f );
}

void QLiveECGWidget::clearData()
{
	for ( int ch = 0; ch < 3; ch++ ) {
		m_channelData[ch].clear();
	}

	m_channelCount = 0;
    m_CableId = CABLETYPE_UNKNOWN;

	m_yOffset.clear();

	m_currentXPosition = 0;

	update();
}

int QLiveECGWidget::channelCount()
{
	return m_channelCount;
}

void QLiveECGWidget::setCableId( quint8 type )
{
	clearData();

	switch ( type ) {
        case CABLETYPE_I_CHAN_3_WIRE:
        case CABLETYPE_I_CHAN_2_WIRE:
        case CABLETYPE_I_CHAN_POST:
			m_channelCount = 1;
			break;
        case CABLETYPE_II_CHAN_3_WIRE:
        case CABLETYPE_II_CHAN_5_WIRE:
        case CABLETYPE_II_CHAN_PATCH:
			m_channelCount = 2;
			break;
        case CABLETYPE_III_CHAN_5_WIRE:
        case CABLETYPE_III_CHAN_7_WIRE:
			m_channelCount = 3;
			break;
		default:
			m_channelCount = 0;
			break;
	}

	m_CableId = type;

	updateOffsets();

	update();
}

void QLiveECGWidget::setDeviceMode( quint8 type )
{
	m_deviceMode = type;
	return;
}

void QLiveECGWidget::addSample( int channel1, int channel2, int channel3 )
{
	switch ( m_channelCount ) {
		case 1:
			if ( channel1 > 102 ) {
				channel1 = 102;
			} else if ( channel1 < -102 ) {
				channel1 = -102;
			}
			if ( channel2 > 102 ) {
				channel2 = 102;
			} else if ( channel2 < -102 ) {
				channel2 = -102;
			}
			if ( channel3 > 102 ) {
				channel3 = 102;
			} else if ( channel3 < -102 ) {
				channel3 = -102;
			}
			break;
		case 2:
			if ( channel1 > 51 ) {
				channel1 = 51;
			} else if ( channel1 < -51 ) {
				channel1 = -51;
			}
			if ( channel2 > 51 ) {
				channel2 = 51;
			} else if ( channel2 < -51 ) {
				channel2 = -51;
			}
			if ( channel3 > 51 ) {
				channel3 = 51;
			} else if ( channel3 < -51 ) {
				channel3 = -51;
			}
			break;
		case 3:
			if ( channel1 > 34 ) {
				channel1 = 34;
			} else if ( channel1 < -34 ) {
				channel1 = -34;
			}
			if ( channel2 > 34 ) {
				channel2 = 34;
			} else if ( channel2 < -34 ) {
				channel2 = -34;
			}
			if ( channel3 > 34 ) {
				channel3 = 34;
			} else if ( channel3 < -34 ) {
				channel3 = -34;
			}
			break;
	}

	switch ( m_channelCount ) {
		case 3:
			m_channelData[2].append( channel3 );
		case 2:
			m_channelData[1].append( channel2 );
		case 1:
			m_channelData[0].append( channel1 );
			break;
		default:
			return;
	}

	trimOldData();

	m_currentXPosition = ( m_currentXPosition + 1 ) % SAMPLES_ON_WIDGET;

	update();
}

void QLiveECGWidget::addMissingSample()
{
	switch ( m_channelCount ) {
		case 3:
			m_channelData[2].append( INT_MAX );
		case 2:
			m_channelData[1].append( INT_MAX );
		case 1:
			m_channelData[0].append( INT_MAX );
			break;
		default:
			return;
	}

	trimOldData();

	m_currentXPosition = ( m_currentXPosition + 1 ) % SAMPLES_ON_WIDGET;

	update();
}

void QLiveECGWidget::paintEvent( QPaintEvent *event )
{
	QPainter painter( this );
	QPainterPath ecgPath, missingEcgPath;

	updateEcgSections();

	for ( int ch = 0; ch < m_channelCount; ch++ ) {

		for ( int section = 0; section < 2; section++ ) {
			if ( m_ecgTraceSections[section].length == 0 ) {
				continue;
			}

			for ( int index = 0; index < m_ecgTraceSections[section].length; index++ ) {
				qreal x = ( m_ecgTraceSections[section].latestXPos - index ) * m_xScale;

				int sampleVal = m_channelData[ch].at( m_ecgTraceSections[section].latestSampPos - index );

				if ( sampleVal != INT_MAX ) {
					qreal y = m_yOffset[ch] - ( sampleVal * m_yScale );

					if ( index == 0 ) {
						ecgPath.moveTo( x, y );
					} else {
						ecgPath.lineTo( x, y );
					}

					m_lastGoodSmpleYPos[ch] = y;
					m_lastSampleWasMissing = false;
				} else {
					ecgPath.moveTo( x, m_lastGoodSmpleYPos[ch] );

					if ( index == 0 ) {
						missingEcgPath.moveTo( x, m_lastGoodSmpleYPos[ch] );
					} else {
						missingEcgPath.lineTo( x, m_lastGoodSmpleYPos[ch] );
					}
					m_lastSampleWasMissing = true;
				}
			}
		}
	}

	painter.fillRect( event->rect(), m_backgroundColor );

	painter.setPen( m_gridPen );
	painter.drawPath( m_gridPath );

	painter.setRenderHint( QPainter::Antialiasing );

	painter.setPen( m_missingEcgPen );
	painter.drawPath( missingEcgPath );
	painter.setPen( m_ecgPen );
	painter.drawPath( ecgPath );

	drawLeadIndicators( painter );
}

void QLiveECGWidget::resizeEvent( QResizeEvent *event )
{
	qreal width = event->size().width();
	qreal height = event->size().height();
	qreal fiveMM = ( width / 4 ) / 5;

	m_xScale = width / SAMPLES_ON_WIDGET;
	m_yScale = ( width / 10.0 ) / 25.6;

	m_gridPath = QPainterPath();

	for ( int x = 0; x <= 4 * 5; x++ ) {
		qreal xVal = fiveMM * x;
		m_gridPath.moveTo( xVal, 0.0 );
		m_gridPath.lineTo( xVal, height );
	}

	qreal yVal = height;
	while ( yVal >= 0 ) {

		m_gridPath.moveTo( 0.0, yVal );
		m_gridPath.lineTo( width, yVal );

		yVal -= fiveMM;
	}

	updateOffsets();
}

void QLiveECGWidget::updateOffsets()
{
	switch ( m_channelCount ) {
		case 1:
			m_yOffset.resize( 1 );
			m_yOffset[0] = ( height() * 1 ) / 2;
			break;
		case 2:
			m_yOffset.resize( 2 );
			m_yOffset[0] = ( height() * 1 ) / 4;
			m_yOffset[1] = ( height() * 3 ) / 4;
			break;
		case 3:
			m_yOffset.resize( 3 );
			m_yOffset[0] = ( height() * 1 ) / 6;
			m_yOffset[1] = ( height() * 3 ) / 6;
			m_yOffset[2] = ( height() * 5 ) / 6;
			break;
		default:
			m_yOffset.clear();
			break;
	}
}

void QLiveECGWidget::updateEcgSections()
{
	int samplesCount = m_channelData[0].count();

	if ( samplesCount <= ( SAMPLES_ON_WIDGET - SAMPLE_GAP ) ) {
		m_ecgTraceSections[0].latestXPos = samplesCount - 1;
		m_ecgTraceSections[0].latestSampPos = samplesCount - 1;
		m_ecgTraceSections[0].length = samplesCount;
		m_ecgTraceSections[1].latestXPos = 0;
		m_ecgTraceSections[1].latestXPos = 0;
		m_ecgTraceSections[1].length = 0;
	} else {
		if ( m_currentXPosition >= ( SAMPLES_ON_WIDGET - SAMPLE_GAP ) ) {
			m_ecgTraceSections[0].latestXPos = m_currentXPosition;
			m_ecgTraceSections[0].latestSampPos = samplesCount - 1;
			m_ecgTraceSections[0].length = ( SAMPLES_ON_WIDGET - SAMPLE_GAP );
			m_ecgTraceSections[1].latestXPos = 0;
			m_ecgTraceSections[1].latestXPos = 0;
			m_ecgTraceSections[1].length = 0;
		} else {
			m_ecgTraceSections[0].latestXPos = m_currentXPosition;
			m_ecgTraceSections[0].latestSampPos = samplesCount - 1;
			m_ecgTraceSections[0].length = m_currentXPosition;
			m_ecgTraceSections[1].latestXPos = SAMPLES_ON_WIDGET;
			m_ecgTraceSections[1].latestSampPos = samplesCount - 1 - m_currentXPosition;
			m_ecgTraceSections[1].length = ( SAMPLES_ON_WIDGET - SAMPLE_GAP ) - m_currentXPosition;
		}
	}
}

void QLiveECGWidget::trimOldData()
{
	for ( int ch = 0; ch < 3; ch++ ) {
		while ( m_channelData[ch].count() > ECG_BUFFER_SIZE ) {
			m_channelData[ch].removeFirst();
		}
	}
}

void QLiveECGWidget::drawLeadIndicators( QPainter &painter )
{
	const int LEAD_INDICATOR_OFFSET = 15;
	const int CIRCLE_OFFSET = 30;

	int x = CIRCLE_OFFSET;
	int y = 0;

	switch ( m_CableId ) {
        case CABLETYPE_I_CHAN_2_WIRE:
        case CABLETYPE_I_CHAN_3_WIRE:
			y = m_yOffset[0] - LEAD_INDICATOR_OFFSET;
			drawLeadIndicator( painter, x, y, Qt::red );
			y = m_yOffset[0] + LEAD_INDICATOR_OFFSET;
			drawLeadIndicator( painter, x, y, Qt::white );
			break;
        case CABLETYPE_II_CHAN_3_WIRE:
			y = m_yOffset[0] - LEAD_INDICATOR_OFFSET;
			drawLeadIndicator( painter, x, y, Qt::red );
			y = m_yOffset[0] + LEAD_INDICATOR_OFFSET;
			drawLeadIndicator( painter, x, y, Qt::white );

			y = m_yOffset[1] - LEAD_INDICATOR_OFFSET;
			drawLeadIndicator( painter, x, y, Qt::black );
			y = m_yOffset[1] + LEAD_INDICATOR_OFFSET;
			drawLeadIndicator( painter, x, y, Qt::white );
			break;
        case CABLETYPE_II_CHAN_5_WIRE:
			y = m_yOffset[0] - LEAD_INDICATOR_OFFSET;
			drawLeadIndicator( painter, x, y, Qt::red );
			y = m_yOffset[0] + LEAD_INDICATOR_OFFSET;
			drawLeadIndicator( painter, x, y, Qt::white );

			y = m_yOffset[1] - LEAD_INDICATOR_OFFSET;
			drawLeadIndicator( painter, x, y, Qt__brown );
			y = m_yOffset[1] + LEAD_INDICATOR_OFFSET;
			drawLeadIndicator( painter, x, y, Qt::black );
			break;
        case CABLETYPE_III_CHAN_5_WIRE:
			y = m_yOffset[0] - LEAD_INDICATOR_OFFSET;
			drawLeadIndicator( painter, x, y, Qt__brown );
			y = m_yOffset[0] + LEAD_INDICATOR_OFFSET;
			drawLeadIndicator( painter, x, y, Qt::red );

			y = m_yOffset[1] - LEAD_INDICATOR_OFFSET;
			drawLeadIndicator( painter, x, y, Qt::black );
			y = m_yOffset[1] + LEAD_INDICATOR_OFFSET;
			drawLeadIndicator( painter, x, y, Qt::red );

			y = m_yOffset[2] - LEAD_INDICATOR_OFFSET;
			drawLeadIndicator( painter, x, y, Qt::black );
			y = m_yOffset[2] + LEAD_INDICATOR_OFFSET;
			drawLeadIndicator( painter, x, y, Qt::white );
			break;
        case CABLETYPE_III_CHAN_7_WIRE:
			y = m_yOffset[0] - LEAD_INDICATOR_OFFSET;
			drawLeadIndicator( painter, x, y, Qt::red );
			y = m_yOffset[0] + LEAD_INDICATOR_OFFSET;
			drawLeadIndicator( painter, x, y, Qt::white );

			y = m_yOffset[1] - LEAD_INDICATOR_OFFSET;
			drawLeadIndicator( painter, x, y, Qt__brown );
			y = m_yOffset[1] + LEAD_INDICATOR_OFFSET;
			drawLeadIndicator( painter, x, y, Qt::black );

			y = m_yOffset[2] - LEAD_INDICATOR_OFFSET;
			drawLeadIndicator( painter, x, y, Qt__orange );
			y = m_yOffset[2] + LEAD_INDICATOR_OFFSET;
			drawLeadIndicator( painter, x, y, Qt::blue );
			break;
        case CABLETYPE_II_CHAN_PATCH:
        case CABLETYPE_I_CHAN_POST:
		default:
			break;
	}
}

void QLiveECGWidget::drawLeadIndicator( QPainter &painter, int x, int y, QColor color )
{
	const int LEAD_INDICATOR_SIZE = 20;

	color.setAlpha( 180 );

	QPen pen( Qt::black );
	QBrush brush( color );

	pen.setWidth( 2 );

	painter.setPen( pen );
	painter.setBrush( brush );

	painter.drawEllipse( x, y, LEAD_INDICATOR_SIZE, LEAD_INDICATOR_SIZE );
}
