#pragma once

#include <QListWidget>
#include <QMouseEvent>
#include <QWidget>

class QDeselectListWidget : public QListWidget
{
public:
	QDeselectListWidget( QWidget *parent = nullptr );

protected:
	void mousePressEvent( QMouseEvent *event );
	void mouseReleaseEvent( QMouseEvent *event );
	void mouseMoveEvent( QMouseEvent *event );

private:
	bool m_mousePress;
};
