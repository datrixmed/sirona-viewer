
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

#ifdef _WIN32
#include <io.h>
#endif // _WIN32

#include "utilities.h"
#include "curl_client.h"



curl_client *curl_client::current_curl = nullptr;

curl_client::curl_client( QObject *parent, CustomerInfo *customer_defs )
{
	this->setParent( parent );
	customer = customer_defs;
	curl_global_init( CURL_GLOBAL_ALL );
	current_curl = this;
    sendFileResult = CURLE_FAILED_INIT;     // haven't called sendFile yet
}

int curl_client::sendFile( QByteArray file_name, ftp_url_info url_info )
{
	FILE *hd_src;
	struct stat file_info;
	curl_off_t fsize;
	curl = curl_easy_init();
    sendFileResult = CURLE_FAILED_INIT;

	struct curl_slist *headerlist = NULL;

	QByteArray short_file_name = file_name.right( file_name.length() - file_name.lastIndexOf( "/" ) - 1 );

	/* get the file size of the local file */
	if ( stat( file_name, &file_info ) ) {
		/*couldn't open file*/
		return 1;
	}
	fsize = ( curl_off_t )file_info.st_size;

	/* get a FILE * of the same file */
	hd_src = fopen( file_name, "rb" );

	if ( curl ) {
		/* we want to use our own read function */
		curl_easy_setopt( curl, CURLOPT_READFUNCTION, read_callback );

		/* enable uploading */
		curl_easy_setopt( curl, CURLOPT_UPLOAD, 1L );

		/* specify target */
		QByteArray remote_url = "";
		remote_url.append( url_info.scheme + "://" );
        remote_url.append(url_info.host + ":" + url_info.port + QUrl::toPercentEncoding(url_info.directory, "/", ""));
		remote_url.append( short_file_name );
		curl_easy_setopt( curl, CURLOPT_URL, remote_url.data() );

        curl_easy_setopt(curl, CURLOPT_USERNAME, url_info.username.toLatin1().data());
        curl_easy_setopt(curl, CURLOPT_PASSWORD, url_info.password.toLatin1().data());

		/* now specify which file to upload */
		curl_easy_setopt( curl, CURLOPT_READDATA, hd_src );

		/* Set the size of the file to upload (optional).  If you give a *_LARGE
		   option you MUST make sure that the type of the passed-in argument is a
		   curl_off_t. If you use CURLOPT_INFILESIZE (without _LARGE) you must
		   make sure that to pass in a type 'long' argument. */
		curl_easy_setopt( curl, CURLOPT_INFILESIZE_LARGE, ( curl_off_t )fsize );

		/* Set curl to show progress */
		curl_easy_setopt( curl, CURLOPT_NOPROGRESS, 0 );
		curl_easy_setopt( curl, CURLOPT_PROGRESSFUNCTION, progress_callback );

		if ( url_info.security > NO_TLS ) {
			/* We activate SSL and we require it for both control and data */
			curl_easy_setopt( curl, CURLOPT_USE_SSL, CURLUSESSL_ALL );

			/*turn off certificate verification (not great)*/
			curl_easy_setopt( curl, CURLOPT_SSL_VERIFYPEER, 0 );
		}

		/* Now run off and do what you've been told! */
        sendFileResult = curl_easy_perform(curl);
        LOGMSG("cURL upload result: %s", curl_easy_strerror(sendFileResult));

		/* clean up the FTP commands list */
		curl_slist_free_all( headerlist );

		/* always cleanup */
		curl_easy_cleanup( curl );
	}
	fclose( hd_src ); /* close the local file */

    return (sendFileResult != CURLE_OK);
}

size_t curl_client::read_callback( FILE *ptr, size_t size, size_t nmemb, FILE *stream )
{
	curl_off_t nread;
	/* in real-world cases, this would probably get this data differently
	   as this fread() stuff is exactly what the library already would do
	   by default internally */
	size_t retcode = fread( ptr, size, nmemb, stream );

	nread = ( curl_off_t )retcode;

	LOGMSG( "*** We read %ld bytes from file", (long) nread );
	emit current_curl->reloadTimer();

	return retcode;
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
int curl_client::progress_callback( void *clientp, double dltotal, double dlnow, double ultotal, double ulnow )
{
	if ( ultotal == 0 ) {
		return 0;
	}
	qint64 currentPercentage = lroundf( ( float ) ulnow / ( float )ultotal * 100. );
	if ( currentPercentage != current_curl->lastReportedPercentage ) {
		current_curl->lastReportedPercentage = currentPercentage;
		emit current_curl->percentageChanged( currentPercentage );
        emit current_curl->reloadTimer();
    }
	return 0;
}
#pragma GCC diagnostic pop

const char *curl_client::getErrorString()
{
    if ((sendFileResult == CURLE_OK) || (sendFileResult == CURLE_FAILED_INIT)) {
        return NULL;        // no useful error message
    }
    return curl_easy_strerror(sendFileResult);
}

curl_client::~curl_client()
{
	curl_global_cleanup();
}
