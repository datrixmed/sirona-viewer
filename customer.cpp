

#include <QJsonDocument>
#include <QJsonObject>
#include <QSettings>
#include <QDebug>

#include "customer.h"
#include "cryptjson.h"



CustomerInfo::CustomerInfo( QObject *parent )
{
	this->setParent( parent );

	load();
}


CustomerInfo::~CustomerInfo()
{
}


bool CustomerInfo::hasConfig( QString encryptedString )
{
	if ( encryptedString.isEmpty() ) {
		QSettings settings( QSettings::IniFormat, QSettings::UserScope, MANUFACTURER_NAME, APPLICATION_NAME );
		encryptedString = settings.value("config_string").toString();
	}

	char *strJSON = (char *) malloc( encryptedString.length() + 1 );
	strncpy( strJSON, encryptedString.toLatin1().constData(), encryptedString.length() );
	strJSON[encryptedString.length()] = 0;
	decryptJSON( strJSON );

	QJsonParseError err;
	QMap<QString,QVariant> testcfg = QJsonDocument().fromJson(strJSON,&err).object().toVariantMap();
	// qDebug() << "JSON conversion: " << err.errorString();

	free( strJSON );

	return ! testcfg["serial_number_prefix"].toStringList().isEmpty();	/* test if this is empty versus 1 (or more) strings */
}


#define LOADSETTING_STRINGLIST(lval,key)	{ lval = config.value(key, settings.value(key).toStringList() ).toStringList(); }
#define LOADSETTING_STRING(lval,key)	{ lval = config.value(key, settings.value(key).toString() ).toString(); }
#define LOADSETTING_INT(lval,key)	{ lval = config.value(key, settings.value(key).toInt() ).toInt(); }
#define LOADSETTING_BOOL(lval,key)	{ lval = config.value(key, settings.value(key).toBool() ).toBool(); }


void CustomerInfo::load()
{
    QSettings settings( QSettings::IniFormat, QSettings::UserScope, MANUFACTURER_NAME, APPLICATION_NAME );

    QString encryptedString = settings.value("config_string").toString();
	char *strJSON = (char *) malloc( encryptedString.length() + 1 );
	strncpy( strJSON, encryptedString.toLatin1().constData(), encryptedString.length() );
	strJSON[encryptedString.length()] = 0;
	decryptJSON( strJSON );

	QJsonParseError err;
	QMap<QString,QVariant> config = QJsonDocument().fromJson(strJSON,&err).object().toVariantMap();
	// qDebug() << "JSON conversion: " << err.errorString();

	free( strJSON );

    LOADSETTING_STRING( customCode, "customCode" );

    LOADSETTING_STRING( company_name, "company_name" );
	LOADSETTING_STRING( product_name, "product_name" );
    LOADSETTING_STRING( phone_number, "phone_number" );
    LOADSETTING_STRING( website, "website" );
    LOADSETTING_STRING( description, "description" );
	LOADSETTING_STRING( sv_icon, "sv_icon" );

	QString strBase64Logo;
    LOADSETTING_STRING( strBase64Logo, "company_logo" );
	if ( ! strBase64Logo.isEmpty() ) {
		QByteArray pngData( QByteArray::fromBase64( strBase64Logo.toLatin1().constData() ) );
		QPixmap p;
		p.loadFromData( pngData );
		logo = *(new QPixmap(p));	// memory leak here, but who cares
	} else {
		QString strLogo;
		LOADSETTING_STRING( strLogo, "logo" );
		logo = *(new QPixmap( strLogo ));	// memory leak here, but who cares
		LOADSETTING_INT( logo_width, "logo_width" );
	}

    serial_number_prefix.append( config["serial_number_prefix"].toString() );

	LOADSETTING_STRINGLIST( recording_durations, "recording_durations");
	LOADSETTING_STRINGLIST( mct_recording_durations, "mct_recording_durations");

    LOADSETTING_INT( default_pre_trigger, "default_pre_trigger" );
    LOADSETTING_INT( default_post_trigger, "default_post_trigger" );
    LOADSETTING_INT( default_auto_event_limit, "default_auto_event_limit" );
    LOADSETTING_INT( default_manual_event_limit, "default_manual_event_limit" );
	LOADSETTING_INT( default_ttm_speed, "default_ttm_speed" );
    LOADSETTING_INT( default_tachy_threshold, "default_tachy_threshold" );
	LOADSETTING_INT( default_brady_threshold, "default_brady_threshold" );
	LOADSETTING_INT( default_pause_threshold, "default_pause_threshold" );
	LOADSETTING_INT( default_afib_setting, "default_afib_setting" );
	LOADSETTING_INT( default_recording_duration, "default_recording_duration" );
    LOADSETTING_INT( default_mct_recording_duration, "default_mct_recording_duration" );
	LOADSETTING_INT( default_sample_rate, "default_sample_rate" );
	LOADSETTING_BOOL( show_128_sps, "show_128_sps" );
	LOADSETTING_BOOL( show_sample_rate, "show_sample_rate" );
    LOADSETTING_BOOL( review_patient_before_download, "review_patient_before_download" );

	LOADSETTING_BOOL( server_enabled, "server_enabled" );
	LOADSETTING_BOOL( mct_server, "mct_server" );

	LOADSETTING_STRING( server_scheme, "server_scheme" );
	LOADSETTING_STRING( server_host, "server_host" );
	LOADSETTING_STRING( server_directory, "server_directory" );
	LOADSETTING_INT( server_port, "server_port" );
	LOADSETTING_STRING( server_user_name, "server_user_name" );
	LOADSETTING_STRING( server_password, "server_password" );

	network_url_holter.setScheme(server_scheme);
	network_url_holter.setHost(server_host);
	network_url_holter.setPort(server_port);
	network_url_holter.setPath(server_directory);
	network_url_holter.setUserName(server_user_name);
	network_url_holter.setPassword(server_password);

	LOADSETTING_STRING( server_scheme2, "server_scheme2" );
	LOADSETTING_STRING( server_host2, "server_host2" );
	LOADSETTING_STRING( server_directory2, "server_directory2" );
	LOADSETTING_INT( server_port2, "server_port2" );
	LOADSETTING_STRING( server_user_name2, "server_user_name2" );
	LOADSETTING_STRING( server_password2, "server_password2" );

	network_url_event.setScheme(server_scheme2);
	network_url_event.setHost(server_host2);
	network_url_event.setPort(server_port2);
	network_url_event.setPath(server_directory2);
	network_url_event.setUserName(server_user_name2);
	network_url_event.setPassword(server_password2);

	LOADSETTING_STRING( zip_password, "zip_password" );

	LOADSETTING_STRING( color_main, "color_main" );
	LOADSETTING_STRING( color_accent1, "color_accent1" );
	LOADSETTING_STRING( color_accent2, "color_accent2" );
	LOADSETTING_STRING( color_contrast, "color_contrast" );
	LOADSETTING_STRING( color_caution, "color_caution" );
	LOADSETTING_STRING( color_disabled, "color_disabled" );

	LOADSETTING_INT( device_key[0], "device_key0" );
	LOADSETTING_INT( device_key[1], "device_key1" );
	LOADSETTING_INT( device_key[2], "device_key2" );
	LOADSETTING_INT( device_key[3], "device_key3" );

	LOADSETTING_STRING( di_number, "di_number" );

	LOADSETTING_INT( single_device_mode, "single_device_mode" );
	LOADSETTING_BOOL( indications, "indications" );
	LOADSETTING_BOOL( manufacturing, "manufacturing" );
	LOADSETTING_BOOL( hide_first_name, "hide_first_name" );
	LOADSETTING_BOOL( hide_middle_name, "hide_middle_name" );
	LOADSETTING_BOOL( hide_last_name, "hide_last_name" );
	LOADSETTING_BOOL( hide_dob, "hide_dob" );
	LOADSETTING_BOOL( hide_physician, "hide_physician" );
	LOADSETTING_BOOL( hide_patient_id, "hide_patient_id" );
	LOADSETTING_BOOL( hide_comment, "hide_comment" );
	LOADSETTING_BOOL( hide_technician, "hide_technician" );
	LOADSETTING_BOOL( hide_facility, "hide_facility" );
	LOADSETTING_BOOL( generateResFile, "generateResFile" );

	server_tls = (tls_type) config.value("server_tls", settings.value("server_tls").toInt() ).toInt();
	server_tls2 = (tls_type) config.value("server_tls2", settings.value("server_tls2").toInt() ).toInt();
}


