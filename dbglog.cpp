

#include "dbglog.h"


/** {{{ QQQ::QQQ(QString filename )
 */
QQQ::QQQ(QString filename )
{
    pFile = new QFile( filename );
	if ( pFile == NULL ) {
		pFile->open( stdout, QIODevice::Text );
	} else {
		pFile->open( QIODevice::Append | QIODevice::Text );
	}
    stream = new Stream( pFile );
    stream->space = false;
    stream->ts << QDateTime::currentDateTime().toString("\n[yyyy-MM-dd hh:mm:ss.zzz] ");
#ifdef ANTIQUE
	stream->ts << QTime::currentTime().toString("\n[hh:mm:ss.zzz] ");
#endif
}
/* }}} */


/** {{{ QQQ & QQQ::operator<<( QVector <float> & floatVector )
*/
QQQ & QQQ::operator<<( QVector <float> & floatVector )
{
    int i;
    for ( i = 0 ; i < floatVector.size() ; i++ ) {
        if ( (i % 16) == 0 ) {
            stream->ts << "\n";
        }
        qreal val = (qreal) qRound(floatVector[i] * 100) / 100.0;
        val = qMin(val,9999.0);
        val = qMax(val,-9999.0);
        stream->ts << QString().sprintf("%7.1f ", val );
    }

    return maybeSpace();
}
/* }}} */


/** {{{ QQQ & QQQ::operator<<( QVector <int> & intVector )
*/
QQQ & QQQ::operator<<( QVector <int> & intVector )
{
    int i;
    for ( i = 0 ; i < intVector.size() ; i++ ) {
        if ( (i % 16) == 0 ) {
            stream->ts << "\n";
        }
        stream->ts << QString().sprintf("%d ", (int) intVector[i] );
    }

    return maybeSpace();
}
/* }}} */


/** {{{ QQQ & QQQ::operator<<( QMap<QString, short> m );
*/
QQQ & QQQ::operator<<( QMap<QString, short> m )
{
    extern bool caseInsensitiveLessThan( const QString &s1, const QString &s2 );

    QStringList keyList = m.keys();
    qSort( keyList.begin(), keyList.end(), caseInsensitiveLessThan );

    QString lastKey = "";
    foreach ( QString keyStr, keyList ) {
        /* if this key is very similar to the last one... */
        QString keyWithoutNumerals = keyStr;
        keyWithoutNumerals.remove(QRegExp("\\d+"));
        if ( lastKey == keyWithoutNumerals ) {
            /* ...then just separate it by a space and no newline */
            stream->ts << "  " << m[keyStr];
        } else {
            stream->ts << "\n" << "  " << keyStr << " = " << m[keyStr];
        }
        lastKey = keyWithoutNumerals;
    }

    stream->ts << "\n";

    return maybeSpace();
}
/* }}} */


/** {{{ QQQ & QQQ::operator<<( QMap<QString, long> m );
*/
QQQ & QQQ::operator<<( QMap<QString, long> m )
{
    extern bool caseInsensitiveLessThan( const QString &s1, const QString &s2 );

    QStringList keyList = m.keys();
    qSort( keyList.begin(), keyList.end(), caseInsensitiveLessThan );

    QString lastKey = "";
    foreach ( QString keyStr, keyList ) {
        /* if this key is very similar to the last one... */
        QString keyWithoutNumerals = keyStr;
        keyWithoutNumerals.remove(QRegExp("\\d+"));
        if ( lastKey == keyWithoutNumerals ) {
            /* ...then just separate it by a space and no newline */
            stream->ts << "  " << m[keyStr];
        } else {
            stream->ts << "\n" << "  " << keyStr << " = " << m[keyStr];
        }
        lastKey = keyWithoutNumerals;
    }

    stream->ts << "\n";

    return maybeSpace();
}
/* }}} */



/** {{{ QQQ & QQQ::operator<<( QHash<QString, short> m );
*/
QQQ & QQQ::operator<<( QHash<QString, short> m )
{
    extern bool caseInsensitiveLessThan( const QString &s1, const QString &s2 );

    QStringList keyList = m.keys();
    qSort( keyList.begin(), keyList.end(), caseInsensitiveLessThan );

    QString lastKey = "";
    foreach ( QString keyStr, keyList ) {
        /* if this key is very similar to the last one... */
        QString keyWithoutNumerals = keyStr;
        keyWithoutNumerals.remove(QRegExp("\\d+"));
        if ( lastKey == keyWithoutNumerals ) {
            /* ...then just separate it by a space and no newline */
            stream->ts << "  " << m[keyStr];
        } else {
            stream->ts << "\n" << "  " << keyStr << " = " << m[keyStr];
        }
        lastKey = keyWithoutNumerals;
    }

    stream->ts << "\n";

    return maybeSpace();
}
/* }}} */


/** {{{ QQQ & QQQ::operator<<( QHash<QString, long> m );
*/
QQQ & QQQ::operator<<( QHash<QString, long> m )
{
    extern bool caseInsensitiveLessThan( const QString &s1, const QString &s2 );

    QStringList keyList = m.keys();
    qSort( keyList.begin(), keyList.end(), caseInsensitiveLessThan );

    QString lastKey = "";
    foreach ( QString keyStr, keyList ) {
        /* if this key is very similar to the last one... */
        QString keyWithoutNumerals = keyStr;
        keyWithoutNumerals.remove(QRegExp("\\d+"));
        if ( lastKey == keyWithoutNumerals ) {
            /* ...then just separate it by a space and no newline */
            stream->ts << "  " << m[keyStr];
        } else {
            stream->ts << "\n" << "  " << keyStr << " = " << m[keyStr];
        }
        lastKey = keyWithoutNumerals;
    }

    stream->ts << "\n";

    return maybeSpace();
}
/* }}} */


/** {{{ bool caseInsensitiveLessThan(const QString &s1, const QString &s2)
*/
bool caseInsensitiveLessThan( const QString &s1, const QString &s2 )
{
	if ( s1[0] == s2[0] ) {
		QRegExp rx1("(\\D*)(\\d*)_(\\w*)");
		QRegExp rx2("(\\D*)(\\d*)_(\\w*)");

		if ( (rx1.indexIn(s1) != -1) && (rx2.indexIn(s2) != -1) ) {

			/* if the end of the string (the suffix part) is the same, then we should compare the number... */
			if ( rx1.cap(3) == rx2.cap(3) ) {

				return rx1.cap(2).toInt() < rx2.cap(2).toInt();

			} else {	/* ...otherwise, these aren't the same kind of parameters, so do a comparison of the suffix part only */
				return rx1.cap(3).toLower() < rx2.cap(3).toLower();
			}

		} else {	/* regexp failed, so do a normal comparison */
			return s1.toLower() < s2.toLower();
		}
	} else {	/* the first letter isn't the same, so do a normal comparison */
		return s1.toLower() < s2.toLower();
	}
}
/* }}} */

