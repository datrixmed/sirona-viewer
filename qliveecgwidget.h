#ifndef QLIVEECGWIDGET_H
#define QLIVEECGWIDGET_H

#include "interface.h"
#include <QWidget>
#include <QVector>
#include <QPen>
#include <QPainterPath>

struct QEcgTraceSection {
	QEcgTraceSection(): latestXPos(), latestSampPos( 0 ), length( 0 ) {}
	int latestXPos;
	int latestSampPos;
	int length;
};

class QLiveECGWidget: public QWidget
{
public:
	explicit QLiveECGWidget( QWidget *parent );

	void clearData();
	int channelCount();
	void setCableId( quint8 type );
	void setDeviceMode( quint8 type );
	void addSample( int channel1, int channel2 = 0, int channel3 = 0 );
	void addMissingSample();

protected:
	void paintEvent( QPaintEvent *event ) override;
	void resizeEvent( QResizeEvent *event ) override;

private:
	void updateOffsets();
	void updateEcgSections();
	void trimOldData();
	void drawLeadIndicators( QPainter &painter );
	void drawLeadIndicator( QPainter &painter, int x, int y, QColor color );
	QVector<int> m_channelData[3];
	int m_lastGoodSmpleYPos[3];
	bool m_lastSampleWasMissing;
	QEcgTraceSection m_ecgTraceSections[2];
	QVector<int> m_yOffset;
	qint64 m_startTime;
	int m_currentXPosition;
	int m_channelCount;
	int m_CableId;
	int m_deviceMode;
	qreal m_xScale;
	qreal m_yScale;
	QColor m_backgroundColor;
	QPen m_missingEcgPen;
	QPen m_ecgPen;
	QPen m_gridPen;
	QPainterPath m_gridPath;
};

#endif // QLIVEECGWIDGET_H
