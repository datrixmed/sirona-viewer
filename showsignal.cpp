
#include <QtGlobal>
#include <QPrinter>
#include <QPrintDialog>
#include <QtWidgets>

#include "showsignal.h"
#include "interface.h"
#include <QDebug>
#include "dbglog.h"

// #define ENABLE_DEBUG_CRASH_LIVE3CH

#define min(a,b)	    ( (a) < (b) ? (a) : (b) )

#define ECG_BACKGROUND_LIGHT
#undef  ECG_BACKGROUND_LIGHT

#ifdef ECG_BACKGROUND_LIGHT
#define MYCOLOR_GRID_SOLID	(QColor("#c8c8c8"))
#define MYCOLOR_GRID_DOTTED	(QColor("#e0e0e0"))
#define MYCOLOR_DATA			(QColor(Qt::red))
#define MYCOLOR_BACKGROUND	(QPalette::Window.color())
#define MYCOLOR_TEXT			(QColor(Qt::black))
#else
#define MYCOLOR_GRID_SOLID	(QColor("#505050"))
#define MYCOLOR_GRID_DOTTED	(QColor("#303030"))
#define MYCOLOR_DATA			(QColor(Qt::yellow).lighter())
#define MYCOLOR_BACKGROUND	(QColor(Qt::black))
#define MYCOLOR_TEXT			(QColor(Qt::white))
#endif

static int whichChannelsAreViewable = CHANNELS_TO_VIEW_ALL;

/** {{{ ShowSignal::ShowSignal( QWidget *parent, EcgData *theEcgData )
	@brief Define a constructor for my canvas
*/
ShowSignal::ShowSignal( QWidget *parent, EcgData *theEcgData, bool liveEcg ) :
	QWidget( parent ),
	m_ecgdata( theEcgData ),
	m_PostTrigger( 0 )
{
	if ( m_ecgdata == NULL ) {
		m_ecgdata = new EcgData;
	}
	curpos_samples = 0;
	m_testspeed = false;
	m_test_antialiasing = false;

	isLiveEcg = liveEcg;

	end_time_samps = m_ecgdata->samps_per_chan_per_sec * m_ecgdata->datalen_secs;

	QPalette pal = palette();
#ifdef ECG_BACKGROUND_LIGHT
#else
	pal.setColor( QPalette::Window, MYCOLOR_BACKGROUND );
	pal.setColor( QPalette::WindowText, MYCOLOR_TEXT );
#endif
	setPalette( pal );
	setAutoFillBackground( true );

	setAttribute( Qt::WA_DeleteOnClose );
	isUntitled = true;

	gain_mm_per_mV = 10;
	zoom_amount = 1.0;
	m_zoom_x = 0.0;
	m_zoom_y = 0.0;
	setMouseTracking( true );

	is_printing = false;

	setObjectName( "ShowSignal" );

#ifdef ANTIQUE
	QWidget *deskTop = QApplication::desktop();
	QPainter dc( deskTop );
	QSize realSize(
		qRound( ECG_DISPLAY_WINDOW_SIZE_SECONDS * ( dc.device()->logicalDpiX() * 2.5 / 2.54 ) ),
		qRound( 20 * 5.0 * ( dc.device()->logicalDpiY() / 25.4 ) )
	);

	// QQQ("workecg.log") << "screen resolution DPI = " << (dc.device()->logicalDpiX() * 2.5 / 2.54) << " and " << (dc.device()->logicalDpiY()) << " so ShowSignal size() = " << realSize.width() << ", " << realSize.height();

	parentWidget()->resize( realSize );
	resize( realSize );
#endif
	CreateCableMap();
}
/* }}} */


/** {{{ ShowSignal::~ShowSignal()
*/
ShowSignal::~ShowSignal()
{
	m_ecgdata = NULL;
}
/* }}} */



/** {{{ ShowSignal::minimumSizeHint()
	@brief Set minimum canvas size
*/
QSize ShowSignal::minimumSizeHint() const
{
	return QSize( 100, 100 );
}
/* }}} */


/** {{{ ShowSignal::sizeHint()
	@brief Set canvas size
*/
QSize ShowSignal::sizeHint() const
{
	return parentWidget()->size();
}
/* }}} */


/** {{{ ShowSignal::resizeEvent()
	@brief Resize event
*/
void ShowSignal::resizeEvent( QResizeEvent *event )
{
	Q_UNUSED( event );
#ifdef QT_DEBUG
	// qDebug("ShowSignal::resizing to %d x %d but instead we want the height to be closer to %d", width(), height(), draw_height );
#endif
	// QQQ("workecg.log") << "ShowSignal size() = " << width() << ", " << height();
}

/* }}} */











/** {{{ void ShowSignal::wheelEvent( QWheelEvent *e )
	@brief Mouse wheel events
 */
void ShowSignal::wheelEvent( QWheelEvent *event )
{
	float spinning = ( event->delta() > 0 ) ? zoom_amount / 10.0 : -zoom_amount / 10.0;

	double multiplier = 1.0;

	if ( event->modifiers() & Qt::CTRL ) {
		multiplier *= 1.0 / 3;
	}
	if ( event->modifiers() & Qt::SHIFT ) {
		multiplier *= 5.0;
	}
	if ( event->modifiers() & Qt::ALT ) {
		multiplier *= 2.0;
	}

	/*
		m_zoom_x = VIEWWIDTH  * (float) event->pos().x() / (float) draw_width;
		m_zoom_y = VIEWHEIGHT * (float) event->pos().y() / (float) draw_height;
	*/
	if ( zoom_amount > 1.0 ) {
		m_zoom_x = ( float ) event->pos().x();
		m_zoom_y = ( float ) event->pos().y();
	} else {
		m_zoom_x = ( float ) width() / 2;
		m_zoom_y = ( float ) height() / 2;
	}

	/*
	qDebug() << event->pos();
	*/


	zoom_amount -= multiplier * spinning;
	/* don't let the zoom grow too big */
	if ( zoom_amount > 10.0 ) {
		zoom_amount = 10.0;
	}
	/* don't let the zoom grow too big */
	if ( zoom_amount < 0.25 ) {
		zoom_amount = 0.25;
	}
	/* if the zoom is pretty near zero, then make it zero */
	if ( fabs( zoom_amount - 1.0 ) < 0.1 ) {
		zoom_amount = 1.0;
	}

	update();
}
/* }}} */


/** {{{ void ShowSignal::mousePressEvent(QMouseEvent *event)
	@brief Mouse press event
*/
void ShowSignal::mousePressEvent( QMouseEvent *event )
{
	lastPos = event->pos();
	/// QQQ("workecg.log") << QString("mousePressEvent(%1,%2)").arg(event->pos().x()).arg(event->pos().y() );
	// QQQ("workecg.log") << "ShowSignal::ShowData()     sample_count = " << sample_count << "  datalen_secs = " << m_ecgdata->datalen_secs;

	if ( event->button() == Qt::MiddleButton ) {
		zoom_amount = 1.0;
	}

	update();
}
/* }}} */


/** {{{ void ShowSignal::mouseMoveEvent(QMouseEvent *event)
	@brief Mouse move event
*/
void ShowSignal::mouseMoveEvent( QMouseEvent *event )
{
	int dx = event->x() - lastPos.x();
	long samps_per_sec = m_ecgdata->samps_per_chan_per_sec;

	if ( event->buttons() & Qt::LeftButton ) {
		long offset = GetPos();
		double multiplier = 1.0;

		if ( event->modifiers() & Qt::CTRL ) {
			multiplier *= 1.0 / 3;
		}
		if ( event->modifiers() & Qt::SHIFT ) {
			multiplier *= 5.0;
		}
		if ( event->modifiers() & Qt::ALT ) {
			multiplier *= 2.0;
		}

		offset -= qRound( multiplier * ( dx ) );

		if ( offset > ( m_ecgdata->datalen_secs - ECG_DISPLAY_WINDOW_SIZE_SECONDS ) * samps_per_sec ) {
			offset = ( m_ecgdata->datalen_secs - ECG_DISPLAY_WINDOW_SIZE_SECONDS ) * samps_per_sec;
		}
		if ( offset < 0 ) {
			offset = 0;
		}

		SetPos( offset );
		update();

		/*
		#ifdef QT_DEBUG
				qDebug() << "Qt::LeftButton" << lastPos;
		#endif
		*/
	} else if ( event->buttons() & Qt::RightButton ) {
		/*
		#ifdef QT_DEBUG
				qDebug() << "Qt::RightButton" << lastPos;
		#endif
		*/
	} else {
		/*
				m_zoom_x = VIEWWIDTH  * (float) event->pos().x() / (float) draw_width;
				m_zoom_y = VIEWHEIGHT * (float) event->pos().y() / (float) draw_height;
		*/
		if ( zoom_amount > 1.0 ) {
			m_zoom_x = ( float ) event->pos().x();
			m_zoom_y = ( float ) event->pos().y();
		} else {
			m_zoom_x = ( float ) width() / 2;
			m_zoom_y = ( float ) height() / 2;
		}
		if ( zoom_amount != 1.0 ) {
			update();
		}
		/*
		#ifdef QT_DEBUG
				qDebug("mouseMoveEvent(%d,%d)", event->pos().x(), event->pos().y() );
		#endif
		*/

	}
	lastPos = event->pos();
}
/* }}} */




/** {{{ void ShowSignal::keyPressEvent(wxKeyEvent &event)
	@brief Key press event
*/
void ShowSignal::keyPressEvent( QKeyEvent *event )
{
	long offset = GetPos();

	int key = 0;
	if ( event->text().size() > 0 ) {
		key = ( const char ) event->text().at( 0 ).toLatin1();
	} else {
		key = event->key();
	}

	long samps_per_sec = m_ecgdata->samps_per_chan_per_sec;

	double multiplier = 1.0;
	if ( event->modifiers() & Qt::CTRL ) {
		multiplier *= 1.0 / samps_per_sec;
	}
	if ( event->modifiers() & Qt::SHIFT ) {
		multiplier *= 10.0;
	}
	if ( event->modifiers() & Qt::ALT ) {
		multiplier *= 2.0;
	}

	switch ( key ) {

		case Qt::ALT:
			break;
		case Qt::CTRL:
			break;
		case Qt::SHIFT:
			break;
		case Qt::Key_Plus:
			offset += qRound( multiplier * ( 1 * samps_per_sec ) );
			break;
		case Qt::Key_Minus:
			offset -= qRound( multiplier * ( 1 * samps_per_sec ) );
			break;
		case Qt::Key_Left:
			offset -= qRound( multiplier * ( 1 * samps_per_sec ) );
			break;
		case Qt::Key_Down:
			offset += qRound( multiplier * ( ECG_DISPLAY_WINDOW_SIZE_SECONDS * samps_per_sec ) );
			break;
		case Qt::Key_Up:
			offset -= qRound( multiplier * ( ECG_DISPLAY_WINDOW_SIZE_SECONDS * samps_per_sec ) );
			break;
		case Qt::Key_Home:
			offset = 0;
			break;
		case Qt::Key_End:
			offset = ( m_ecgdata->datalen_secs - ECG_DISPLAY_WINDOW_SIZE_SECONDS ) * samps_per_sec;
			break;
		case Qt::Key_PageDown:
			offset += qRound( multiplier * ( 15 * 60 * samps_per_sec ) );
			break;
		case Qt::Key_PageUp:
			offset -= qRound( multiplier * ( 15 * 60 * samps_per_sec ) );
			break;

		case Qt::Key_Right:
			offset += qRound( multiplier * ( 1 * samps_per_sec ) );
			break;
		case ' ':
			offset += qRound( multiplier * ( 1 * samps_per_sec ) );
			break;

		case 'A':
		case 'a':
			m_test_antialiasing = !  m_test_antialiasing;
			break;

		case '1':
			break;

		case 'S':
		case 's': {
		}
		break;
	}

	if ( offset > ( m_ecgdata->datalen_secs - ECG_DISPLAY_WINDOW_SIZE_SECONDS ) * samps_per_sec ) {
		offset = ( m_ecgdata->datalen_secs - ECG_DISPLAY_WINDOW_SIZE_SECONDS ) * samps_per_sec;
	}
	if ( offset < 0 ) {
		offset = 0;
	}

	if ( offset != GetPos() ) {
		SetPos( offset );
		update();
	}
	event->ignore();
}
/* }}} */





/** {{{ ShowSignal::paintEvent()
	@brief  Paint events are sent to widgets that need to update themselves
*/
void ShowSignal::paintEvent( QPaintEvent *event )
{
	Q_UNUSED( event );

	if ( !isVisible() ) {
		return;
	}

	QPainter painter( this );

	painter.setFont( QFont( "Helvetica", 22 ) );
	if ( m_test_antialiasing ) {
		painter.setRenderHint( QPainter::Antialiasing, true );
	}

	/* rescale the window when the window gets resized */
	float pixelSizeWidth = ( 8.0 * ( painter.device()->logicalDpiX() * 2.5 / 2.54 ) );
	float pixelSizeHeight = ( ( STRIPHEIGHT_MM + 5 ) * ( painter.device()->logicalDpiY() / 25.4 ) );
	float scaleOntoScreen = parentWidget()->size().width() / pixelSizeWidth;

	if ( pixelSizeHeight * scaleOntoScreen > parentWidget()->size().height() ) {
		scaleOntoScreen = parentWidget()->size().height() / pixelSizeHeight;
	}

	painter.scale( scaleOntoScreen, scaleOntoScreen );

	/*
		QQQ("workecg.log") << "ShowSignal::paintEvent()   "
			<< " scaleOntoScreen = " << scaleOntoScreen
			<< " ( (STRIPHEIGHT_MM + 9) * (painter.device()->logicalDpiY() * 2.54) ) = " << ( (STRIPHEIGHT_MM + 9) * (painter.device()->logicalDpiY() * 2.54) )
			<< " parentWidget()->size().height() = " << parentWidget()->size().height()
			<< " pixelSizeWidth = " << pixelSizeWidth
			<< " pixelSizeHeight = " << pixelSizeHeight
			;
	*/

	painter.translate( m_zoom_x, m_zoom_y );
	painter.scale( zoom_amount, zoom_amount );
	painter.translate( -m_zoom_x, -m_zoom_y );

#ifdef ANTIQUE
	// Debug code for paint optimization.
	static int frame_cointer = 0;
	frame_cointer++;
	painter.drawText( 10, 100, QString::number( frame_cointer ) );
#endif

	Render( &painter );


}
/* }}} */


/** {{{ void ShowSignal::Render(QPainter dc)
	@brief Define the repainting behaviour
*/
void ShowSignal::Render( QPainter *dc )
{
	// QQQ("workecg.log") << "ShowSignal::render()";

	ShowGrid( dc, ECG_DISPLAY_WINDOW_SIZE_SECONDS * 5, STRIPHEIGHT_MM /* mm */ );
	ShowData( dc );
	ShowAnnotation( dc );

}
/* }}} */




/** {{{ void ShowSignal::ShowGrid(QPaintEvent * dc, int cols, int rows )
	@brief Show an ECG gridlike thing
*/
void ShowSignal::ShowGrid( QPainter *dc, int cols, int height_mm )
{
	double size5mm = ( 5.0 * ( dc->device()->logicalDpiY() / 25.4 ) );
	double boxX_200ms = size5mm;
	double boxY_5mm = size5mm;
	double boxY_height_mm = ( height_mm * ( dc->device()->logicalDpiY() / 25.4 ) );

	boxX_200ms = size5mm;
	boxY_5mm = size5mm;
	boxY_height_mm = height_mm / 5 * size5mm;

	QPen pen_solid( MYCOLOR_GRID_SOLID, 0, Qt::SolidLine, Qt::FlatCap, Qt::MiterJoin );
	QPen pen_dotted( MYCOLOR_GRID_DOTTED, 0, Qt::SolidLine, Qt::FlatCap, Qt::MiterJoin );
	if ( is_printing ) {
		pen_solid = QPen( QColor( "#404040" ), 2, Qt::SolidLine, Qt::FlatCap, Qt::MiterJoin );
		pen_dotted = QPen( QColor( "#404040" ), 3, Qt::DotLine, Qt::FlatCap, Qt::MiterJoin );
	}
	dc->setPen( pen_dotted );

	for ( double i = 0 ; i <= cols ; i++ ) {
		if ( ( ( int )i % 5 ) == 0 ) {
			dc->setPen( pen_solid );
		}
		dc->drawLine( qRound( i * boxX_200ms ), 0, qRound( i * boxX_200ms ), qRound( boxY_height_mm ) );
		if ( ( ( int )i % 5 ) == 0 ) {
			dc->setPen( pen_dotted );
		}
	}
	for ( double j = 0 ; j < boxY_height_mm ; j += boxY_5mm ) {
		dc->drawLine( 0, qRound( j ), qRound( boxX_200ms * 5 * ECG_DISPLAY_WINDOW_SIZE_SECONDS ), qRound( j ) );
	}
}
/* }}} */


#define min(a,b)	( (a) < (b) ? (a) : (b) )

/** {{{ void ShowSignal::ShowData(QPainter * dc )
 *
 * @brief Show the ECG data
 *
*/
void ShowSignal::ShowData( QPainter *dc )
{
	int i;
	double range_per_sample = m_ecgdata->range_per_sample;
	long *chdata[MAX_CHANNELS];
	long samples_across_grid = m_ecgdata->samps_per_chan_per_sec * ECG_DISPLAY_WINDOW_SIZE_SECONDS;
	long sample_count = m_ecgdata->samps_per_chan_per_sec * min( ECG_DISPLAY_WINDOW_SIZE_SECONDS, m_ecgdata->datalen_secs );

#ifdef ENABLE_DEBUG_CRASH_LIVE3CH
	QQQ( "workecg.log" ) << "ShowSignal::ShowData()     sample_count = " << sample_count << "  datalen_secs = " << m_ecgdata->datalen_secs;
#endif

	if ( sample_count <= 0 ) {
		return;
	}

	if ( !isLiveEcg ) {
		long posEndOfRequestedData = ( GetPos() + ECG_DISPLAY_WINDOW_SIZE_SECONDS * m_ecgdata->samps_per_chan_per_sec );

		if ( posEndOfRequestedData > m_ecgdata->samples_len ) {
			sample_count -= ( posEndOfRequestedData - m_ecgdata->samples_len );
			// qDebug() << "Cutting" << (posEndOfRequestedData - m_ecgdata->samples_len) << "samples off of sample_count";
		}
	}

	for ( int ch = 0 ; ch < m_ecgdata->channel_count ; ch++ ) {
		if ( isChannelViewable( ch ) ) {
			chdata[ch] = m_ecgdata->get( ch, GetPos(), ECG_DISPLAY_WINDOW_SIZE_SECONDS * m_ecgdata->samps_per_chan_per_sec );
#ifdef ENABLE_DEBUG_CRASH_LIVE3CH
			QQQ( "workecg.log" ) << QString( "ShowSignal::ShowData()     chdata[%1] = %2     GetPos() = %3    duration = %4" )
								 .arg( ch ).arg( ( long ) chdata[ch], 2, 16 ).arg( GetPos() ).arg( ECG_DISPLAY_WINDOW_SIZE_SECONDS * m_ecgdata->samps_per_chan_per_sec )
								 .toLatin1().constData();
#endif
		}
	}


	double size5mm = ( 5.0 * ( dc->device()->logicalDpiY() / 25.4 ) );

	double device_dots_per_sec = size5mm * 5;
	double device_dots_per_mm = size5mm / 5.0;

	QPointF *pts = new QPointF[sample_count];

	qreal mV_per_digital_sample = ( qreal ) m_ecgdata->device_range_mV / ( qreal ) range_per_sample;

	QPen pen_solid( MYCOLOR_DATA, 0, Qt::SolidLine );
	if ( is_printing ) {
		pen_solid.setColor( Qt::red );
		pen_solid.setWidth( 3 );
	}
	dc->setPen( pen_solid );

	int maxViewableChannels = cntViewableChannels( m_ecgdata->channel_count );
	int thisViewableChannel = 0;

	/** For each channel... */
	for ( int ch = 0 ; ch < m_ecgdata->channel_count ; ch++ ) {
		if ( isChannelViewable( ch ) ) {
			qreal baseline_offset;

			baseline_offset = STRIPHEIGHT_MM * device_dots_per_mm * ( thisViewableChannel + 1 ) / ( maxViewableChannels + 1 );
			thisViewableChannel += 1;

			int ptsCount = 0;

			QString str;
			QRect textrect;

			QPen penAnnotation( palette().color( QPalette::WindowText ) );
			QFont fontAnnotation( "Helvetica", 10 );
			if ( is_printing ) {
				fontAnnotation.setPixelSize( size5mm );
				penAnnotation.setColor( Qt::black );
			}
			dc->setFont( fontAnnotation );
			dc->setPen( penAnnotation );

			/* draw the channel names on the display */
			str = QString( "CH%1" ).arg( ch + 1 );

			textrect = dc->boundingRect( 100, 100, 10000, 10000, Qt::AlignVCenter | Qt::AlignRight, str );
			dc->drawText(
				qRound( 0.0 ),
				qRound( baseline_offset - 50 - ( size5mm / 2 ) ),
				textrect.width(), textrect.height(),
				Qt::AlignVCenter | Qt::AlignRight, str );

			int multiplier = 1;

			QList<QColor>::iterator j;

			QMap < int, QList<QColor> > tempMap;

			tempMap = cableTypeChanColors[m_ecgdata->cableType];

			for ( j = tempMap[ch].begin(); j != tempMap[ch].end(); ++j ) {
				dc->setBrush( *j );
				dc->drawEllipse( QPointF( 20 + ( size5mm * multiplier ), baseline_offset - 50 ), size5mm / 2, size5mm / 2 );
				multiplier++;
			}

			for ( i = 0 ; i < sample_count ; i++ ) {

				/** Don't display anything IF 'missing data marker' is found */
				if ( chdata[ch][i] == MARKER_MISSING_DATA ) {
#ifdef DISPLAY_NOISE_INSTEAD_OF_GAPS_BETWEEN_GOOD_PACKETS
					pts[ptsCount].setX( ( qreal ) i * ( device_dots_per_sec * ECG_DISPLAY_WINDOW_SIZE_SECONDS ) / samples_across_grid );
					pts[ptsCount].setY( ( qreal ) ( ( range_per_sample / 2 - ( ( i & 1 ) ? MARKER_MISSING_DATA : -MARKER_MISSING_DATA ) ) * ( device_dots_per_mm * gain_mm_per_mV * mV_per_digital_sample ) + baseline_offset ) );
					ptsCount++;
#else
					/* Terminate this line so that the next line can start at the appropriate X position after the gap. */
					if ( ptsCount > 0 ) {
						dc->drawPolyline( pts, ptsCount );
					}
					ptsCount = 0;
#endif
				} else {
					pts[ptsCount].setX( ( qreal ) i * ( device_dots_per_sec * ECG_DISPLAY_WINDOW_SIZE_SECONDS ) / samples_across_grid );
					pts[ptsCount].setY( ( qreal ) ( ( range_per_sample / 2 - chdata[ch][i] ) * ( device_dots_per_mm * gain_mm_per_mV * mV_per_digital_sample ) + baseline_offset ) );
					ptsCount++;
				}
			}

			dc->drawPolyline( pts, ptsCount );
		}
	}

	dc->setPen( QPen() );
	delete[] pts;
}
/* }}} */


/** {{{ void ShowSignal::ShowAnnotation( QPaintEvent * dc )
	@brief Show the annotations
*/
void ShowSignal::ShowAnnotation( QPainter *dc )
{
	QString str;
	QRect textrect;

	double size5mm = ( 5.0 * ( dc->device()->logicalDpiY() / 25.4 ) );

	double device_dots_per_sec = size5mm * 5;
	double device_dots_per_mm = size5mm / 5.0;


	if ( m_PostTrigger > 0 ) {
		int sample_count;
		if ( isLiveEcg ) {
			sample_count = m_ecgdata->samps_per_chan_per_sec * m_ecgdata->datalen_secs;
		} else {
			sample_count = min( m_ecgdata->samps_per_chan_per_sec * m_ecgdata->datalen_secs, m_ecgdata->samples_len );
		}
		int sample_pos = ( sample_count - ( m_PostTrigger * m_ecgdata->samps_per_chan_per_sec ) ) - GetPos();
		long samples_across_grid = m_ecgdata->samps_per_chan_per_sec * ECG_DISPLAY_WINDOW_SIZE_SECONDS;

		qreal x_pos = ( qreal ) sample_pos * ( device_dots_per_sec * ECG_DISPLAY_WINDOW_SIZE_SECONDS ) / samples_across_grid;

		if ( ( x_pos >= 0 ) && ( x_pos <= width() ) ) {
			dc->setPen( Qt::magenta );
			dc->drawLine( x_pos, 0, x_pos, height() );
		}
	}

	QPen penAnnotation( palette().color( QPalette::WindowText ) );
	QFont fontAnnotation( "Helvetica", 8 );
	if ( is_printing ) {
		fontAnnotation.setPixelSize( size5mm );
		penAnnotation.setColor( Qt::black );
	}
	dc->setFont( fontAnnotation );
	dc->setPen( penAnnotation );

	/* draw the current gain on the display */
	str = QString( "channels: %1    %2 mm/sec    %3 mm/mV" ).arg( cntViewableChannels( m_ecgdata->channel_count ) ).arg( 25 ).arg( gain_mm_per_mV );
	textrect = dc->boundingRect( 100, 100, 10000, 10000, Qt::AlignVCenter | Qt::AlignRight, str );
	dc->drawText(
		qRound( device_dots_per_sec * ECG_DISPLAY_WINDOW_SIZE_SECONDS ) - textrect.width(),
		qRound( device_dots_per_mm * 100 ),
		textrect.width(), textrect.height(),
		Qt::AlignVCenter | Qt::AlignRight, str );


	/* draw the time of the beginning of visible data on the display */
	if ( m_ecgdata->utcStartTime > 0 ) {
		QDateTime when;
		when.setTime_t( m_ecgdata->utcStartTime + ( GetPos() / m_ecgdata->samps_per_chan_per_sec ) );
		str = when.toString( "ddd, d-MMM  h:mm:ss ap" );
	} else {
		int second_pos = GetPos() / m_ecgdata->samps_per_chan_per_sec;
		str = QString( "%1:%2:%3" )
			  .arg( ( second_pos / ( 60 * 60 ) ), 2, 10, QLatin1Char( '0' ) )
			  .arg( ( second_pos / 60 ) % 60, 2, 10, QLatin1Char( '0' ) )
			  .arg( ( second_pos / 1 ) % 60, 2, 10, QLatin1Char( '0' ) );
	}
	textrect = dc->boundingRect ( 100, 100, 1000, 1000, Qt::AlignVCenter | Qt::AlignLeft, str );
	dc->drawText(
		0,
		qRound( device_dots_per_mm * 100 ),
		textrect.width(), textrect.height(),
		Qt::AlignVCenter | Qt::AlignRight, str );

//    /* draw the channel names on the display */
//    str = QString( "CH1" );
//    textrect = dc->boundingRect( 100, 100, 10000, 10000, Qt::AlignVCenter | Qt::AlignRight, str );
//    dc->drawText(
//        qRound( 0.0 ),
//        qRound( 100.0 ),
//        textrect.width(), textrect.height(),
//        Qt::AlignVCenter | Qt::AlignRight, str );


	QPen pen_solid( QColor( "#0000ff" ), 3, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin );
	dc->setPen( pen_solid );

	dc->setFont( QFont( "Helvetica", 12 ) );
	if ( is_printing ) {
		fontAnnotation.setPixelSize( size5mm / 5 * 12 );
	}
	dc->setFont( fontAnnotation );
	textrect = dc->boundingRect ( 100, 100, 1000, 1000, Qt::AlignCenter, tr( "UNKNOWN" ) );

}

void ShowSignal::CreateCableMap()
{
	// 1 Channel 2 Wire
	cableTypeChanColors[I_CHANNEL_2_WIRE][0].append( Qt::red );
	cableTypeChanColors[I_CHANNEL_2_WIRE][0].append( Qt::white );

	// 1 Channel 3 Wire
	cableTypeChanColors[I_CHANNEL_3_WIRE][0].append( Qt::red );
	cableTypeChanColors[I_CHANNEL_3_WIRE][0].append( Qt::white );

	// 2 Channel 3 Wire
	cableTypeChanColors[II_CHANNEL_3_WIRE][0].append( Qt::black );
	cableTypeChanColors[II_CHANNEL_3_WIRE][0].append( Qt::white );

	cableTypeChanColors[II_CHANNEL_3_WIRE][1].append( Qt::red );
	cableTypeChanColors[II_CHANNEL_3_WIRE][1].append( Qt::white );

	// 2 Channel 5 Wire
	cableTypeChanColors[II_CHANNEL_5_WIRE][0].append( Qt::red );
	cableTypeChanColors[II_CHANNEL_5_WIRE][0].append( Qt::white );

	cableTypeChanColors[II_CHANNEL_5_WIRE][1].append( QColor( 0xA5, 0x2A, 0x2A ) );
	cableTypeChanColors[II_CHANNEL_5_WIRE][1].append( Qt::black );

	// 3 Channel 5 Wire
	cableTypeChanColors[III_CHANNEL_5_WIRE][0].append( QColor( 0xA5, 0x2A, 0x2A ) );
	cableTypeChanColors[III_CHANNEL_5_WIRE][0].append( Qt::red );

	cableTypeChanColors[III_CHANNEL_5_WIRE][1].append( Qt::black );
	cableTypeChanColors[III_CHANNEL_5_WIRE][1].append( Qt::red );

	cableTypeChanColors[III_CHANNEL_5_WIRE][2].append( Qt::black );
	cableTypeChanColors[III_CHANNEL_5_WIRE][2].append( Qt::white );

	// 3 Channel 7 Wire
	cableTypeChanColors[III_CHANNEL_7_WIRE][0].append( Qt::red );
	cableTypeChanColors[III_CHANNEL_7_WIRE][0].append( Qt::white );

	cableTypeChanColors[III_CHANNEL_7_WIRE][1].append( QColor( 0xA5, 0x2A, 0x2A ) );
	cableTypeChanColors[III_CHANNEL_7_WIRE][1].append( Qt::black );

	cableTypeChanColors[III_CHANNEL_7_WIRE][2].append( QColor( 0xFF, 0xA5, 0x00 ) );
	cableTypeChanColors[III_CHANNEL_7_WIRE][2].append( Qt::blue );
}
/* }}} */


/** {{{ void ShowSignal::SetPos( long start_time_secs )
	@brief Select where this ECG data starts
*/
long ShowSignal::SetPos( long start_time_samps )
{
#ifdef QT_DEBUG
// qDebug() << "SetPos(" << start_time_samps << ")";
#endif

	/*if ( which_mode == "Event Mode" ) {
		if ((start_time_samps) > ( 2*end_time_samps - ECG_DISPLAY_WINDOW_SIZE_SECONDS * m_ecgdata->samps_per_chan_per_sec ) )
		{
			qDebug() << "Here ends the ecg data of the event!";
			return curpos_samples;
		}
	}*/

	if ( curpos_samples != start_time_samps ) {
		curpos_samples = start_time_samps;
#ifdef QT_DEBUG
		QString str = QString( tr( "Current ECG Time: %1:%2:%3" ) )
					  .arg( ( int ) ( ( ( curpos_samples + ( ECG_DISPLAY_WINDOW_SIZE_SECONDS * m_ecgdata->samps_per_chan_per_sec ) / 2 ) / m_ecgdata->samps_per_chan_per_sec ) / ( 60 * 60 ) ) )
					  .arg( ( int ) ( ( ( curpos_samples + ( ECG_DISPLAY_WINDOW_SIZE_SECONDS * m_ecgdata->samps_per_chan_per_sec ) / 2 ) / m_ecgdata->samps_per_chan_per_sec ) / ( 60 ) ) % 60, 2, 10, QLatin1Char( '0' ) )
					  .arg( ( int ) ( ( ( curpos_samples + ( ECG_DISPLAY_WINDOW_SIZE_SECONDS * m_ecgdata->samps_per_chan_per_sec ) / 2 ) / m_ecgdata->samps_per_chan_per_sec ) / 1 ) % 60, 2, 10, QLatin1Char( '0' ) );
		// qDebug( str.toLatin1() );
#endif
	}
	return curpos_samples;
}
/* }}} */



/** {{{ int isChannelViewable( int chan )
 */
int isChannelViewable( int chan )
{
	return ( ( 0x01 << chan ) & whichChannelsToView() );
}
/* }}} */


/** {{{ int cntViewableChannels()
 */
int cntViewableChannels( int maxChannels )
{
	int maxViewableChannels = 0;
	for ( int ch = 0; ch < maxChannels ; ch++ ) {
		if ( isChannelViewable( ch ) ) {
			maxViewableChannels++;
		}
	}
	return maxViewableChannels;
}
/* }}} */


/** {{{ void setWhichChannelsToView( int whichChannelsToView )
 */
void setWhichChannelsToView( int whichChannelsToView )
{
	whichChannelsAreViewable = whichChannelsToView;
}
/* }}} */


/** {{{ int whichChannelsToView()
 */
int whichChannelsToView()
{
	return whichChannelsAreViewable;
}
/* }}} */




/** {{{ void ShowSignal::smooth_advance()
	@brief Smooth advance of the ECG data
*/
void ShowSignal::smooth_advance()
{
}
/* }}} */


/** {{{ void ShowSignal::print()
	@brief Print the ECG data
*/
void ShowSignal::print()
{
#ifdef QT_DEBUG
	qDebug( "ShowSignal::print()" );
#endif

#ifndef QT_NO_PRINTER

	QPrinter printer( QPrinter::HighResolution );
	printer.setPageMargins( 5, 10, 5, 10, QPrinter::Millimeter );

	QPrintDialog printDialog( &printer, this );
#ifdef ANTIQUE
	QPrintPreviewDialog printDialog( &printer, this );
	printDialog.exec();
	connect( &printDialog, SIGNAL( paintRequested( QPrinter * ) ), this, SLOT( printRender( QPrinter * ) ) );
#else

	if ( printDialog.exec() == QDialog::Accepted ) {

		QPainter painter( &printer );

		is_printing = true;
		Render( &painter );
		is_printing = false;
	}
#endif

#endif // QT_NO_PRINTER

}
/* }}} */


/** {{{ void ShowSignal::printPDF()
	@brief Print ECG data on PDF document
*/
void ShowSignal::printPDF()
{

#ifndef QT_NO_PRINTER

	QDateTime when;
	when.setTime_t( m_ecgdata->utcStartTime + ( GetPos() / m_ecgdata->samps_per_chan_per_sec ) );
	QString fileNameTemplate = when.toString( "'ecg-'yyyyMMdd-hhmmss" );
	QString fileName = QString( "%1.pdf" ).arg( fileNameTemplate );
	if ( QFile::exists( fileName ) ) {
		for ( int filenum = 0 ; filenum < 10000 ; filenum++ ) {
			fileName = QString( "%1-%2.pdf" ).arg( fileNameTemplate ).arg( filenum, 4, 10, QChar( '0' ) );
			if ( ! QFile::exists( fileName ) ) {
				break;
			}
		}
	}

	fileName = QFileDialog::getSaveFileName( this, tr( "Save PDF File" ), fileName, tr( "PDF (*.pdf);;All Files (*.*)" ) );

	if ( fileName.isEmpty() ) {
		qDebug() << "Cancel PDF export...";
		return;
	} else {
		qDebug() << "PDF export using filename:" << fileName;
	}

	QPrinter printer( QPrinter::HighResolution );
	printer.setPageMargins( 5, 10, 5, 10, QPrinter::Millimeter );

	printer.setOutputFormat( QPrinter::PdfFormat );
	printer.setOutputFileName( fileName );
	printer.newPage();

	QPainter painter( &printer );

	is_printing = true;
	Render( &painter );
	is_printing = false;

#endif // QT_NO_PRINTER

}
/* }}} */


/** {{{ void MainWindow::moveLeft()
	@brief Move the ECG data to the left
*/
void ShowSignal::moveLeft()
{
	SetPos( GetPos() - 1 * m_ecgdata->samps_per_chan_per_sec );
	update();
}
/* }}} */


/** {{{ void MainWindow::moveRight()
	@brief Move the ECG data to the right
*/
void ShowSignal::moveRight()
{
	SetPos( GetPos() + 1 * m_ecgdata->samps_per_chan_per_sec );
	update();
}
/* }}} */


/** {{{ void ShowSignal::setPostTrigger(int postTrigger)
    @brief Sets post trigger so the moment of the event can be drawn.
*/
void ShowSignal::setPostTrigger( int postTrigger )
{
	m_PostTrigger = postTrigger;

	update();
}
/* }}} */



