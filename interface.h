#ifndef INTERFACE_H
#define INTERFACE_H

#include <QHash>
#include <QString>
#include "sironadriver.h"

#define ECG_DATA_FORMAT                        311
#define POSIBLE_VALUES_FOR_10_BIT_RESOLUTION  1024
#define ECG_SIGNAL_RANGE_10_MV                  10
#define ADC_BIT_RESOLUTION_FOR_311_FORMAT       10
#define ADC_UNITS_OFFSET_TO_MIDDLE_OF_RANGE      0

#define SECTOR_SIZE            512
#define LENGTH_DISCOVERY_PACK   20

#define HOLTER_START_SECTOR           2049

#define SIRONA_LEAD_RED              0x01
#define SIRONA_LEAD_WHITE            0x02
#define SIRONA_LEAD_GREEN            0x04
#define SIRONA_LEAD_BLACK            0x08
#define SIRONA_LEAD_BROWN            0x10
#define SIRONA_LEAD_BLUE             0x20
#define SIRONA_LEAD_ORANGE           0x40
#define SIRONA_LEAD_ALL              0x7F

#define HOLTER_DOWNLOAD_COMPLETE  0b00000001

#define RECORDING_LENGTH_1_DAY     24
#define RECORDING_LENGTH_2_DAYS    48
#define RECORDING_LENGTH_3_DAYS    72
#define RECORDING_LENGTH_4_DAYS    96
#define RECORDING_LENGTH_5_DAYS   120
#define RECORDING_LENGTH_6_DAYS   144
#define RECORDING_LENGTH_7_DAYS   168
#define RECORDING_LENGTH_10_DAYS  240
#define RECORDING_LENGTH_14_DAYS  336
#define RECORDING_LENGTH_21_DAYS  504
#define RECORDING_LENGTH_30_DAYS  720

#define PRE_TRIGGER_TIME_0_SEC      0
#define PRE_TRIGGER_TIME_15_SEC    15
#define PRE_TRIGGER_TIME_30_SEC    30
#define PRE_TRIGGER_TIME_45_SEC    45
#define PRE_TRIGGER_TIME_60_SEC    60
#define PRE_TRIGGER_TIME_90_SEC    90
#define PRE_TRIGGER_TIME_120_SEC  120
#define PRE_TRIGGER_TIME_180_SEC  180
#define PRE_TRIGGER_TIME_300_SEC  300

#define POST_TRIGGER_TIME_15_SEC    15
#define POST_TRIGGER_TIME_30_SEC    30
#define POST_TRIGGER_TIME_45_SEC    45
#define POST_TRIGGER_TIME_60_SEC    60
#define POST_TRIGGER_TIME_90_SEC    90
#define POST_TRIGGER_TIME_120_SEC  120
#define POST_TRIGGER_TIME_180_SEC  180
#define POST_TRIGGER_TIME_300_SEC  300

#define EVENT_LIMIT_0        0
#define EVENT_LIMIT_1        1
#define EVENT_LIMIT_2        2
#define EVENT_LIMIT_3        3
#define EVENT_LIMIT_4        4
#define EVENT_LIMIT_5        5
#define EVENT_LIMIT_6        6
#define EVENT_LIMIT_7        7
#define EVENT_LIMIT_8        8
#define EVENT_LIMIT_9        9
#define EVENT_LIMIT_10      10
#define EVENT_LIMIT_11      11
#define EVENT_LIMIT_12      12
#define EVENT_LIMIT_13      13
#define EVENT_LIMIT_14      14
#define EVENT_LIMIT_15      15
#define EVENT_LIMIT_16      16
#define EVENT_LIMIT_17      17
#define EVENT_LIMIT_18      18
#define EVENT_LIMIT_19      19
#define EVENT_LIMIT_20      20
#define EVENT_LIMIT_50      50
#define EVENT_LIMIT_100    100
#define EVENT_LIMIT_1000  1000

#define EVENT_COUNT_ALERT_THRESHOLD_1        1
#define EVENT_COUNT_ALERT_THRESHOLD_2        2
#define EVENT_COUNT_ALERT_THRESHOLD_3        3
#define EVENT_COUNT_ALERT_THRESHOLD_4        4
#define EVENT_COUNT_ALERT_THRESHOLD_5        5
#define EVENT_COUNT_ALERT_THRESHOLD_6        6
#define EVENT_COUNT_ALERT_THRESHOLD_7        7
#define EVENT_COUNT_ALERT_THRESHOLD_8        8
#define EVENT_COUNT_ALERT_THRESHOLD_9        9
#define EVENT_COUNT_ALERT_THRESHOLD_10      10
#define EVENT_COUNT_ALERT_THRESHOLD_25      25
#define EVENT_COUNT_ALERT_THRESHOLD_50      50
#define EVENT_COUNT_ALERT_THRESHOLD_100    100
#define EVENT_COUNT_ALERT_THRESHOLD_1000  1000

#define AUDIABLE_OFF  (quint8)0
#define AUDIABLE_ON   (quint8)1

#define ANALYZER_OFF        (quint8)0
#define ANALYZER_ON         (quint8)1
#define TACHY_ANALYZER_OFF  ANALYZER_OFF
#define TACHY_ANALYZER_ON   ANALYZER_ON
#define BRADY_ANALYZER_OFF  ANALYZER_OFF
#define BRADY_ANALYZER_ON   ANALYZER_ON
#define PAUSE_ANALYZER_OFF  ANALYZER_OFF
#define PAUSE_ANALYZER_ON   ANALYZER_ON

#define TACHY_THRESHOLD_110  1100
#define TACHY_THRESHOLD_115  1150
#define TACHY_THRESHOLD_120  1200
#define TACHY_THRESHOLD_125  1250
#define TACHY_THRESHOLD_130  1300
#define TACHY_THRESHOLD_135  1350
#define TACHY_THRESHOLD_140  1400
#define TACHY_THRESHOLD_145  1450
#define TACHY_THRESHOLD_150  1500
#define TACHY_THRESHOLD_155  1550
#define TACHY_THRESHOLD_160  1600
#define TACHY_THRESHOLD_165  1650
#define TACHY_THRESHOLD_170  1700
#define TACHY_THRESHOLD_175  1750
#define TACHY_THRESHOLD_180  1800
#define TACHY_THRESHOLD_185  1850
#define TACHY_THRESHOLD_190  1900
#define TACHY_THRESHOLD_195  1950
#define TACHY_THRESHOLD_200  2000
#define TACHY_THRESHOLD_205  2050
#define TACHY_THRESHOLD_210  2100
#define TACHY_THRESHOLD_215  2150
#define TACHY_THRESHOLD_220  2200
#define TACHY_THRESHOLD_225  2250
#define TACHY_THRESHOLD_230  2300
#define TACHY_THRESHOLD_235  2350
#define TACHY_THRESHOLD_240  2400

#define BRADY_THRESHOLD_15  150
#define BRADY_THRESHOLD_20  200
#define BRADY_THRESHOLD_25  250
#define BRADY_THRESHOLD_30  300
#define BRADY_THRESHOLD_35  350
#define BRADY_THRESHOLD_40  400
#define BRADY_THRESHOLD_45  450
#define BRADY_THRESHOLD_50  500
#define BRADY_THRESHOLD_55  550
#define BRADY_THRESHOLD_60  600

#define PAUSE_THRESHOLD_1_5    15
#define PAUSE_THRESHOLD_2_0    20
#define PAUSE_THRESHOLD_2_5    25
#define PAUSE_THRESHOLD_3_0    30
#define PAUSE_THRESHOLD_3_5    35
#define PAUSE_THRESHOLD_4_0    40
#define PAUSE_THRESHOLD_4_5    45
#define PAUSE_THRESHOLD_5_0    50
#define PAUSE_THRESHOLD_5_5    55
#define PAUSE_THRESHOLD_6_0    60
#define PAUSE_THRESHOLD_6_5    65
#define PAUSE_THRESHOLD_7_0    70
#define PAUSE_THRESHOLD_7_5    75
#define PAUSE_THRESHOLD_8_0    80
#define PAUSE_THRESHOLD_8_5    85
#define PAUSE_THRESHOLD_9_0    90
#define PAUSE_THRESHOLD_9_5    95
#define PAUSE_THRESHOLD_10_0  100

#define AF_OFF               0
#define AF_START_ON          1
#define AF_START_ON_STOP_ON  2

#define LIVE_ECG_TIMEOUT_NONE       0
#define LIVE_ECG_TIMEOUT_30_SEC    30
#define LIVE_ECG_TIMEOUT_60_SEC    60
#define LIVE_ECG_TIMEOUT_120_SEC  120
#define LIVE_ECG_TIMEOUT_300_SEC  300
#define LIVE_ECG_TIMEOUT_600_SEC  600

#define RECORDING_START_TIMEOUT_NONE   0
#define RECORDING_START_TIMEOUT_5_MIN  5

#define TTM_SPEED_1X  (quint8)0
#define TTM_SPEED_3X  (quint8)1

#define TTM_SEND_CHANNEL_1    (quint8)0
#define TTM_SEND_CHANNEL_2    (quint8)1
#define TTM_SEND_CHANNEL_ALL  (quint8)2

#define SAMPLE_RATE_128  (128)
#define SAMPLE_RATE_256  (256)

#define DEFAULT_FW_REV                       999
#define DEFAULT_PRE_TRIGGER_TIME             PRE_TRIGGER_TIME_30_SEC
#define DEFAULT_POST_TRIGGER_TIME            POST_TRIGGER_TIME_30_SEC
#define DEFAULT_CONFIGURE_DATE               1356998400
#define DEFAULT_TIME_ZONE                    0
#define DEFAULT_RECORDING_LENGTH             RECORDING_LENGTH_1_DAY
#define DEFAULT_AUTOMATIC_EVENT_LIMIT        EVENT_LIMIT_1000
#define DEFAULT_MANUAL_EVENT_LIMIT           EVENT_LIMIT_1000
#define DEFAULT_AUDIABLE                     1
#define DEFAULT_EVENT_COUNT_ALERT_THRESHOLD  EVENT_COUNT_ALERT_THRESHOLD_5
#define DEFAULT_TTM_ENABLED                  0
#define DEFAULT_TTM_SPEED                    TTM_SPEED_3X
#define DEFAULT_TTM_TIMEOUT                  15
#define DEFAULT_LIVE_ECG_TIMEOUT             LIVE_ECG_TIMEOUT_300_SEC
#define DEFAULT_RECORDING_START_TIMEOUT      RECORDING_START_TIMEOUT_5_MIN
#define DEFAULT_TTM_SEND_CHANNEL             TTM_SEND_CHANNEL_2
#define DEFAULT_ANALYZER_STATE               ANALYZER_ON
#define DEFAULT_TACHY_ANALYZER_STATE         TACHY_ANALYZER_ON
#define DEFAULT_BRADY_ANALYZER_STATE         BRADY_ANALYZER_ON
#define DEFAULT_PAUSE_ANALYZER_STATE         PAUSE_ANALYZER_ON
#define DEFAULT_TACHY_THRESHOLD              TACHY_THRESHOLD_140
#define DEFAULT_BRADY_THRESHOLD              BRADY_THRESHOLD_40
#define DEFAULT_PAUSE_THRESHOLD              PAUSE_THRESHOLD_3_0
#define DEFAULT_AF                           AF_START_ON
#define DEFAULT_SAMPLE_RATE                  SAMPLE_RATE_256
#define DEFAULT_BT_TIMEOUT_INTERVAL          (5)

#define PATIENT_ID_SIZE              (16 * 6) // Worst case scenario for UTF-8 conversion(1 characer = 6 bytes)
#define PATIENT_FIRST_NAME_SIZE      (20 * 6) // Worst case scenario for UTF-8 conversion(1 characer = 6 bytes)
#define PATIENT_LAST_NAME_SIZE       (20 * 6) // Worst case scenario for UTF-8 conversion(1 characer = 6 bytes)
#define PATIENT_MIDDLE_INITIAL_SIZE  ( 2 * 6) // Worst case scenario for UTF-8 conversion(1 characer = 6 bytes)
#define PHYSICIAN_NAME_SIZE          (20 * 6) // Worst case scenario for UTF-8 conversion(1 characer = 6 bytes)
#define TECHNICIAN_NAME_SIZE         (20 * 6) // Worst case scenario for UTF-8 conversion(1 characer = 6 bytes)
#define FACILITY_SIZE                (20 * 6) // Worst case scenario for UTF-8 conversion(1 characer = 6 bytes)

enum cable_mode {
	NO_CABLE,
	CHANNEL_1_EVENT,
	CHANNEL_2_EVENT,
	CHANNEL_2_HOLTER,
	CHANNEL_3_HOLTER,
	CHARGER_CABLE,
	USB_DIRECT_CONNECT,
	CHANNEL_3_EVENT,
	CHANNEL_1_HOLTER,
	CHANNEL_1_POST,
	CHANNEL_2_PATCH
};

#pragma pack(push)
#pragma pack(1)
struct ConfigParams {
	quint32 svn_rev = 0;                            // 4
	quint8 procedure_state = 0;                     // 1 - possible values: 0 (false) and 1 (true)
    quint8 device_mode = 0;                         // 1 - AKA PROCTYPE
	quint8 class_id = 0;                            // 1
	quint32 device_id = 0;                          // 4
	quint16 fw_rev = 0;                             // 2
	quint16 pre_trigger = 0;                        // 2
	quint16 post_trigger = 0;                       // 2
	qint32 error_code = 0;                          // 4 - written by Sirona Viewer, read and reset(to 0) by Sirona Viewer
	quint32 operation_mode = 0;                     // 4 - written by firmware, read by Sirona Viewer, possible values(0 - No Cable, 1 - 1 Channel Event mode, 2 - 2 Channel Event mode, 3 - 2 Channel Holter mode, 4 - 3 Channel Holter mode)
	quint32 procedure_start_date = 0;               // 4
	quint8 download_complete = 0;                   // 1
	quint16 recording_len = 0;                      // 2
	quint16 automatic_event_limit = 0;              // 2
	quint16 manual_event_limit = 0;                 // 2
	quint8 audiable_on = 0;                         // 1 - possible values: 0 (false) and 1 (true)
	quint8 analyzer_tachy_on = 0;                   // 1 - possible values: 0 (false) and 1 (true)
	quint8 analyzer_brady_on = 0;                   // 1 - possible values: 0 (false) and 1 (true)
	quint8 analyzer_pause_on = 0;                   // 1 - possible values: 0 (false) and 1 (true)
	quint8 analyzer_pace_on = 0;                    // 1 - possible values: 0 (false) and 1 (true)
	quint8 analyzer_af_on = 0;                      // 1 - possible values: 0, 1, 2(0 = no af, 1 = af on only, 2 af on, af off)
	quint16 analyzer_tachy_threshold = 0;           // 2 - 800 - 2000 (real value is divided by 10)
	quint16 analyzer_brady_threshold = 0;           // 2 - 200 - 700 (real value is divided by 10)
	quint16 analyzer_pause_threshold = 0;           // 2 - 10 - 300 (real value is divided by 10)
	quint8 analyzer_channel_mask = 0;               // 1 - as number 0 -> 1Ch, 1 -> 2 Ch, 2 -> 3Ch
	quint8 ttm_enabled = 0;                         // 1 - possible values: 0 (false) and 1 (true)
	quint8 ttm_speedup_on = 0;                      // 1 - possible values: 0 (false) and 1 (true)
	QString patient_id = "";                        // Max: 16 * 6 + 1
	quint8 cable_id = 0;                            // 1
	quint16 sample_rate = 0;                        // 2
	QString patient_first_name = "";                // Max: 20 * 6 + 1
    quint8 first_name_required = 0;
    quint8 last_name_required = 0;
    quint8 dob_required = 0;
    quint8 physician_name_required = 0;
    quint8 patient_id_required = 0;
    quint8 middle_name_required = 0;
    quint8 comment_required = 0;
    quint8 technician_name_required = 0;
    quint8 facility_required = 0;
	QString patient_last_name = "";                 // Max: 20 * 6 + 1
	QString patient_middle_initial = "";            // Max: 2 * 6 + 1
	QString physician_name = "";                    // Max: 20 * 6 + 1
	QString comment = "";                           // 500
	qint32 patient_date_of_birth = 0;               // 4
    QString technician_name = "";
    QString facility = "";
	qint16 time_zone = 0;                           // 2 - time zone offset from GMT in minutes
	QString model_number = "";
	quint8  mct_enabled = 0;
	// quint8 lastRebootReason;
	quint16 procedure_time = 0;
	QString medication_notes = "";
	QString diagnosis = "";
	QList< QHash<QString,QString> >	diary_entries;						// every 4 strings constitute a single entry
	QString sitecode = "";
};
#pragma pack(pop)
#endif // INTERFACE_H
