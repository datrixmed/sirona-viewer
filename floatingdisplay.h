
#ifndef FLOATINGDISPLAY_H
#define FLOATINGDISPLAY_H

#include <QtWidgets>


namespace Ui
{
class FloatingDisplay;
}

class FloatingDisplay : public QWidget
{
	Q_OBJECT

public:
	FloatingDisplay( QWidget *parent = 0 );
	~FloatingDisplay();

protected:
	void changeEvent( QEvent *e );
	void wheelEvent( QWheelEvent *event );
	void keyPressEvent( QKeyEvent *event );
	void closeEvent( QCloseEvent *event );
	void paintEvent( QPaintEvent *event );
	void resizeEvent( QResizeEvent * /* event */ );
	void mouseMoveEvent( QMouseEvent *event );
	void mousePressEvent( QMouseEvent *event );
	void populate_comboAudioSelector( void );
	QString getBackgroundImage();

private:
	QPixmap *pixmap;
	QPoint dragPosition;
	bool isDragging;

public slots:
	void append( QStringList strList );
	void append( QString str );

private:
	Ui::FloatingDisplay *ui;
};

#endif							// FLOATINGDISPLAY_H
