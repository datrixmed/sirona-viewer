#ifndef HTTPMCTDATA_H
#define HTTPMCTDATA_H

#include <QIODevice>
#include <QFile>

class HttpMctData : public QIODevice
{
	Q_OBJECT

public:
        HttpMctData(QString boundary, QString atrFileName, QString datFileName, QString heaFileName, QString jsonFileName);
        ~HttpMctData();
        bool open(OpenMode mode);
	void close();
	bool atEnd() const;
	qint64 bytesAvailable() const;
	bool isSequential() const;
	qint64 size() const;

protected:
        qint64 readData(char *data, qint64 maxSize);
        qint64 writeData(const char *data, qint64 len);

private:
	qint64 totalSize;
        char preAtr[1000];
        qint64 preAtrSize;
        char preDat[1000];
        qint64 preDatSize;
        char preHea[1000];
        qint64 preHeaSize;
        char preJson[1000];
        qint64 preJsonSize;
        char postJson[1000];
        qint64 postJsonSize;
        QFile *atrFile;
        QFile *datFile;
        QFile *heaFile;
        QFile *jsonFile;

	qint64 lastReportedPercentage = -1;
signals:
	void percentageChanged( qint64 percentage );
	void reloadTimer();
};

#endif // HTTPMCTDATA_H
