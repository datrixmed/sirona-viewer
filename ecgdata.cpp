/**
 * @file ecgdata.cpp
 *
 * @note	http://www.physionet.org/physiotools/wag/signal-5.htm describes each of the possible formats
 *
*/
#include <QApplication>
#include <QFile>
#include <QFileInfo>
#include <QDataStream>
#include <QString>
#include <QStringList>
#include <time.h>
#include <math.h>

#include "ecgdata.h"
#include "Sironaviewerui.h"

#define MASK_THESE_BITS(b,bits)	((unsigned long) ((b) & ((1 << (bits)) - 1)))
#define MASK_4_BIT(b)	MASK_THESE_BITS(b, 4)
#define MASK_8_BIT(b)	MASK_THESE_BITS(b, 8)
#define MASK_10_BIT(b)	MASK_THESE_BITS(b,10)
#define MASK_12_BIT(b)	MASK_THESE_BITS(b,12)


void EcgData::STORE_INTO_CHDATA( int ch, int i, long val )
{
	if ( i >= this->sizeChData[ch] ) {
		this->sizeChData[ch] += 120 * samps_per_chan_per_sec;
		this->chData[ch] = ( long * ) realloc( this->chData[ch], this->sizeChData[ch] * sizeof( long ) );
		this->rawData[ch] = ( long * ) realloc( this->rawData[ch], this->sizeChData[ch] * sizeof( long ) );

		/* initialize the data arrays with some default fake data */
		memset( ( void * ) & ( this->chData[ch][sizeChData[ch] - 120 * samps_per_chan_per_sec] ), ECG_DEFAULT_FAKE_BYTE, 120 * samps_per_chan_per_sec * sizeof( long ) );
		memset( ( void * ) & ( this->rawData[ch][sizeChData[ch] - 120 * samps_per_chan_per_sec] ), ECG_DEFAULT_FAKE_BYTE, 120 * samps_per_chan_per_sec * sizeof( long ) );
	}

	this->chData[ch][i] = val;

	if ( i >= samples_len ) {
		samples_len = i;
		datalen_secs = samples_len / samps_per_chan_per_sec;
	}
}

void EcgData::STORE_RAW_INTO_CHDATA( int ch, int i, long val )
{
	this->rawData[ch][i] = val;
}

// Define a constructor for holding the ECG data
EcgData::EcgData()
{
	device_range_mV = 10;
	range_per_sample = 50000;
	channel_count = 3;
	datalen_secs = 0;
	samps_per_chan_per_sec = 256;
	signal_format_specifier = 80;
	bytes_per_samp = 1;
	utcStartTime = 0L;
	samples_len = 0;
	data_loading = false;

	for ( int ch = 0 ; ch < MAX_CHANNELS ; ch++ ) {
		sizeChData[ch] = 0;
		chData[ch] = nullptr;
		rawData[ch] = nullptr;
	}
}

// Define a destructor
EcgData::~EcgData()
{
	for ( int ch = 0 ; ch < MAX_CHANNELS ; ch++ ) {
		free( chData[ch] );
		free( rawData[ch] );

		sizeChData[ch] = 0;
		chData[ch] = nullptr;
		rawData[ch] = nullptr;
	}
}

// Cancel the loading of the ecg data
void EcgData::cancel_data_loading()
{
	data_loading = false;
	emit loading_finished();
}

long *EcgData::get( int channel_num, long start_time_samps, long duration_samps )
{
	if ( channel_num >= channel_count ) {
		LOGMSG( "Channel number out of range: ch = %d and channel_count = %d", channel_num, channel_count );
		channel_num = 0;
	}
	if ( channel_num >= 3 ) {
		LOGMSG( "Channel number way out of range (>= 3): ch = %d", channel_num );
		channel_num = 0;
	}
	/* if past the end of valid data, then point to the end of data */
	if ( start_time_samps + duration_samps > datalen_secs * samps_per_chan_per_sec ) {
		start_time_samps = datalen_secs * samps_per_chan_per_sec - duration_samps;
	}
	if ( start_time_samps < 0 ) {
		start_time_samps = 0;
	}
	if ( start_time_samps >= sizeChData[channel_num] ) {
		LOGMSG( "Error: chData[%d][%ld] out of range", channel_num, start_time_samps );
		return &( chData[channel_num][0] );
	}
	return &( chData[channel_num][start_time_samps] );
}

long EcgData::getRaw( int channel_num, long start_time_samps )
{
	if ( channel_num >= channel_count ) {
		LOGMSG( "Channel number out of range: ch = %d and channel_count = %d", channel_num, channel_count );
		channel_num = 0;
	}
	if ( channel_num >= 3 ) {
		LOGMSG( "Channel number way out of range (>= 3): ch = %d", channel_num );
		channel_num = 0;
	}

	if ( start_time_samps < 0 ) {
		start_time_samps = 0;
	}
	if ( start_time_samps >= sizeChData[channel_num] ) {
		LOGMSG( "Error: chData[%d][%ld] out of range", channel_num, start_time_samps );
		return 0;
	}
	return rawData[channel_num][start_time_samps];
}

void EcgData::save( QString fileName )
{
	LOGMSG( "EcgData::save(%s) where sizeChData[0] = %ld", fileName.toLatin1().data(), sizeChData[0] );
	QFile filedata( fileName );
	if ( ! filedata.open( QFile::WriteOnly ) ) {
		return;
	}

	QDataStream out( &filedata );
	out << *this;
}

void EcgData::restore( QString fileName )
{
	LOGMSG( "EcgData::restore(%s)", fileName.toLatin1().data() );
	QFile filedata( fileName );
	if ( ! filedata.open( QFile::ReadOnly ) ) {
		return;
	}

	QDataStream in( &filedata );
	in >> *this;
}

QDataStream &operator<<( QDataStream &out, const EcgData &lEcgData )
{
	QByteArray storedChData0( ( char * ) lEcgData.chData[0], lEcgData.sizeChData[0] * sizeof( long ) );
	QByteArray storedChData1( ( char * ) lEcgData.chData[1], lEcgData.sizeChData[1] * sizeof( long ) );
	QByteArray storedChData2( ( char * ) lEcgData.chData[2], lEcgData.sizeChData[2] * sizeof( long ) );

	out << lEcgData.range_per_sample;
	out << lEcgData.device_range_mV;
	out << lEcgData.channel_count;
	out << lEcgData.datalen_secs;
	out << lEcgData.samps_per_chan_per_sec;
	out << lEcgData.signal_format_specifier;
	out << lEcgData.bytes_per_samp;
	out << lEcgData.utcStartTime;
	out << lEcgData.samples_len;
	out << storedChData0;
	out << storedChData1;
	out << storedChData2;

	return out;
}

QDataStream &operator>>( QDataStream &in, EcgData &lEcgData )
{
	QByteArray storedChData0;
	QByteArray storedChData1;
	QByteArray storedChData2;

	in >> lEcgData.range_per_sample;
	in >> lEcgData.device_range_mV;
	in >> lEcgData.channel_count;
	in >> lEcgData.datalen_secs;
	in >> lEcgData.samps_per_chan_per_sec;
	in >> lEcgData.signal_format_specifier;
	in >> lEcgData.bytes_per_samp;
	in >> lEcgData.utcStartTime;
	in >> lEcgData.samples_len;
	in >> storedChData0;
	in >> storedChData1;
	in >> storedChData2;

	lEcgData.chData[0] = ( long * ) malloc( storedChData0.size() );
	lEcgData.chData[1] = ( long * ) malloc( storedChData1.size() );
	lEcgData.chData[2] = ( long * ) malloc( storedChData2.size() );
	memcpy( ( void * ) lEcgData.chData[0], ( void * ) storedChData0.constData(), storedChData0.size() );
	memcpy( ( void * ) lEcgData.chData[1], ( void * ) storedChData1.constData(), storedChData1.size() );
	memcpy( ( void * ) lEcgData.chData[2], ( void * ) storedChData2.constData(), storedChData2.size() );

	lEcgData.sizeChData[0] = storedChData0.size() / sizeof( long );
	lEcgData.sizeChData[1] = storedChData1.size() / sizeof( long );
	lEcgData.sizeChData[2] = storedChData2.size() / sizeof( long );

	return in;
}
