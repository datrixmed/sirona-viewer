#ifndef SIRONAVIEWERUI_H
#define SIRONAVIEWERUI_H

#include <QDialog>
#include <QSystemTrayIcon>
#include <QMessageBox>
#include <QListWidgetItem>
#include <QCompleter>
#include <QDateTime>
#include <QProcess>
#include <QThread>
#include <QIcon>
#include <QNetworkReply>
#include <QTableWidgetItem>

#if defined(Q_OS_WIN)
#include <winsock2.h>
#endif

#include "showsignalstreaming.h"
#include "interface.h"
#include "qcustomplot.h"
#include "dataformat.h"
#include "utilities.h"
#include "devicehandler.h"
#include "customer.h"
#include "curl_client.h"

namespace Ui
{
class SironaViewerUi;
}

/* For battery representation in percent */
struct batteryCharge_t {
	batteryCharge_t(): voltage( 0 ), percent( 0 ) {}
	batteryCharge_t( int voltage, int percent ): voltage( voltage ), percent( percent ) {}
	int voltage;
	int percent;
};
/* }}} */

typedef enum {
	NO_DEVICE_CONNECTED,
	DEVICE_CONNECTING,
	USB_DEVICE_CONNECTED,
	BT_DEVICE_CONNECTED
} deviceConnectionStatus_t;

typedef enum {
	PAGE_INITIAL_VALUE = -1,
	PAGE_WELCOME_SCREEN = 0,
	PAGE_CONNECTIONS_SCREEN,
	PAGE_CABLE_SCREEN,
	PAGE_PATIENT_SCREEN,
	PAGE_PROCEDURE_SCREEN,
	PAGE_USB_SAVED_SCREEN,
	PAGE_HOOKUP_SCREEN,
	PAGE_ECG_SCREEN,
	PAGE_PROCEDURE_ACTIVE_SCREEN,
    PAGE_DOWNLOAD_OPTIONS_SCREEN,
    PAGE_DOWNLOAD_SCREEN,
    PAGE_ERASE_SCREEN,
	PAGE_COMPLETE_SCREEN,
	PAGE_DEVICE_SCREEN,
	PAGE_COMPANY_SCREEN
} pageNames_t;

typedef enum {
	DEVICE_IDLE,
	DEVICE_READ_PARAMETER,
	DEVICE_WRITE_PARAMETER,
	DEVICE_WRITE_LIST_OF_PARAMETERS,
	DEVICE_COMMIT_PARAMETERS,
	DEVICE_READ_ALL_PARAMETERS,
	DEVICE_WRITE_ALL_PARAMETERS,
	DEVICE_IS_THERE_RECORDED_DATA,
	DEVICE_SAVE_RECORDED_DATA,
	DEVICE_DELETE_RECORDED_DATA,
	DEVICE_START_DEVICE_PROCEDURE,
	DEVICE_STOP_DEVICE_PROCEDURE,
	DEVICE_SOFT_RESET,
	DEVICE_GET_BATTERY_STATUS,
	DEVICE_SET_SYSTEM_TIME,
	DEVICE_LIVE_ECG
} deviceCommunicationStatus_t;

typedef enum {
	QPanelExitOperationNoOperation,
	QPanelExitOperationNextScreen,
	QPanelExitOperationPreviousScreen,
	QPanelExitOperationSettingsScreen,
	QPanelExitOperationLogoScreen
} QPanelExitOperation;

struct QFutureLiveECGSample {
	QFutureLiveECGSample(): msSinceEpox( 0 ), ch1( 0 ), ch2( 0 ), ch3( 0 ) {}
	QFutureLiveECGSample( qint64 msSinceEpox, int ch1, int ch2, int ch3 ): msSinceEpox( msSinceEpox ), ch1( ch1 ), ch2( ch2 ), ch3( ch3 ) {}
	qint64 msSinceEpox;
	int ch1;
	int ch2;
	int ch3;
};

class SironaViewerUi : public QWidget
{
	Q_OBJECT

public:
	explicit SironaViewerUi( QWidget *parent = 0, CustomerInfo *pCustomer = NULL );
	~SironaViewerUi();
	bool isLowDiskSpace() const;
	void startEnumerateTimer();
	void stopEnumerateTimer();
    void startStatusTimer();
    void stopStatusTimer();
	void enumPwmDevices();

	QThread deviceHandlerThread;
	DeviceHandler *deviceHandler = nullptr;

	bool	dbgTesting;

protected:
	void changeEvent( QEvent *e ) override;
	bool nativeEvent( const QByteArray &eventType, void *message, long *result ) override;
	void keyPressEvent( QKeyEvent *event ) override;
	void mousePressEvent( QMouseEvent *event ) override;
	void timerEvent( QTimerEvent *event ) override;

	curl_client *curl_sender = nullptr;

	QProcess *myProcess;

private:
	Ui::SironaViewerUi *m_ui = nullptr;

	deviceConnectionStatus_t deviceConnectionStatus = NO_DEVICE_CONNECTED;
	deviceCommunicationStatus_t deviceCommunicationStatus = DEVICE_IDLE;

	QString connectedDeviceSN;

	QMap<QString, QString> deviceList;
	QVector<QFutureLiveECGSample> m_futureSamples;
	qint64 liveEcgStartmsSinceEpox = 0;
	int liveEcgSampleCount = 0;

	CustomerInfo *customer;

	QNetworkAccessManager *network_access_manager = nullptr;
	QNetworkReply *network_reply = nullptr;
	QString m_datFileName;
	QList<QString> uploadFileList;
	QUrl customer_url;
	ftp_url_info customer_ftp_url_info;

	EcgData *m_EcgData = nullptr;
	ConfigParams m_configParams;

	pageNames_t previousPage = PAGE_INITIAL_VALUE;
	pageNames_t futurePage = PAGE_INITIAL_VALUE;

	bool procedurePageAdveancedOptionsEnabled = false;
	bool lowDiskSpace = false;
    int m_statusTimer = 0;
	int m_enumerateTimer = 0;
	int m_leadoffTimer = 0;
	int m_liveEcgTimer = 0;
	int m_bluetoothTimer = 0;
	int m_activationTimer = 0;
	int m_upload_timeout = 0;
	int statusMessageTimer = 0;
    int m_procType = 0;
    int m_procState = 0;
	int m_leadOff = 0;
	int m_cableId = 0;
	uint32_t m_ecgSeq = 0;
	bool m_showLead = false;
	quint8 m_percentage = 0;
	int m_files_to_send = 0;
	int m_files_sent = 0;
	QIODevice *uploadData = nullptr;
	bool upload_cancelled = false;
    bool downloadOptionsActive = false;

	QPixmap m_torsoImage = QPixmap( "images/XLead_XChannel.png" );
	QPixmap m_sirona_PER_image = QPixmap( "sirona_per.png" );
	QPixmap m_patch_image = QPixmap( "patch_small.png" );

	QIcon m_usb_image = QIcon( "images/usb_image.png" );
	QIcon m_bt_image = QIcon( "images/bt_image.png" );
	QIcon m_caution_symbol = QIcon( "images/caution_symbol.png" );

	QPanelExitOperation m_exitScreenOperation = QPanelExitOperationNoOperation;

	QHash<QString,QCompleter*> hashCompleters;

signals:
	void connectPwmDevice( deviceInfo_t device );
	void disconnectPwmDevice( bool silant, int retVal );
	void isDeviceConnected();
	void sendDeviceConfig( ConfigParams devConf );
	void isThereRecordedData();
	void saveDataOnDevice( bool saveEvents, QString eventPath, bool saveHolter, QString holterPath );
	void deleteDataOnDevice();
	void parameterRead( QString parName );
	void parameterListWrite( ConfigParams devConf, QList<QString> paramNamesList );
	void parameterReadAll();
	void eventGetList();
	void firmwareUpload( QString filename );
	void getBatteryStatus();
	void startProcedure();
	void stopProcedure();
	void softReset();
	void setDeviceTime();
	void enumerateBtDevices();
	void deviceStartLiveECG();
	void markFilesSent( qint32 );
	void setZipPassword( QString );
	void getRequiredFields();
    void getStatus();
    void logDownload();
	void setZipPrefix( QString prefixForZip );

private:
	void populateDefaultConfig( ConfigParams *defaultConfig );
	void evaluateConfigData( ConfigParams *config );
	QString getPatientInfoSummaryString();
	void updateElectrodesWidget();
	void updateDeviceScreenWidget();
	static void paintElectrodeLead( QPainter &painter, int x, int y, quint8 leadOff, quint8 leadId );
	void restartBluetoothTimer();
	void stopBluetoothTimer();
    void on_pageCable_Button_clicked(quint8 devMode);
	void updateShortcut( QString iconName );
	void initGetTextCompleters();
	QString getTextCompleterThing( QLineEdit *edtField, QString whichCompleter );
	QList< QHash<QString,QString> > getDiaryEntries();


private slots:
	void showFloatingDisplay( QString str );
	void getNewConfiguration();
	void onQueryConfigFinish( QNetworkReply *reply );
	bool readConfigFromDevice();
	void sendReportViaHttp( QString datFileName );
	void sendReportViaFtp( QString datFileName );
	void uploadFinished( QNetworkReply *reply );
    void statusReceived( quint8 leadOff, quint8 cableId, quint8 procState, quint8 procType);
	void pwmDeviceConnectStatus( bool connected, QString devSerNum , int devType );
	void pwmDeviceHandlerMessage( bool silant, bool success, QString message );
	void pwmDeviceConfiguration( ConfigParams devConf );
	void pwmDeviceParameterListWritten( bool parameterListWrittenWithSuccess );
    void pwmDeviceThereIsDataRecorded( bool hasData );
	void pwmDeviceDataSaved ( bool eventSaveSuccess , bool holterSaveSuccess, QList<QString> uploadFileList );
	void pwmDeviceDataDeleted ( bool deleteSuccess );
	void pwmDeviceBatteryStatus( quint32 batteryVoltage );
	void pwmDeviceProcedureStopped();
	void pwmDeviceDownloadProgress ( quint32 current , quint32 total );
	void pwmDeviceEnumerateBtDevices( QList<deviceInfo_t> devices );
	void pwmDeviceStopLiveECG( int errorCode );
	void pwmDeviceLiveECG( uint32_t seq, uint32_t status, QByteArray data );
	void pwmDeviceWarning( QString warning );
	void pwmDeviceComputerOutOfSpace( QString message );
    void pwmSoftResetComplete();
	void contentStackedWidgetPageChanged( int index );
	void on_pageWelcome_NextButton_clicked();
	void on_pageConnection_LogoLabel_clicked();
	void on_pageConnection_NextButton_clicked();
	void on_pageCable_LogoLabel_clicked();
	void on_pageCable_GearButton_clicked();
	void on_pageCable_HolterButton_clicked();
	void on_pageCable_EventButton_clicked();
	void on_pageCable_PostButton_clicked();
	void on_pageCable_MCTButton_clicked();
	void on_pagePatient_LogoLabel_clicked();
	void on_pagePatient_GearButton_clicked();
	void on_pagePatient_BackButton_clicked();
	void on_pagePatient_NextButton_clicked();
	void on_pagePatientMain_CommentTextEdit_textChanged();
	void cable_setupUI();
    void pageConnections_setupUI();
	void pagePatient_setupUI();
	void on_pageProcedure_LogoLabel_clicked();
	void on_pageProcedureMain_AdvancedOptionsLabel_clicked();
	void on_pageProcedure_GearButton_clicked();
	void on_pageProcedure_BackButton_clicked();
	void on_pageProcedure_NextButton_clicked();
	void pageProcedure_setupUI( bool fullSetup );
	void on_pageUSBSaved_LogoLabel_clicked();
	void on_pageUSBSaved_GearButton_clicked();
	void on_pageUSBSaved_BackButton_clicked();
	void on_pageHookup_LogoLabel_clicked();
	void on_pageHookup_GearButton_clicked();
	void on_pageHookup_BackButton_clicked();
	void on_pageHookup_NextButton_clicked();
	void on_pageECG_LogoLabel_clicked();
	void on_pageECG_GearButton_clicked();
	void on_pageECG_BackButton_clicked();
	void on_pageECG_NextButton_clicked();
	void on_pageProcedureActive_LogoLabel_clicked();
	void on_pageProcedureActive_GearButton_clicked();
	void on_pageProcedureActive_DisconnectButton_clicked();
	void on_pageProcedureActive_EndProcedureButton_clicked();
    void on_pageDownloadOptions_LogoLabel_clicked();
    void on_pageDownloadOptions_GearButton_clicked();
    void on_pageDownloadOptions_ReviewButton_clicked();
    void on_pageDownloadOptions_DownloadButton_clicked();
    void on_pageDownload_LogoLabel_clicked();
	void on_pageDownload_GearButton_clicked();
	void on_pageDownload_DisconnectButton_clicked();
    void on_pageDownloadOptions_EraseButton_clicked();
    void on_pageErase_EraseButton_clicked();
    void on_pageErase_BackButton_clicked();
    void on_pageErase_GearButton_clicked();
	void on_pageComplete_LogoLabel_clicked();
	void on_pageComplete_GearButton_clicked();
	void on_pageComplete_NewProcedureButton_clicked();
	void on_pageDevice_LogoLabel_clicked();
	void on_pageDevice_GearButton_clicked();
	void on_pageDevice_BackButton_clicked();
	void on_pageDevice_NextButton_clicked();
	void on_pageDevice_DisconnectButton_clicked();
	void on_pageDevice_MuteCheckBox_toggled( bool checked );
    void on_edtDiagnosis_editingFinished();
	void on_edtActivity_editingFinished();
	void on_edtSymptoms_editingFinished();
    void on_edtSiteCode_editingFinished();
    void on_edtSiteCode_textChanged( const QString &str );
	void pageProcedureActive_setupUI();
    void pageDownloadOptions_setupUI();
    void pageErase_setupUI();
    void pageDevice_setupUI();
	void pageHookup_setupUI();
	void pageEcg_setupUI();
	void pageComplete_setupUI();
	void on_pageCompany_NextButton_clicked();
	void on_pageProcedureMain_SampleRateComboBox_activated( const QString &text );
	void on_pageWelcome_LogoLabel_clicked();
	void on_pageConnection_ChooseDevicelistWidget_itemDoubleClicked( QListWidgetItem *item );
	void on_pageConnection_ChooseDevicelistWidget_itemSelectionChanged();
	void removeStatusMessage();
	void on_pageProcedureMain_RecordingLengthSelector_indexChanged( int index );
	void on_pageProcedureMain_ManualRecordingLengthSpinBox_valueChanged( int value );
	void on_webAddressLabel_linkActivated( const QString &link );
	void onUploadPercentageChange( qint64 percentage );
	void reloadUploadTimer();
	void file_handler();
	void preprocess_stylesheet( QString *text );
	void cancelUpload();
    void requiredFields(uint8_t, uint8_t, uint8_t, uint8_t, uint8_t, uint8_t, uint8_t, uint8_t, uint8_t);
	void patientFieldChanged();
	void savePatientInfo();
	void saveProcedureInfo();
    void on_btnAddDiaryEntry_clicked();
    void on_cbNoDiaryEventsNoted_toggled(bool checked);
	void tableItemClicked(QTableWidgetItem *);

};

extern char *strProcType( int mode );
extern char *strProcState( int state );

extern QString glbCustomCode;

#endif // SIRONAVIEWERUI_H
