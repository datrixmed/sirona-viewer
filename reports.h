#ifndef REPORTS_H
#define REPORTS_H

#include <QDebug>

#include <QDateTime>
#include <QPrinter>
#include <QWidget>
#include <QPen>
#include "utilities.h"
#include "interface.h"
#include "ecgdata.h"
#include "version.h"

#define DOT_PER_MM(dc,dots)					((dots) * dc->device()->logicalDpiY() / (INCH_TO_MM))
#define DPM(dc,dots)						(DOT_PER_MM(dc,dots))
#define REPORTHEADER_HEIGHT_MM				(10)	/* millimeters */
#define REPORTHEADER_HEIGHT(dc)				(DOT_PER_MM(dc,REPORTHEADER_HEIGHT_MM))
#define REPORTHEADER_INTERIOR_MARGIN_MM		(8)	/* millimeters */
#define REPORTHEADER_INTERIOR_MARGIN(dc)	(DOT_PER_MM(dc,REPORTHEADER_INTERIOR_MARGIN_MM))

#define MAX_GRAPHS_PER_PAGE		((pPainter->device()->height() - ECGGRID_MARGIN_TOP) / ECGGRID_STRIP_HEIGHT_AND_MARGIN)
#define TOTAL_GRAPHS_TO_PRINT	(theEcgData->datalen_secs / ECG_DISPLAY_WINDOW_SIZE_SECONDS * theEcgData->channel_count)
#define CEILING(a,b)			(((a) % (b)) ? (a) / (b) + 1 : (a)  / (b))

extern QString getServerUrl();
extern QString EncodeXML( const QString &encodeMe );
extern QString DecodeXML( const QString &decodeMe );

typedef enum {
	REPORT_30_SEC = 0,
	REPORT_60_SEC
} ReportsECGLength_t;

typedef void ( *func_t )( QPainter * ); // pointer to function with no args and void return


class Reports : public QObject
{
	Q_OBJECT

public:

	Reports( EcgData *pEcgData = 0, ConfigParams *configSector = 0 );
	~Reports();

	void save( QString fileName );
	void restore( QString fileName );

	QPrinter *setPrinter( QPrinter *pPrinter )
	{
		if ( pConfigSector ) {
			QString version;
			QString major = QString::number( ( pConfigSector->fw_rev >> 8 ) & 0xff );
			QString minor;
			minor.sprintf( "%.2d", pConfigSector->fw_rev & 0xFF );
			QString revision = QString::number( pConfigSector->svn_rev );

			version = major + "." + minor + "." + revision;

			pPrinter->setCreator( QString( "Generic Sirona PC App rev %1.%2, device firmware %3" ).arg( MAJOR_REVISION ).arg( MINOR_REVISION ).arg( version ) );
		} else {
			pPrinter->setCreator( QString( "Generic Sirona PC App rev %1.%2, device firmware is UNKNOWN" ).arg( MAJOR_REVISION ).arg( MINOR_REVISION ) );
		}
		return thePrinter = pPrinter;
	}
	void setEcgData( EcgData *pEcgData )
	{
		theEcgData = pEcgData;
		qDebug() << "Reports::setEcgData(" << ( long )pEcgData << ")";
	}
	void setMetaData( QMap < QString, QString > metaData )
	{
		theMetaData = metaData;
	}
	void setDate ( QDateTime date )
	{
		theDate = date;
	}
	void generateXmlTo( QString xmlPathName );

	EcgData *getEcgData( void )
	{
		return theEcgData;
	}
	QMap < QString, QString > getMetaData ( void )
	{
		return theMetaData;
	}
	QDateTime getDate( void )
	{
		return theDate;
	}

	QString pdfTempFilename()
	{
		return pdfFilename;
	}
	void setPageNumber( int pgNum )
	{
		pageNum = pgNum;
	}

	void pageEcg( QPainter *pPainter );
	void printHtml( QPainter *pPainter, QString filename );

protected:
	int pageNum;
	int pageMax;

	QDateTime theDate;

	QString pdfFilename;

	EcgData *theEcgData;
	ConfigParams *pConfigSector;
	QPrinter *thePrinter;

	QMap < QString, QString > theMetaData;

	void newPage();
	void ShowGrid( QPainter *pPainter, int gridbox_cols, int height_mm, int ecgSeconds, double xScale = 1.0, double yScale = 1.0 );
	void ShowData( QPainter *pPainter, long samplePos, int chanNum, int graphCnt );
	void pageHeader( QPainter *pPainter,  QString title );

public slots:
	void doPaint( QPrinter *thePrinter );

private:
	QPen m_GraphPen;
};


extern QString getLocalStoragePath( QString subFolder = "" );

#endif // REPORTS_H
