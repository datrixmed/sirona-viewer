
#include <QSettings>

#include "Sironaviewerui.h"
#include "ui_Sironaviewerui.h"



void SironaViewerUi::initGetTextCompleters()
{
	if ( customer->customCode == "ACT1" ) {
		hashCompleters["sitecode"] = NULL;
		hashCompleters["diagnosis"] = NULL;
		hashCompleters["activities"] = NULL;
		hashCompleters["symptoms"] = NULL;

		getTextCompleterThing( m_ui->edtSiteCode, "sitecode" );
		getTextCompleterThing( m_ui->edtDiagnosis, "diagnosis" );
		getTextCompleterThing( m_ui->edtSymptoms, "symptoms" );
		getTextCompleterThing( m_ui->edtActivity, "activities" );
	}
}



QString SironaViewerUi::getTextCompleterThing( QLineEdit *edtField, QString whichCompleter )
{
	// qDebug() << QString("getTextCompleterThing(%1,%2)").arg(edtField->text()).arg(whichCompleter);

	QSettings settings( QSettings::IniFormat, QSettings::UserScope, MANUFACTURER_NAME, APPLICATION_NAME );

	QString configKey = QString("TextList%1").arg(  whichCompleter.replace(0, 1, whichCompleter[0].toUpper()) );

	QString str = edtField->text();
	QStringList lstPhrases = settings.value(configKey).toStringList();
	if ( ! str.isEmpty() && ! lstPhrases.contains(str) && (str.count() > 1) ) {
		lstPhrases << str;
		settings.setValue( configKey, lstPhrases );	/* update the settings file with the new phrase */
	}

	QCompleter *theCompleter = hashCompleters[whichCompleter];

	if ( theCompleter ) {
		delete theCompleter;
	}

	lstPhrases.sort();
	theCompleter = new QCompleter(lstPhrases, this);
	// theCompleter->setCompletionMode( QCompleter::InlineCompletion );
	theCompleter->setCompletionMode( QCompleter::UnfilteredPopupCompletion );
	theCompleter->setCaseSensitivity(Qt::CaseInsensitive);
	edtField->setCompleter(theCompleter);

	return str;
}

