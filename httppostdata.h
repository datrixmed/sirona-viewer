#ifndef HTTPPOSTDATA_H
#define HTTPPOSTDATA_H

#include <QIODevice>
#include <QFile>

class HttpPostData : public QIODevice
{
	Q_OBJECT

public:
	HttpPostData( QString serialNumber, const char *requestTypeVal, QString boundary, QString zipFileName );
	~HttpPostData();
	bool open( OpenMode mode );
	void close();
	bool atEnd() const;
	qint64 bytesAvailable() const;
	bool isSequential() const;
	qint64 size() const;

protected:
	qint64 readData( char *data, qint64 maxSize );
	qint64 writeData( const char *data, qint64 len );

private:
	qint64 totalSize;
	char preamble[1000];
	qint64 preambleSize;
	char postamble[1000];
	qint64 postambleSize;
	QFile *zipFile;

	qint64 lastReportedPercentage = -1;
signals:
	void percentageChanged( qint64 percentage );
	void reloadTimer();
};

#endif // HTTPPOSTDATA_H
