<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="cs_CZ">
<context>
    <name>DeviceHandler</name>
    <message>
        <location filename="../devicehandler.cpp" line="91"/>
        <location filename="../devicehandler.cpp" line="96"/>
        <location filename="../devicehandler.cpp" line="129"/>
        <source>ERROR: </source>
        <translation>CHYBA:</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="304"/>
        <source>ERROR: Error happened during event download.</source>
        <translation>CHYBA: Během stahování události došlo k chybě.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="363"/>
        <source>INFO: Read parameter command executed with success.</source>
        <translation>INFO: Příkaz čtení parametru provedený s úspěchem.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="454"/>
        <source>ERROR: Invalid parameter or parameter not writable.</source>
        <translation>CHYBA: Neplatný parametr nebo parametr nelze zapisovat.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="498"/>
        <source>INFO: Commit parameters command executed with success.</source>
        <translation>INFO: Příkaz pro potvrzení parametrů byl proveden s úspěchem.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="543"/>
        <source>Read all parameters command executed with success.</source>
        <translation>Příkaz pro přečtení všech parametrů proveden s úspěchem.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="582"/>
        <source>INFO: Get Event Header command, for Event nr.%1 executed with success.</source>
        <translation>INFO: Získání příkazu Záhlaví události, pro událost č.%1 provedené s úspěchem.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="593"/>
        <source>ERROR: Invalid event size returned by the device.
Command execution cannot proceed.</source>
        <translation>CHYBA: Zařízení vrátilo neplatnou velikost události.
Provedení příkazu nemůže pokračovat.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="617"/>
        <source>ERROR: Couldn&apos;t open event file to write to.
Command execution cannot proceed.</source>
        <translation>CHYBA: Soubor události pro zápis se nepodařilo otevřít.
Provedení příkazu nemůže pokračovat.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="660"/>
        <location filename="../devicehandler.cpp" line="831"/>
        <source>ERROR: Couldn&apos;t open annotation file to write to.
Command execution cannot proceed.</source>
        <translation>CHYBA: Soubor anotací pro zápis se nepodařilo otevřít.
Provedení příkazu nemůže pokračovat.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="708"/>
        <source>INFO: Event %1 from %2 downloaded with success.</source>
        <translation>INFO: Událost %1 z %2 byla stažena s úspěchem.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="721"/>
        <source>INFO: Event %1 marked as sent with success.</source>
        <translation>INFO: Událost %1 byla označena jako úspěšně odeslána.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="723"/>
        <source>ERROR: Event downloaded with success, but failed to mark Event as sent.</source>
        <translation>CHYBA: Událost byla stažena s úspěchem, ale nepodařilo se označit událost jako odeslanou.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="732"/>
        <location filename="../devicehandler.cpp" line="739"/>
        <source>ERROR: Holter downloaded with success, but failed to mark Holter as sent.</source>
        <translation>CHYBA: Holter se stáhl s úspěchem, ale nedokázal označit Holtera jako odeslaného.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="741"/>
        <source>INFO: Holter marked as sent with success.</source>
        <translation>INFO: Holter byl úspěšně označen jako odeslaný.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="788"/>
        <source>ERROR: Couldn&apos;t open holter file to write to.
Command execution cannot proceed.</source>
        <translation>CHYBA: Nelze otevřít soubor holter pro zápis.
Provedení příkazu nemůže pokračovat.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="802"/>
        <source>ERROR: There was an error during ECG data transfer.</source>
        <translation>CHYBA: Při přenosu dat EKG došlo k chybě.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="862"/>
        <source>INFO: Download holter command executed with success.</source>
        <translation>INFO: Příkaz k stáhnutú holtera provedený s úspěchem.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="876"/>
        <source>INFO: Get battery status command executed with success.</source>
        <translation>INFO: Získání příkazu stavu baterie provedeno s úspěchem.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="892"/>
        <source>INFO: Start Procedure command executed with success.</source>
        <translation>INFO: Příkaz pro Start Procedury proveden s úspěchem.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="908"/>
        <source>INFO: Stop Procedure command executed with success.</source>
        <translation>INFO: Příkaz pro Stop procedury proveden s úspěchem.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="924"/>
        <source>INFO: Soft Reset command executed with success.</source>
        <translation>INFO: Příkaz pro Soft reset proveden s úspěchem.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="952"/>
        <source>INFO: Successfully sent system time to the device.</source>
        <translation>INFO: Systémový čas byl úspěšně odeslán do zařízení.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="1146"/>
        <source>ERROR: createHeaFile called with nullptr eventHeader.</source>
        <translation>CHYBA: createHeaFile s názvem nullptr eventHeader.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="1216"/>
        <source>ERROR: createHeaFile called with NULL eventHeader.</source>
        <translation>CHYBA: createHeaFile s názvem NULL eventHeader.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="1294"/>
        <source>ERROR: Invalid input parameter device mode, couldn&apos;t create hea file.</source>
        <translation>CHYBA: Neplatný režim zařízení vstupních parametrů, nelze vytvořit soubor hea.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="1301"/>
        <source>ERROR: Couldn&apos;t open header file to write to.</source>
        <translation>CHYBA: Nelze otevřít soubor záhlaví pro zápis.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="1313"/>
        <source>File open json file for writing(%1).</source>
        <translation>Soubor otevřít soubor json pro zápis (%1).</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="1403"/>
        <source>ERROR: createResFile failed.</source>
        <translation>CHYBA: createResFile se nezdařilo.</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../Sironaviewerui.cpp" line="1042"/>
        <source>Sirona Disconnected!</source>
        <translation>Sirona odpojena!</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="95"/>
        <source>Another instance of this application is already running</source>
        <translation>Další instance této aplikace již běží</translation>
    </message>
    <message>
        <location filename="../utilities.cpp" line="37"/>
        <location filename="../utilities.cpp" line="43"/>
        <source>Error creating directory </source>
        <translation>Při vytváření adresáře došlo k chybě</translation>
    </message>
    <message>
        <location filename="../utilities.cpp" line="37"/>
        <location filename="../utilities.cpp" line="43"/>
        <source>. Check path and permissions.</source>
        <translation>. Zkontrolujte cestu a oprávnění.</translation>
    </message>
</context>
<context>
    <name>SironaViewerUi</name>
    <message>
        <location filename="../Sironaviewerui.ui" line="32"/>
        <source>Sirona Viewer</source>
        <translation>Prohlížeč Sirona</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="325"/>
        <location filename="../Sironaviewerui.ui" line="1297"/>
        <location filename="../Sironaviewerui.ui" line="2315"/>
        <location filename="../Sironaviewerui.ui" line="4208"/>
        <location filename="../Sironaviewerui.ui" line="7244"/>
        <location filename="../Sironaviewerui.ui" line="12129"/>
        <location filename="../Sironaviewerui.ui" line="12881"/>
        <location filename="../Sironaviewerui.ui" line="13721"/>
        <location filename="../Sironaviewerui.ui" line="14539"/>
        <location filename="../Sironaviewerui.ui" line="15810"/>
        <location filename="../Sironaviewerui.ui" line="17506"/>
        <location filename="../Sironaviewerui.ui" line="18766"/>
        <location filename="../Sironaviewerui.ui" line="19675"/>
        <location filename="../Sironaviewerui.ui" line="20754"/>
        <source>800 555 1234</source>
        <translation>800 555 1234</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="514"/>
        <source>Sirona</source>
        <translation>Sirona</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="606"/>
        <source>Procedure setup
and
data download</source>
        <translation>Nastavení procedury a stahování dat</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="913"/>
        <source>Begin</source>
        <translation>Začít</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="1467"/>
        <source>Connect</source>
        <translation>Připojit</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="1549"/>
        <location filename="../Sironaviewerui.ui" line="2596"/>
        <location filename="../Sironaviewerui.ui" line="4593"/>
        <location filename="../Sironaviewerui.ui" line="11693"/>
        <location filename="../Sironaviewerui.ui" line="12477"/>
        <location filename="../Sironaviewerui.ui" line="14106"/>
        <location filename="../Sironaviewerui.ui" line="14820"/>
        <location filename="../Sironaviewerui.ui" line="16091"/>
        <location filename="../Sironaviewerui.ui" line="17787"/>
        <location filename="../Sironaviewerui.ui" line="19026"/>
        <location filename="../Sironaviewerui.ui" line="19956"/>
        <location filename="../Sironaviewerui.ui" line="21139"/>
        <source>This is the status message bar</source>
        <translation>Toto je stavová lišta</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="1656"/>
        <source>Choose Sirona device</source>
        <translation>Zvolte Sirona zařízení</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="2052"/>
        <location filename="../Sironaviewerui.ui" line="6983"/>
        <location filename="../Sironaviewerui.ui" line="11869"/>
        <location filename="../Sironaviewerui.ui" line="13463"/>
        <source>Next</source>
        <translation>Další</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="2514"/>
        <source>Cable</source>
        <translation>Kabel</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="3005"/>
        <source>Holter</source>
        <translation>Holter</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="3320"/>
        <source>Event</source>
        <translation>Událost</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="3635"/>
        <source>Post-Event</source>
        <translation>Po události</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="3950"/>
        <source>MCT</source>
        <translation>MCT</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="4471"/>
        <source>Patient</source>
        <translation>Pacient</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="4779"/>
        <source>Patient ID#:</source>
        <translation>ID pacienta:</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="5064"/>
        <source>Patient Name:</source>
        <translation>Jméno pacienta:</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="5207"/>
        <source>last name</source>
        <translation>Příjmení</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="5346"/>
        <source>first name</source>
        <translation>Jméno</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="5434"/>
        <source>middle initial</source>
        <translation>Střední iniciál</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="5598"/>
        <source>Date of Birth:</source>
        <translation>Datum narození:</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="5811"/>
        <source>Physician name:</source>
        <translation>Jméno lékaře:</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="6102"/>
        <source>Technician name:</source>
        <translation>Jméno technika:</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="6393"/>
        <source>Facility:</source>
        <translation>Zařízení:</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="6684"/>
        <source>Comment:</source>
        <translation>Komentář:</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="6815"/>
        <source>insert comment (up to 500 characters)</source>
        <translation>vložit komentář (až 500 znaků)</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="7507"/>
        <source>Procedure</source>
        <translation>Procedura</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="7753"/>
        <location filename="../Sironaviewerui.ui" line="21443"/>
        <source>Monitor battery</source>
        <translation>Baterie monitoru</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="7988"/>
        <source>Pre-trigger</source>
        <translation>Před spuštěním</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8068"/>
        <location filename="../Sironaviewerui.ui" line="8343"/>
        <source>15 sec</source>
        <translation>15 sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8073"/>
        <location filename="../Sironaviewerui.ui" line="8348"/>
        <source>30 sec</source>
        <translation>30 sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8078"/>
        <location filename="../Sironaviewerui.ui" line="8353"/>
        <source>45 sec</source>
        <translation>45 sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8083"/>
        <location filename="../Sironaviewerui.ui" line="8358"/>
        <source>60 sec</source>
        <translation>60 sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8088"/>
        <location filename="../Sironaviewerui.ui" line="8363"/>
        <source>90 sec</source>
        <translation>90 sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8093"/>
        <location filename="../Sironaviewerui.ui" line="8368"/>
        <source>120 sec</source>
        <translation>120 sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8098"/>
        <location filename="../Sironaviewerui.ui" line="8373"/>
        <source>180 sec</source>
        <translation>180 sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8103"/>
        <location filename="../Sironaviewerui.ui" line="8378"/>
        <location filename="../Sironaviewerui.cpp" line="3305"/>
        <source>300 sec</source>
        <translation>300 sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8263"/>
        <location filename="../Sironaviewerui.cpp" line="3226"/>
        <location filename="../Sironaviewerui.cpp" line="3277"/>
        <source>Post-trigger</source>
        <translation>Po spuštění</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8538"/>
        <source>Record duration</source>
        <translation>Trvání záznamu</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8545"/>
        <location filename="../Sironaviewerui.ui" line="9121"/>
        <location filename="../Sironaviewerui.ui" line="9476"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8545"/>
        <location filename="../Sironaviewerui.ui" line="9126"/>
        <location filename="../Sironaviewerui.ui" line="9481"/>
        <source>2</source>
        <translation>2</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8545"/>
        <location filename="../Sironaviewerui.ui" line="9141"/>
        <location filename="../Sironaviewerui.ui" line="9496"/>
        <source>5</source>
        <translation>5</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8545"/>
        <location filename="../Sironaviewerui.ui" line="9151"/>
        <location filename="../Sironaviewerui.ui" line="9506"/>
        <source>7</source>
        <translation>7</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8545"/>
        <location filename="../Sironaviewerui.ui" line="9166"/>
        <location filename="../Sironaviewerui.ui" line="9521"/>
        <source>10</source>
        <translation>10</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8545"/>
        <location filename="../Sironaviewerui.ui" line="9186"/>
        <location filename="../Sironaviewerui.ui" line="9541"/>
        <source>14</source>
        <translation>14</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8717"/>
        <source>Manual Set</source>
        <translation>Ruční nastavení</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8724"/>
        <location filename="../Sironaviewerui.cpp" line="4296"/>
        <source> day</source>
        <translation>den</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8877"/>
        <source>Advanced options &gt;&gt;&gt;</source>
        <translation>Pokročilé možnosti &gt;&gt;&gt;</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9036"/>
        <source>Auto Event Limit</source>
        <translation>Limit automatické události</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9116"/>
        <location filename="../Sironaviewerui.ui" line="9471"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9131"/>
        <location filename="../Sironaviewerui.ui" line="9486"/>
        <source>3</source>
        <translation>3</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9136"/>
        <location filename="../Sironaviewerui.ui" line="9491"/>
        <source>4</source>
        <translation>4</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9146"/>
        <location filename="../Sironaviewerui.ui" line="9501"/>
        <source>6</source>
        <translation>6</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9156"/>
        <location filename="../Sironaviewerui.ui" line="9511"/>
        <source>8</source>
        <translation>8</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9161"/>
        <location filename="../Sironaviewerui.ui" line="9516"/>
        <source>9</source>
        <translation>9</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9171"/>
        <location filename="../Sironaviewerui.ui" line="9526"/>
        <source>11</source>
        <translation>11</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9176"/>
        <location filename="../Sironaviewerui.ui" line="9531"/>
        <source>12</source>
        <translation>12</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9181"/>
        <location filename="../Sironaviewerui.ui" line="9536"/>
        <source>13</source>
        <translation>13</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9191"/>
        <location filename="../Sironaviewerui.ui" line="9546"/>
        <source>15</source>
        <translation>15</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9196"/>
        <location filename="../Sironaviewerui.ui" line="9551"/>
        <source>16</source>
        <translation>16</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9201"/>
        <location filename="../Sironaviewerui.ui" line="9556"/>
        <source>17</source>
        <translation>17</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9206"/>
        <location filename="../Sironaviewerui.ui" line="9561"/>
        <source>18</source>
        <translation>18</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9211"/>
        <location filename="../Sironaviewerui.ui" line="9566"/>
        <source>19</source>
        <translation>19</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9216"/>
        <location filename="../Sironaviewerui.ui" line="9571"/>
        <source>20</source>
        <translation>20</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9221"/>
        <location filename="../Sironaviewerui.ui" line="9576"/>
        <source>50</source>
        <translation>50</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9226"/>
        <location filename="../Sironaviewerui.ui" line="9581"/>
        <source>100</source>
        <translation>100</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9231"/>
        <location filename="../Sironaviewerui.ui" line="9586"/>
        <source>1000</source>
        <translation>1000</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9391"/>
        <source>Manual Event Limit</source>
        <translation>Limit ruční události</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9746"/>
        <source>TTM Speed</source>
        <translation>Rychlost TTM</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9826"/>
        <source>1x</source>
        <translation>1x</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9831"/>
        <source>3x</source>
        <translation>3x</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9991"/>
        <source>Tachy</source>
        <translation>Tachy</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10071"/>
        <location filename="../Sironaviewerui.ui" line="10436"/>
        <location filename="../Sironaviewerui.ui" line="10726"/>
        <location filename="../Sironaviewerui.ui" line="11056"/>
        <source>Off</source>
        <translation>Vyp</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10076"/>
        <source>120 bpm</source>
        <translation>120 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10081"/>
        <source>125 bpm</source>
        <translation>125 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10086"/>
        <source>130 bpm</source>
        <translation>130 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10091"/>
        <source>135 bpm</source>
        <translation>135 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10096"/>
        <source>140 bpm</source>
        <translation>140 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10101"/>
        <source>145 bpm</source>
        <translation>145 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10106"/>
        <source>150 bpm</source>
        <translation>150 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10111"/>
        <source>155 bpm</source>
        <translation>155 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10116"/>
        <source>160 bpm</source>
        <translation>160 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10121"/>
        <source>165 bpm</source>
        <translation>165 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10126"/>
        <source>170 bpm</source>
        <translation>170 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10131"/>
        <source>175 bpm</source>
        <translation>175 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10136"/>
        <source>180 bpm</source>
        <translation>180 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10141"/>
        <source>185 bpm</source>
        <translation>185 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10146"/>
        <source>190 bpm</source>
        <translation>190 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10151"/>
        <source>195 bpm</source>
        <translation>195 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10156"/>
        <source>200 bpm</source>
        <translation>200 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10161"/>
        <source>205 bpm</source>
        <translation>205 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10166"/>
        <source>210 bpm</source>
        <translation>210 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10171"/>
        <source>215 bpm</source>
        <translation>215 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10176"/>
        <source>220 bpm</source>
        <translation>220 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10181"/>
        <source>225 bpm</source>
        <translation>225 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10186"/>
        <source>230 bpm</source>
        <translation>230 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10191"/>
        <source>235 bpm</source>
        <translation>235 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10196"/>
        <source>240 bpm</source>
        <translation>240 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10356"/>
        <source>Brady</source>
        <translation>Brady</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10441"/>
        <source>15 bpm</source>
        <translation>15 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10446"/>
        <source>20 bpm</source>
        <translation>20 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10451"/>
        <source>25 bpm</source>
        <translation>25 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10456"/>
        <source>30 bpm</source>
        <translation>30 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10461"/>
        <source>35 bpm</source>
        <translation>35 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10466"/>
        <source>40 bpm</source>
        <translation>40 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10471"/>
        <source>45 bpm</source>
        <translation>45 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10476"/>
        <source>50 bpm</source>
        <translation>50 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10481"/>
        <source>55 bpm</source>
        <translation>55 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10486"/>
        <source>60 bpm</source>
        <translation>60 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10646"/>
        <source>Pause</source>
        <translation>Pauza</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10731"/>
        <source>1.5 sec</source>
        <translation>1,5 sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10736"/>
        <source>2.0 sec</source>
        <translation>2,0 sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10741"/>
        <source>2.5 sec</source>
        <translation>2,5 sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10746"/>
        <source>3.0 sec</source>
        <translation>3,0 sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10751"/>
        <source>3.5 sec</source>
        <translation>3,5 sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10756"/>
        <source>4.0 sec</source>
        <translation>4,0 sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10761"/>
        <source>4.5 sec</source>
        <translation>4,5 sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10766"/>
        <source>5.0 sec</source>
        <translation>5,0 sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10771"/>
        <source>5.5 sec</source>
        <translation>5,5 sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10776"/>
        <source>6.0 sec</source>
        <translation>6,0 sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10781"/>
        <source>6.5 sec</source>
        <translation>6,5 sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10786"/>
        <source>7.0 sec</source>
        <translation>7,0 sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10791"/>
        <source>7.5 sec</source>
        <translation>7,5 sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10796"/>
        <source>8.0 sec</source>
        <translation>8,0 sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10801"/>
        <source>8.5 sec</source>
        <translation>8,5 sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10806"/>
        <source>9.0 sec</source>
        <translation>9,0 sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10811"/>
        <source>9.5 sec</source>
        <translation>9,5 sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10816"/>
        <source>10.0 sec</source>
        <translation>10,0 sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10976"/>
        <source>Afib</source>
        <translation>Afib</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="11061"/>
        <source>On</source>
        <translation>Zap</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="11221"/>
        <source>Sample rate</source>
        <translation>Vzorkovací frekvence</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="11301"/>
        <location filename="../Sironaviewerui.cpp" line="4316"/>
        <source>128 sps</source>
        <translation>128 sps</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="11306"/>
        <location filename="../Sironaviewerui.cpp" line="4155"/>
        <location filename="../Sironaviewerui.cpp" line="4314"/>
        <source>256 sps</source>
        <translation>256 sps</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="11466"/>
        <source>Pacemaker</source>
        <translation>Kardiostimulátor</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="11546"/>
        <source>OFF</source>
        <translation>VYP</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="11551"/>
        <source>ON</source>
        <translation>ZAP</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="12392"/>
        <source>Settings Saved</source>
        <translation>Nastavení uloženo</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="12584"/>
        <source>Settings have been saved.

Disconnect USB and connect a patient cable to finish setting up the patient. 

Use Bluetooth to confirm hookup.</source>
        <translation>Nastavení bylo uloženo.

Odpojte USB a připojte kabel pacienta pro dokončení nastavení pacienta.

Připojení potvrďte pomocí Bluetooth.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="13162"/>
        <location filename="../Sironaviewerui.cpp" line="4123"/>
        <source>Attach Electrodes</source>
        <translation>Připojte elektrody</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="13984"/>
        <source>ECG</source>
        <translation>EKG</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="14279"/>
        <source>Start</source>
        <translation>Start</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="14738"/>
        <source>Procedure Active</source>
        <translation>Procedura aktivní</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="14927"/>
        <source>This monitor is currently active in a procedure.</source>
        <translation>Tento monitor je aktuálně aktivní v proceduře.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="15235"/>
        <location filename="../Sironaviewerui.ui" line="18506"/>
        <location filename="../Sironaviewerui.ui" line="22065"/>
        <source>Disconnect</source>
        <translation>Odpojit</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="15550"/>
        <source>End Procedure</source>
        <translation>Ukončit proceduru</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="16009"/>
        <location filename="../Sironaviewerui.ui" line="17705"/>
        <location filename="../Sironaviewerui.cpp" line="1267"/>
        <location filename="../Sironaviewerui.cpp" line="2522"/>
        <source>Downloading</source>
        <translation>Stahování</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="16211"/>
        <location filename="../Sironaviewerui.ui" line="19118"/>
        <source>&lt;Patient Info&gt;</source>
        <translation>&lt;Info o pacientovi&gt;</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="16307"/>
        <source>Review patient information before download?</source>
        <translation>Zkontrolovat informace o pacientovi před stažením?</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="16628"/>
        <source>Review</source>
        <translation>Zkontrolovat</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="16943"/>
        <source>Download</source>
        <translation>Stáhnout</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="17246"/>
        <location filename="../Sironaviewerui.ui" line="19303"/>
        <source>Erase</source>
        <translation>Vymazat</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="17904"/>
        <location filename="../Sironaviewerui.cpp" line="1678"/>
        <source>Downloading data…</source>
        <translation>Stahování dat…</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="18071"/>
        <source>Please connect USB cable 
to download holter data.</source>
        <translation>Připojte prosím kabel USB ke stažení dat z holteru.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="18156"/>
        <source>Sending data to server...</source>
        <translation>Odesílání dat na server...</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="18326"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:14pt;&quot;&gt;Failed to send data to server.&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:14pt;&quot;&gt;Please disconnect and &lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:14pt;&quot;&gt;try again later.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:14pt;&quot;&gt;Nepodařilo se odeslat data na server.&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:14pt;&quot;&gt;Prosím odpojte a &lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:14pt;&quot;&gt;zkuste to znovu.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="18406"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="18959"/>
        <location filename="../Sironaviewerui.cpp" line="1221"/>
        <source>Monitoring Complete</source>
        <translation>Monitorování bylo dokončeno</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="19201"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:14pt;&quot;&gt;Erase the Data?&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:14pt;&quot;&gt;Smazat data?&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="19415"/>
        <location filename="../Sironaviewerui.ui" line="23694"/>
        <location filename="../Sironaviewerui.ui" line="24416"/>
        <source>Back</source>
        <translation>Zpět</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="19874"/>
        <source>Complete</source>
        <translation>Dokončeno</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="20076"/>
        <source>Download complete</source>
        <translation>Stahování dokončeno</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="20175"/>
        <source>Monitor Erased</source>
        <translation>Monitor byl smazán</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="20496"/>
        <source>New Procedure</source>
        <translation>Nová procedura</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="21017"/>
        <source>Monitor</source>
        <translation>Monitor</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="21306"/>
        <source>Device status detailed description</source>
        <translation>Podrobný popis stavu zařízení</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="21679"/>
        <source>Mute</source>
        <translation>Ztlumit</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="21849"/>
        <source>Monitor signal strength</source>
        <translation>Síla signálu monitoru</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="22188"/>
        <source>Internet status label</source>
        <translation>Štítek stavu internetu</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="22305"/>
        <source>Connect to server</source>
        <translation>Připojte se k serveru</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="22452"/>
        <source>Monitor serial number</source>
        <translation>Sériové číslo monitoru</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="22688"/>
        <source>Monitor model number</source>
        <translation>Modelové číslo monitoru</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="22924"/>
        <source>Monitor firmware version</source>
        <translation>Verze firmwaru monitoru</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="23160"/>
        <source>App version</source>
        <translation>Verze aplikace</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="23396"/>
        <source>App DI</source>
        <translation>ID Aplikace</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="23485"/>
        <source>00851074007125</source>
        <translation>00851074007125</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="23939"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;http://www.DatrixMed.com/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;www.DatrixMed.com&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;http://www.DatrixMed.com/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;www.DatrixMed.com&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="24073"/>
        <source>800-555-1234</source>
        <translation>800-555-1234</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="24153"/>
        <source>24 hours / 7 days</source>
        <translation>24 hodin / 7 dní</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="24259"/>
        <source>Datrix enhances the mobility and
effectiveness of miniature ambulatory medical
wireless body-worn devices.</source>
        <translation>Datrix zvyšuje mobilitu a
efektivitu miniaturních ambulantních lékařských
bezdrátových zařízení nosených na těle.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="92"/>
        <source>Sirona Viewer (%1.%2.%3)</source>
        <translation>Prohlížeč Sirona (%1.%2.%3)</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="141"/>
        <source>Directory error</source>
        <translation>Chyba adresáře</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="189"/>
        <source>Choose a </source>
        <translation>Vybrat</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="189"/>
        <source> device</source>
        <translation>zařízení</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="475"/>
        <source>Server request timed out.</source>
        <translation>Časový limit požadavku serveru vypršel.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="494"/>
        <source>Are you sure you would like to exit?</source>
        <translation>Opravdu chcete skončit?</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="660"/>
        <source>Low Battery</source>
        <translation>Slabá baterie</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="662"/>
        <source>The monitor’s battery is low. Connect the charger.</source>
        <translation>Baterie monitoru je slabá. Připojte nabíječku.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="669"/>
        <source>Sensor Disconnected</source>
        <translation>Senzor odpojen</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="671"/>
        <source>The sensor is disconnected. Connect the sensor.</source>
        <translation>Senzor je odpojen. Připojte senzor.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="673"/>
        <location filename="../Sironaviewerui.cpp" line="679"/>
        <source>Cable Disconnected</source>
        <translation>Kabel odpojen</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="675"/>
        <location filename="../Sironaviewerui.cpp" line="682"/>
        <source>The patient cable is disconnected. Connect the cable.</source>
        <translation>Kabel pacienta je odpojen. Připojte kabel.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="686"/>
        <location filename="../Sironaviewerui.cpp" line="719"/>
        <location filename="../Sironaviewerui.cpp" line="1923"/>
        <source>Ensure electrodes are connected</source>
        <translation>Ujistěte se, že jsou připojeny elektrody</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="688"/>
        <source>One or more electrodes may not be properly attached to the patient.</source>
        <translation>Jedna nebo více elektrod nemusí být správně připojena k pacientovi.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="693"/>
        <location filename="../Sironaviewerui.cpp" line="4085"/>
        <source>Monitor Functioning Properly</source>
        <translation>Monitor funguje správně</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="709"/>
        <source>Sensor connected</source>
        <translation>Senzor připojen</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="711"/>
        <location filename="../Sironaviewerui.cpp" line="721"/>
        <location filename="../Sironaviewerui.cpp" line="1930"/>
        <source>All electrodes connected</source>
        <translation>Všechny elektrody jsou připojeny</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="717"/>
        <location filename="../Sironaviewerui.cpp" line="1825"/>
        <source>Connect sensor</source>
        <translation>Připojte senzor</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="852"/>
        <source>Incorrect cable connected.</source>
        <translation>Je připojen nesprávný kabel.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="858"/>
        <source>Connect sensor.</source>
        <translation>Připojte senzor.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="861"/>
        <source>Connect patient cable.</source>
        <translation>Připojte kabel pacienta.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="1081"/>
        <source>Communication error.</source>
        <translation>Chyba v komunikaci.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="1365"/>
        <source>Unexpected device mode.</source>
        <translation>Neočekávaný režim zařízení.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="1419"/>
        <source>Server scheme not set.</source>
        <translation>Schéma serveru není nastavena.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="1479"/>
        <source>Could not open data file.</source>
        <translation>Nelze otevřít datový soubor.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="1505"/>
        <location filename="../Sironaviewerui.cpp" line="1545"/>
        <source>
Communication Error:
</source>
        <translation>Chyba v komunikaci:</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="1520"/>
        <source>
Unknown server error.</source>
        <translation>Neznámá chyba serveru.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="1530"/>
        <location filename="../Sironaviewerui.cpp" line="1538"/>
        <source>
Server Message:
</source>
        <translation>Zpráva serveru:</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="1536"/>
        <source>
Server error message missing</source>
        <translation>Chybí chybová zpráva serveru</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="1827"/>
        <source>Connect patient cable</source>
        <translation>Připojte kabel pacienta</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="1921"/>
        <source>Ensure sensor is connected</source>
        <translation>Ujistěte se, že je připojen senzor</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="1928"/>
        <source>Sensor is connected</source>
        <translation>Senzor je připojen</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="2428"/>
        <location filename="../Sironaviewerui.cpp" line="2453"/>
        <location filename="../Sironaviewerui.cpp" line="2466"/>
        <location filename="../Sironaviewerui.cpp" line="2478"/>
        <source>Select mode</source>
        <translation>Vyberte režim</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="2439"/>
        <source>Insert cable.</source>
        <translation>Vložte kabel.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="2524"/>
        <source>New Patient</source>
        <translation>Nový pacient</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="2537"/>
        <source>Indications:</source>
        <translation>Indikace:</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="3212"/>
        <location filename="../Sironaviewerui.cpp" line="3215"/>
        <location filename="../Sironaviewerui.cpp" line="3233"/>
        <location filename="../Sironaviewerui.cpp" line="3242"/>
        <location filename="../Sironaviewerui.cpp" line="3266"/>
        <location filename="../Sironaviewerui.cpp" line="3270"/>
        <location filename="../Sironaviewerui.cpp" line="3283"/>
        <location filename="../Sironaviewerui.cpp" line="3293"/>
        <source>Advanced options %1</source>
        <translation>Rozšířené možnosti %1</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="3255"/>
        <source>Record length</source>
        <translation>Délka záznamu</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="4121"/>
        <source>Monitor Placement</source>
        <translation>Umístění monitoru</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="4159"/>
        <source>Record duration decreased due to high sample rate</source>
        <translation>Doba záznamu se snížila kvůli vysoké vzorkovací frekvenci</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="4298"/>
        <source> days</source>
        <translation>dny</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="4318"/>
        <source>Sample rate decreased due to long duration</source>
        <translation>Vzorkovací frekvence se snížila kvůli dlouhému trvání</translation>
    </message>
</context>
<context>
    <name>customer_defs</name>
    <message>
        <location filename="../customizations/datrix/customer.cpp" line="16"/>
        <source>Datrix enhances the mobility
and effectiveness of miniature medical body-worn devices.</source>
        <translation>Datrix zvyšuje mobilitu a efektivitu miniaturních zdravotnických zařízení nosených na těle.</translation>
    </message>
</context>
</TS>
