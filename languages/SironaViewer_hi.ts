<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="hi_IN">
<context>
    <name>DeviceHandler</name>
    <message>
        <location filename="../devicehandler.cpp" line="91"/>
        <location filename="../devicehandler.cpp" line="96"/>
        <location filename="../devicehandler.cpp" line="129"/>
        <source>ERROR: </source>
        <translation>त्रुटि: </translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="304"/>
        <source>ERROR: Error happened during event download.</source>
        <translation>त्रुटि: कार्यक्रम डाउनलोड के दौरान त्रुटि हुई।</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="363"/>
        <source>INFO: Read parameter command executed with success.</source>
        <translation>जानकारी: पैरामीटर पढ़ने का कमांड सफलतापूर्वक निष्पादित किया गया।</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="454"/>
        <source>ERROR: Invalid parameter or parameter not writable.</source>
        <translation>त्रुटि: अमान्य पैरामीटर या पैरामीटर परिवर्तनीय नहीं है।</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="498"/>
        <source>INFO: Commit parameters command executed with success.</source>
        <translation>जानकारी: कमीट पैरामीटर कमांड सफ़लतापूर्वक निष्पादित किया गया।</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="543"/>
        <source>Read all parameters command executed with success.</source>
        <translation>सभी पैरामीटर को पढ़ने का कमांड सफ़लतापूर्वक निष्पादित किया गया।</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="582"/>
        <source>INFO: Get Event Header command, for Event nr.%1 executed with success.</source>
        <translation>जानकारी: कार्यक्रम nr.%1 के लिए कार्यक्रम हेडर प्राप्त करें कमांड सफ़लतापूर्वक निष्पादित किया गया।</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="593"/>
        <source>ERROR: Invalid event size returned by the device.
Command execution cannot proceed.</source>
        <translation>त्रुटि: उपकरण द्वारा अमान्य कार्यक्रम आकार लौटाया गया है।
कमांड का निष्पादन आगे नहीं बढ़ सकता।</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="617"/>
        <source>ERROR: Couldn&apos;t open event file to write to.
Command execution cannot proceed.</source>
        <translation>त्रुटि: परिवर्तन के लिए कार्यक्रम फ़ाइल खोलने में असमर्थ हैं।
कमांड का निष्पादन आगे नहीं बढ़ सकता।</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="660"/>
        <location filename="../devicehandler.cpp" line="831"/>
        <source>ERROR: Couldn&apos;t open annotation file to write to.
Command execution cannot proceed.</source>
        <translation>त्रुटि: परिवर्तन के लिए एनोटेशन फ़ाइल खोलने में असमर्थ हैं।
कमांड का निष्पादन आगे नहीं बढ़ सकता।</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="708"/>
        <source>INFO: Event %1 from %2 downloaded with success.</source>
        <translation>जानकारी: %2 से %1 कार्यक्रम सफ़लतापूर्वक डाउनलोड हो गया है।</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="721"/>
        <source>INFO: Event %1 marked as sent with success.</source>
        <translation>जानकारी: कार्यक्रम %1 को सफ़लतापूर्वक भेजे जानें के रूप में चिन्हित किया गया।</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="723"/>
        <source>ERROR: Event downloaded with success, but failed to mark Event as sent.</source>
        <translation>त्रुटि: कार्यक्रम सफ़लतापूर्वक डाउनलोड हो गया लेकिन कार्यक्रम को भेजे जानें के रूप में चिन्हित करने में विफ़ल रहे।</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="732"/>
        <location filename="../devicehandler.cpp" line="739"/>
        <source>ERROR: Holter downloaded with success, but failed to mark Holter as sent.</source>
        <translation>त्रुटि: हॉल्टर सफ़लतापूर्वक डाउनलोड हो गया लेकिन हॉल्टर को भेजे जानें के रूप में चिन्हित करने में विफ़ल रहे।</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="741"/>
        <source>INFO: Holter marked as sent with success.</source>
        <translation>जानकारी: हॉल्टर को सफ़लतापूर्वक भेजे जानें के रूप में चिन्हित किया गया।</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="788"/>
        <source>ERROR: Couldn&apos;t open holter file to write to.
Command execution cannot proceed.</source>
        <translation>त्रुटि: परिवर्तन के लिए हॉल्टर फ़ाइल खोलने में असमर्थ हैं।
कमांड का निष्पादन आगे नहीं बढ़ सकता।</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="802"/>
        <source>ERROR: There was an error during ECG data transfer.</source>
        <translation>त्रुटि: ईसीजी डेटा ट्रांसफ़र में कुछ समस्या थी।</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="862"/>
        <source>INFO: Download holter command executed with success.</source>
        <translation>जानकारी: होल्टर डाउनलोड करें कमांड सफ़लतापूर्वक निष्पादित हो गया।</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="876"/>
        <source>INFO: Get battery status command executed with success.</source>
        <translation>जानकारी: बैटरी की स्थिति जानें कमांड को सफ़लतापूर्वक निष्पादित हो गया।</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="892"/>
        <source>INFO: Start Procedure command executed with success.</source>
        <translation>जानकारी: प्रक्रिया शुरू करें कमांड को सफ़लतापूर्वक निष्पादित हो गया।</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="908"/>
        <source>INFO: Stop Procedure command executed with success.</source>
        <translation>जानकारी: प्रक्रिया रोकें कमांड सफ़लतापूर्वक निष्पादित हो गया।</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="924"/>
        <source>INFO: Soft Reset command executed with success.</source>
        <translation>जानकारी: सॉफ़्ट रिसेट कमांड सफ़लतापूर्वक निष्पादित हो गया।</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="952"/>
        <source>INFO: Successfully sent system time to the device.</source>
        <translation>जानकारी: सिस्टम का समय सफ़लतापूर्वक उपकरण में भेजा गया।</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="1146"/>
        <source>ERROR: createHeaFile called with nullptr eventHeader.</source>
        <translation>त्रुटि: nullptr इवेंटहेडर के साथ की Hea फ़ाइल बनाएँ।</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="1216"/>
        <source>ERROR: createHeaFile called with NULL eventHeader.</source>
        <translation>त्रुटि: NULL इवेंटहेडर के साथ की Hea फ़ाइल बनाएँ।</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="1294"/>
        <source>ERROR: Invalid input parameter device mode, couldn&apos;t create hea file.</source>
        <translation>त्रुटि: अमान्य इनपुट पैरामीटर उपकरण मोड, hea फ़ाइल बनाने में असमर्थ हैं।</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="1301"/>
        <source>ERROR: Couldn&apos;t open header file to write to.</source>
        <translation>त्रुटि: परिवर्तन के लिए हेडर फ़ाइल को खोलने में असमर्थ हैं।</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="1313"/>
        <source>File open json file for writing(%1).</source>
        <translation>परिवर्तन के लिए फ़ाइल json फ़ाइल को खोलें (%1)।</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="1403"/>
        <source>ERROR: createResFile failed.</source>
        <translation>त्रुटि: createResFile विफ़ल रहा।</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../Sironaviewerui.cpp" line="1042"/>
        <source>Sirona Disconnected!</source>
        <translation>सिरोंना डिस्कनेक्ट हो गया!</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="95"/>
        <source>Another instance of this application is already running</source>
        <translation>इस एप्लिकेशन का एक और इंस्टेंस पहले से ही चल रहा है</translation>
    </message>
    <message>
        <location filename="../utilities.cpp" line="37"/>
        <location filename="../utilities.cpp" line="43"/>
        <source>Error creating directory </source>
        <translation>डायरेक्टरी बनाने में समस्या हुई है </translation>
    </message>
    <message>
        <location filename="../utilities.cpp" line="37"/>
        <location filename="../utilities.cpp" line="43"/>
        <source>. Check path and permissions.</source>
        <translation>. पाथ और अनुमतियों की जाँच करें।</translation>
    </message>
</context>
<context>
    <name>SironaViewerUi</name>
    <message>
        <location filename="../Sironaviewerui.ui" line="32"/>
        <source>Sirona Viewer</source>
        <translation>सिरोना देखनेवाला</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="325"/>
        <location filename="../Sironaviewerui.ui" line="1297"/>
        <location filename="../Sironaviewerui.ui" line="2315"/>
        <location filename="../Sironaviewerui.ui" line="4208"/>
        <location filename="../Sironaviewerui.ui" line="7244"/>
        <location filename="../Sironaviewerui.ui" line="12129"/>
        <location filename="../Sironaviewerui.ui" line="12881"/>
        <location filename="../Sironaviewerui.ui" line="13721"/>
        <location filename="../Sironaviewerui.ui" line="14539"/>
        <location filename="../Sironaviewerui.ui" line="15810"/>
        <location filename="../Sironaviewerui.ui" line="17506"/>
        <location filename="../Sironaviewerui.ui" line="18766"/>
        <location filename="../Sironaviewerui.ui" line="19675"/>
        <location filename="../Sironaviewerui.ui" line="20754"/>
        <source>800 555 1234</source>
        <translation>800 555 1234</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="514"/>
        <source>Sirona</source>
        <translation>सिरोना</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="606"/>
        <source>Procedure setup
and
data download</source>
        <translation>प्रक्रिया सेटअप एवं डेटा डाउनलोड</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="913"/>
        <source>Begin</source>
        <translation>शुरू करें</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="1467"/>
        <source>Connect</source>
        <translation>कनेक्ट करें</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="1549"/>
        <location filename="../Sironaviewerui.ui" line="2596"/>
        <location filename="../Sironaviewerui.ui" line="4593"/>
        <location filename="../Sironaviewerui.ui" line="11693"/>
        <location filename="../Sironaviewerui.ui" line="12477"/>
        <location filename="../Sironaviewerui.ui" line="14106"/>
        <location filename="../Sironaviewerui.ui" line="14820"/>
        <location filename="../Sironaviewerui.ui" line="16091"/>
        <location filename="../Sironaviewerui.ui" line="17787"/>
        <location filename="../Sironaviewerui.ui" line="19026"/>
        <location filename="../Sironaviewerui.ui" line="19956"/>
        <location filename="../Sironaviewerui.ui" line="21139"/>
        <source>This is the status message bar</source>
        <translation>यह स्थिति सन्देश बार है</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="1656"/>
        <source>Choose Sirona device</source>
        <translation>सिरोना उपकरण चुनें</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="2052"/>
        <location filename="../Sironaviewerui.ui" line="6983"/>
        <location filename="../Sironaviewerui.ui" line="11869"/>
        <location filename="../Sironaviewerui.ui" line="13463"/>
        <source>Next</source>
        <translation>अगला</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="2514"/>
        <source>Cable</source>
        <translation>केबल</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="3005"/>
        <source>Holter</source>
        <translation>हॉल्टर</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="3320"/>
        <source>Event</source>
        <translation>कार्यक्रम</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="3635"/>
        <source>Post-Event</source>
        <translation>कार्यक्रम के बाद</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="3950"/>
        <source>MCT</source>
        <translation>MCT</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="4471"/>
        <source>Patient</source>
        <translation>मरीज</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="4779"/>
        <source>Patient ID#:</source>
        <translation>मरीज ID#:</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="5064"/>
        <source>Patient Name:</source>
        <translation>मरीज का नाम:</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="5207"/>
        <source>last name</source>
        <translation>अंतिम नाम</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="5346"/>
        <source>first name</source>
        <translation>पहला नाम</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="5434"/>
        <source>middle initial</source>
        <translation>मध्य का प्रारंभिक</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="5598"/>
        <source>Date of Birth:</source>
        <translation>जन्म तारीख:</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="5811"/>
        <source>Physician name:</source>
        <translation>चिकित्सक का नाम:</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="6102"/>
        <source>Technician name:</source>
        <translation>तकनीशियन का नाम:</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="6393"/>
        <source>Facility:</source>
        <translation>सुविधा:</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="6684"/>
        <source>Comment:</source>
        <translation>टिप्पणी:</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="6815"/>
        <source>insert comment (up to 500 characters)</source>
        <translation>टिप्पणी दर्ज करें (500 अक्षरों तक)</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="7507"/>
        <source>Procedure</source>
        <translation>प्रक्रिया</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="7753"/>
        <location filename="../Sironaviewerui.ui" line="21443"/>
        <source>Monitor battery</source>
        <translation>मॉनिटर की बैटरी</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="7988"/>
        <source>Pre-trigger</source>
        <translation>प्री-ट्रिगर</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8068"/>
        <location filename="../Sironaviewerui.ui" line="8343"/>
        <source>15 sec</source>
        <translation>15 सेकंड</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8073"/>
        <location filename="../Sironaviewerui.ui" line="8348"/>
        <source>30 sec</source>
        <translation>30 सेकंड</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8078"/>
        <location filename="../Sironaviewerui.ui" line="8353"/>
        <source>45 sec</source>
        <translation>45 सेकंड</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8083"/>
        <location filename="../Sironaviewerui.ui" line="8358"/>
        <source>60 sec</source>
        <translation>60 सेकंड</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8088"/>
        <location filename="../Sironaviewerui.ui" line="8363"/>
        <source>90 sec</source>
        <translation>90 सेकंड</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8093"/>
        <location filename="../Sironaviewerui.ui" line="8368"/>
        <source>120 sec</source>
        <translation>120 सेकंड</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8098"/>
        <location filename="../Sironaviewerui.ui" line="8373"/>
        <source>180 sec</source>
        <translation>180 सेकंड</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8103"/>
        <location filename="../Sironaviewerui.ui" line="8378"/>
        <location filename="../Sironaviewerui.cpp" line="3305"/>
        <source>300 sec</source>
        <translation>300 सेकंड</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8263"/>
        <location filename="../Sironaviewerui.cpp" line="3226"/>
        <location filename="../Sironaviewerui.cpp" line="3277"/>
        <source>Post-trigger</source>
        <translation>पोस्ट-ट्रिगर</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8538"/>
        <source>Record duration</source>
        <translation>रिकॉर्ड की अवधि</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8545"/>
        <location filename="../Sironaviewerui.ui" line="9121"/>
        <location filename="../Sironaviewerui.ui" line="9476"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8545"/>
        <location filename="../Sironaviewerui.ui" line="9126"/>
        <location filename="../Sironaviewerui.ui" line="9481"/>
        <source>2</source>
        <translation>2</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8545"/>
        <location filename="../Sironaviewerui.ui" line="9141"/>
        <location filename="../Sironaviewerui.ui" line="9496"/>
        <source>5</source>
        <translation>5</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8545"/>
        <location filename="../Sironaviewerui.ui" line="9151"/>
        <location filename="../Sironaviewerui.ui" line="9506"/>
        <source>7</source>
        <translation>7</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8545"/>
        <location filename="../Sironaviewerui.ui" line="9166"/>
        <location filename="../Sironaviewerui.ui" line="9521"/>
        <source>10</source>
        <translation>10</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8545"/>
        <location filename="../Sironaviewerui.ui" line="9186"/>
        <location filename="../Sironaviewerui.ui" line="9541"/>
        <source>14</source>
        <translation>14</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8717"/>
        <source>Manual Set</source>
        <translation>मैन्युअल रूप से सेट करें</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8724"/>
        <location filename="../Sironaviewerui.cpp" line="4296"/>
        <source> day</source>
        <translation> दिन</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8877"/>
        <source>Advanced options &gt;&gt;&gt;</source>
        <translation>उन्नत विकल्प &gt;&gt;&gt;</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9036"/>
        <source>Auto Event Limit</source>
        <translation>स्वचालित कार्यक्रम की सीमा</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9116"/>
        <location filename="../Sironaviewerui.ui" line="9471"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9131"/>
        <location filename="../Sironaviewerui.ui" line="9486"/>
        <source>3</source>
        <translation>3</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9136"/>
        <location filename="../Sironaviewerui.ui" line="9491"/>
        <source>4</source>
        <translation>4</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9146"/>
        <location filename="../Sironaviewerui.ui" line="9501"/>
        <source>6</source>
        <translation>6</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9156"/>
        <location filename="../Sironaviewerui.ui" line="9511"/>
        <source>8</source>
        <translation>8</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9161"/>
        <location filename="../Sironaviewerui.ui" line="9516"/>
        <source>9</source>
        <translation>9</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9171"/>
        <location filename="../Sironaviewerui.ui" line="9526"/>
        <source>11</source>
        <translation>11</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9176"/>
        <location filename="../Sironaviewerui.ui" line="9531"/>
        <source>12</source>
        <translation>12</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9181"/>
        <location filename="../Sironaviewerui.ui" line="9536"/>
        <source>13</source>
        <translation>13</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9191"/>
        <location filename="../Sironaviewerui.ui" line="9546"/>
        <source>15</source>
        <translation>15</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9196"/>
        <location filename="../Sironaviewerui.ui" line="9551"/>
        <source>16</source>
        <translation>16</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9201"/>
        <location filename="../Sironaviewerui.ui" line="9556"/>
        <source>17</source>
        <translation>17</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9206"/>
        <location filename="../Sironaviewerui.ui" line="9561"/>
        <source>18</source>
        <translation>18</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9211"/>
        <location filename="../Sironaviewerui.ui" line="9566"/>
        <source>19</source>
        <translation>19</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9216"/>
        <location filename="../Sironaviewerui.ui" line="9571"/>
        <source>20</source>
        <translation>20</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9221"/>
        <location filename="../Sironaviewerui.ui" line="9576"/>
        <source>50</source>
        <translation>50</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9226"/>
        <location filename="../Sironaviewerui.ui" line="9581"/>
        <source>100</source>
        <translation>100</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9231"/>
        <location filename="../Sironaviewerui.ui" line="9586"/>
        <source>1000</source>
        <translation>1000</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9391"/>
        <source>Manual Event Limit</source>
        <translation>मैन्युअल कार्यक्रम की सीमा</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9746"/>
        <source>TTM Speed</source>
        <translation>TTM की गति</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9826"/>
        <source>1x</source>
        <translation>1x</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9831"/>
        <source>3x</source>
        <translation>3x</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9991"/>
        <source>Tachy</source>
        <translation>टैकी</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10071"/>
        <location filename="../Sironaviewerui.ui" line="10436"/>
        <location filename="../Sironaviewerui.ui" line="10726"/>
        <location filename="../Sironaviewerui.ui" line="11056"/>
        <source>Off</source>
        <translation>बंद</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10076"/>
        <source>120 bpm</source>
        <translation>120 बीपीएम</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10081"/>
        <source>125 bpm</source>
        <translation>125 बीपीएम</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10086"/>
        <source>130 bpm</source>
        <translation>130 बीपीएम</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10091"/>
        <source>135 bpm</source>
        <translation>135 बीपीएम</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10096"/>
        <source>140 bpm</source>
        <translation>140 बीपीएम</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10101"/>
        <source>145 bpm</source>
        <translation>145 बीपीएम</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10106"/>
        <source>150 bpm</source>
        <translation>150 बीपीएम</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10111"/>
        <source>155 bpm</source>
        <translation>155 बीपीएम</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10116"/>
        <source>160 bpm</source>
        <translation>160 बीपीएम</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10121"/>
        <source>165 bpm</source>
        <translation>165 बीपीएम</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10126"/>
        <source>170 bpm</source>
        <translation>170 बीपीएम</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10131"/>
        <source>175 bpm</source>
        <translation>175 बीपीएम</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10136"/>
        <source>180 bpm</source>
        <translation>180 बीपीएम</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10141"/>
        <source>185 bpm</source>
        <translation>185 बीपीएम</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10146"/>
        <source>190 bpm</source>
        <translation>190 बीपीएम</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10151"/>
        <source>195 bpm</source>
        <translation>195 बीपीएम</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10156"/>
        <source>200 bpm</source>
        <translation>200 बीपीएम</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10161"/>
        <source>205 bpm</source>
        <translation>205 बीपीएम</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10166"/>
        <source>210 bpm</source>
        <translation>210 बीपीएम</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10171"/>
        <source>215 bpm</source>
        <translation>215 बीपीएम</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10176"/>
        <source>220 bpm</source>
        <translation>220 बीपीएम</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10181"/>
        <source>225 bpm</source>
        <translation>225 बीपीएम</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10186"/>
        <source>230 bpm</source>
        <translation>230 बीपीएम</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10191"/>
        <source>235 bpm</source>
        <translation>235 बीपीएम</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10196"/>
        <source>240 bpm</source>
        <translation>240 बीपीएम</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10356"/>
        <source>Brady</source>
        <translation>ब्रैडी</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10441"/>
        <source>15 bpm</source>
        <translation>15 बीपीएम</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10446"/>
        <source>20 bpm</source>
        <translation>20 बीपीएम</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10451"/>
        <source>25 bpm</source>
        <translation>25 बीपीएम</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10456"/>
        <source>30 bpm</source>
        <translation>30 बीपीएम</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10461"/>
        <source>35 bpm</source>
        <translation>35 बीपीएम</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10466"/>
        <source>40 bpm</source>
        <translation>40 बीपीएम</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10471"/>
        <source>45 bpm</source>
        <translation>45 बीपीएम</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10476"/>
        <source>50 bpm</source>
        <translation>50 बीपीएम</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10481"/>
        <source>55 bpm</source>
        <translation>55 बीपीएम</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10486"/>
        <source>60 bpm</source>
        <translation>60 बीपीएम</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10646"/>
        <source>Pause</source>
        <translation>पॉज़</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10731"/>
        <source>1.5 sec</source>
        <translation>1.5 सेकंड</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10736"/>
        <source>2.0 sec</source>
        <translation>2.0 सेकंड</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10741"/>
        <source>2.5 sec</source>
        <translation>2.5 सेकंड</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10746"/>
        <source>3.0 sec</source>
        <translation>3.0 सेकंड</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10751"/>
        <source>3.5 sec</source>
        <translation>3.5 सेकंड</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10756"/>
        <source>4.0 sec</source>
        <translation>4.0 सेकंड</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10761"/>
        <source>4.5 sec</source>
        <translation>4.5 सेकंड</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10766"/>
        <source>5.0 sec</source>
        <translation>5.0 सेकंड</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10771"/>
        <source>5.5 sec</source>
        <translation>5.5 सेकंड</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10776"/>
        <source>6.0 sec</source>
        <translation>6.0 सेकंड</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10781"/>
        <source>6.5 sec</source>
        <translation>6.5 सेकंड</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10786"/>
        <source>7.0 sec</source>
        <translation>7.0 सेकंड</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10791"/>
        <source>7.5 sec</source>
        <translation>7.5 सेकंड</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10796"/>
        <source>8.0 sec</source>
        <translation>8.0 सेकंड</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10801"/>
        <source>8.5 sec</source>
        <translation>8.5 सेकंड</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10806"/>
        <source>9.0 sec</source>
        <translation>9.0 सेकंड</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10811"/>
        <source>9.5 sec</source>
        <translation>9.5 सेकंड</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10816"/>
        <source>10.0 sec</source>
        <translation>10.0 सेकंड</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10976"/>
        <source>Afib</source>
        <translation>Afib</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="11061"/>
        <source>On</source>
        <translation>शुरू</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="11221"/>
        <source>Sample rate</source>
        <translation>सैंपल दर</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="11301"/>
        <location filename="../Sironaviewerui.cpp" line="4316"/>
        <source>128 sps</source>
        <translation>128 एसपीएस</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="11306"/>
        <location filename="../Sironaviewerui.cpp" line="4155"/>
        <location filename="../Sironaviewerui.cpp" line="4314"/>
        <source>256 sps</source>
        <translation>256 एसपीएस</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="11466"/>
        <source>Pacemaker</source>
        <translation>पेसमेकर</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="11546"/>
        <source>OFF</source>
        <translation>बंद</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="11551"/>
        <source>ON</source>
        <translation>शुरू</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="12392"/>
        <source>Settings Saved</source>
        <translation>सेटिंग को सहेजा गया</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="12584"/>
        <source>Settings have been saved.

Disconnect USB and connect a patient cable to finish setting up the patient. 

Use Bluetooth to confirm hookup.</source>
        <translation>सेटिंग को सहेजा लिया गया है।

मरीज का सेट अप पूरा करने के लिए USB डिस्कनेक्ट करें और मरीज का केबल कनेक्ट करें।

हुकअप की पुष्टि के लिए ब्लूटूथ का उपयोग करें।</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="13162"/>
        <location filename="../Sironaviewerui.cpp" line="4123"/>
        <source>Attach Electrodes</source>
        <translation>इलेक्ट्रोड्स अटैच करें।</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="13984"/>
        <source>ECG</source>
        <translation>ईसीजी</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="14279"/>
        <source>Start</source>
        <translation>शुरू करें</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="14738"/>
        <source>Procedure Active</source>
        <translation>प्रक्रिया सक्रीय है</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="14927"/>
        <source>This monitor is currently active in a procedure.</source>
        <translation>यह मॉनिटर वर्तमान में एक प्रक्रिया में सक्रिय है।</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="15235"/>
        <location filename="../Sironaviewerui.ui" line="18506"/>
        <location filename="../Sironaviewerui.ui" line="22065"/>
        <source>Disconnect</source>
        <translation>डिस्कनेक्ट करें</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="15550"/>
        <source>End Procedure</source>
        <translation>समाप्ति प्रक्रिया</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="16009"/>
        <location filename="../Sironaviewerui.ui" line="17705"/>
        <location filename="../Sironaviewerui.cpp" line="1267"/>
        <location filename="../Sironaviewerui.cpp" line="2522"/>
        <source>Downloading</source>
        <translation>डाउनलोड हो रहा है</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="16211"/>
        <location filename="../Sironaviewerui.ui" line="19118"/>
        <source>&lt;Patient Info&gt;</source>
        <translation>&lt;मरीज की जानकारी&gt;</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="16307"/>
        <source>Review patient information before download?</source>
        <translation>डाउनलोड करने से पहले मरीज की जानकारी की समीक्षा करें</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="16628"/>
        <source>Review</source>
        <translation>समीक्षा करें</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="16943"/>
        <source>Download</source>
        <translation>डाउनलोड करें</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="17246"/>
        <location filename="../Sironaviewerui.ui" line="19303"/>
        <source>Erase</source>
        <translation>हटाएँ</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="17904"/>
        <location filename="../Sironaviewerui.cpp" line="1678"/>
        <source>Downloading data…</source>
        <translation>डेटा डाउनलोड हो रहा है...</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="18071"/>
        <source>Please connect USB cable 
to download holter data.</source>
        <translation>हॉल्टर डेटा डाउनलोड करने के लिए कृपया USB केबल कनेक्ट करें।</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="18156"/>
        <source>Sending data to server...</source>
        <translation>डेटा को सर्वर पर भेज रहे हैं...</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="18326"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:14pt;&quot;&gt;Failed to send data to server.&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:14pt;&quot;&gt;Please disconnect and &lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:14pt;&quot;&gt;try again later.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:14pt;&quot;&gt;सर्वर पर डेटा भेजने में विफ़ल रहे।&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:14pt;&quot;&gt;कृपया डिस्कनेक्ट करें और &lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:14pt;&quot;&gt;बाद में पुनः प्रयास करें। &lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="18406"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="18959"/>
        <location filename="../Sironaviewerui.cpp" line="1221"/>
        <source>Monitoring Complete</source>
        <translation>मॉनिटरिंग पूरी हुई</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="19201"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:14pt;&quot;&gt;Erase the Data?&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:14pt;&quot;&gt;डेटा हटाएँ?&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="19415"/>
        <location filename="../Sironaviewerui.ui" line="23694"/>
        <location filename="../Sironaviewerui.ui" line="24416"/>
        <source>Back</source>
        <translation>बापस जाएँ</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="19874"/>
        <source>Complete</source>
        <translation>पूरा हुआ</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="20076"/>
        <source>Download complete</source>
        <translation>डाउनलोड पूरा हुआ</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="20175"/>
        <source>Monitor Erased</source>
        <translation>मॉनिटर को हटाया गया</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="20496"/>
        <source>New Procedure</source>
        <translation>नई प्रक्रिया</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="21017"/>
        <source>Monitor</source>
        <translation>मॉनिटर</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="21306"/>
        <source>Device status detailed description</source>
        <translation>उपकरण की स्थिति का विस्तृत विवरण</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="21679"/>
        <source>Mute</source>
        <translation>म्यूट</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="21849"/>
        <source>Monitor signal strength</source>
        <translation>मॉनिटर के सिग्नल की गुणवत्ता</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="22188"/>
        <source>Internet status label</source>
        <translation>इंटरनेट की स्थिति का लेबल</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="22305"/>
        <source>Connect to server</source>
        <translation>सर्वर से कनेक्ट करें</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="22452"/>
        <source>Monitor serial number</source>
        <translation>मॉनिटर का सीरियल नंबर</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="22688"/>
        <source>Monitor model number</source>
        <translation>मॉनिटर का मॉडल नंबर</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="22924"/>
        <source>Monitor firmware version</source>
        <translation>मॉनिटर का फ़र्मवेयर संस्करण</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="23160"/>
        <source>App version</source>
        <translation>ऐप का संस्करण</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="23396"/>
        <source>App DI</source>
        <translation>ऐप का DI</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="23485"/>
        <source>00851074007125</source>
        <translation>00851074007125</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="23939"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;http://www.DatrixMed.com/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;www.DatrixMed.com&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;http://www.DatrixMed.com/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;www.DatrixMed.com&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="24073"/>
        <source>800-555-1234</source>
        <translation>800-555-1234</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="24153"/>
        <source>24 hours / 7 days</source>
        <translation>24 घंटे / 7 दिन</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="24259"/>
        <source>Datrix enhances the mobility and
effectiveness of miniature ambulatory medical
wireless body-worn devices.</source>
        <translation>Datrix शरीर पर धारण किए जानें वाले छोटे एंबुलेटरी मेडिकल वायरलेस उपकरणों की गतिशीलता और कार्यक्षमता में वृद्धि करता है।</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="92"/>
        <source>Sirona Viewer (%1.%2.%3)</source>
        <translation>सिरोना देखनेवाला (%1.%2.%3)</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="141"/>
        <source>Directory error</source>
        <translation>डायरेक्टरी त्रुटि</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="189"/>
        <source>Choose a </source>
        <translation>एक चुनें </translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="189"/>
        <source> device</source>
        <translation> उपकरण</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="475"/>
        <source>Server request timed out.</source>
        <translation>सर्वर अनुरोध का समय समाप्त हो गया।</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="494"/>
        <source>Are you sure you would like to exit?</source>
        <translation>क्या आप वाकई बाहर निकलना चाहते हैं?</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="660"/>
        <source>Low Battery</source>
        <translation>बैटरी कम है</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="662"/>
        <source>The monitor’s battery is low. Connect the charger.</source>
        <translation>मॉनिटर की बैटरी कम है। चार्जर कनेक्ट करें।</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="669"/>
        <source>Sensor Disconnected</source>
        <translation>सेंसर डिस्कनेक्ट हो गया</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="671"/>
        <source>The sensor is disconnected. Connect the sensor.</source>
        <translation>सेंसर डिस्कनेक्ट हो गया है। सेंसर कनेक्ट करें।</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="673"/>
        <location filename="../Sironaviewerui.cpp" line="679"/>
        <source>Cable Disconnected</source>
        <translation>केबल डिस्कनेक्ट हो गया</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="675"/>
        <location filename="../Sironaviewerui.cpp" line="682"/>
        <source>The patient cable is disconnected. Connect the cable.</source>
        <translation>मरीज का केबल डिस्कनेक्ट हो गया है। केबल कनेक्ट करें।</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="686"/>
        <location filename="../Sironaviewerui.cpp" line="719"/>
        <location filename="../Sironaviewerui.cpp" line="1923"/>
        <source>Ensure electrodes are connected</source>
        <translation>यह सुनिश्चित करें कि इलेक्ट्रोड कनेक्ट कर दिए गए हैं</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="688"/>
        <source>One or more electrodes may not be properly attached to the patient.</source>
        <translation>एक या अधिक इलेक्ट्रोड को मरीज से उचित तरीके से अटैच नहीं किया गया है।</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="693"/>
        <location filename="../Sironaviewerui.cpp" line="4085"/>
        <source>Monitor Functioning Properly</source>
        <translation>मॉनिटर ठीक से काम कर रहा है</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="709"/>
        <source>Sensor connected</source>
        <translation>सेंसर कनेक्ट कर दिया गया है</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="711"/>
        <location filename="../Sironaviewerui.cpp" line="721"/>
        <location filename="../Sironaviewerui.cpp" line="1930"/>
        <source>All electrodes connected</source>
        <translation>सभी इलेक्ट्रोड कनेक्ट कर दिए गए हैं</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="717"/>
        <location filename="../Sironaviewerui.cpp" line="1825"/>
        <source>Connect sensor</source>
        <translation>सेंसर कनेक्ट करें</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="852"/>
        <source>Incorrect cable connected.</source>
        <translation>गलत केबल कनेक्ट की गई है।</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="858"/>
        <source>Connect sensor.</source>
        <translation>सेंसर कनेक्ट करें।</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="861"/>
        <source>Connect patient cable.</source>
        <translation>मरीज का केबल कनेक्ट करें।</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="1081"/>
        <source>Communication error.</source>
        <translation>संचार में त्रुटि है।</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="1365"/>
        <source>Unexpected device mode.</source>
        <translation>अप्रत्याशित उपकरण मोड।</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="1419"/>
        <source>Server scheme not set.</source>
        <translation>सेंसर स्कीम सेट नहीं की गई है।</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="1479"/>
        <source>Could not open data file.</source>
        <translation>डेटा फ़ाइल खोलने में असमर्थ हैं।</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="1505"/>
        <location filename="../Sironaviewerui.cpp" line="1545"/>
        <source>
Communication Error:
</source>
        <translation>
संचार में त्रुटि है।
</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="1520"/>
        <source>
Unknown server error.</source>
        <translation>
अज्ञात सर्वर त्रुटि।</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="1530"/>
        <location filename="../Sironaviewerui.cpp" line="1538"/>
        <source>
Server Message:
</source>
        <translation>
सर्वर से सन्देश:
</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="1536"/>
        <source>
Server error message missing</source>
        <translation>
सर्वर त्रुटि संदेश उपलब्ध नहीं है</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="1827"/>
        <source>Connect patient cable</source>
        <translation>मरीज का केबल कनेक्ट करें।</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="1921"/>
        <source>Ensure sensor is connected</source>
        <translation>यह सुनिश्चित करें कि सेंसर कनेक्ट किया गया है</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="1928"/>
        <source>Sensor is connected</source>
        <translation>सेंसर कनेक्ट किया गया है</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="2428"/>
        <location filename="../Sironaviewerui.cpp" line="2453"/>
        <location filename="../Sironaviewerui.cpp" line="2466"/>
        <location filename="../Sironaviewerui.cpp" line="2478"/>
        <source>Select mode</source>
        <translation>मोड चुनें</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="2439"/>
        <source>Insert cable.</source>
        <translation>केबल लगाएँ।</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="2524"/>
        <source>New Patient</source>
        <translation>नया मरीज</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="2537"/>
        <source>Indications:</source>
        <translation>संकेत:</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="3212"/>
        <location filename="../Sironaviewerui.cpp" line="3215"/>
        <location filename="../Sironaviewerui.cpp" line="3233"/>
        <location filename="../Sironaviewerui.cpp" line="3242"/>
        <location filename="../Sironaviewerui.cpp" line="3266"/>
        <location filename="../Sironaviewerui.cpp" line="3270"/>
        <location filename="../Sironaviewerui.cpp" line="3283"/>
        <location filename="../Sironaviewerui.cpp" line="3293"/>
        <source>Advanced options %1</source>
        <translation>उन्नत विकल्प %1</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="3255"/>
        <source>Record length</source>
        <translation>रिकॉर्ड की अवधि</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="4121"/>
        <source>Monitor Placement</source>
        <translation>मॉनिटर की स्थापना</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="4159"/>
        <source>Record duration decreased due to high sample rate</source>
        <translation>उच्च सैंपल दर के कारण रिकॉर्ड की अवधि कम हो गई</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="4298"/>
        <source> days</source>
        <translation> दिन</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="4318"/>
        <source>Sample rate decreased due to long duration</source>
        <translation>लंबी अवधि के कारण सैंपल दर घट गई</translation>
    </message>
</context>
<context>
    <name>customer_defs</name>
    <message>
        <location filename="../customizations/datrix/customer.cpp" line="16"/>
        <source>Datrix enhances the mobility
and effectiveness of miniature medical body-worn devices.</source>
        <translation>Datrix शरीर पर धारण किए जानें वाले छोटे मेडिकल उपकरणों की गतिशीलता और कार्यक्षमता में वृद्धि करता है।</translation>
    </message>
</context>
</TS>
