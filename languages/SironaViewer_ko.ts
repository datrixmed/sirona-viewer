<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ko_KR">
<context>
    <name>DeviceHandler</name>
    <message>
        <location filename="../devicehandler.cpp" line="91"/>
        <location filename="../devicehandler.cpp" line="96"/>
        <location filename="../devicehandler.cpp" line="129"/>
        <source>ERROR: </source>
        <translation>오류:</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="304"/>
        <source>ERROR: Error happened during event download.</source>
        <translation>오류:이벤트 다운로드 중 에러가 발생했습니다.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="363"/>
        <source>INFO: Read parameter command executed with success.</source>
        <translation>정보:성공적으로 실행된 매개변수 명령어를 읽으세요.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="454"/>
        <source>ERROR: Invalid parameter or parameter not writable.</source>
        <translation>오류:유효하지 않은 매개변수 또는 쓸 수 없는 매개변수입니다.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="498"/>
        <source>INFO: Commit parameters command executed with success.</source>
        <translation>정보:성공적으로 실행된 매개변수 명령어를 사용하세요.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="543"/>
        <source>Read all parameters command executed with success.</source>
        <translation>성공적으로 실행된 모든 매개변수 명령어를 읽으세요.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="582"/>
        <source>INFO: Get Event Header command, for Event nr.%1 executed with success.</source>
        <translation>정보:성공적으로 실행된 이벤트 nr.%1를 위해 이벤트 헤더 명령어를 가져 옵니다.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="593"/>
        <source>ERROR: Invalid event size returned by the device.
Command execution cannot proceed.</source>
        <translation>오류:디바이스에서 반환된 유효하지 않은 이벤트 사이즈
실행 명령을 실행할 수 없습니다.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="617"/>
        <source>ERROR: Couldn&apos;t open event file to write to.
Command execution cannot proceed.</source>
        <translation>오류:쓰기 대상 이벤트 파일을 열 수 없습니다.
실행 명령을 실행할 수 없습니다.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="660"/>
        <location filename="../devicehandler.cpp" line="831"/>
        <source>ERROR: Couldn&apos;t open annotation file to write to.
Command execution cannot proceed.</source>
        <translation>오류:쓰기 대상 주석 파일을 열 수 없습니다.
실행 명령을 실행할 수 없습니다.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="708"/>
        <source>INFO: Event %1 from %2 downloaded with success.</source>
        <translation>정보:이벤트 %2로 부터 이벤트 %1를 성공적으로 다운로드하였습니다.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="721"/>
        <source>INFO: Event %1 marked as sent with success.</source>
        <translation>정보:이벤트 %1의 보냄 표시를 성공적으로 완료하였습니다.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="723"/>
        <source>ERROR: Event downloaded with success, but failed to mark Event as sent.</source>
        <translation>오류:이벤트 다운로드는 성공적으로 완료하였으나, 이벤트 보냄 표시를 완료하지 못했습니다.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="732"/>
        <location filename="../devicehandler.cpp" line="739"/>
        <source>ERROR: Holter downloaded with success, but failed to mark Holter as sent.</source>
        <translation>오류:홀터 다운로드는 성공적으로 완료하였으나, 홀터 보냄 표시를 완료하지 못했습니다.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="741"/>
        <source>INFO: Holter marked as sent with success.</source>
        <translation>정보:홀터 보냄 표시를 성공적으로 완료하였습니다.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="788"/>
        <source>ERROR: Couldn&apos;t open holter file to write to.
Command execution cannot proceed.</source>
        <translation>오류:쓰기 대상 홀터 파일을 열 수 없습니다.
실행 명령을 실행할 수 없습니다.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="802"/>
        <source>ERROR: There was an error during ECG data transfer.</source>
        <translation>오류:심전도 데이터 전송 중 오류가 발생하였습니다.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="862"/>
        <source>INFO: Download holter command executed with success.</source>
        <translation>정보:성공적으로 실행된 홀터 명령어를 다운로드 하세요.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="876"/>
        <source>INFO: Get battery status command executed with success.</source>
        <translation>정보:성공적으로 실행된 배터리 상태 명령어를 가져 옵니다.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="892"/>
        <source>INFO: Start Procedure command executed with success.</source>
        <translation>정보:성공적으로 실행된 절차 명령을 시작합니다.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="908"/>
        <source>INFO: Stop Procedure command executed with success.</source>
        <translation>정보:성공적으로 실행된 절차 명령을 중지하세요.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="924"/>
        <source>INFO: Soft Reset command executed with success.</source>
        <translation>정보:성공적으로 실행된 명령을 소프트 리셋합니다.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="952"/>
        <source>INFO: Successfully sent system time to the device.</source>
        <translation>정보:디바이스에 시스템 시간을 성공적으로 보냄.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="1146"/>
        <source>ERROR: createHeaFile called with nullptr eventHeader.</source>
        <translation>오류:createHEA 파일이 nullptr 이벤트헤더로 호출됨.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="1216"/>
        <source>ERROR: createHeaFile called with NULL eventHeader.</source>
        <translation>오류:createHEA 파일이 NULL 이벤트 헤더로 호출됨.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="1294"/>
        <source>ERROR: Invalid input parameter device mode, couldn&apos;t create hea file.</source>
        <translation>오류:유효하지 않은 입력 매개변수 디바이스 모드. HEA 파일을 생성할 수 없음.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="1301"/>
        <source>ERROR: Couldn&apos;t open header file to write to.</source>
        <translation>오류:쓰기 대상 헤더 파일을 열 수 없습니다.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="1313"/>
        <source>File open json file for writing(%1).</source>
        <translation>쓰기를 위한(%1) json 파일 열기</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="1403"/>
        <source>ERROR: createResFile failed.</source>
        <translation>오류:createResFile 실패.</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../Sironaviewerui.cpp" line="1042"/>
        <source>Sirona Disconnected!</source>
        <translation>시로나 연결 종료.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="95"/>
        <source>Another instance of this application is already running</source>
        <translation>본 응용프로램의 또 다른 인스턴스가 이미 실행 중임.</translation>
    </message>
    <message>
        <location filename="../utilities.cpp" line="37"/>
        <location filename="../utilities.cpp" line="43"/>
        <source>Error creating directory </source>
        <translation>오류 생성 디렉토리 </translation>
    </message>
    <message>
        <location filename="../utilities.cpp" line="37"/>
        <location filename="../utilities.cpp" line="43"/>
        <source>. Check path and permissions.</source>
        <translation>. 경로 및 승인을 확인하세요.</translation>
    </message>
</context>
<context>
    <name>SironaViewerUi</name>
    <message>
        <location filename="../Sironaviewerui.ui" line="32"/>
        <source>Sirona Viewer</source>
        <translation>시로나 뷰어(Sirona Viewer)</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="325"/>
        <location filename="../Sironaviewerui.ui" line="1297"/>
        <location filename="../Sironaviewerui.ui" line="2315"/>
        <location filename="../Sironaviewerui.ui" line="4208"/>
        <location filename="../Sironaviewerui.ui" line="7244"/>
        <location filename="../Sironaviewerui.ui" line="12129"/>
        <location filename="../Sironaviewerui.ui" line="12881"/>
        <location filename="../Sironaviewerui.ui" line="13721"/>
        <location filename="../Sironaviewerui.ui" line="14539"/>
        <location filename="../Sironaviewerui.ui" line="15810"/>
        <location filename="../Sironaviewerui.ui" line="17506"/>
        <location filename="../Sironaviewerui.ui" line="18766"/>
        <location filename="../Sironaviewerui.ui" line="19675"/>
        <location filename="../Sironaviewerui.ui" line="20754"/>
        <source>800 555 1234</source>
        <translation>800 555 1234</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="514"/>
        <source>Sirona</source>
        <translation>시로나(Sirona)</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="606"/>
        <source>Procedure setup
and
data download</source>
        <translation>절차 설정 및 데이터 다운로드</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="913"/>
        <source>Begin</source>
        <translation>시작하기</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="1467"/>
        <source>Connect</source>
        <translation>연결하기</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="1549"/>
        <location filename="../Sironaviewerui.ui" line="2596"/>
        <location filename="../Sironaviewerui.ui" line="4593"/>
        <location filename="../Sironaviewerui.ui" line="11693"/>
        <location filename="../Sironaviewerui.ui" line="12477"/>
        <location filename="../Sironaviewerui.ui" line="14106"/>
        <location filename="../Sironaviewerui.ui" line="14820"/>
        <location filename="../Sironaviewerui.ui" line="16091"/>
        <location filename="../Sironaviewerui.ui" line="17787"/>
        <location filename="../Sironaviewerui.ui" line="19026"/>
        <location filename="../Sironaviewerui.ui" line="19956"/>
        <location filename="../Sironaviewerui.ui" line="21139"/>
        <source>This is the status message bar</source>
        <translation>이것은 상태 메시지 줄입니다.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="1656"/>
        <source>Choose Sirona device</source>
        <translation>시로나 장비를 선택하세요.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="2052"/>
        <location filename="../Sironaviewerui.ui" line="6983"/>
        <location filename="../Sironaviewerui.ui" line="11869"/>
        <location filename="../Sironaviewerui.ui" line="13463"/>
        <source>Next</source>
        <translation>다음</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="2514"/>
        <source>Cable</source>
        <translation>케이블</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="3005"/>
        <source>Holter</source>
        <translation>홀터 검사</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="3320"/>
        <source>Event</source>
        <translation>이벤트</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="3635"/>
        <source>Post-Event</source>
        <translation>포스트-이벤트</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="3950"/>
        <source>MCT</source>
        <translation>중쇄 중성지방 오일(MCT)</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="4471"/>
        <source>Patient</source>
        <translation>환자</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="4779"/>
        <source>Patient ID#:</source>
        <translation>환자 주민등록 번호:</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="5064"/>
        <source>Patient Name:</source>
        <translation>환자 이름:</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="5207"/>
        <source>last name</source>
        <translation>성</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="5346"/>
        <source>first name</source>
        <translation>이름</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="5434"/>
        <source>middle initial</source>
        <translation>중간 이니셜</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="5598"/>
        <source>Date of Birth:</source>
        <translation>생년월일:</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="5811"/>
        <source>Physician name:</source>
        <translation>의사 이름:</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="6102"/>
        <source>Technician name:</source>
        <translation>테크니션 이름:</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="6393"/>
        <source>Facility:</source>
        <translation>시설:</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="6684"/>
        <source>Comment:</source>
        <translation>지적 사항:</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="6815"/>
        <source>insert comment (up to 500 characters)</source>
        <translation>지적 사항(최대 500자)을 입력하세요.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="7507"/>
        <source>Procedure</source>
        <translation>절차</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="7753"/>
        <location filename="../Sironaviewerui.ui" line="21443"/>
        <source>Monitor battery</source>
        <translation>모니터 배터리</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="7988"/>
        <source>Pre-trigger</source>
        <translation>프리트리거</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8068"/>
        <location filename="../Sironaviewerui.ui" line="8343"/>
        <source>15 sec</source>
        <translation>15초</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8073"/>
        <location filename="../Sironaviewerui.ui" line="8348"/>
        <source>30 sec</source>
        <translation>30초</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8078"/>
        <location filename="../Sironaviewerui.ui" line="8353"/>
        <source>45 sec</source>
        <translation>45초</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8083"/>
        <location filename="../Sironaviewerui.ui" line="8358"/>
        <source>60 sec</source>
        <translation>60초</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8088"/>
        <location filename="../Sironaviewerui.ui" line="8363"/>
        <source>90 sec</source>
        <translation>90초</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8093"/>
        <location filename="../Sironaviewerui.ui" line="8368"/>
        <source>120 sec</source>
        <translation>120초</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8098"/>
        <location filename="../Sironaviewerui.ui" line="8373"/>
        <source>180 sec</source>
        <translation>180초</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8103"/>
        <location filename="../Sironaviewerui.ui" line="8378"/>
        <location filename="../Sironaviewerui.cpp" line="3305"/>
        <source>300 sec</source>
        <translation>300초</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8263"/>
        <location filename="../Sironaviewerui.cpp" line="3226"/>
        <location filename="../Sironaviewerui.cpp" line="3277"/>
        <source>Post-trigger</source>
        <translation>포스트트리거</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8538"/>
        <source>Record duration</source>
        <translation>기록 기간</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8545"/>
        <location filename="../Sironaviewerui.ui" line="9121"/>
        <location filename="../Sironaviewerui.ui" line="9476"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8545"/>
        <location filename="../Sironaviewerui.ui" line="9126"/>
        <location filename="../Sironaviewerui.ui" line="9481"/>
        <source>2</source>
        <translation>2</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8545"/>
        <location filename="../Sironaviewerui.ui" line="9141"/>
        <location filename="../Sironaviewerui.ui" line="9496"/>
        <source>5</source>
        <translation>5</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8545"/>
        <location filename="../Sironaviewerui.ui" line="9151"/>
        <location filename="../Sironaviewerui.ui" line="9506"/>
        <source>7</source>
        <translation>7</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8545"/>
        <location filename="../Sironaviewerui.ui" line="9166"/>
        <location filename="../Sironaviewerui.ui" line="9521"/>
        <source>10</source>
        <translation>10</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8545"/>
        <location filename="../Sironaviewerui.ui" line="9186"/>
        <location filename="../Sironaviewerui.ui" line="9541"/>
        <source>14</source>
        <translation>14</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8717"/>
        <source>Manual Set</source>
        <translation>수동 설정</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8724"/>
        <location filename="../Sironaviewerui.cpp" line="4296"/>
        <source> day</source>
        <translation> 일</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8877"/>
        <source>Advanced options &gt;&gt;&gt;</source>
        <translation>고급 옵션 &gt;&gt;&gt;</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9036"/>
        <source>Auto Event Limit</source>
        <translation>자동 이벤트 제한</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9116"/>
        <location filename="../Sironaviewerui.ui" line="9471"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9131"/>
        <location filename="../Sironaviewerui.ui" line="9486"/>
        <source>3</source>
        <translation>3</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9136"/>
        <location filename="../Sironaviewerui.ui" line="9491"/>
        <source>4</source>
        <translation>4</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9146"/>
        <location filename="../Sironaviewerui.ui" line="9501"/>
        <source>6</source>
        <translation>6</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9156"/>
        <location filename="../Sironaviewerui.ui" line="9511"/>
        <source>8</source>
        <translation>8</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9161"/>
        <location filename="../Sironaviewerui.ui" line="9516"/>
        <source>9</source>
        <translation>9</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9171"/>
        <location filename="../Sironaviewerui.ui" line="9526"/>
        <source>11</source>
        <translation>11</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9176"/>
        <location filename="../Sironaviewerui.ui" line="9531"/>
        <source>12</source>
        <translation>12</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9181"/>
        <location filename="../Sironaviewerui.ui" line="9536"/>
        <source>13</source>
        <translation>13</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9191"/>
        <location filename="../Sironaviewerui.ui" line="9546"/>
        <source>15</source>
        <translation>15</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9196"/>
        <location filename="../Sironaviewerui.ui" line="9551"/>
        <source>16</source>
        <translation>16</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9201"/>
        <location filename="../Sironaviewerui.ui" line="9556"/>
        <source>17</source>
        <translation>17</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9206"/>
        <location filename="../Sironaviewerui.ui" line="9561"/>
        <source>18</source>
        <translation>18</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9211"/>
        <location filename="../Sironaviewerui.ui" line="9566"/>
        <source>19</source>
        <translation>19</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9216"/>
        <location filename="../Sironaviewerui.ui" line="9571"/>
        <source>20</source>
        <translation>20</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9221"/>
        <location filename="../Sironaviewerui.ui" line="9576"/>
        <source>50</source>
        <translation>50</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9226"/>
        <location filename="../Sironaviewerui.ui" line="9581"/>
        <source>100</source>
        <translation>100</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9231"/>
        <location filename="../Sironaviewerui.ui" line="9586"/>
        <source>1000</source>
        <translation>1000</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9391"/>
        <source>Manual Event Limit</source>
        <translation>수동 이벤트 제한</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9746"/>
        <source>TTM Speed</source>
        <translation>TTM 속도</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9826"/>
        <source>1x</source>
        <translation>1x</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9831"/>
        <source>3x</source>
        <translation>3x</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9991"/>
        <source>Tachy</source>
        <translation>고속</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10071"/>
        <location filename="../Sironaviewerui.ui" line="10436"/>
        <location filename="../Sironaviewerui.ui" line="10726"/>
        <location filename="../Sironaviewerui.ui" line="11056"/>
        <source>Off</source>
        <translation>꺼짐</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10076"/>
        <source>120 bpm</source>
        <translation>120 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10081"/>
        <source>125 bpm</source>
        <translation>125 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10086"/>
        <source>130 bpm</source>
        <translation>130 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10091"/>
        <source>135 bpm</source>
        <translation>135 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10096"/>
        <source>140 bpm</source>
        <translation>140 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10101"/>
        <source>145 bpm</source>
        <translation>145 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10106"/>
        <source>150 bpm</source>
        <translation>150 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10111"/>
        <source>155 bpm</source>
        <translation>155 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10116"/>
        <source>160 bpm</source>
        <translation>160 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10121"/>
        <source>165 bpm</source>
        <translation>165 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10126"/>
        <source>170 bpm</source>
        <translation>170 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10131"/>
        <source>175 bpm</source>
        <translation>175 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10136"/>
        <source>180 bpm</source>
        <translation>180 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10141"/>
        <source>185 bpm</source>
        <translation>185 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10146"/>
        <source>190 bpm</source>
        <translation>190 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10151"/>
        <source>195 bpm</source>
        <translation>195 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10156"/>
        <source>200 bpm</source>
        <translation>200 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10161"/>
        <source>205 bpm</source>
        <translation>205 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10166"/>
        <source>210 bpm</source>
        <translation>210 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10171"/>
        <source>215 bpm</source>
        <translation>215 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10176"/>
        <source>220 bpm</source>
        <translation>220 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10181"/>
        <source>225 bpm</source>
        <translation>225 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10186"/>
        <source>230 bpm</source>
        <translation>230 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10191"/>
        <source>235 bpm</source>
        <translation>235 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10196"/>
        <source>240 bpm</source>
        <translation>240 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10356"/>
        <source>Brady</source>
        <translation>저속</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10441"/>
        <source>15 bpm</source>
        <translation>15 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10446"/>
        <source>20 bpm</source>
        <translation>20 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10451"/>
        <source>25 bpm</source>
        <translation>25 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10456"/>
        <source>30 bpm</source>
        <translation>30 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10461"/>
        <source>35 bpm</source>
        <translation>35 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10466"/>
        <source>40 bpm</source>
        <translation>40 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10471"/>
        <source>45 bpm</source>
        <translation>45 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10476"/>
        <source>50 bpm</source>
        <translation>50 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10481"/>
        <source>55 bpm</source>
        <translation>55 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10486"/>
        <source>60 bpm</source>
        <translation>60 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10646"/>
        <source>Pause</source>
        <translation>일시정지</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10731"/>
        <source>1.5 sec</source>
        <translation>1.5 초</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10736"/>
        <source>2.0 sec</source>
        <translation>2.0 초</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10741"/>
        <source>2.5 sec</source>
        <translation>2.5 초</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10746"/>
        <source>3.0 sec</source>
        <translation>3.0 초</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10751"/>
        <source>3.5 sec</source>
        <translation>3.5 초</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10756"/>
        <source>4.0 sec</source>
        <translation>4.0 초</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10761"/>
        <source>4.5 sec</source>
        <translation>4.5 초</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10766"/>
        <source>5.0 sec</source>
        <translation>5.0 초</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10771"/>
        <source>5.5 sec</source>
        <translation>5.5 초</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10776"/>
        <source>6.0 sec</source>
        <translation>6.0 초</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10781"/>
        <source>6.5 sec</source>
        <translation>6.5 초</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10786"/>
        <source>7.0 sec</source>
        <translation>7.0 초</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10791"/>
        <source>7.5 sec</source>
        <translation>7.5 초</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10796"/>
        <source>8.0 sec</source>
        <translation>8.0 초</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10801"/>
        <source>8.5 sec</source>
        <translation>8.5 초</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10806"/>
        <source>9.0 sec</source>
        <translation>9.0 초</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10811"/>
        <source>9.5 sec</source>
        <translation>9.5 초</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10816"/>
        <source>10.0 sec</source>
        <translation>10.0 초</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10976"/>
        <source>Afib</source>
        <translation>심방세동</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="11061"/>
        <source>On</source>
        <translation>켜짐</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="11221"/>
        <source>Sample rate</source>
        <translation>샘플링 주파수</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="11301"/>
        <location filename="../Sironaviewerui.cpp" line="4316"/>
        <source>128 sps</source>
        <translation>128 sps</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="11306"/>
        <location filename="../Sironaviewerui.cpp" line="4155"/>
        <location filename="../Sironaviewerui.cpp" line="4314"/>
        <source>256 sps</source>
        <translation>256 sps</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="11466"/>
        <source>Pacemaker</source>
        <translation>페이스메이커</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="11546"/>
        <source>OFF</source>
        <translation>꺼짐</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="11551"/>
        <source>ON</source>
        <translation>켜짐</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="12392"/>
        <source>Settings Saved</source>
        <translation>설정 저장</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="12584"/>
        <source>Settings have been saved.

Disconnect USB and connect a patient cable to finish setting up the patient. 

Use Bluetooth to confirm hookup.</source>
        <translation>설정이 저장되었습니다.

USB를 제거하고 환자 케이블을 연결해 해당 환자에 대한 설정을 완료합니다.

블루투쓰를 사용해 연결을 확인합니다.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="13162"/>
        <location filename="../Sironaviewerui.cpp" line="4123"/>
        <source>Attach Electrodes</source>
        <translation>전극을 연결합니다.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="13984"/>
        <source>ECG</source>
        <translation>심전도</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="14279"/>
        <source>Start</source>
        <translation>시작</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="14738"/>
        <source>Procedure Active</source>
        <translation>절차 활성화</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="14927"/>
        <source>This monitor is currently active in a procedure.</source>
        <translation>본 모니터는 현재 활성화된 상태입니다.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="15235"/>
        <location filename="../Sironaviewerui.ui" line="18506"/>
        <location filename="../Sironaviewerui.ui" line="22065"/>
        <source>Disconnect</source>
        <translation>연결 끊기</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="15550"/>
        <source>End Procedure</source>
        <translation>절차 종료</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="16009"/>
        <location filename="../Sironaviewerui.ui" line="17705"/>
        <location filename="../Sironaviewerui.cpp" line="1267"/>
        <location filename="../Sironaviewerui.cpp" line="2522"/>
        <source>Downloading</source>
        <translation>다운로드하기</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="16211"/>
        <location filename="../Sironaviewerui.ui" line="19118"/>
        <source>&lt;Patient Info&gt;</source>
        <translation>&lt;환자 정보&gt;</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="16307"/>
        <source>Review patient information before download?</source>
        <translation>다운로드 전에 환자 정보를 검토하시겠습니까?</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="16628"/>
        <source>Review</source>
        <translation>검토</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="16943"/>
        <source>Download</source>
        <translation>다운로드</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="17246"/>
        <location filename="../Sironaviewerui.ui" line="19303"/>
        <source>Erase</source>
        <translation>삭제</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="17904"/>
        <location filename="../Sironaviewerui.cpp" line="1678"/>
        <source>Downloading data…</source>
        <translation>데이터 다운로드 중...</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="18071"/>
        <source>Please connect USB cable 
to download holter data.</source>
        <translation>USB 케이블을 제거하여 홀터 데이터를 다운로드합니다.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="18156"/>
        <source>Sending data to server...</source>
        <translation>서버에 데이터 전송중...</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="18326"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:14pt;&quot;&gt;Failed to send data to server.&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:14pt;&quot;&gt;Please disconnect and &lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:14pt;&quot;&gt;try again later.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:14pt;&quot;&gt;Failed to send data to server.&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:14pt;&quot;&gt;Please disconnect and &lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:14pt;&quot;&gt;try again later.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="18406"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="18959"/>
        <location filename="../Sironaviewerui.cpp" line="1221"/>
        <source>Monitoring Complete</source>
        <translation>모니터링 완료</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="19201"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:14pt;&quot;&gt;Erase the Data?&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:14pt;&quot;&gt;Erase the Data?&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="19415"/>
        <location filename="../Sironaviewerui.ui" line="23694"/>
        <location filename="../Sironaviewerui.ui" line="24416"/>
        <source>Back</source>
        <translation>뒤로</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="19874"/>
        <source>Complete</source>
        <translation>완료</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="20076"/>
        <source>Download complete</source>
        <translation>다운로드 완료</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="20175"/>
        <source>Monitor Erased</source>
        <translation>모니터 삭제됨</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="20496"/>
        <source>New Procedure</source>
        <translation>새로운 절차</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="21017"/>
        <source>Monitor</source>
        <translation>모니터</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="21306"/>
        <source>Device status detailed description</source>
        <translation>디바이스 상태 세부 설명</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="21679"/>
        <source>Mute</source>
        <translation>무음</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="21849"/>
        <source>Monitor signal strength</source>
        <translation>모니터 신호 강도</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="22188"/>
        <source>Internet status label</source>
        <translation>인터넷 상태 라벨</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="22305"/>
        <source>Connect to server</source>
        <translation>서버에 연결합니다.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="22452"/>
        <source>Monitor serial number</source>
        <translation>모니터 일련번호</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="22688"/>
        <source>Monitor model number</source>
        <translation>모니터 모델 번호</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="22924"/>
        <source>Monitor firmware version</source>
        <translation>모니터 펌웨어 버전</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="23160"/>
        <source>App version</source>
        <translation>응용프로그램 버전</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="23396"/>
        <source>App DI</source>
        <translation>응용프로그램 DI</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="23485"/>
        <source>00851074007125</source>
        <translation>00851074007125</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="23939"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;http://www.DatrixMed.com/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;www.DatrixMed.com&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;http://www.DatrixMed.com/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;www.DatrixMed.com&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="24073"/>
        <source>800-555-1234</source>
        <translation>800-555-1234</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="24153"/>
        <source>24 hours / 7 days</source>
        <translation>연중무휴</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="24259"/>
        <source>Datrix enhances the mobility and
effectiveness of miniature ambulatory medical
wireless body-worn devices.</source>
        <translation>Datrix는 몸에 걸치는 소형의 보행용 무선 의료 장치의 기동성과 효과를 향상시킵니다.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="92"/>
        <source>Sirona Viewer (%1.%2.%3)</source>
        <translation>시로나 뷰어(%1.%2.%3)</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="141"/>
        <source>Directory error</source>
        <translation>디렉토리 오류</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="189"/>
        <source>Choose a </source>
        <translation>장치를 </translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="189"/>
        <source> device</source>
        <translation> 선택하십시오.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="475"/>
        <source>Server request timed out.</source>
        <translation>서버 요청 시간이 초과하였습니다.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="494"/>
        <source>Are you sure you would like to exit?</source>
        <translation>정말 나가시겠습니까?</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="660"/>
        <source>Low Battery</source>
        <translation>배터리 부족</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="662"/>
        <source>The monitor’s battery is low. Connect the charger.</source>
        <translation>모니터 배터리가 부족합니다. 충전기를 연결하세요.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="669"/>
        <source>Sensor Disconnected</source>
        <translation>센서 연결 종료</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="671"/>
        <source>The sensor is disconnected. Connect the sensor.</source>
        <translation>센서의 연결이 끊어졌습니다. 센서를 연결하세요.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="673"/>
        <location filename="../Sironaviewerui.cpp" line="679"/>
        <source>Cable Disconnected</source>
        <translation>케이블 연결 종료</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="675"/>
        <location filename="../Sironaviewerui.cpp" line="682"/>
        <source>The patient cable is disconnected. Connect the cable.</source>
        <translation>환자 케이블의 연결이 끊어졌습니다. 케이블을 연결하세요.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="686"/>
        <location filename="../Sironaviewerui.cpp" line="719"/>
        <location filename="../Sironaviewerui.cpp" line="1923"/>
        <source>Ensure electrodes are connected</source>
        <translation>전극이 연결되어 있어야 합니다.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="688"/>
        <source>One or more electrodes may not be properly attached to the patient.</source>
        <translation>하나 또는 그 이상의 전극이 환자에게 제대로 부착되어있지 않을 수 있습니다.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="693"/>
        <location filename="../Sironaviewerui.cpp" line="4085"/>
        <source>Monitor Functioning Properly</source>
        <translation>모니터 기능 정상</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="709"/>
        <source>Sensor connected</source>
        <translation>센서가 연결됨.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="711"/>
        <location filename="../Sironaviewerui.cpp" line="721"/>
        <location filename="../Sironaviewerui.cpp" line="1930"/>
        <source>All electrodes connected</source>
        <translation>모든 전극이 연결됨.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="717"/>
        <location filename="../Sironaviewerui.cpp" line="1825"/>
        <source>Connect sensor</source>
        <translation>센서를 연결하세요.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="852"/>
        <source>Incorrect cable connected.</source>
        <translation>다른 케이블이 연결되어 있습니다.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="858"/>
        <source>Connect sensor.</source>
        <translation>센서를 연결하세요.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="861"/>
        <source>Connect patient cable.</source>
        <translation>환자 케이블을 연결하세요.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="1081"/>
        <source>Communication error.</source>
        <translation>커뮤니케이션 오류</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="1365"/>
        <source>Unexpected device mode.</source>
        <translation>예상치 않은 장치 모드</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="1419"/>
        <source>Server scheme not set.</source>
        <translation>서버 체계가 구성되지 않았습니다.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="1479"/>
        <source>Could not open data file.</source>
        <translation>데이터 파일을 열 수 없습니다.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="1505"/>
        <location filename="../Sironaviewerui.cpp" line="1545"/>
        <source>
Communication Error:
</source>
        <translation>
커뮤니케이션 오류:
</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="1520"/>
        <source>
Unknown server error.</source>
        <translation>
알 수 없는 서버 오류</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="1530"/>
        <location filename="../Sironaviewerui.cpp" line="1538"/>
        <source>
Server Message:
</source>
        <translation>
서버 메시지
</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="1536"/>
        <source>
Server error message missing</source>
        <translation>
서버 오류 메시지 누락</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="1827"/>
        <source>Connect patient cable</source>
        <translation>환자 케이블을 연결하세요.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="1921"/>
        <source>Ensure sensor is connected</source>
        <translation>센서가 연결되어 있어야 합니다.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="1928"/>
        <source>Sensor is connected</source>
        <translation>센서가 연결됨.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="2428"/>
        <location filename="../Sironaviewerui.cpp" line="2453"/>
        <location filename="../Sironaviewerui.cpp" line="2466"/>
        <location filename="../Sironaviewerui.cpp" line="2478"/>
        <source>Select mode</source>
        <translation>모드를 선택하세요.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="2439"/>
        <source>Insert cable.</source>
        <translation>케이블을 삽입하세요.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="2524"/>
        <source>New Patient</source>
        <translation>신규 환자</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="2537"/>
        <source>Indications:</source>
        <translation>표시:</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="3212"/>
        <location filename="../Sironaviewerui.cpp" line="3215"/>
        <location filename="../Sironaviewerui.cpp" line="3233"/>
        <location filename="../Sironaviewerui.cpp" line="3242"/>
        <location filename="../Sironaviewerui.cpp" line="3266"/>
        <location filename="../Sironaviewerui.cpp" line="3270"/>
        <location filename="../Sironaviewerui.cpp" line="3283"/>
        <location filename="../Sironaviewerui.cpp" line="3293"/>
        <source>Advanced options %1</source>
        <translation>고급 옵션 %1</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="3255"/>
        <source>Record length</source>
        <translation>레코드 길이</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="4121"/>
        <source>Monitor Placement</source>
        <translation>모니터 설치</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="4159"/>
        <source>Record duration decreased due to high sample rate</source>
        <translation>샘플링 주파수가 높아서 기록 기간이 단축되었습니다.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="4298"/>
        <source> days</source>
        <translation> 일</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="4318"/>
        <source>Sample rate decreased due to long duration</source>
        <translation>기간이 길어 샘플링 주파수가 줄어들었습니다.</translation>
    </message>
</context>
<context>
    <name>customer_defs</name>
    <message>
        <location filename="../customizations/datrix/customer.cpp" line="16"/>
        <source>Datrix enhances the mobility
and effectiveness of miniature medical body-worn devices.</source>
        <translation>Datrix는 몸에 걸치는 소형 의료 장치의 기동성과 효과를 향상시킵니다.</translation>
    </message>
</context>
</TS>
