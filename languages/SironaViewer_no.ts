<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nb_NO">
<context>
    <name>DeviceHandler</name>
    <message>
        <location filename="../devicehandler.cpp" line="100"/>
        <location filename="../devicehandler.cpp" line="105"/>
        <location filename="../devicehandler.cpp" line="140"/>
        <location filename="../devicehandler.cpp" line="1676"/>
        <location filename="../devicehandler.cpp" line="1697"/>
        <source>ERROR: </source>
        <translation>FEIL: </translation>
    </message>
    <message>
        <source>Low disk space</source>
        <translation type="vanished">Lite diskplass</translation>
    </message>
    <message>
        <source>There is low disk space(%1MB). Please close this app. Free some disk space, and restart the app again.</source>
        <translation type="vanished">Det er lite plass på disken (%1MB). Lukk dette programmet. Frigjør diskplass, og starte programmet på nytt.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="320"/>
        <source>ERROR: There are no Events recorded on the device.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="379"/>
        <source>ERROR: Invalid device mode.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="404"/>
        <source>INFO: Read parameter command executed with success.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="514"/>
        <source>ERROR: Invalid parameter or parameter to writable.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="569"/>
        <source>INFO: Commit parameters command executed with success.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="618"/>
        <source>INFO: Read all parameters command executed with success.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="890"/>
        <source>INFO: Write all parameters command executed with success.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="1070"/>
        <source>INFO: Get Event Header command, for Event nr.%1 executed with success.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="290"/>
        <location filename="../devicehandler.cpp" line="333"/>
        <source>Computer out of space.
Free some space and try again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="315"/>
        <source>ERROR: Error happened during event download.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="1087"/>
        <source>ERROR: Invalid event size returned by the device.
Command execution cannot proceed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="1103"/>
        <location filename="../devicehandler.cpp" line="1385"/>
        <location filename="../devicehandler.cpp" line="1636"/>
        <location filename="../devicehandler.cpp" line="1637"/>
        <source>ERROR: Invalid chunk size returned by the device.
Command execution cannot proceed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="1123"/>
        <location filename="../devicehandler.cpp" line="1127"/>
        <location filename="../devicehandler.cpp" line="1404"/>
        <location filename="../devicehandler.cpp" line="1408"/>
        <location filename="../devicehandler.cpp" line="2056"/>
        <location filename="../devicehandler.cpp" line="2204"/>
        <location filename="../devicehandler.cpp" line="2390"/>
        <source>File already exists(%1).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="1141"/>
        <source>ERROR: Couldn&apos;t open event file to write to.
Command execution cannot proceed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="1201"/>
        <location filename="../devicehandler.cpp" line="1501"/>
        <source>ERROR: Couldn&apos;t open annotation file to write to.
Command execution cannot proceed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="1285"/>
        <source>INFO: Event %1 from %2 downloaded with success.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="1289"/>
        <source>INFO: Event %1 from %2 marked as sent with success.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="1291"/>
        <source>ERROR: Event downloaded with success, but failed to mark Event as sent.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="1311"/>
        <source>INFO: Erase all events command executed with success.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="1338"/>
        <source>INFO: There are no Events recorded on the device.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="1359"/>
        <source>INFO: Event list populated with success.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="1417"/>
        <source>ERROR: Couldn&apos;t open holter file to write to.
Command execution cannot proceed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="1587"/>
        <source>INFO: Download holter command executed with success.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="1606"/>
        <source>INFO: Erase holter command executed with success.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="1651"/>
        <location filename="../devicehandler.cpp" line="1652"/>
        <source>ERROR: Couldn&apos;t open firmware file.
Command execution cannot proceed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="1700"/>
        <source>Device Firmware uploaded with success.
Device Disconnected.</source>
        <translation>Enhetens maskinvare ble lastet opp.
Enheten er frakoblet.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="1701"/>
        <source>INFO: Firmware upload command executed with success.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="1721"/>
        <source>INFO: Get battery status command executed with success.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="1742"/>
        <source>INFO: Start Procedure command executed with success.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="1763"/>
        <source>INFO: Stop Procedure command executed with success.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="1784"/>
        <source>INFO: Soft Reset command executed with success.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="1816"/>
        <source>INFO: Successfully sent system time to the device.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="2069"/>
        <source>ERROR: createHeaFile called with nullptr eventHeader.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="2168"/>
        <source>ERROR: Invalid input parameter device mode, couldn&apos;t create hea file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="2175"/>
        <source>ERROR: Couldn&apos;t open header file to write to.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="2215"/>
        <source>ERROR: Couldn&apos;t open event file to write to.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="2221"/>
        <source>ERROR: Couldn&apos;t open dat file to read from.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="2395"/>
        <source>File open json file for writing(%1).</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EventAutoSender</name>
    <message>
        <location filename="../eventautosender.cpp" line="266"/>
        <location filename="../eventautosender.cpp" line="268"/>
        <source>Data uploaded to server successfully.</source>
        <translation>Data ble lastet opp til serveren.</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../main.cpp" line="81"/>
        <source>Another instance of this application is already running</source>
        <translation>En annen utgave av programmet kjører allerede</translation>
    </message>
    <message>
        <source>Device Disconnected</source>
        <translation type="vanished">Enheten Er Frakoblet</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="1200"/>
        <source>Sirona Disconnected!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SironaViewerUi</name>
    <message>
        <location filename="../Sironaviewerui.ui" line="32"/>
        <source>Sirona Viewer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="325"/>
        <location filename="../Sironaviewerui.ui" line="1297"/>
        <location filename="../Sironaviewerui.ui" line="2315"/>
        <location filename="../Sironaviewerui.ui" line="3578"/>
        <location filename="../Sironaviewerui.ui" line="5686"/>
        <location filename="../Sironaviewerui.ui" line="10557"/>
        <location filename="../Sironaviewerui.ui" line="11321"/>
        <location filename="../Sironaviewerui.ui" line="12161"/>
        <location filename="../Sironaviewerui.ui" line="12979"/>
        <location filename="../Sironaviewerui.ui" line="14256"/>
        <location filename="../Sironaviewerui.ui" line="15185"/>
        <location filename="../Sironaviewerui.ui" line="16151"/>
        <source>800 555 1234</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="514"/>
        <source>Sirona</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="606"/>
        <source>Procedure setup
and
data download</source>
        <translation>Prosedyreoppsett
og
datanedlasting</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="913"/>
        <source>Begin</source>
        <translation>Start</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="1467"/>
        <source>Connect</source>
        <translation>Koblet til</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="1549"/>
        <location filename="../Sironaviewerui.ui" line="2596"/>
        <location filename="../Sironaviewerui.ui" line="3963"/>
        <location filename="../Sironaviewerui.ui" line="10124"/>
        <location filename="../Sironaviewerui.ui" line="10905"/>
        <location filename="../Sironaviewerui.ui" line="12546"/>
        <location filename="../Sironaviewerui.ui" line="13260"/>
        <location filename="../Sironaviewerui.ui" line="14537"/>
        <location filename="../Sironaviewerui.ui" line="15466"/>
        <location filename="../Sironaviewerui.ui" line="16536"/>
        <source>This is the status message bar</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="1656"/>
        <source>Choose Sirona device</source>
        <translation>Velg Sirona-enhet</translation>
    </message>
    <message>
        <source>&amp;Connect</source>
        <translation type="vanished">&amp;Koblet til</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="2514"/>
        <source>Cable</source>
        <translation>Kabel</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="3005"/>
        <source>Holter</source>
        <translation>Holter</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="3320"/>
        <source>Event</source>
        <translation>Hendelse</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="3841"/>
        <source>Patient</source>
        <translation>Pasient</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="4107"/>
        <source>Patient ID#:</source>
        <translation>Pasientens ID-nr.:</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="4362"/>
        <source>Patient Name:</source>
        <translation>Pasientens navn:</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="4503"/>
        <source>first name</source>
        <translation>fornavn</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="4614"/>
        <source>middle initial</source>
        <translation>første bokstav i mellomnav</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="4725"/>
        <source>last name</source>
        <translation>etternavn</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="4890"/>
        <source>Physician name:</source>
        <translation>Legens navn:</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="5145"/>
        <source>Comment:</source>
        <translation>Kommentar:</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="5261"/>
        <source>insert comment (up to 80 characters)</source>
        <translation>tast inn kommentar (opptil 80 tegn)</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="2052"/>
        <location filename="../Sironaviewerui.ui" line="5425"/>
        <location filename="../Sironaviewerui.ui" line="10297"/>
        <location filename="../Sironaviewerui.ui" line="11903"/>
        <source>Next</source>
        <translation>Neste</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="5949"/>
        <source>Procedure</source>
        <translation>Prosedyre</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="6195"/>
        <location filename="../Sironaviewerui.ui" line="16840"/>
        <source>Monitor battery</source>
        <translation>Monitorbatteri</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="6430"/>
        <source>Pre-trigger</source>
        <translation>Førutløser</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="6510"/>
        <location filename="../Sironaviewerui.ui" line="6785"/>
        <source>15 sec</source>
        <translation>15 sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="6515"/>
        <location filename="../Sironaviewerui.ui" line="6790"/>
        <source>30 sec</source>
        <translation>30 sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="6520"/>
        <location filename="../Sironaviewerui.ui" line="6795"/>
        <source>45 sec</source>
        <translation>45 sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="6525"/>
        <location filename="../Sironaviewerui.ui" line="6800"/>
        <source>60 sec</source>
        <translation>60 sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="6530"/>
        <location filename="../Sironaviewerui.ui" line="6805"/>
        <source>90 sec</source>
        <translation>90 sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="6535"/>
        <location filename="../Sironaviewerui.ui" line="6810"/>
        <source>120 sec</source>
        <translation>120 sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="6540"/>
        <location filename="../Sironaviewerui.ui" line="6815"/>
        <source>180 sec</source>
        <translation>180 sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="6545"/>
        <location filename="../Sironaviewerui.ui" line="6820"/>
        <location filename="../Sironaviewerui.cpp" line="3017"/>
        <source>300 sec</source>
        <translation>300 sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="6705"/>
        <source>Post-trigger</source>
        <translation>Etterutløser</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="6980"/>
        <source>Record duration</source>
        <translation>Opptaksvarighet</translation>
    </message>
    <message>
        <source>1 day</source>
        <translation type="vanished">1 dag</translation>
    </message>
    <message>
        <source>2 days</source>
        <translation type="vanished">2 dager</translation>
    </message>
    <message>
        <source>3 days</source>
        <translation type="vanished">3 dager</translation>
    </message>
    <message>
        <source>4 days</source>
        <translation type="vanished">4 dager</translation>
    </message>
    <message>
        <source>5 days</source>
        <translation type="vanished">5 dager</translation>
    </message>
    <message>
        <source>6 days</source>
        <translation type="vanished">6 dager</translation>
    </message>
    <message>
        <source>7 days</source>
        <translation type="vanished">7 dager</translation>
    </message>
    <message>
        <source>10 days</source>
        <translation type="vanished">10 dager</translation>
    </message>
    <message>
        <source>14 days</source>
        <translation type="vanished">14 dager</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="7318"/>
        <source>Advanced options &gt;&gt;&gt;</source>
        <translation>Avanserte alternativer &gt;&gt;&gt;</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="7477"/>
        <source>Auto Event Limit</source>
        <translation>Automatisk hendelsesgrense</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="6987"/>
        <location filename="../Sironaviewerui.ui" line="7557"/>
        <location filename="../Sironaviewerui.ui" line="7907"/>
        <source>1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="6987"/>
        <location filename="../Sironaviewerui.ui" line="7562"/>
        <location filename="../Sironaviewerui.ui" line="7912"/>
        <source>2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="7567"/>
        <location filename="../Sironaviewerui.ui" line="7917"/>
        <source>3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="7572"/>
        <location filename="../Sironaviewerui.ui" line="7922"/>
        <source>4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="7577"/>
        <location filename="../Sironaviewerui.ui" line="7927"/>
        <source>5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="7582"/>
        <location filename="../Sironaviewerui.ui" line="7932"/>
        <source>6</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="6987"/>
        <location filename="../Sironaviewerui.ui" line="7587"/>
        <location filename="../Sironaviewerui.ui" line="7937"/>
        <source>7</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="7592"/>
        <location filename="../Sironaviewerui.ui" line="7942"/>
        <source>8</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="7597"/>
        <location filename="../Sironaviewerui.ui" line="7947"/>
        <source>9</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="6987"/>
        <location filename="../Sironaviewerui.ui" line="7602"/>
        <location filename="../Sironaviewerui.ui" line="7952"/>
        <source>10</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="7607"/>
        <location filename="../Sironaviewerui.ui" line="7957"/>
        <source>11</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="7612"/>
        <location filename="../Sironaviewerui.ui" line="7962"/>
        <source>12</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="7617"/>
        <location filename="../Sironaviewerui.ui" line="7967"/>
        <source>13</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="6987"/>
        <location filename="../Sironaviewerui.ui" line="7622"/>
        <location filename="../Sironaviewerui.ui" line="7972"/>
        <source>14</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="7158"/>
        <source>Manual Set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="7165"/>
        <location filename="../Sironaviewerui.cpp" line="4126"/>
        <source> day</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="7627"/>
        <location filename="../Sironaviewerui.ui" line="7977"/>
        <source>15</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="7632"/>
        <location filename="../Sironaviewerui.ui" line="7982"/>
        <source>16</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="7637"/>
        <location filename="../Sironaviewerui.ui" line="7987"/>
        <source>17</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="7642"/>
        <location filename="../Sironaviewerui.ui" line="7992"/>
        <source>18</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="7647"/>
        <location filename="../Sironaviewerui.ui" line="7997"/>
        <source>19</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="7652"/>
        <location filename="../Sironaviewerui.ui" line="8002"/>
        <source>20</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="7657"/>
        <location filename="../Sironaviewerui.ui" line="8007"/>
        <source>50</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="7662"/>
        <location filename="../Sironaviewerui.ui" line="8012"/>
        <source>100</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="7667"/>
        <location filename="../Sironaviewerui.ui" line="8017"/>
        <source>1000</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="7827"/>
        <source>Manual Event Limit</source>
        <translation>Manuell hendelsesgrense</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8177"/>
        <source>TTM Speed</source>
        <translation>TTM-hastighet</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8257"/>
        <source>1x</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8262"/>
        <source>3x</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8422"/>
        <source>Tachy</source>
        <translation>Tachy</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8502"/>
        <location filename="../Sironaviewerui.ui" line="8867"/>
        <location filename="../Sironaviewerui.ui" line="9157"/>
        <location filename="../Sironaviewerui.ui" line="9487"/>
        <source>Off</source>
        <translation>Av</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8507"/>
        <source>120 bpm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8512"/>
        <source>125 bpm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8517"/>
        <source>130 bpm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8522"/>
        <source>135 bpm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8527"/>
        <source>140 bpm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8532"/>
        <source>145 bpm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8537"/>
        <source>150 bpm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8542"/>
        <source>155 bpm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8547"/>
        <source>160 bpm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8552"/>
        <source>165 bpm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8557"/>
        <source>170 bpm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8562"/>
        <source>175 bpm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8567"/>
        <source>180 bpm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8572"/>
        <source>185 bpm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8577"/>
        <source>190 bpm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8582"/>
        <source>195 bpm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8587"/>
        <source>200 bpm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8592"/>
        <source>205 bpm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8597"/>
        <source>210 bpm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8602"/>
        <source>215 bpm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8607"/>
        <source>220 bpm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8612"/>
        <source>225 bpm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8617"/>
        <source>230 bpm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8622"/>
        <source>235 bpm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8627"/>
        <source>240 bpm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8787"/>
        <source>Brady</source>
        <translation>Brady</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8872"/>
        <source>15 bpm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8877"/>
        <source>20 bpm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8882"/>
        <source>25 bpm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8887"/>
        <source>30 bpm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8892"/>
        <source>35 bpm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8897"/>
        <source>40 bpm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8902"/>
        <source>45 bpm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8907"/>
        <source>50 bpm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8912"/>
        <source>55 bpm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8917"/>
        <source>60 bpm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9077"/>
        <source>Pause</source>
        <translation>Pause</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9162"/>
        <source>1.5 sec</source>
        <translation>1,5 sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9167"/>
        <source>2.0 sec</source>
        <translation>2,0 sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9172"/>
        <source>2.5 sec</source>
        <translation>2,5 sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9177"/>
        <source>3.0 sec</source>
        <translation>3,0 sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9182"/>
        <source>3.5 sec</source>
        <translation>3,5 sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9187"/>
        <source>4.0 sec</source>
        <translation>4,0 sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9192"/>
        <source>4.5 sec</source>
        <translation>4,5 sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9197"/>
        <source>5.0 sec</source>
        <translation>5,0 sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9202"/>
        <source>5.5 sec</source>
        <translation>5,5 sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9207"/>
        <source>6.0 sec</source>
        <translation>6,0 sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9212"/>
        <source>6.5 sec</source>
        <translation>6,5 sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9217"/>
        <source>7.0 sec</source>
        <translation>7,0 sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9222"/>
        <source>7.5 sec</source>
        <translation>7,5 sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9227"/>
        <source>8.0 sec</source>
        <translation>8,0 sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9232"/>
        <source>8.5 sec</source>
        <translation>8,5 sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9237"/>
        <source>9.0 sec</source>
        <translation>9,0 sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9242"/>
        <source>9.5 sec</source>
        <translation>9,5 sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9247"/>
        <source>10.0 sec</source>
        <translation>10,0 sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9407"/>
        <source>Afib</source>
        <translation>Afib</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9492"/>
        <source>On</source>
        <translation>På</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9652"/>
        <source>Sample rate</source>
        <translation>Samplingsfrekvens</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9732"/>
        <location filename="../Sironaviewerui.cpp" line="4145"/>
        <source>128 sps</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9737"/>
        <location filename="../Sironaviewerui.cpp" line="3997"/>
        <location filename="../Sironaviewerui.cpp" line="4144"/>
        <source>256 sps</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9897"/>
        <source>Pacemaker</source>
        <translation>Pacemaker</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9977"/>
        <source>OFF</source>
        <translation>AV</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9982"/>
        <source>ON</source>
        <translation>PÅ</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10820"/>
        <source>Settings Saved</source>
        <translation>Innstillingene er lagret</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="11024"/>
        <source>Settings have been saved.

Disconnect USB and connect a patient cable to finish setting up the patient. 

Use Bluetooth to confirm hookup.</source>
        <translation>Innstillingene er lagret.

Koble fra USB og koblet til en pasientkabel for å fullføre pasientoppsettet.

Bruk Bluetooth for å bekrefte oppkoblingen.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="11602"/>
        <source>Attach Electrodes</source>
        <translation>Fest elektroder</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="12424"/>
        <source>ECG</source>
        <translation>EKG</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="12719"/>
        <source>Start</source>
        <translation>Start</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="13178"/>
        <source>Procedure Active</source>
        <translation>Prosedyre aktiv</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="13373"/>
        <source>This monitor is currently active in a procedure.</source>
        <translation>Denne monitoren er nå aktiv i en prosedyre.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="13681"/>
        <location filename="../Sironaviewerui.ui" line="14925"/>
        <location filename="../Sironaviewerui.ui" line="17462"/>
        <source>Disconnect</source>
        <translation>Frakoble</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="13996"/>
        <source>End Procedure</source>
        <translation>Avslutt prosedyre</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="14455"/>
        <source>Downloading</source>
        <translation>Laster ned</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="14654"/>
        <location filename="../Sironaviewerui.cpp" line="1665"/>
        <source>Downloading data…</source>
        <translation>Laster ned data ...</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="14821"/>
        <source>Please connect USB cable 
to download holter data.</source>
        <translation>Koble til USB-kabelen
for å laste ned Holter-data.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="15384"/>
        <source>Complete</source>
        <translation>Fullfø</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="15585"/>
        <source>Procedure complete</source>
        <translation>Prosedyren er fullført</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="15893"/>
        <source>New Procedure</source>
        <translation>Ny prosedyre</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="16414"/>
        <source>Monitor</source>
        <translation>Monitor</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="16703"/>
        <source>Device status detailed description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="17076"/>
        <source>Mute</source>
        <translation>Dempe</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="17246"/>
        <source>Monitor signal strength</source>
        <translation>Monitorens signalstyrke</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="17585"/>
        <source>Internet status label</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="17702"/>
        <source>Connect to server</source>
        <translation>Koble til server</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="17849"/>
        <source>Monitor serial number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="18085"/>
        <source>Monitor model number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="18321"/>
        <source>Monitor firmware version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="18557"/>
        <source>App version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Monitor Serial Number</source>
        <translation type="vanished">Monitorens serienummer</translation>
    </message>
    <message>
        <source>Monitor Model Number</source>
        <translation type="vanished">Monitorens modellnummer</translation>
    </message>
    <message>
        <source>Monitor FW Version</source>
        <translation type="vanished">Monitorens FW-versjon</translation>
    </message>
    <message>
        <source>Application Version</source>
        <translation type="vanished">Programversjon</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="18855"/>
        <location filename="../Sironaviewerui.ui" line="19578"/>
        <source>Back</source>
        <translation>Tilbake</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="19100"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;http://www.DatrixMed.com/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;www.DatrixMed.com&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="19234"/>
        <source>800-555-1234</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="19314"/>
        <source>24 hours / 7 days</source>
        <translation>24 timer / 7 dager</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="19420"/>
        <source>Datrix enhances the mobility
and effectiveness of miniature medical,
hearing health and professional audio
communication wireless body-worn devices.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="102"/>
        <source>Sirona Viewer (r%1)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="264"/>
        <source>Low disk space</source>
        <translation>Lite diskplass</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="264"/>
        <source>There is low disk space(%1MB). Please close this app. Free some disk space, and restart the app again.</source>
        <translation>Det er lite plass på disken (%1MB). Lukk dette programmet. Frigjør diskplass, og starte programmet på nytt.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="486"/>
        <source>Are you sure you would like to exit?</source>
        <translation>Er du sikker på at du vil avslutte?</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="735"/>
        <source>Cable Disconnected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="887"/>
        <source>Incorrect cable connected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="892"/>
        <source>Connect patient cable.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="1257"/>
        <source>Communication error.</source>
        <translation>Kommunikasjonsfeil.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="4147"/>
        <source>Sample rate decreased due to long duration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="1925"/>
        <source>Connect patient cable</source>
        <translation>Tilkoblet pasientkabel</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="2028"/>
        <source>Ensure electrodes are connected</source>
        <translation>Sikre at elektrondene er tilkoblet</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="2031"/>
        <source>All electrodes connected</source>
        <translation>Alle elektrodene er tilkoblet</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="2488"/>
        <source>Select mode</source>
        <translation>Velg modus</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="2492"/>
        <source>Insert cable.</source>
        <translation>Sett inn kabel.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="2973"/>
        <location filename="../Sironaviewerui.cpp" line="2976"/>
        <location filename="../Sironaviewerui.cpp" line="2988"/>
        <location filename="../Sironaviewerui.cpp" line="2997"/>
        <source>Advanced options %1</source>
        <translation>Avanserte alternativer %1</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="4000"/>
        <source>Record duration decreased due to high sample rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="4128"/>
        <source> days</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="3907"/>
        <source>Monitor Functioning Properly</source>
        <translation>Monitoren fungerer slik den skal</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="3934"/>
        <source>Sirona B2</source>
        <translation></translation>
    </message>
</context>
</TS>
