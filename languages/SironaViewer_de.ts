<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>DeviceHandler</name>
    <message>
        <location filename="../devicehandler.cpp" line="91"/>
        <location filename="../devicehandler.cpp" line="96"/>
        <location filename="../devicehandler.cpp" line="129"/>
        <source>ERROR: </source>
        <translation>FEHLER:</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="304"/>
        <source>ERROR: Error happened during event download.</source>
        <translation>FEHLER: Beim Herunterladen der Ereignisse ist ein Fehler aufgetreten.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="363"/>
        <source>INFO: Read parameter command executed with success.</source>
        <translation>INFO: Leseparameterbefehl mit Erfolg ausgeführt.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="454"/>
        <source>ERROR: Invalid parameter or parameter not writable.</source>
        <translation>FEHLER: Ungültiger Parameter oder Parameter nicht beschreibbar.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="498"/>
        <source>INFO: Commit parameters command executed with success.</source>
        <translation>INFO: Commit-Parameter-Befehl mit Erfolg ausgeführt.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="543"/>
        <source>Read all parameters command executed with success.</source>
        <translation>Alle Parameter lesen Befehl mit Erfolg ausgeführt.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="582"/>
        <source>INFO: Get Event Header command, for Event nr.%1 executed with success.</source>
        <translation>INFO: Hole Ereignis-Kopfzeile-Befehl, für Ereignis nr.%1 mit Erfolg ausgeführt.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="593"/>
        <source>ERROR: Invalid event size returned by the device.
Command execution cannot proceed.</source>
        <translation>FEHLER: Vom Gerät zurückgegebene ungültige Ereignisgröße.
Befehlsausführung kann nicht fortgesetzt werden.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="617"/>
        <source>ERROR: Couldn&apos;t open event file to write to.
Command execution cannot proceed.</source>
        <translation>FEHLER: Ereignisdatei konnte zum Schreiben nicht geöffnet werden.
Befehlsausführung kann nicht fortgesetzt werden.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="660"/>
        <location filename="../devicehandler.cpp" line="831"/>
        <source>ERROR: Couldn&apos;t open annotation file to write to.
Command execution cannot proceed.</source>
        <translation>FEHLER: Annotationsdatei konnte zum Schreiben nicht geöffnet werden.
Befehlsausführung kann nicht fortgesetzt werden.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="708"/>
        <source>INFO: Event %1 from %2 downloaded with success.</source>
        <translation>INFO: Ereignis %1 von %2 mit Erfolg heruntergeladen.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="721"/>
        <source>INFO: Event %1 marked as sent with success.</source>
        <translation>INFO: Ereignis %1 wurde als erfolgreich gesendet markiert.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="723"/>
        <source>ERROR: Event downloaded with success, but failed to mark Event as sent.</source>
        <translation>FEHLER: Ereignis erfolgreich heruntergeladen, aber nicht als gesendet markiert.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="732"/>
        <location filename="../devicehandler.cpp" line="739"/>
        <source>ERROR: Holter downloaded with success, but failed to mark Holter as sent.</source>
        <translation>FEHLER: Holter wurde erfolgreich heruntergeladen, aber Holter wurde nicht als gesendet markiert.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="741"/>
        <source>INFO: Holter marked as sent with success.</source>
        <translation>INFO: Holter mit Erfolg als gesendet markiert.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="788"/>
        <source>ERROR: Couldn&apos;t open holter file to write to.
Command execution cannot proceed.</source>
        <translation>FEHLER: Holter-Datei konnte nicht zum Schreiben geöffnet werden.
Befehlsausführung kann nicht fortgesetzt werden.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="802"/>
        <source>ERROR: There was an error during ECG data transfer.</source>
        <translation>FEHLER: Es gab einen Fehler bei der EKG-Datenübertragung.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="862"/>
        <source>INFO: Download holter command executed with success.</source>
        <translation>INFO: Holter-Befehl herunterladen mit Erfolg ausgeführt.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="876"/>
        <source>INFO: Get battery status command executed with success.</source>
        <translation>INFO: Batterie-Status erhalten Befehl mit Erfolg ausgeführt.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="892"/>
        <source>INFO: Start Procedure command executed with success.</source>
        <translation>INFO: Verfahren starten Befehl mit Erfolg ausgeführt.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="908"/>
        <source>INFO: Stop Procedure command executed with success.</source>
        <translation>INFO: Verfahren stoppen Befehl mit Erfolg ausgeführt.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="924"/>
        <source>INFO: Soft Reset command executed with success.</source>
        <translation>INFO: Soft Reset-Befehl mit Erfolg ausgeführt.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="952"/>
        <source>INFO: Successfully sent system time to the device.</source>
        <translation>INFO: Erfolgreiche Übertragung der Systemzeit an das Gerät.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="1146"/>
        <source>ERROR: createHeaFile called with nullptr eventHeader.</source>
        <translation>FEHLER: createHeaFile wird mit nullptr eventHeader aufgerufen.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="1216"/>
        <source>ERROR: createHeaFile called with NULL eventHeader.</source>
        <translation>FEHLER: createHeaFile wird mit NULL eventHeader aufgerufen.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="1294"/>
        <source>ERROR: Invalid input parameter device mode, couldn&apos;t create hea file.</source>
        <translation>FEHLER: Ungültiger Eingabeparameter-Gerätemodus, konnte keine hea-Datei erstellen.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="1301"/>
        <source>ERROR: Couldn&apos;t open header file to write to.</source>
        <translation>FEHLER: Header-Datei konnte zum Schreiben nicht geöffnet werden.</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="1313"/>
        <source>File open json file for writing(%1).</source>
        <translation>Datei json-Datei zum Schreiben öffnen(%1).</translation>
    </message>
    <message>
        <location filename="../devicehandler.cpp" line="1403"/>
        <source>ERROR: createResFile failed.</source>
        <translation>FEHLER: createResFile ist fehlgeschlagen.</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../Sironaviewerui.cpp" line="1042"/>
        <source>Sirona Disconnected!</source>
        <translation>Sirona-Verbindung abgebrochen!</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="95"/>
        <source>Another instance of this application is already running</source>
        <translation>Eine weitere Instanz dieser Anwendung läuft bereits</translation>
    </message>
    <message>
        <location filename="../utilities.cpp" line="37"/>
        <location filename="../utilities.cpp" line="43"/>
        <source>Error creating directory </source>
        <translation>Fehler beim Erstellen des Verzeichnisses </translation>
    </message>
    <message>
        <location filename="../utilities.cpp" line="37"/>
        <location filename="../utilities.cpp" line="43"/>
        <source>. Check path and permissions.</source>
        <translation>. Pfad und Berechtigungen prüfen.</translation>
    </message>
</context>
<context>
    <name>SironaViewerUi</name>
    <message>
        <location filename="../Sironaviewerui.ui" line="32"/>
        <source>Sirona Viewer</source>
        <translation>Sirona-Anzeige</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="325"/>
        <location filename="../Sironaviewerui.ui" line="1297"/>
        <location filename="../Sironaviewerui.ui" line="2315"/>
        <location filename="../Sironaviewerui.ui" line="4208"/>
        <location filename="../Sironaviewerui.ui" line="7244"/>
        <location filename="../Sironaviewerui.ui" line="12129"/>
        <location filename="../Sironaviewerui.ui" line="12881"/>
        <location filename="../Sironaviewerui.ui" line="13721"/>
        <location filename="../Sironaviewerui.ui" line="14539"/>
        <location filename="../Sironaviewerui.ui" line="15810"/>
        <location filename="../Sironaviewerui.ui" line="17506"/>
        <location filename="../Sironaviewerui.ui" line="18766"/>
        <location filename="../Sironaviewerui.ui" line="19675"/>
        <location filename="../Sironaviewerui.ui" line="20754"/>
        <source>800 555 1234</source>
        <translation>800 555 1234</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="514"/>
        <source>Sirona</source>
        <translation>Sirona</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="606"/>
        <source>Procedure setup
and
data download</source>
        <translation>Verfahrenseinrichtung
und
Daten-Download</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="913"/>
        <source>Begin</source>
        <translation>Anfangen</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="1467"/>
        <source>Connect</source>
        <translation>Verbinden</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="1549"/>
        <location filename="../Sironaviewerui.ui" line="2596"/>
        <location filename="../Sironaviewerui.ui" line="4593"/>
        <location filename="../Sironaviewerui.ui" line="11693"/>
        <location filename="../Sironaviewerui.ui" line="12477"/>
        <location filename="../Sironaviewerui.ui" line="14106"/>
        <location filename="../Sironaviewerui.ui" line="14820"/>
        <location filename="../Sironaviewerui.ui" line="16091"/>
        <location filename="../Sironaviewerui.ui" line="17787"/>
        <location filename="../Sironaviewerui.ui" line="19026"/>
        <location filename="../Sironaviewerui.ui" line="19956"/>
        <location filename="../Sironaviewerui.ui" line="21139"/>
        <source>This is the status message bar</source>
        <translation>Das ist die Statusmeldungsleiste</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="1656"/>
        <source>Choose Sirona device</source>
        <translation>Sirona-Gerät wählen</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="2052"/>
        <location filename="../Sironaviewerui.ui" line="6983"/>
        <location filename="../Sironaviewerui.ui" line="11869"/>
        <location filename="../Sironaviewerui.ui" line="13463"/>
        <source>Next</source>
        <translation>Weiter</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="2514"/>
        <source>Cable</source>
        <translation>Kabel</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="3005"/>
        <source>Holter</source>
        <translation>Holter</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="3320"/>
        <source>Event</source>
        <translation>Ereignis</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="3635"/>
        <source>Post-Event</source>
        <translation>Nach dem Ereignis</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="3950"/>
        <source>MCT</source>
        <translation>MCT</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="4471"/>
        <source>Patient</source>
        <translation>Patient</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="4779"/>
        <source>Patient ID#:</source>
        <translation>Patienten-ID#:</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="5064"/>
        <source>Patient Name:</source>
        <translation>Patientenname:</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="5207"/>
        <source>last name</source>
        <translation>Nachname</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="5346"/>
        <source>first name</source>
        <translation>Vorname</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="5434"/>
        <source>middle initial</source>
        <translation>mittlere Initiale</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="5598"/>
        <source>Date of Birth:</source>
        <translation>Geburtsdatum:</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="5811"/>
        <source>Physician name:</source>
        <translation>Name des Arztes:</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="6102"/>
        <source>Technician name:</source>
        <translation>Name des Spezialisten:</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="6393"/>
        <source>Facility:</source>
        <translation>Einrichtung:</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="6684"/>
        <source>Comment:</source>
        <translation>Kommentar:</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="6815"/>
        <source>insert comment (up to 500 characters)</source>
        <translation>Kommentar eingeben (bis zu 500 Zeichen)</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="7507"/>
        <source>Procedure</source>
        <translation>Verfahren</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="7753"/>
        <location filename="../Sironaviewerui.ui" line="21443"/>
        <source>Monitor battery</source>
        <translation>Monitor-Batterie</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="7988"/>
        <source>Pre-trigger</source>
        <translation>Vorab-Auslöser</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8068"/>
        <location filename="../Sironaviewerui.ui" line="8343"/>
        <source>15 sec</source>
        <translation>15 Sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8073"/>
        <location filename="../Sironaviewerui.ui" line="8348"/>
        <source>30 sec</source>
        <translation>30 Sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8078"/>
        <location filename="../Sironaviewerui.ui" line="8353"/>
        <source>45 sec</source>
        <translation>45 Sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8083"/>
        <location filename="../Sironaviewerui.ui" line="8358"/>
        <source>60 sec</source>
        <translation>60 Sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8088"/>
        <location filename="../Sironaviewerui.ui" line="8363"/>
        <source>90 sec</source>
        <translation>90 Sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8093"/>
        <location filename="../Sironaviewerui.ui" line="8368"/>
        <source>120 sec</source>
        <translation>120 Sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8098"/>
        <location filename="../Sironaviewerui.ui" line="8373"/>
        <source>180 sec</source>
        <translation>180 Sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8103"/>
        <location filename="../Sironaviewerui.ui" line="8378"/>
        <location filename="../Sironaviewerui.cpp" line="3305"/>
        <source>300 sec</source>
        <translation>300 Sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8263"/>
        <location filename="../Sironaviewerui.cpp" line="3226"/>
        <location filename="../Sironaviewerui.cpp" line="3277"/>
        <source>Post-trigger</source>
        <translation>Nach-Auslöser</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8538"/>
        <source>Record duration</source>
        <translation>Aufzeichnungsdauer</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8545"/>
        <location filename="../Sironaviewerui.ui" line="9121"/>
        <location filename="../Sironaviewerui.ui" line="9476"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8545"/>
        <location filename="../Sironaviewerui.ui" line="9126"/>
        <location filename="../Sironaviewerui.ui" line="9481"/>
        <source>2</source>
        <translation>2</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8545"/>
        <location filename="../Sironaviewerui.ui" line="9141"/>
        <location filename="../Sironaviewerui.ui" line="9496"/>
        <source>5</source>
        <translation>5</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8545"/>
        <location filename="../Sironaviewerui.ui" line="9151"/>
        <location filename="../Sironaviewerui.ui" line="9506"/>
        <source>7</source>
        <translation>7</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8545"/>
        <location filename="../Sironaviewerui.ui" line="9166"/>
        <location filename="../Sironaviewerui.ui" line="9521"/>
        <source>10</source>
        <translation>10</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8545"/>
        <location filename="../Sironaviewerui.ui" line="9186"/>
        <location filename="../Sironaviewerui.ui" line="9541"/>
        <source>14</source>
        <translation>14</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8717"/>
        <source>Manual Set</source>
        <translation>Manuelle Einstellung</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8724"/>
        <location filename="../Sironaviewerui.cpp" line="4296"/>
        <source> day</source>
        <translation> Tag</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="8877"/>
        <source>Advanced options &gt;&gt;&gt;</source>
        <translation>Fortgeschrittene Optionen &gt;&gt;&gt;</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9036"/>
        <source>Auto Event Limit</source>
        <translation>Auto-Ereignisgrenze</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9116"/>
        <location filename="../Sironaviewerui.ui" line="9471"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9131"/>
        <location filename="../Sironaviewerui.ui" line="9486"/>
        <source>3</source>
        <translation>3</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9136"/>
        <location filename="../Sironaviewerui.ui" line="9491"/>
        <source>4</source>
        <translation>4</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9146"/>
        <location filename="../Sironaviewerui.ui" line="9501"/>
        <source>6</source>
        <translation>6</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9156"/>
        <location filename="../Sironaviewerui.ui" line="9511"/>
        <source>8</source>
        <translation>8</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9161"/>
        <location filename="../Sironaviewerui.ui" line="9516"/>
        <source>9</source>
        <translation>9</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9171"/>
        <location filename="../Sironaviewerui.ui" line="9526"/>
        <source>11</source>
        <translation>11</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9176"/>
        <location filename="../Sironaviewerui.ui" line="9531"/>
        <source>12</source>
        <translation>12</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9181"/>
        <location filename="../Sironaviewerui.ui" line="9536"/>
        <source>13</source>
        <translation>13</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9191"/>
        <location filename="../Sironaviewerui.ui" line="9546"/>
        <source>15</source>
        <translation>15</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9196"/>
        <location filename="../Sironaviewerui.ui" line="9551"/>
        <source>16</source>
        <translation>16</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9201"/>
        <location filename="../Sironaviewerui.ui" line="9556"/>
        <source>17</source>
        <translation>17</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9206"/>
        <location filename="../Sironaviewerui.ui" line="9561"/>
        <source>18</source>
        <translation>18</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9211"/>
        <location filename="../Sironaviewerui.ui" line="9566"/>
        <source>19</source>
        <translation>19</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9216"/>
        <location filename="../Sironaviewerui.ui" line="9571"/>
        <source>20</source>
        <translation>20</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9221"/>
        <location filename="../Sironaviewerui.ui" line="9576"/>
        <source>50</source>
        <translation>50</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9226"/>
        <location filename="../Sironaviewerui.ui" line="9581"/>
        <source>100</source>
        <translation>100</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9231"/>
        <location filename="../Sironaviewerui.ui" line="9586"/>
        <source>1000</source>
        <translation>1000</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9391"/>
        <source>Manual Event Limit</source>
        <translation>Manuelles Ereignislimit</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9746"/>
        <source>TTM Speed</source>
        <translation>TTM-Geschwindigkeit</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9826"/>
        <source>1x</source>
        <translation>1x</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9831"/>
        <source>3x</source>
        <translation>3x</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="9991"/>
        <source>Tachy</source>
        <translation>Tachy</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10071"/>
        <location filename="../Sironaviewerui.ui" line="10436"/>
        <location filename="../Sironaviewerui.ui" line="10726"/>
        <location filename="../Sironaviewerui.ui" line="11056"/>
        <source>Off</source>
        <translation>Aus</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10076"/>
        <source>120 bpm</source>
        <translation>120 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10081"/>
        <source>125 bpm</source>
        <translation>125 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10086"/>
        <source>130 bpm</source>
        <translation>130 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10091"/>
        <source>135 bpm</source>
        <translation>135 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10096"/>
        <source>140 bpm</source>
        <translation>140 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10101"/>
        <source>145 bpm</source>
        <translation>145 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10106"/>
        <source>150 bpm</source>
        <translation>150 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10111"/>
        <source>155 bpm</source>
        <translation>155 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10116"/>
        <source>160 bpm</source>
        <translation>160 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10121"/>
        <source>165 bpm</source>
        <translation>165 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10126"/>
        <source>170 bpm</source>
        <translation>170 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10131"/>
        <source>175 bpm</source>
        <translation>175 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10136"/>
        <source>180 bpm</source>
        <translation>180 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10141"/>
        <source>185 bpm</source>
        <translation>185 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10146"/>
        <source>190 bpm</source>
        <translation>190 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10151"/>
        <source>195 bpm</source>
        <translation>195 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10156"/>
        <source>200 bpm</source>
        <translation>200 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10161"/>
        <source>205 bpm</source>
        <translation>205 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10166"/>
        <source>210 bpm</source>
        <translation>210 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10171"/>
        <source>215 bpm</source>
        <translation>215 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10176"/>
        <source>220 bpm</source>
        <translation>220 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10181"/>
        <source>225 bpm</source>
        <translation>225 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10186"/>
        <source>230 bpm</source>
        <translation>230 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10191"/>
        <source>235 bpm</source>
        <translation>235 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10196"/>
        <source>240 bpm</source>
        <translation>240 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10356"/>
        <source>Brady</source>
        <translation>Brady</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10441"/>
        <source>15 bpm</source>
        <translation>15 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10446"/>
        <source>20 bpm</source>
        <translation>20 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10451"/>
        <source>25 bpm</source>
        <translation>25 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10456"/>
        <source>30 bpm</source>
        <translation>30 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10461"/>
        <source>35 bpm</source>
        <translation>35 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10466"/>
        <source>40 bpm</source>
        <translation>40 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10471"/>
        <source>45 bpm</source>
        <translation>45 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10476"/>
        <source>50 bpm</source>
        <translation>50 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10481"/>
        <source>55 bpm</source>
        <translation>55 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10486"/>
        <source>60 bpm</source>
        <translation>60 bpm</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10646"/>
        <source>Pause</source>
        <translation>Pause</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10731"/>
        <source>1.5 sec</source>
        <translation>1.5 Sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10736"/>
        <source>2.0 sec</source>
        <translation>2.0 Sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10741"/>
        <source>2.5 sec</source>
        <translation>2.5 Sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10746"/>
        <source>3.0 sec</source>
        <translation>3.0 Sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10751"/>
        <source>3.5 sec</source>
        <translation>3.5 Sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10756"/>
        <source>4.0 sec</source>
        <translation>4.0 Sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10761"/>
        <source>4.5 sec</source>
        <translation>4.5 Sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10766"/>
        <source>5.0 sec</source>
        <translation>5.0 Sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10771"/>
        <source>5.5 sec</source>
        <translation>5.5 Sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10776"/>
        <source>6.0 sec</source>
        <translation>6.0 Sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10781"/>
        <source>6.5 sec</source>
        <translation>6.5 Sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10786"/>
        <source>7.0 sec</source>
        <translation>7.0 Sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10791"/>
        <source>7.5 sec</source>
        <translation>7.5 Sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10796"/>
        <source>8.0 sec</source>
        <translation>8.0 Sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10801"/>
        <source>8.5 sec</source>
        <translation>8.5 Sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10806"/>
        <source>9.0 sec</source>
        <translation>9.0 Sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10811"/>
        <source>9.5 sec</source>
        <translation>9.5 Sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10816"/>
        <source>10.0 sec</source>
        <translation>10.0 Sek</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="10976"/>
        <source>Afib</source>
        <translation>Afib</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="11061"/>
        <source>On</source>
        <translation>Ein</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="11221"/>
        <source>Sample rate</source>
        <translation>Abtastrate</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="11301"/>
        <location filename="../Sironaviewerui.cpp" line="4316"/>
        <source>128 sps</source>
        <translation>128 sps</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="11306"/>
        <location filename="../Sironaviewerui.cpp" line="4155"/>
        <location filename="../Sironaviewerui.cpp" line="4314"/>
        <source>256 sps</source>
        <translation>256 sps</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="11466"/>
        <source>Pacemaker</source>
        <translation>Schrittmacher</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="11546"/>
        <source>OFF</source>
        <translation>AUS</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="11551"/>
        <source>ON</source>
        <translation>EIN</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="12392"/>
        <source>Settings Saved</source>
        <translation>Einstellungen gespeichert</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="12584"/>
        <source>Settings have been saved.

Disconnect USB and connect a patient cable to finish setting up the patient. 

Use Bluetooth to confirm hookup.</source>
        <translation>Einstellungen wurden gespeichert.

Trennen Sie die USB-Verbindung und schließen Sie ein Patientenkabel an, um die Einrichtung des Patienten abzuschließen.

Verwenden Sie Bluetooth, um die Verbindung zu bestätigen.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="13162"/>
        <location filename="../Sironaviewerui.cpp" line="4123"/>
        <source>Attach Electrodes</source>
        <translation>Elektroden anbringen</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="13984"/>
        <source>ECG</source>
        <translation>EKG</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="14279"/>
        <source>Start</source>
        <translation>Starten</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="14738"/>
        <source>Procedure Active</source>
        <translation>Verfahren Aktiv</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="14927"/>
        <source>This monitor is currently active in a procedure.</source>
        <translation>Dieser Monitor ist derzeit in einem Verfahren aktiv.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="15235"/>
        <location filename="../Sironaviewerui.ui" line="18506"/>
        <location filename="../Sironaviewerui.ui" line="22065"/>
        <source>Disconnect</source>
        <translation>Verbindung trennen</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="15550"/>
        <source>End Procedure</source>
        <translation>Verfahren beenden</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="16009"/>
        <location filename="../Sironaviewerui.ui" line="17705"/>
        <location filename="../Sironaviewerui.cpp" line="1267"/>
        <location filename="../Sironaviewerui.cpp" line="2522"/>
        <source>Downloading</source>
        <translation>Herunterladen</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="16211"/>
        <location filename="../Sironaviewerui.ui" line="19118"/>
        <source>&lt;Patient Info&gt;</source>
        <translation>&lt;Patienten-Info&gt;</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="16307"/>
        <source>Review patient information before download?</source>
        <translation>Patienteninformationen vor dem Herunterladen überprüfen?</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="16628"/>
        <source>Review</source>
        <translation>Überprüfung</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="16943"/>
        <source>Download</source>
        <translation>Download</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="17246"/>
        <location filename="../Sironaviewerui.ui" line="19303"/>
        <source>Erase</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="17904"/>
        <location filename="../Sironaviewerui.cpp" line="1678"/>
        <source>Downloading data…</source>
        <translation>Herunterladen der Daten...</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="18071"/>
        <source>Please connect USB cable 
to download holter data.</source>
        <translation>Bitte USB-Kabel anschließen 
um Holter-Daten herunterzuladen.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="18156"/>
        <source>Sending data to server...</source>
        <translation>Daten an Server senden...</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="18326"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:14pt;&quot;&gt;Failed to send data to server.&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:14pt;&quot;&gt;Please disconnect and &lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:14pt;&quot;&gt;try again later.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:14pt;&quot;&gt;Failed to send data to server.&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:14pt;&quot;&gt;Please disconnect and &lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:14pt;&quot;&gt;try again later.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="18406"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="18959"/>
        <location filename="../Sironaviewerui.cpp" line="1221"/>
        <source>Monitoring Complete</source>
        <translation>Überwachung beendet</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="19201"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:14pt;&quot;&gt;Erase the Data?&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:14pt;&quot;&gt;Daten löschen?&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="19415"/>
        <location filename="../Sironaviewerui.ui" line="23694"/>
        <location filename="../Sironaviewerui.ui" line="24416"/>
        <source>Back</source>
        <translation>Zurück</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="19874"/>
        <source>Complete</source>
        <translation>Abgeschlossen</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="20076"/>
        <source>Download complete</source>
        <translation>Herunterladen abgeschlossen</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="20175"/>
        <source>Monitor Erased</source>
        <translation>Monitor gelöscht</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="20496"/>
        <source>New Procedure</source>
        <translation>Neues Verfahren</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="21017"/>
        <source>Monitor</source>
        <translation>Monitor</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="21306"/>
        <source>Device status detailed description</source>
        <translation>Detaillierte Beschreibung des Gerätestatus</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="21679"/>
        <source>Mute</source>
        <translation>Stummschalten</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="21849"/>
        <source>Monitor signal strength</source>
        <translation>Monitor-Signalstärke</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="22188"/>
        <source>Internet status label</source>
        <translation>Internet-Status-Label</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="22305"/>
        <source>Connect to server</source>
        <translation>Verbindung zum Server</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="22452"/>
        <source>Monitor serial number</source>
        <translation>Monitor-Seriennummer</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="22688"/>
        <source>Monitor model number</source>
        <translation>Monitor-Modellnummer</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="22924"/>
        <source>Monitor firmware version</source>
        <translation>Monitor Firmware-Version</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="23160"/>
        <source>App version</source>
        <translation>App-Version</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="23396"/>
        <source>App DI</source>
        <translation>App-DI</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="23485"/>
        <source>00851074007125</source>
        <translation>00851074007125</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="23939"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;http://www.DatrixMed.com/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;www.DatrixMed.com&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;http://www.DatrixMed.com/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;www.DatrixMed.com&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="24073"/>
        <source>800-555-1234</source>
        <translation>800-555-1234</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="24153"/>
        <source>24 hours / 7 days</source>
        <translation>24 Stunden / 7 Tage</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.ui" line="24259"/>
        <source>Datrix enhances the mobility and
effectiveness of miniature ambulatory medical
wireless body-worn devices.</source>
        <translation>Datrix verbessert die Mobilität und
Wirksamkeit von miniaturisierten ambulanten medizinischen
am Körper getragenen drahtlosen Geräten.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="92"/>
        <source>Sirona Viewer (%1.%2.%3)</source>
        <translation>Sirona-Anzeige (%1.%2.%3)</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="141"/>
        <source>Directory error</source>
        <translation>Verzeichnis-Fehler</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="189"/>
        <source>Choose a </source>
        <translation>Wählen Sie ein </translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="189"/>
        <source> device</source>
        <translation> Gerät</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="475"/>
        <source>Server request timed out.</source>
        <translation>Serveranfrage zeitlich abgelaufen.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="494"/>
        <source>Are you sure you would like to exit?</source>
        <translation>Möchten Sie wirklich beenden?</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="660"/>
        <source>Low Battery</source>
        <translation>Schwache Batterie</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="662"/>
        <source>The monitor’s battery is low. Connect the charger.</source>
        <translation>Die Batterie des Monitors ist schwach. Verbinden Sie das Ladegerät.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="669"/>
        <source>Sensor Disconnected</source>
        <translation>Sensor nicht angeschlossen</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="671"/>
        <source>The sensor is disconnected. Connect the sensor.</source>
        <translation>Der Sensor ist nicht angeschlossen. Verbinden Sie den Sensor.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="673"/>
        <location filename="../Sironaviewerui.cpp" line="679"/>
        <source>Cable Disconnected</source>
        <translation>Kabel nicht angeschlossen</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="675"/>
        <location filename="../Sironaviewerui.cpp" line="682"/>
        <source>The patient cable is disconnected. Connect the cable.</source>
        <translation>Das Patientenkabel ist nicht angeschlossen. Verbinden Sie das Kabel.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="686"/>
        <location filename="../Sironaviewerui.cpp" line="719"/>
        <location filename="../Sironaviewerui.cpp" line="1923"/>
        <source>Ensure electrodes are connected</source>
        <translation>Sicherstellen, dass Elektroden angeschlossen sind</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="688"/>
        <source>One or more electrodes may not be properly attached to the patient.</source>
        <translation>Eine oder mehrere Elektroden sind möglicherweise nicht richtig am Patienten angebracht.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="693"/>
        <location filename="../Sironaviewerui.cpp" line="4085"/>
        <source>Monitor Functioning Properly</source>
        <translation>Monitor funktioniert ordnungsgemäss</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="709"/>
        <source>Sensor connected</source>
        <translation>Sensor angeschlossen</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="711"/>
        <location filename="../Sironaviewerui.cpp" line="721"/>
        <location filename="../Sironaviewerui.cpp" line="1930"/>
        <source>All electrodes connected</source>
        <translation>Alle Elektroden angeschlossen</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="717"/>
        <location filename="../Sironaviewerui.cpp" line="1825"/>
        <source>Connect sensor</source>
        <translation>Sensor verbinden</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="852"/>
        <source>Incorrect cable connected.</source>
        <translation>Falsches Kabel angeschlossen.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="858"/>
        <source>Connect sensor.</source>
        <translation>Sensor verbinden.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="861"/>
        <source>Connect patient cable.</source>
        <translation>Patientenkabel verbinden.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="1081"/>
        <source>Communication error.</source>
        <translation>Kommunikationsfehler.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="1365"/>
        <source>Unexpected device mode.</source>
        <translation>Unerwarteter Geräte-Modus.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="1419"/>
        <source>Server scheme not set.</source>
        <translation>Server-Schema nicht festgelegt.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="1479"/>
        <source>Could not open data file.</source>
        <translation>Datendatei konnte nicht geöffnet werden.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="1505"/>
        <location filename="../Sironaviewerui.cpp" line="1545"/>
        <source>
Communication Error:
</source>
        <translation>
Kommunikationsfehler:
</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="1520"/>
        <source>
Unknown server error.</source>
        <translation>
Unbekannter Serverfehler.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="1530"/>
        <location filename="../Sironaviewerui.cpp" line="1538"/>
        <source>
Server Message:
</source>
        <translation>
Server-Nachricht:
</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="1536"/>
        <source>
Server error message missing</source>
        <translation>
Server-Fehlermeldung fehlt</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="1827"/>
        <source>Connect patient cable</source>
        <translation>Patientenkabel verbinden</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="1921"/>
        <source>Ensure sensor is connected</source>
        <translation>Sicherstellen, dass der Sensor angeschlossen ist</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="1928"/>
        <source>Sensor is connected</source>
        <translation>Sensor ist angeschlossen</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="2428"/>
        <location filename="../Sironaviewerui.cpp" line="2453"/>
        <location filename="../Sironaviewerui.cpp" line="2466"/>
        <location filename="../Sironaviewerui.cpp" line="2478"/>
        <source>Select mode</source>
        <translation>Modus auswählen</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="2439"/>
        <source>Insert cable.</source>
        <translation>Kabel einführen.</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="2524"/>
        <source>New Patient</source>
        <translation>Neuer Patient</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="2537"/>
        <source>Indications:</source>
        <translation>Indikationen:</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="3212"/>
        <location filename="../Sironaviewerui.cpp" line="3215"/>
        <location filename="../Sironaviewerui.cpp" line="3233"/>
        <location filename="../Sironaviewerui.cpp" line="3242"/>
        <location filename="../Sironaviewerui.cpp" line="3266"/>
        <location filename="../Sironaviewerui.cpp" line="3270"/>
        <location filename="../Sironaviewerui.cpp" line="3283"/>
        <location filename="../Sironaviewerui.cpp" line="3293"/>
        <source>Advanced options %1</source>
        <translation>Erweiterte Optionen %1</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="3255"/>
        <source>Record length</source>
        <translation>Länge der Aufzeichnung</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="4121"/>
        <source>Monitor Placement</source>
        <translation>Monitor-Platzierung</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="4159"/>
        <source>Record duration decreased due to high sample rate</source>
        <translation>Die Aufzeichnungsdauer verringerte sich aufgrund der hohen Abtastrate</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="4298"/>
        <source> days</source>
        <translation> Tage</translation>
    </message>
    <message>
        <location filename="../Sironaviewerui.cpp" line="4318"/>
        <source>Sample rate decreased due to long duration</source>
        <translation>Abtastrate aufgrund langer Dauer gesunken</translation>
    </message>
</context>
<context>
    <name>customer_defs</name>
    <message>
        <location filename="../customizations/datrix/customer.cpp" line="16"/>
        <source>Datrix enhances the mobility
and effectiveness of miniature medical body-worn devices.</source>
        <translation>Datrix steigert die Mobilität
und die Wirksamkeit von am Körper getragenen medizinischen Miniaturgeräten.</translation>
    </message>
</context>
</TS>
