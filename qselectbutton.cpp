#include <math.h>
#include "qselectbutton.h"
#include <QPainter>
#include <QMouseEvent>

QSelectButton::QSelectButton( QWidget *parent )
	: QWidget( parent )
	, m_selectedItemColor( 57, 145, 195 )
	, m_hoverItemColor( 225, 239, 245 )
{
	setMouseTracking( true );
}

void QSelectButton::setItems( const QStringList items )
{
	m_items = items;

	m_selectedIndex = -1;
	m_hoverIndex = -1;

	updateVerticalBarPositions();

	update();
}

QStringList QSelectButton::items() const
{
	return m_items;
}

void QSelectButton::setRadius( int radius )
{
	if ( radius >= 0 ) {
		m_radius = radius;

		update();
	}
}

int QSelectButton::radius() const
{
	return m_radius;
}

void QSelectButton::setMargin( int margin )
{
	if ( margin >= 0 ) {
		m_margin = margin;

		update();
	}
}

int QSelectButton::margin() const
{
	return m_margin;
}

void QSelectButton::setOutlineWidth( int width )
{
	if ( width >= 1 ) {
		m_outlineWidth = width;

		update();
	}
}

int QSelectButton::outlineWidth() const
{
	return m_outlineWidth;
}

void QSelectButton::setCurrentIndex( int index )
{
	if ( ( index >= -1 ) && ( index < m_items.count() ) ) {
		m_selectedIndex = index;

		update();
	}
}

int QSelectButton::currentIndex() const
{
	return m_selectedIndex;
}

void QSelectButton::paintEvent( QPaintEvent *event )
{
	Q_UNUSED( event );

	if ( m_items.isEmpty() ) {
		return;
	}

	QPainter painter( this );
	QBrush selectedBrush( m_selectedItemColor );
	QBrush hoverBrush( m_hoverItemColor );
	QRectF rectf;
	QPen pen( Qt::black );

	int marginLeft = m_margin;
	int marginTop = m_margin;
	int marginRight = width() - 1 - m_margin;
	int marginBottom = height() - 1 - m_margin;
	int itemCount = m_items.count();

	pen.setWidth( m_outlineWidth );
	pen.setCapStyle( Qt::SquareCap );
	pen.setColor( m_selectedItemColor );

	painter.setRenderHint( QPainter::Antialiasing );
	painter.setPen( pen );

	rectf.setLeft( marginLeft );
	rectf.setTop( marginTop );
	rectf.setRight( marginRight );
	rectf.setBottom( marginBottom );

	painter.drawRoundedRect( rectf.translated( 0.5, 0.5 ), ( qreal )m_radius, ( qreal )m_radius );

	if ( m_hoverIndex >= 0 ) {

		QRect rect;

		int left = m_hoverIndex <= 0 ? marginLeft : m_verticalBarPositions.at( m_hoverIndex - 1 );
		int right = m_hoverIndex >= itemCount - 1 ? marginRight : m_verticalBarPositions.at( m_hoverIndex );

		painter.setBrush( hoverBrush );

		rect.setLeft( left );
		rect.setRight( right );
		rect.setTop( marginTop );
		rect.setBottom( marginBottom );

		painter.setClipRect( rect );

		painter.drawRoundedRect( rectf.translated( 0.5, 0.5 ), ( qreal )m_radius, ( qreal )m_radius );

		painter.setClipping( false );
	}

	if ( m_selectedIndex >= 0 ) {

		QRect rect;

		int left = m_selectedIndex <= 0 ? marginLeft : m_verticalBarPositions.at( m_selectedIndex - 1 );
		int right = m_selectedIndex >= itemCount - 1 ? marginRight : m_verticalBarPositions.at( m_selectedIndex );

		painter.setBrush( selectedBrush );

		rect.setLeft( left );
		rect.setRight( right );
		rect.setTop( marginTop );
		rect.setBottom( marginBottom );

		painter.setClipRect( rect );

		painter.drawRoundedRect( rectf.translated( 0.5, 0.5 ), ( qreal )m_radius, ( qreal )m_radius );

		painter.setClipping( false );
	}

	for ( int x : m_verticalBarPositions ) {
		QLineF linef;

		linef.setP1( QPointF( x + 0.5, marginTop + 0.5 ) );
		linef.setP2( QPointF( x + 0.5, marginBottom + 0.5 ) );

		painter.drawLine( linef );
	}

	for ( int item = 0; item < itemCount; item++ ) {
		QRect rect;

		int left = item <= 0 ? marginLeft : m_verticalBarPositions.at( item - 1 );
		int right = item >= itemCount - 1 ? marginRight : m_verticalBarPositions.at( item );

		rect.setLeft( left );
		rect.setRight( right );
		rect.setTop( marginTop );
		rect.setBottom( marginBottom );

		if ( item == m_selectedIndex ) {
			painter.setPen( palette().base().color() );
		} else {
			painter.setPen( m_selectedItemColor );
		}

		painter.drawText( rect, Qt::AlignCenter, m_items.at( item ) );
	}
}

void QSelectButton::resizeEvent( QResizeEvent *event )
{
	Q_UNUSED( event );

	updateVerticalBarPositions();
}

void QSelectButton::mouseMoveEvent( QMouseEvent *event )
{
	if ( event->buttons() == Qt::NoButton ) {
		int marginLeft = m_margin;
		int marginTop = m_margin;
		int marginRight = width() - 1 - m_margin;
		int marginBottom = height() - 1 - m_margin;
		int w = marginRight - marginLeft;
		int itemCount = m_items.count();

		int x = event->x();
		int y = event->y();
		int hoverIndex = -1;

		if ( ( x >= marginLeft ) && ( x < marginRight ) && ( y >= marginTop ) && ( y <= marginBottom ) ) {
			hoverIndex = floor( ( x - marginLeft ) / ( ( qreal )w / itemCount ) );
		}

		if ( m_hoverIndex != hoverIndex ) {
			m_hoverIndex = hoverIndex;

			update();
		}
	}
}

void QSelectButton::leaveEvent( QEvent *event )
{
	Q_UNUSED( event );

	int hoverIndex = -1;

	if ( m_hoverIndex != hoverIndex ) {
		m_hoverIndex = hoverIndex;

		update();
	}
}

void QSelectButton::mousePressEvent( QMouseEvent *event )
{
	Q_UNUSED( event );

	if ( ( m_hoverIndex >= 0 ) && ( m_selectedIndex != m_hoverIndex ) ) {
		m_selectedIndex = m_hoverIndex;

		update();

		emit indexChanged( m_selectedIndex );
	}
}

void QSelectButton::updateVerticalBarPositions()
{
	if ( m_items.count() < 2 ) {
		m_verticalBarPositions.clear();

		return;
	}

	int marginLeft = m_margin;
	int marginRight = width() - 1 - m_margin;
	int itemCount = m_items.count();
	int w = marginRight - marginLeft;

	m_verticalBarPositions.resize( m_items.count() - 1 );

	for ( int c = 0; c < m_verticalBarPositions.count(); c++ ) {
		m_verticalBarPositions[c] = marginLeft + qRound( ( ( qreal )w / itemCount ) * ( c + 1 ) );
	}
}
