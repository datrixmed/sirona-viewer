
import os
import sys
from win32com.client import Dispatch

def createShortcut(linkName, target='', wDir='', icon='', installationPath = ''):
	shell = Dispatch('WScript.Shell')
	folderDesktop = shell.SpecialFolders("Desktop")
	shortcut = shell.CreateShortCut(os.path.join(folderDesktop,linkName))
	shortcut.Targetpath = target
	shortcut.WorkingDirectory = wDir
	if icon == '':
		pass
	else:
		shortcut.IconLocation = icon
	shortcut.save()


exeName = 'SironaViewer.exe';

installationPath = 'C:/Program Files (x86)/Datrix Sirona Viewer';
if len(sys.argv) > 2:
	installationPath = sys.argv[2];

iconName = 'icon.ico';
if len(sys.argv) > 1:
	iconName = os.path.join(installationPath,sys.argv[1]);

createShortcut( 'Sirona Viewer.lnk', os.path.join(installationPath,exeName), installationPath, os.path.join(installationPath,'images',iconName) );

