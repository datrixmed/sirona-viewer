#ifndef CURL_CLIENT_H
#define CURL_CLIENT_H

#include <QObject>
#include "curl/include/curl/curl.h"
#include "customer.h"

struct ftp_url_info {
	QString scheme;
	QString username;
	QString password;
	QString host;
	QString port;
	tls_type security;
	QString directory;
};


class curl_client : public QObject
{
	Q_OBJECT

public:
	~curl_client();
	curl_client( QObject *parent, CustomerInfo *customer );
	int sendFile( QByteArray file_name, ftp_url_info url_info );
    const char *getErrorString();

private:
	static size_t read_callback( FILE *ptr, size_t size, size_t nmemb, FILE *stream );
	static int progress_callback( void *clientp, double dltotal, double dlnow, double ultotal, double ulnow );
	CustomerInfo *customer = nullptr;
	CURL *curl;
	static curl_client *current_curl;
	qint64 lastReportedPercentage = -1;
    CURLcode sendFileResult;

signals:
	void percentageChanged( qint64 );
	void reloadTimer();

};

#endif // CURL_CLIENT_H
