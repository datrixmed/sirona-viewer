/**
 * @file        dataformat.h
*/

#include <stdint.h>
#include <QString>

#ifndef DATAFORMAT_H
#define DATAFORMAT_H

/* legacy definitions from the smartdock.h file, still needed in the code */
#define BATTERY_FIRMWARE_VERSION 1815
#define CONFIG_SECTOR_START (0)
#define CONFIG_SECTOR_SIZE (1)
#define DEFAULT_CONFIG_SECTOR_START (CONFIG_SECTOR_START + CONFIG_SECTOR_SIZE)
#define DEFAULT_CONFIG_SECTOR_SIZE (1)
#define EVENT_HEADERS_SECTOR_START (DEFAULT_CONFIG_SECTOR_START + DEFAULT_CONFIG_SECTOR_SIZE)
#define EVENT_HEADERS_SECTOR_SIZE (2047)
#define EVENT_HEADERS_COUNT (EVENT_HEADERS_SECTOR_SIZE)
#define EVENT_DATA_SECTOR_START (EVENT_HEADERS_SECTOR_START + EVENT_HEADERS_SECTOR_SIZE)
#define FIRMWARE_UPDATE_SECTOR_START (EVENT_DATA_SECTOR_START)
#define FW_SECTOR_START (0x1C2000)


/* {{{ Structure definitions */
#pragma pack(push)
#pragma pack(1)
struct EventHeader {
    EventHeader(): event_type( 0 ), ECG_SD_byte_offset( 0 ), creation_time( 0 ), sent( 0 ), num_sample_prior_to_event( 0 ), ev_size( 0 ), no_channels( 0 ) {}
	quint32 event_type;
	quint32 ECG_SD_byte_offset;
	quint32 creation_time;
	quint8 sent;
	quint32 num_sample_prior_to_event;
    quint16 ev_size;
	quint8 no_channels;
};
#pragma pack(pop)

#pragma pack(push)
#pragma pack(1)
struct WriteToFileParams_t {
	WriteToFileParams_t(): eventCount( 0 ), format( 0 ) {}
	QString patientName;
	QString patientId;
	QString serialNumber;
	int eventCount;
	int format;
};
#pragma pack(pop)
/* }}} */

#endif // DATAFORMAT_H
