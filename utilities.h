#ifndef UTILITIES_H
#define UTILITIES_H

#include <QString>

#define MANUFACTURER_NAME			"Datrix"
#define APPLICATION_NAME			"Sirona Viewer"

#define LOGMSG(...) Utilities::logMsg(__VA_ARGS__)
#define SEC_TO_MSEC			(1000)

class Utilities
{
public:
    static QString createDirectories(bool directoriesRequired);
    static QString changeExtenstion(QString pathName, QString newExtension);
    static QString getLocalStoragePath(QString subFolder);
    static void moveProcedureToSavePath(QString datFileName, bool generateResFile);
    static void removeAllProcedureFiles(QString datFileName);

	// invoke with the LOGMSG macro
    static void logMsg(const char *format, ...);

private:
    static void backupSavePathFiles();
};

#endif //UTILITIES_H
