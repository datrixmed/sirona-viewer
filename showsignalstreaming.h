#ifndef SHOWSIGNALSTREAMING_H
#define SHOWSIGNALSTREAMING_H

#include "showsignal.h"
#include <QTime>

#define ECG_DISPLAY_WINDOW_SIZE_SECONDS		(8)


/** {{{ class ShowSignalStreaming : public ShowSignal
   @brief	Displays continuously streaming ECG signal on the screen
 */
class ShowSignalStreaming : public ShowSignal
{
	Q_OBJECT

public:
	ShowSignalStreaming( QWidget *parent = 0, EcgData *theEcgData = 0, bool liveEcg = true  );
	~ShowSignalStreaming();

	void start_scrolling();
	void stop_scrolling();

public slots:
	void mousePressEvent( QMouseEvent *event );

protected:
	virtual void timerEvent( QTimerEvent *event );

private:
	QTime last_update_time;
	int timerSmoothAdvance;

};
/* }}} */


#endif // SHOWSIGNALSTREAMING_H
