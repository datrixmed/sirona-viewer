#include <math.h>
#include "httppostdata.h"
#include "utilities.h"


HttpPostData::HttpPostData( QString serialNumber, const char *requestTypeVal, QString boundary, QString zipFileName )
{
	QString preQString = "--" + boundary +
						 "\r\nContent-Disposition: form-data; name=\"serial_number\"\r\n\r\n" +
						 serialNumber + "\r\n--" +
						 boundary +
						 "\r\nContent-Disposition: form-data; name=\"request_type\"\r\n\r\n" +
						 requestTypeVal + "\r\n--" +
						 boundary +
						 "\r\nContent-Disposition: form-data; name=\"zipfile\"; filename=\"" + zipFileName +
						 "\"\r\nContent-Type: application/zip\r\n\r\n";
	preambleSize = ( preQString.length() < ( int )sizeof( preamble ) ) ? preQString.length() : sizeof( preamble );
	strncpy( preamble, preQString.toLatin1().data(), preambleSize );

	QString postQString = "\r\n--" + boundary + "--\r\n";
	postambleSize = ( postQString.length() < ( int )sizeof( postamble ) ) ? postQString.length() : sizeof( postamble );
	strncpy( postamble, postQString.toLatin1().data(), postambleSize );

	zipFile = new QFile( zipFileName );
}

HttpPostData::~HttpPostData()
{
	delete zipFile;
}

bool HttpPostData::open( OpenMode mode )
{
	if ( mode & ( WriteOnly | Append | Truncate ) ) {
		return false;       // modification of data is not supported
	}

	if ( !zipFile->open( ReadOnly ) ) {
		LOGMSG( "ZIP file not found!" );
		return false;
	}

	totalSize = preambleSize + zipFile->size() + postambleSize;
	setOpenMode( mode );
	return true;
}

void HttpPostData::close()
{
	totalSize = 0;
	zipFile->close();
	setOpenMode( NotOpen );
}

bool HttpPostData::atEnd() const
{
	return pos() >= totalSize;
}

qint64 HttpPostData::bytesAvailable() const
{
	qint64 avail = ( pos() < totalSize ) ? totalSize - pos() : 0;
	return avail + QIODevice::bytesAvailable();
}

bool HttpPostData::isSequential() const
{
	return false;
}

qint64 HttpPostData::size() const
{
	return totalSize;
}

qint64 HttpPostData::readData( char *data, qint64 maxSize )
{
	qint64 position = pos();

	if ( position >= totalSize ) {
		return 0;
	}
	if ( maxSize > ( totalSize - position ) ) {
		maxSize = totalSize - position;
	}
	qint64 remainingSize = maxSize;

	if ( position < preambleSize ) {
		qint64 size = ( remainingSize < ( preambleSize - position ) ) ? remainingSize : preambleSize - position;
		strncpy( data, &preamble[position], size );
		data += size;
		position += size;
		remainingSize -= size;
	}
	position -= preambleSize;           // move position relative to the start of the zip file

	if ( ( remainingSize > 0 ) && ( position < zipFile->size() ) ) {
		zipFile->seek( position );
		qint64 size = ( remainingSize < zipFile->bytesAvailable() ) ? remainingSize : zipFile->bytesAvailable();
		memcpy( data, zipFile->read( size ).data(), size );
		data += size;
		position += size;
		remainingSize -= size;
	}
	position -= zipFile->size();        // move position relative to the start of the postamble

	if ( ( remainingSize > 0 ) && ( position < postambleSize ) ) {
		qint64 size = ( remainingSize < ( postambleSize - position ) ) ? remainingSize : postambleSize - position;
		strncpy( data, &postamble[position], size );
		data += size;
		position += size;
		remainingSize -= size;
	}

	if ( remainingSize > 0 ) {
		LOGMSG( "HttpPostData: unexpectedly ran out of data" );
		memset( data, 0, remainingSize );
	}

	qint64 currentPercentage = lroundf( ( float )( pos() + maxSize ) / ( float )totalSize * 100. );
	if ( currentPercentage != lastReportedPercentage ) {
		lastReportedPercentage = currentPercentage;
		emit percentageChanged( lastReportedPercentage );
	}

	emit reloadTimer();

	return maxSize;
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
qint64 HttpPostData::writeData( const char *data, qint64 len )
{
	// writing is not supported
	return 0;
}
#pragma GCC diagnostic pop
