/**
 * @file        qpdfpixmap.h
*/
#ifndef QPDFPIXMAP_H
#define QPDFPIXMAP_H

#include <QPixmap>
#include <QtGlobal>

class QPDFPixmap : public QPixmap
{
public:
	QPDFPixmap();
	explicit QPDFPixmap( QPlatformPixmap *data );
	QPDFPixmap( int w, int h );
	QPDFPixmap( const QSize & );
	QPDFPixmap( const QString &fileName, const char *format = 0, Qt::ImageConversionFlags flags = Qt::AutoColor );
#ifndef QT_NO_IMAGEFORMAT_XPM
	QPDFPixmap( const char *const xpm[] );
#endif
	QPDFPixmap( const QPixmap & );

	// class functions
	void setDPI( int dpi );
protected:
	virtual int metric( PaintDeviceMetric metric ) const;
private:
	int m_DPI;
};

#endif // QPDFPIXMAP_H
