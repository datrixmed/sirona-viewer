#include "../version.h"

#define MyAppName       "Sirona Viewer 3"
#define MyAppExeName    "SironaViewer.exe"
#define MyAppIniName    "Sirona Viewer.ini"
#define QTDIR           "C:\Qt\5.6\mingw49_32"

[Registry]
; set SIRONAVIEWER_INSTALL_PATH
Root: HKLM; Subkey: "SYSTEM\CurrentControlSet\Control\Session Manager\Environment"; ValueType:string; ValueName:"SIRONAVIEWER_INSTALL_PATH"; ValueData:{app}; Flags: preservestringtype

[Setup]
AppName={#MyAppName}
AppVerName={#MyAppName}
AppPublisher="Datrix"
AppPublisherURL=""
AppVersion="{#REVISION_REV}"
DefaultDirName={commonpf}\Datrix Sirona Viewer 3
DefaultGroupName=Datrix
UninstallDisplayIcon={app}\{#MyAppExeName}
Compression=lzma
;Compression=none
SolidCompression=yes
OutputBaseFilename=Setup-Sirona-Viewer3-{#REVISION_REV}
OutputDir=../../bin/installer
VersionInfoVersion="{#REVISION_REV}"
VersionInfoDescription={#MyAppName}(Qt 5.6.1)
; Tell Windows Explorer to reload the environment
ChangesEnvironment=yes

[Dirs]
Name: "{app}";
Name: "{app}\images";
Name: "{app}\imageformats";
Name: "{app}\iconengines";

[Icons]
Name: "{group}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; WorkingDir: "{app}"
Name: "{group}\{cm:UninstallProgram,{#MyAppName}}"; Filename: "{uninstallexe}"
Name: "{commondesktop}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; IconFilename: "{app}\images\sironaViewer.ico"; Check: checkDesktopIcon

[Code]
//util method, equivalent to C# string.StartsWith
function StartsWith(SubStr, S: String):Boolean;
begin
   Result:= Pos(SubStr, S) = 1;
end;

//util method, equivalent to C# string.Replace
function StringReplace(S, oldSubString, newSubString: String) : String;
var
  stringCopy : String;
begin
  stringCopy := S; //Prevent modification to the original string
  StringChange(stringCopy, oldSubString, newSubString);
  Result := stringCopy;
end;

//==================================================================
function GetCommandlineParam (inParamName: String):String;
var
   paramNameAndValue: String;
   i: Integer;
begin
   Result := '';

   for i:= 0 to ParamCount do
   begin
     paramNameAndValue := ParamStr(i);
     if (StartsWith(inParamName, paramNameAndValue)) then
     begin
       Result := StringReplace(paramNameAndValue, inParamName + '=', '');
       break;
     end;
   end;
end;

//==================================================================
function checkDesktopIcon(): Boolean;
var
  installDesktopIcon : String;
begin
  installDesktopIcon := GetCommandLineParam('desktopIcon');
  if installDesktopIcon = 'NO' then
    Result := FALSE
  else
  Result := TRUE
end;

[Files]
; MinGW runtime
Source: "{#QTDIR}\bin\libwinpthread-1.dll"; DestDir: "{app}";
Source: "{#QTDIR}\bin\libgcc_s_dw2-1.dll"; DestDir: "{app}";
Source: "{#QTDIR}\bin\libstdc++-6.dll"; DestDir: "{app}";
; ICU unicode/utf conversion tables
Source: "{#QTDIR}\bin\icudt54.dll"; DestDir: "{app}";
Source: "{#QTDIR}\bin\icuin54.dll"; DestDir: "{app}";
Source: "{#QTDIR}\bin\icuuc54.dll"; DestDir: "{app}";
; Qt runtime
Source: "{#QTDIR}\bin\Qt5Core.dll"; DestDir: "{app}";
Source: "{#QTDIR}\bin\Qt5Gui.dll"; DestDir: "{app}";
Source: "{#QTDIR}\bin\Qt5Widgets.dll"; DestDir: "{app}";
Source: "{#QTDIR}\bin\Qt5PrintSupport.dll"; DestDir: "{app}";
Source: "{#QTDIR}\bin\Qt5Svg.dll"; DestDir: "{app}";
Source: "{#QTDIR}\bin\Qt5Network.dll"; DestDir: "{app}";
; Qt platforms
Source: "{#QTDIR}\plugins\platforms\qwindows.dll"; DestDir: "{app}\platforms";
; Qt imageformats
Source: "{#QTDIR}\plugins\imageformats\qsvg.dll"; DestDir: "{app}\imageformats";
Source: "{#QTDIR}\plugins\imageformats\qgif.dll"; DestDir: "{app}\imageformats";
; Qt icon engines
Source: "{#QTDIR}\plugins\iconengines\qsvgicon.dll"; DestDir: "{app}\iconengines";
; Other libraries
Source: "..\..\bin\libcurl.dll"; DestDir: "{app}";
Source: "..\..\bin\libeay32.dll"; DestDir: "{app}";
Source: "..\..\bin\libidn-11.dll"; DestDir: "{app}";
Source: "..\..\bin\ssleay32.dll"; DestDir: "{app}";
Source: "..\..\bin\sironadriver.dll"; DestDir: "{app}";
Source: "..\zlib128-dll\zlib1.dll"; DestDir: "{app}";
Source: "..\zlib128-dll\zlib.dll"; DestDir: "{app}";
Source: "..\minizip\minizip.dll"; DestDir: "{app}";
; Sirona viewer app
Source: "..\..\bin\{#MyAppExeName}";  DestDir: "{app}"; Flags: ignoreversion;
Source: "..\out\stylesheet.css";  DestDir: "{app}"; Flags: ignoreversion;
; Translations
Source: "..\languages\*.qm";  DestDir: "{app}"; Flags: ignoreversion;
; python-3.9.2
Source: "../out/python-3.9.2/*"; Excludes: ".svn"; DestDir: "{app}/python-3.9.2"; Flags: ignoreversion recursesubdirs createallsubdirs; Permissions: everyone-full;
; Sirona viewer images
Source: "../out/images/*"; Excludes: ".svn"; DestDir: "{app}/images"; Flags: ignoreversion recursesubdirs createallsubdirs; Permissions: everyone-full;
; Sirona driver and driver installer
Source: "../../windrv/dpinst 32bit/DPInst.exe";  DestDir: "{tmp}"; Flags: ignoreversion replacesameversion;
Source: "../../windrv/dpinst 64bit/DPInst.exe";  DestDir: "{tmp}"; Flags: ignoreversion replacesameversion; Check: IsWin64
Source: "..\..\windrv\sironadrv.cat";  DestDir: "{tmp}"; Flags: ignoreversion replacesameversion;
Source: "..\..\windrv\sironadrv.inf";  DestDir: "{tmp}"; Flags: ignoreversion replacesameversion;

[Run]
Filename: "{tmp}\DPInst.exe"; Description: "Install Sirona driver for USB transfers"; Parameters: "/q /se" ; WorkingDir: {tmp};
Filename: "{app}\{#MyAppExeName}"; Description: "{cm:LaunchProgram,{#StringChange(MyAppName, "&", "&&")}}"; Flags: nowait postinstall runascurrentuser
