
#define MyManufacturer   "Datrix"
#define MyDevFolder  	 "../out"

#define MyAppName       "Sirona Viewer"
#define MyAppExeName    "SironaViewer.exe"
#define MyAppIniName    "SironaViewer.ini"

#define MyAppURL		"http://www.datrixmed.com"
#define MyVersion		"6d36138"

[Setup]
; NOTE: The value of AppId uniquely identifies this application.
; Do not use the same AppId value in installers for other applications.
; (To generate a new GUID, click Tools | Generate GUID inside the IDE.)
AppID={{dabdab5e-8d1c-4dab-ae4c-3dab75dabfe9}}
AppName={#MyAppName}
AppVerName={#MyAppName}
AppPublisher={#MyManufacturer}
AppPublisherURL={#MyAppURL}
AppVersion={#MyVersion}
DefaultDirName={commonpf}\{#MyManufacturer} {#MyAppName}
DefaultGroupName={#MyAppName}
UninstallDisplayIcon={app}\{#MyAppExeName}
Compression=lzma/Ultra
SolidCompression=yes
OutputBaseFilename=Setup-{#MyAppName}-{#MyVersion}
OutputDir=.
VersionInfoDescription={#MyAppName}(Qt 5.13.0)
AppCopyright=(C) Copyright 2021, {#MyManufacturer}


[Dirs]
Name: "{app}";
Name: "{app}\images";
Name: "{app}\imageformats";
Name: "{app}\iconengines";

[Icons]
Name: "{group}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; WorkingDir: "{app}"
Name: "{commonstartup}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"
Name: "{group}\Uninstall"; Filename: "{uninstallexe}"

[Code]
//util method, equivalent to C# string.StartsWith
function StartsWith(SubStr, S: String):Boolean;
begin
   Result:= Pos(SubStr, S) = 1;
end;

//util method, equivalent to C# string.Replace
function StringReplace(S, oldSubString, newSubString: String) : String;
var
  stringCopy : String;
begin
  stringCopy := S; //Prevent modification to the original string
  StringChange(stringCopy, oldSubString, newSubString);
  Result := stringCopy;
end;

//==================================================================
function GetCommandlineParam (inParamName: String):String;
var
   paramNameAndValue: String;
   i: Integer;
begin
   Result := '';

   for i:= 0 to ParamCount do
   begin
     paramNameAndValue := ParamStr(i);
     if (StartsWith(inParamName, paramNameAndValue)) then
     begin
       Result := StringReplace(paramNameAndValue, inParamName + '=', '');
       break;
     end;
   end;
end;

//==================================================================
function checkDesktopIcon(): Boolean;
var
  installDesktopIcon : String;
begin
  installDesktopIcon := GetCommandLineParam('desktopIcon');
  if installDesktopIcon = 'NO' then
    Result := FALSE
  else
  Result := TRUE
end;

[Files]
Source: "{#MyDevFolder}/{#MyAppExeName}"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "{#MyDevFolder}/stylesheet.css"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "{#MyDevFolder}/libcurl.dll"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "{#MyDevFolder}/minizip.dll"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "{#MyDevFolder}/libidn-11.dll"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "{#MyDevFolder}/libeay32.dll"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "{#MyDevFolder}/ssleay32.dll"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "{#MyDevFolder}/zlib.dll"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "{#MyDevFolder}/zlib1.dll"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs createallsubdirs

; Qt platforms
Source: "{#MyDevFolder}/platforms/qwindows.dll"; DestDir: "{app}/platforms";
; Qt imageformats
Source: "{#MyDevFolder}/imageformats/qsvg.dll"; DestDir: "{app}/imageformats";
Source: "{#MyDevFolder}/imageformats/qgif.dll"; DestDir: "{app}/imageformats";
; Qt icon engines
Source: "{#MyDevFolder}/iconengines/qsvgicon.dll"; DestDir: "{app}/iconengines";
; python-3.9.2
Source: "../out/python-3.9.2/*"; Excludes: ".svn"; DestDir: "{app}/python-3.9.2"; Flags: ignoreversion recursesubdirs createallsubdirs; Permissions: everyone-full;
; Sirona viewer images
Source: "../out/images/*"; Excludes: ".svn"; DestDir: "{app}/images"; Flags: ignoreversion recursesubdirs createallsubdirs; Permissions: everyone-full;

Source: "../windrv/dpinst 32bit/DPInst.exe";  DestDir: "{tmp}"; Flags: ignoreversion replacesameversion;
Source: "../windrv/dpinst 64bit/DPInst.exe";  DestDir: "{tmp}"; Flags: ignoreversion replacesameversion; Check: IsWin64
Source: "../windrv/sironadrv.cat";  DestDir: "{tmp}"; Flags: ignoreversion replacesameversion;
Source: "../windrv/sironadrv.inf";  DestDir: "{tmp}"; Flags: ignoreversion replacesameversion;


[Run]
Filename: "{tmp}\DPInst.exe"; Description: "Install Sirona driver for USB transfers"; Parameters: "/q /se" ; WorkingDir: {tmp};
Filename: "{app}\{#MyAppExeName}"; Description: "{cm:LaunchProgram,{#StringChange(MyAppName, "&", "&&")}}"; Flags: nowait postinstall skipifsilent


