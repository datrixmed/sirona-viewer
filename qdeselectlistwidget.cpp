#include "qdeselectlistwidget.h"
#include <QListWidgetItem>
#include <QMouseEvent>

QDeselectListWidget::QDeselectListWidget( QWidget *parent )
	: QListWidget( parent )
	, m_mousePress( false )
{

}

void QDeselectListWidget::mousePressEvent( QMouseEvent *event )
{
	QListWidgetItem *item = itemAt( event->x(), event->y() );

	m_mousePress = true;

	if ( ( item ) && ( item->isSelected() ) ) {
		clearSelection();
	} else {
		QListWidget::mousePressEvent( event );
	}
}

void QDeselectListWidget::mouseReleaseEvent( QMouseEvent *event )
{
	m_mousePress = false;

	QListWidget::mouseReleaseEvent( event );
}

void QDeselectListWidget::mouseMoveEvent( QMouseEvent *event )
{
	if ( !m_mousePress ) {
		QListWidget::mouseMoveEvent( event );
	}
}
