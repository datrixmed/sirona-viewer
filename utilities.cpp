#include <QDateTime>
#include <QDebug>
#include <QDir>
#include <QFileDialog>
#include <QMutex>
#include <QObject>
#include <QSettings>
#include <QString>
#include <time.h>
#include "utilities.h"


QString Utilities::createDirectories( bool directoriesRequired )
{
	QSettings settings( QSettings::IniFormat, QSettings::UserScope, MANUFACTURER_NAME, APPLICATION_NAME );
	// If the INI file doesn't have the settings, add them. (Yes, we could use default values, but this way users know what to edit in the INI.)
	if ( settings.value( "Path/SavePath" ).isNull() ) {
		settings.setValue( "Path/SavePath", "C:/SironaData/" );
	}
	QString savePath = settings.value( "Path/SavePath" ).toString();
	if ( settings.value( "Path/BackupPath" ).isNull() ) {
		settings.setValue( "Path/BackupPath", "C:/SironaData/Backup/" );
	}
	QString backupPath = settings.value( "Path/BackupPath" ).toString();
	if ( settings.value( "Options/BackupRetentionDays" ).isNull() ) {
		settings.setValue( "Options/BackupRetentionDays", 7 );
	}
	int backupRetentionDays = settings.value( "Options/BackupRetentionDays" ).toInt();

	if ( directoriesRequired || backupRetentionDays > 0 ) {
		if ( !QDir( QDir::cleanPath( savePath ) ).exists() ) {
			QDir pathname;
			if ( !pathname.mkpath( QDir::cleanPath( savePath ) ) ) {
				return QString( QObject::tr( "Error creating directory " ) + QDir::cleanPath( savePath ) + QObject::tr( ". Check path and permissions." ) );
			}
		}
		if ( !QDir( QDir::cleanPath( backupPath ) ).exists() ) {
			QDir pathname;
			if ( !pathname.mkpath( QDir::cleanPath( backupPath ) ) ) {
				return QString( QObject::tr( "Error creating directory " ) + QDir::cleanPath( backupPath ) + QObject::tr( ". Check path and permissions." ) );
			}
		}

		// remove any files from BackupPath that are more than BackupRetentionDays old
		QDir backupPathDir = QDir( backupPath );
		QFileInfoList list = backupPathDir.entryInfoList();
		for ( int index = 0; index < list.size(); index++ ) {
			QFileInfo fileInfo = list.at( index );
			if ( ( fileInfo.fileName() == "." ) || ( fileInfo.fileName() == ".." ) ) {
				continue;
			}
			if ( fileInfo.lastModified().daysTo( QDateTime::currentDateTime() ) > backupRetentionDays ) {
				LOGMSG( QString( "Removing obsolete file " + fileInfo.fileName() ).toLatin1().data() );
				backupPathDir.remove( fileInfo.fileName() );
			}
		}
	}
	return QString();   // no error string
}

QString Utilities::changeExtenstion( QString pathName, QString newExtension )
{
	// LOGMSG( "changeExtension" );
	pathName.replace( QRegExp( "\\.[a-zA-Z]*$" ), "." + newExtension );
	return QDir::cleanPath( pathName );
}

QString Utilities::getLocalStoragePath( QString subFolder )
{
	static QString folderAppData;

	if ( folderAppData.isEmpty() || !QDir( QDir::cleanPath( folderAppData + "/" + subFolder ) ).exists() ) {
		QDir pathname;

		// obtain (platform specific) application's data/settings directory
		QSettings settings( QSettings::IniFormat, QSettings::UserScope, MANUFACTURER_NAME, APPLICATION_NAME );
		settings.setValue( "Test/Existence", true );	// make sure that all the path directories exist by making sure *something* is in the settings file

		folderAppData = QDir::cleanPath( QFileInfo( settings.fileName() ).absolutePath() );

		pathname.mkpath( folderAppData );
		pathname.mkpath( QDir::cleanPath( folderAppData + "/" + "Holter" ) );
		pathname.mkpath( QDir::cleanPath( folderAppData + "/" + "Event" ) );
		pathname.mkpath( QDir::cleanPath( folderAppData + "/" + "Post" ) );
		pathname.mkpath( QDir::cleanPath( folderAppData + "/" + "MCT" ) );
	}

	return QDir::cleanPath( folderAppData + "/" + subFolder );
}

void Utilities::moveProcedureToSavePath( QString datFileName, bool generateResFile )
{
	QString destinationFile;

	// move out any old files in SavePath
	backupSavePathFiles();

	QSettings settings( QSettings::IniFormat, QSettings::UserScope, MANUFACTURER_NAME, APPLICATION_NAME );
	QString savePath = settings.value( "Path/SavePath" ).toString();
	QDir savePathDir = QDir( savePath );
	QString heaFileName = Utilities::changeExtenstion( datFileName, "hea" );
	QString atrFileName = Utilities::changeExtenstion( datFileName, "atr" );
	QString jsonFileName = Utilities::changeExtenstion( datFileName, "json" );
	QString resFileName = Utilities::changeExtenstion( datFileName, "res" );
	QString zipFileName = Utilities::changeExtenstion( datFileName, "zip" );
	LOGMSG( QString( "Moving new files " + Utilities::changeExtenstion( datFileName, "*" ) ).toLatin1().data() );

	// if moving the file fails, remove the file that's already there and try again.
	if ( generateResFile ) {
		destinationFile = savePath + "/" + QFileInfo( resFileName ).fileName();
		if ( !savePathDir.rename( resFileName, destinationFile ) ) {
			savePathDir.remove( destinationFile );
			savePathDir.rename( resFileName, destinationFile );
		}
		// we don't need the original files if we have the res
		QFile datFile( datFileName );
		if ( datFile.exists() ) {
			datFile.remove();
		}
		QFile heaFile( heaFileName );
		if ( heaFile.exists() ) {
			heaFile.remove();
		}
		QFile annotFile( atrFileName );
		if ( annotFile.exists() ) {
			annotFile.remove();
		}
		QFile jsonFile( jsonFileName );
		if ( jsonFile.exists() ) {
			jsonFile.remove();
		}
	} else {
		destinationFile = savePath + "/" + QFileInfo( datFileName ).fileName();
		if ( !savePathDir.rename( datFileName, destinationFile ) ) {
			savePathDir.remove( destinationFile );
			savePathDir.rename( datFileName, destinationFile );
		}
		destinationFile = savePath + "/" + QFileInfo( heaFileName ).fileName();
		if ( !savePathDir.rename( heaFileName, destinationFile ) ) {
			savePathDir.remove( destinationFile );
			savePathDir.rename( heaFileName, destinationFile );
		}
		destinationFile = savePath + "/" + QFileInfo( atrFileName ).fileName();
		if ( !savePathDir.rename( atrFileName, destinationFile ) ) {
			savePathDir.remove( destinationFile );
			savePathDir.rename( atrFileName, destinationFile );
		}
		destinationFile = savePath + "/" + QFileInfo( jsonFileName ).fileName();
		if ( !savePathDir.rename( jsonFileName, destinationFile ) ) {
			savePathDir.remove( destinationFile );
			savePathDir.rename( jsonFileName, destinationFile );
		}
	}
	destinationFile = savePath + "/" + QFileInfo( zipFileName ).fileName();
	if ( !savePathDir.rename( zipFileName, destinationFile ) ) {
		savePathDir.remove( destinationFile );
		savePathDir.rename( zipFileName, destinationFile );
	}
}

void Utilities::backupSavePathFiles()
{
	QSettings settings( QSettings::IniFormat, QSettings::UserScope, MANUFACTURER_NAME, APPLICATION_NAME );
	QString savePath = settings.value( "Path/SavePath" ).toString();
	QString backupPath = settings.value( "Path/BackupPath" ).toString();

	// move any old files from SavePath to BackupPath
	QDir savePathDir = QDir( savePath );
	QStringList files = savePathDir.entryList();
	for ( QList<QString>::iterator file = files.begin(); file != files.end(); ++file ) {
		if ( ( *file == "." ) || ( *file == ".." ) ||
				( QDir::cleanPath( savePath + "/" + *file ) == QDir::cleanPath( backupPath ) ) ) {
			// don't try to backup backupPath
			continue;
		}
		LOGMSG( QString( "Backing up old file " + *file ).toLatin1().data() );
		QString backupFile = backupPath + "/" + *file;
		// if moving the file fails, remove the file that's already there and try again.
		if ( !savePathDir.rename( *file, backupFile ) ) {
			savePathDir.remove( backupFile );
			savePathDir.rename( *file, backupFile );
		}
	}
}

void Utilities::removeAllProcedureFiles( QString datFileName )
{
	LOGMSG( "removeAllProcedureFiles" );
	QFile datFile( datFileName );
	QString heaFileName = changeExtenstion( datFileName, "hea" );
	QFile heaFile( heaFileName );
	QString annotFileName = changeExtenstion( datFileName, "atr" );
	QFile annotFile( annotFileName );
	QString jsonFileName = changeExtenstion( datFileName, "json" );
	QFile jsonFile( jsonFileName );
	QString resFileName = changeExtenstion( datFileName, "res" );
	QFile resFile( resFileName );
	QString zipFileName = changeExtenstion( datFileName, "zip" );
	QFile zipFile( zipFileName );

	QSettings settings( QSettings::IniFormat, QSettings::UserScope, MANUFACTURER_NAME, APPLICATION_NAME );
	if ( settings.value( "Options/BackupRetentionDays" ).toInt() > 0 ) {
		if ( zipFile.exists() ) {
			LOGMSG( QString( "Backing up zip file " + QFileInfo( zipFile ).fileName() ).toLatin1().data() );
			QString backupPath = settings.value( "Path/BackupPath" ).toString();
			QDir backupPathDir = QDir( backupPath );
			QString destinationFile = backupPath + "/" + QFileInfo( zipFile ).fileName();
			// if moving the file fails, remove the file that's already there and try again.
			if ( !backupPathDir.rename( zipFileName, destinationFile ) ) {
				backupPathDir.remove( destinationFile );
				backupPathDir.rename( zipFileName, destinationFile );
			}
			// don't need the individual files if we have the zip
			if ( datFile.exists() ) {
				datFile.remove();
			}
			if ( heaFile.exists() ) {
				heaFile.remove();
			}
			if ( annotFile.exists() ) {
				annotFile.remove();
			}
			if ( jsonFile.exists() ) {
				jsonFile.remove();
			}
			if ( resFile.exists() ) {
				resFile.remove();
			}
		} else {    // no zip
			LOGMSG( QString( "Backing up unzipped files " + QFileInfo( datFile ).fileName() ).toLatin1().data() );
			QString backupPath = settings.value( "Path/BackupPath" ).toString();
			QDir backupPathDir = QDir( backupPath );
			QString destinationFile = backupPath + "/" + QFileInfo( datFile ).fileName();
			// if moving the file fails, remove the file that's already there and try again.
			if ( datFile.exists() && !backupPathDir.rename( datFileName, destinationFile ) ) {
				backupPathDir.remove( destinationFile );
				backupPathDir.rename( datFileName, destinationFile );
			}
			destinationFile = backupPath + "/" + QFileInfo( heaFile ).fileName();
			// if moving the file fails, remove the file that's already there and try again.
			if ( heaFile.exists() && !backupPathDir.rename( heaFileName, destinationFile ) ) {
				backupPathDir.remove( destinationFile );
				backupPathDir.rename( heaFileName, destinationFile );
			}
			destinationFile = backupPath + "/" + QFileInfo( annotFile ).fileName();
			// if moving the file fails, remove the file that's already there and try again.
			if ( annotFile.exists() && !backupPathDir.rename( annotFileName, destinationFile ) ) {
				backupPathDir.remove( destinationFile );
				backupPathDir.rename( annotFileName, destinationFile );
			}
			destinationFile = backupPath + "/" + QFileInfo( jsonFile ).fileName();
			// if moving the file fails, remove the file that's already there and try again.
			if ( jsonFile.exists() && !backupPathDir.rename( jsonFileName, destinationFile ) ) {
				backupPathDir.remove( destinationFile );
				backupPathDir.rename( jsonFileName, destinationFile );
			}
			destinationFile = backupPath + "/" + QFileInfo( resFile ).fileName();
			// if moving the file fails, remove the file that's already there and try again.
			if ( resFile.exists() && !backupPathDir.rename( resFileName, destinationFile ) ) {
				backupPathDir.remove( destinationFile );
				backupPathDir.rename( resFileName, destinationFile );
			}
		}
	} else {
		// no backup retention
		LOGMSG( QString( "Deleting " + QFileInfo( datFile ).fileName() ).toLatin1().data() );
		if ( datFile.exists() ) {
			datFile.remove();
		}
		if ( heaFile.exists() ) {
			heaFile.remove();
		}
		if ( annotFile.exists() ) {
			annotFile.remove();
		}
		if ( jsonFile.exists() ) {
			jsonFile.remove();
		}
		if ( resFile.exists() ) {
			resFile.remove();
		}
		if ( zipFile.exists() ) {
			zipFile.remove();
		}
	}
}

// invoke with the LOGMSG macro
void Utilities::logMsg( const char *format, ... )
{
	static QMutex mutex;
	static FILE *logfile = NULL;
	va_list args;

	mutex.lock();
	if ( logfile == NULL ) {
		logfile = fopen( ( getLocalStoragePath( "" ) + "/viewerlog.txt" ).toLatin1().data(), "w" );
	}

	time_t curtime;
	struct tm *cur_tm;
	time( &curtime );
	cur_tm = localtime( &curtime );

#if !defined(QT_NO_DEBUG_OUTPUT)
	va_start( args, format );
	qDebug().noquote() << QString::asprintf( "%04d-%02d-%02d %02d:%02d:%02d ", cur_tm->tm_year + 1900, cur_tm->tm_mon + 1, cur_tm->tm_mday, cur_tm->tm_hour, cur_tm->tm_min, cur_tm->tm_sec ) + QString::vasprintf( format, args );
	va_end( args );
#endif

	if ( logfile != NULL ) {
		fprintf( logfile, "%04d-%02d-%02d %02d:%02d:%02d ", cur_tm->tm_year + 1900, cur_tm->tm_mon + 1, cur_tm->tm_mday, cur_tm->tm_hour, cur_tm->tm_min, cur_tm->tm_sec );

		va_start( args, format );
		vfprintf( logfile, format, args );
		va_end( args );

		fprintf( logfile, "\n" );
		fflush( logfile );
	}
	mutex.unlock();
}
