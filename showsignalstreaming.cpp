/**
 * @file showsignalstreaming.cpp
*/


#include "showsignalstreaming.h"
#include <QTimerEvent>
#include <QDebug>
#include "dbglog.h"


#define MAX_BUFFER_SIZE_SECONDS	(8)
#define SQUARED(x)	((x) * (x))


/** {{{ ShowSignalStreaming::ShowSignalStreaming()
	: ShowSignal()
*/
ShowSignalStreaming::ShowSignalStreaming( QWidget *parent, EcgData *theEcgData, bool liveEcg )
	: ShowSignal( parent, theEcgData, liveEcg )
	, timerSmoothAdvance( 0 )
{
	m_testspeed = false;

	SetPos( m_ecgdata->size() );	/* start scrolling from the end of data */
	start_scrolling();
}
/* }}} */


/** {{{ ShowSignalStreaming::~ShowSignalStreaming()
*/
ShowSignalStreaming::~ShowSignalStreaming()
{
	stop_scrolling();
}
/* }}} */


/** {{{ void ShowSignalStreaming::start_scrolling()
 */
void ShowSignalStreaming::start_scrolling()
{
	if ( !timerSmoothAdvance ) {
		timerSmoothAdvance = startTimer( 16 ); /* period chosen arbitrarily through trial and error for smoothest output on display */
	}
}
/* }}} */


/** {{{ void ShowSignalStreaming::stop_scrolling()
 */
void ShowSignalStreaming::stop_scrolling()
{
	if ( timerSmoothAdvance ) {
		killTimer( timerSmoothAdvance );
		timerSmoothAdvance = 0;
	}
}
/* }}} */



/** {{{ void ShowSignalStreaming::mousePressEvent(QMouseEvent *event)
*/
void ShowSignalStreaming::mousePressEvent( QMouseEvent *event )
{
	if ( event->buttons() & Qt::LeftButton ) {
		if ( timerSmoothAdvance != 0 ) {
			stop_scrolling();
		}
	}

	if ( event->buttons() & Qt::RightButton ) {
		if ( timerSmoothAdvance == 0 ) {
			start_scrolling();
		}
	}

	ShowSignal::mousePressEvent( event );
}
/* }}} */


/** {{{ void ShowSignalStreaming::timerEvent(QTimerEvent *event)
 */
void ShowSignalStreaming::timerEvent( QTimerEvent *event )
{
	if ( event->timerId() == timerSmoothAdvance ) {
		long old_getpos = GetPos();
		long last_ecgpos = qMax( 0L, ( long ) ( ( double ) m_ecgdata->size() - ( double ) m_ecgdata->samps_per_chan_per_sec * ECG_DISPLAY_WINDOW_SIZE_SECONDS ) );

		if ( last_ecgpos == 0 ) {
			SetPos( 0 );
			update();
			return;
		}

		int how_far_behind_rightnow = qMax( m_ecgdata->size() - ( GetPos() + m_ecgdata->samps_per_chan_per_sec * ECG_DISPLAY_WINDOW_SIZE_SECONDS ), ( ulong )0 );

		if ( last_update_time.elapsed() / 1000 > MAX_BUFFER_SIZE_SECONDS * 2 ) {
#ifdef QT_DEBUG
			// qDebug() << QString("initial elapsed time = %1").arg(last_update_time.elapsed() / 1000).toLatin1().constData();
#endif
			SetPos( last_ecgpos - m_ecgdata->samps_per_chan_per_sec * 1 );
			last_update_time.restart();
		} else {
			int catchup_quicker = qMax( 0, how_far_behind_rightnow / m_ecgdata->samps_per_chan_per_sec - 2 );
			long samples_to_advance = qRound( ( float ) m_ecgdata->samps_per_chan_per_sec * ( float ) last_update_time.elapsed() / 1000.0 ) + catchup_quicker;
#ifdef QT_DEBUG
			// QQQ("workecg.log") << QString("last_ecgpos = %1     samples_to_advance = %2 because elapsed time = %3").arg(last_ecgpos).arg(samples_to_advance).arg(last_update_time.elapsed()).toLatin1().constData();
#endif
			if ( samples_to_advance > 0 ) {
				SetPos( GetPos() + samples_to_advance );

				/* if we are positioned off the end of the strip, then set the position to near the end */
				if ( GetPos() > last_ecgpos ) {
					SetPos( last_ecgpos );
				}

				last_update_time.restart();
			}
		}

		if ( GetPos() != old_getpos ) {
			update();
		}
	} else {
		qDebug() << "Unknown timer id at ShowSignalStreaming::timerEvent";
	}
}
/* }}} */


