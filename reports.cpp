
#include <QPrinterInfo>
#include <QPainter>
#include <QtGui>
#include <QDebug>

#include <time.h>

#include "reports.h"
#include "printpreviewer.h"

#define HORIZONTAL_MARGIN_1MM	((qreal)pPainter->device()->logicalDpiX() / INCH_TO_MM)
#define VERTICAL_MARGIN_1MM		((qreal)pPainter->device()->logicalDpiX() / INCH_TO_MM)
#define TWO_MM_HMARGIN (((qreal)pPainter->device()->logicalDpiX() / INCH_TO_MM) * 2)
#define TWO_MM_VMARGIN (((qreal)pPainter->device()->logicalDpiY() / INCH_TO_MM) * 2)
#define DOT_PER_MM(dc,dots)					((dots) * dc->device()->logicalDpiY() / (INCH_TO_MM))
#define DPM(dc,dots)						(DOT_PER_MM(dc,dots))

/** {{{ Reports::Reports( EcgData *pEcgData, ConfigParams *configSector  )
 */
Reports::Reports( EcgData *pEcgData, ConfigParams *configSector  )
{
	thePrinter = NULL;
	pConfigSector = configSector;
	pageNum = 1;
	pageMax = pageNum;
	pdfFilename = QDir::cleanPath( QDir::tempPath() + "/" + REPORT_FILENAME + ".pdf" );
	theEcgData = pEcgData;
}
/* }}} */


/** {{{ Reports::~Reports()
*/
Reports::~Reports()
{
}

/* }}} */


/** {{{ void Reports::newPage()
 */
void Reports::newPage()
{
	// qDebug() << "Reports::newPage()";

	if ( thePrinter ) {
		thePrinter->newPage();
	}
}
/* }}} */


/** {{{ void Reports::doPaint( QPrinter *thePrinter )
 */
void Reports::doPaint( QPrinter *thePrinter )
{
	// qDebug() << "Reports::doPaint()";

	QPainter p( thePrinter );
	pageEcg( &p );

}
/* }}} */




/** {{{ void Reports::printHtml( QPainter *pPainter, QString filename )
 */
void Reports::printHtml( QPainter *pPainter, QString filename )
{
	// qDebug() << "printHtml()";

	QTextDocument document;

	QFile filedata( filename );
	if ( ! filedata.open( QFile::ReadOnly ) ) {
		return;
	}

	QTextStream out( &filedata );

	QString textBody = out.readAll();

	/* reportmetadata.html variables */
	textBody.replace( "«veterinarianName»", theMetaData["veterinarianName"] );
	textBody.replace( "«institutionName»", theMetaData["institutionName"] );
	textBody.replace( "«institutionAddress»", theMetaData["institutionAddress"] );
	textBody.replace( "«institutionAddress_2»", theMetaData["institutionAddress_2"] );
	textBody.replace( "«institutionAddress_3»", theMetaData["institutionAddress_3"] );
	textBody.replace( "«institutionPhone»", theMetaData["institutionPhone"] );
	textBody.replace( "«examDate»", theMetaData["examDate"] );
	textBody.replace( "«patientName»", theMetaData["patientName"] );
	textBody.replace( "«ownerName»", theMetaData["ownerName"] );
	textBody.replace( "«ownerLastName»", theMetaData["ownerLastName"] );
	textBody.replace( "«patientID»", theMetaData["patientID"] );
	textBody.replace( "«breed»", theMetaData["breed"] );
	textBody.replace( "«species»", theMetaData["species"] );
	textBody.replace( "«gender»", theMetaData["gender"] );
	textBody.replace( "«ageYears»", theMetaData["ageYears"] );
	textBody.replace( "«ageMonths»", theMetaData["ageMonths"] );
	textBody.replace( "«weight»", theMetaData["weight"] );
	textBody.replace( "«urgency»", theMetaData["urgency"] );
	textBody.replace( "«heartMurmur»", theMetaData["heartMurmur"] );
	textBody.replace( "«examOrdered»", theMetaData["examOrdered"] );
	textBody.replace( "«XRay»", theMetaData["XRay"] );
	textBody.replace( "«medicationName_1»", theMetaData["medicationName_1"] );
	textBody.replace( "«medicationDosage_1»", theMetaData["medicationDosage_1"] );
	textBody.replace( "«medicationName_2»", theMetaData["medicationName_2"] );
	textBody.replace( "«medicationDosage_2»", theMetaData["medicationDosage_2"] );
	textBody.replace( "«medicationName_3»", theMetaData["medicationName_3"] );
	textBody.replace( "«medicationDosage_3»", theMetaData["medicationDosage_3"] );
	textBody.replace( "«medicationName_4»", theMetaData["medicationName_4"] );
	textBody.replace( "«medicationDosage_4»", theMetaData["medicationDosage_4"] );
	textBody.replace( "«medicationName_5»", theMetaData["medicationName_5"] );
	textBody.replace( "«medicationDosage_5»", theMetaData["medicationDosage_5"] );

	textBody.replace( "«Anesthesia»", theMetaData["Anesthesia"] );
	textBody.replace( "«AnesthmedicationName_1»", theMetaData["AnestmedicationName_1"] );
	textBody.replace( "«AnesthmedicationDosage_1»", theMetaData["AnestmedicationDosage_1"] );
	textBody.replace( "«AnesthmedicationName_2»", theMetaData["AnestmedicationName_2"] );
	textBody.replace( "«AnesthmedicationDosage_2»", theMetaData["AnestmedicationDosage_2"] );
	textBody.replace( "«AnesthmedicationName_3»", theMetaData["AnestmedicationName_3"] );
	textBody.replace( "«AnesthmedicationDosage_3»", theMetaData["AnestmedicationDosage_3"] );
	textBody.replace( "«AnesthmedicationName_4»", theMetaData["AnestmedicationName_4"] );
	textBody.replace( "«AnesthmedicationDosage_4»", theMetaData["AnestmedicationDosage_4"] );
	textBody.replace( "«AnesthmedicationName_5»", theMetaData["AnestmedicationName_5"] );
	textBody.replace( "«AnesthmedicationDosage_5»", theMetaData["AnestmedicationDosage_5"] );

	textBody.replace( "«history»", theMetaData["history"] );

	/* reportecg.html variables */
	textBody.replace( "«examTime»", theMetaData["examTime"] );
	textBody.replace( "«reportNumber»", theMetaData["reportNumber"] );
	textBody.replace( "«length»", theMetaData["length"] );
	textBody.replace( "«deviceSN»", theMetaData["deviceSN"] );

	document.setHtml( textBody );

	qreal w = pPainter->device()->width();
	qreal h = pPainter->device()->height();

	//Set the TextDocument's page size
	document.documentLayout()->setPaintDevice( pPainter->device() );
	document.setPageSize( QSizeF( w, h ) );

	document.drawContents( pPainter, QRectF( 0, 0, w, h ) );

	return;
}

/* }}} */


#define STRIPHEIGHT_MM	                    (40)
#define FULLSTRIPWIDTH_MM	                (250)
#define FULLSTRIPHEIGHT_MM	                (160)
#define ECGGRID_MARGIN_LEFT					(50)
#define ECGGRID_STRIP_HEIGHT_AND_MARGIN		((STRIPHEIGHT_MM + 1) * (pPainter->device()->logicalDpiY() / 25.4))
#define ECGGRID_MARGIN_TOP					(35 * (pPainter->device()->logicalDpiY() / 25.4))
#define PRINTED_LABEL_TOP					(34 * (pPainter->device()->logicalDpiY() / 25.4))
#define PRINTED_LABEL_LEFT					(210 * (pPainter->device()->logicalDpiY() / 25.4))
#define MYCOLOR_DATA	                    (QColor(Qt::black))
#define ECG_DISPLAY_WINDOW_SIZE_SECONDS		(10)
#define min(a,b)	                        ( (a) < (b) ? (a) : (b) )

/** {{{ void Reports::pageEcg( QPainter *pPainter )
 *
 * @brief
 *
 */
void Reports::pageEcg( QPainter *pPainter )
{
	qDebug() << "Reports::pageEcg()   theEcgData =" << ( long ) theEcgData;

	pageNum = pageMax = 1;

	if ( theEcgData == NULL ) {
		return;
	}

	/* if there is NO ecg to print, then just exit after printing the metadata page */
	if ( theEcgData->datalen_secs <= 0 ) {
		return;
	}

	for ( int i = 0 ; i < theEcgData->channel_count ; i++ ) {
		if ( theEcgData->sizeChData[i] < ( theEcgData->datalen_secs * theEcgData->samps_per_chan_per_sec ) ) {
			qDebug() << QString( "Reports::pageEcg()   NOT printing ecg on PDF report because sizeChData[%1] = %2" ).arg( i ).arg( ( long ) theEcgData->sizeChData[i] ).toLatin1().constData();
			return;
		}
	}

	pageMax = CEILING( ( int )TOTAL_GRAPHS_TO_PRINT, ( int )MAX_GRAPHS_PER_PAGE );

	pPainter->save();

	int graphCnt = 0;
	int graphsOnPage = 0;

	pageHeader( pPainter, tr( "Sirona Ecg Report" ) );
	printHtml( pPainter, "reportecg.html" ); // TODO: Localize reportecg.html

	pPainter->drawText( PRINTED_LABEL_LEFT, PRINTED_LABEL_TOP, tr( "ECG Collected by PC" ) );

	pPainter->translate( ECGGRID_MARGIN_LEFT, ECGGRID_MARGIN_TOP + graphsOnPage * ECGGRID_STRIP_HEIGHT_AND_MARGIN );
	ShowGrid( pPainter, ECG_DISPLAY_WINDOW_SIZE_SECONDS * 5, FULLSTRIPHEIGHT_MM, ECG_DISPLAY_WINDOW_SIZE_SECONDS );
	pPainter->restore();

	for ( int secondsOfEcg = 0 ; secondsOfEcg < theEcgData->datalen_secs ; graphCnt++, graphsOnPage++ ) {

		QString leadStr, mmPerSec;

		/* sanity check; make sure we have less than 10 minutes of ecg data to display */
		if ( graphCnt >= 10 * 60 / ECG_DISPLAY_WINDOW_SIZE_SECONDS * theEcgData->channel_count ) {
			break;
		}

		qDebug() << QString( "ypos = %1  pageHgt = %2   secondsOfEcg = %3" )
				 .arg( ECGGRID_MARGIN_TOP + ( graphsOnPage + 1 ) * ECGGRID_STRIP_HEIGHT_AND_MARGIN )
				 .arg( pPainter->device()->height() )
				 .arg( secondsOfEcg );

		if ( ( ECGGRID_MARGIN_TOP + ( graphsOnPage + 1 ) * ECGGRID_STRIP_HEIGHT_AND_MARGIN ) > pPainter->device()->height() ) {
			pPainter->restore();
			pageHeader( pPainter, tr( "Sirona Ecg Report" ) );
			printHtml( pPainter, "reportecg.html" ); // TODO: Localize reportecg.html
			pPainter->save();
			graphsOnPage = 0;
			pPainter->drawText( PRINTED_LABEL_LEFT, PRINTED_LABEL_TOP, tr( "ECG Collected by PC" ) );
			pPainter->translate( ECGGRID_MARGIN_LEFT, ECGGRID_MARGIN_TOP + graphsOnPage * ECGGRID_STRIP_HEIGHT_AND_MARGIN );
			ShowGrid( pPainter, ECG_DISPLAY_WINDOW_SIZE_SECONDS * 5, FULLSTRIPHEIGHT_MM, ECG_DISPLAY_WINDOW_SIZE_SECONDS );
			pPainter->restore();
		}

		pPainter->translate( ECGGRID_MARGIN_LEFT, ECGGRID_MARGIN_TOP + graphsOnPage * ECGGRID_STRIP_HEIGHT_AND_MARGIN );

		switch ( ( graphCnt % theEcgData->channel_count ) + 1 ) {
			case 1:
				leadStr = "I";
				break;
			case 2:
				leadStr = "II";
				break;
			case 3:
				leadStr = "III";
				break;
			default:
				leadStr = tr( "unknown" );
				break;
		}

		mmPerSec = tr( "25 mm/sec" );


		pPainter->drawText( -ECGGRID_MARGIN_LEFT / 2, ECGGRID_STRIP_HEIGHT_AND_MARGIN / 2, leadStr );
		pPainter->drawText( ECGGRID_MARGIN_LEFT * 16, ECGGRID_STRIP_HEIGHT_AND_MARGIN - 20, mmPerSec );

		if ( theEcgData ) {
			pPainter->setClipRect( QRectF( 0, -graphsOnPage * ECGGRID_STRIP_HEIGHT_AND_MARGIN, FULLSTRIPWIDTH_MM * ( pPainter->device()->logicalDpiX() / 25.4 ), FULLSTRIPHEIGHT_MM * ( pPainter->device()->logicalDpiY() / 25.4 ) ) );
			ShowData( pPainter, secondsOfEcg * theEcgData->samps_per_chan_per_sec, ( graphCnt % theEcgData->channel_count ), graphCnt );
			pPainter->setClipping( false );
		}

		pPainter->translate( -ECGGRID_MARGIN_LEFT, -( ECGGRID_MARGIN_TOP + graphsOnPage * ECGGRID_STRIP_HEIGHT_AND_MARGIN ) );

		/* if this is the last channel of this section of Ecg, then go to the next section */
		if ( ( graphCnt % theEcgData->channel_count ) == ( theEcgData->channel_count - 1 ) ) {
			secondsOfEcg += ECG_DISPLAY_WINDOW_SIZE_SECONDS;
		}
	}

	pPainter->restore();

}
/* }}} */


/** {{{ void Reports::ShowGrid( QPainter * pPainter, int gridbox_cols, int height_mm, int ecgSeconds, double xScale, double yScale )
  @brief Show an ECG gridlike thing
  */
void Reports::ShowGrid( QPainter *pPainter, int gridbox_cols, int height_mm, int ecgSeconds, double xScale, double yScale )
{
	double boxX_200ms = ( 0.2 * ( ( double )pPainter->device()->logicalDpiX() * 2.5 / 2.54 ) );
	double boxX_40ms = ( 0.04 * ( ( double )pPainter->device()->logicalDpiX() * 2.5 / 2.54 ) );
	double boxY_1mm =  ( ( double )pPainter->device()->logicalDpiY() / 25.4 );
	double boxY_height_mm = ( height_mm * ( ( double )pPainter->device()->logicalDpiY() / 25.4 ) );

	QPen pen_solid( QColor( "#c8c8c8" ) );
	QPen pen_dotted( QColor( "#e0e0e0" ) );

	pen_solid = QPen( QColor( "#ffa0a0" ), DPM( pPainter, 0.6 ) );
	pen_dotted = QPen( QColor( "#ffd0d0" ), DPM( pPainter, 0.4 ) );

	/* draw all the horizontal lines */
	int c = 0;
	for ( double j = 0 ; j <= boxY_height_mm ; j += boxY_1mm ) {
		int y = c % 5;
		if ( y > 0 ) {
			pPainter->setPen( QPen( QColor( "#ffd0d0" ), DPM( pPainter, 0.2 ) ) );
			pPainter->drawLine( QPointF( 0.0, yScale * j ), QPointF( xScale * boxX_200ms * 5 * ecgSeconds, yScale * j ) );
		} else {
			pPainter->setPen( pen_dotted );
			pPainter->drawLine( QPointF( 0.0, yScale * j ), QPointF( xScale * boxX_200ms * 5 * ecgSeconds, yScale * j ) );
		}

		c++;
	}

	pPainter->setPen( pen_dotted );

	/* draw all the vertical lines */
	for ( int i = 0 ; i <= gridbox_cols ; i++ ) {
		if ( ( i % 5 ) == 0 ) {
			pPainter->setPen( pen_solid );
		}
		pPainter->drawLine(
			QPointF( ( xScale * ( double )i * boxX_200ms ), 0.0 ),
			QPointF( ( xScale * ( double )i * boxX_200ms ), ( yScale * boxY_height_mm ) )
		);
		if ( ( i % 5 ) == 0 ) {
			pPainter->setPen( pen_dotted );
		}

		if ( i > 0 ) {
			pPainter->setPen( QPen( QColor( "#ffd0d0" ), DPM( pPainter, 0.2 ) ) );

			for ( int y = 1; y <= 4; y++ ) {
				pPainter->drawLine(
					QPointF( ( xScale * ( double )i * boxX_200ms ) - ( xScale * ( double )y * boxX_40ms ), 0.0 ),
					QPointF( ( xScale * ( double )i * boxX_200ms ) - ( xScale * ( double )y * boxX_40ms ), ( yScale * boxY_height_mm ) )
				);
			}

			pPainter->setPen( pen_dotted );
		}
	}

#ifdef DISPLAY_EXTRA_GRID_DOTS
	pPainter->setPen( QPen( QColor( "#ffa0a0" ), DPM( pPainter, 0.2 ) ) );
	for ( double j = 0 ; j < gridbox_rows ; j += 1 ) {
		for ( int i = 0 ; i < gridbox_cols ; i += 1 ) {
			for ( int x = 1 ; x < 5 ; x++ ) {
				for ( int y = 1 ; y < 5 ; y++ ) {
					pPainter->drawPoint(
						( xScale * ( ( double ) ( i * 5 + x ) / gridbox_cols * grid_width ) ),
						( yScale * ( ( double ) ( j * 5 + y ) / gridbox_rows * grid_height ) )
					);
				}
			}
		}
	}
#endif

	/* draw a box around the whole grid */
	pPainter->setPen( QPen( QColor( "#ffa0a0" ), DPM( pPainter, 0.7 ) ) );
	pPainter->drawRect( QRectF( 0.0, 0.0, ( xScale * boxX_200ms * 5 * ecgSeconds ), ( yScale * boxY_height_mm ) ) );
}
/* }}} */



/** {{{ void Reports::ShowData( QPainter * pPainter, long samplePos, int chanNum, int graphCnt )
 *
 * @brief Show the ECG data
 *
*/
void Reports::ShowData( QPainter *pPainter, long samplePos, int chanNum, int graphCnt )
{
	Q_UNUSED( graphCnt );

	int i;
	qreal gain_mm_per_mV = 10;
	double range_per_sample = theEcgData->range_per_sample;
	long *chdata;
	long samples_across_grid;
	long sample_count;

	// qDebug() << "ShowSignal::ShowData()  samplePos = " << samplePos << "    sample_count = " << sample_count << "  datalen_secs = " << theEcgData->datalen_secs;

	samples_across_grid = theEcgData->samps_per_chan_per_sec * ECG_DISPLAY_WINDOW_SIZE_SECONDS;
	sample_count = theEcgData->samps_per_chan_per_sec * min( ECG_DISPLAY_WINDOW_SIZE_SECONDS, theEcgData->datalen_secs );

	if ( sample_count <= 0 ) {
		return;
	}

	chdata = theEcgData->get( chanNum, samplePos, ECG_DISPLAY_WINDOW_SIZE_SECONDS * theEcgData->samps_per_chan_per_sec );

	long posEndOfRequestedData = ( samplePos + ECG_DISPLAY_WINDOW_SIZE_SECONDS * theEcgData->samps_per_chan_per_sec );

	if ( posEndOfRequestedData > theEcgData->samples_len ) {
		sample_count -= ( posEndOfRequestedData - theEcgData->samples_len );
		// qDebug() << "Cutting" << (posEndOfRequestedData - theEcgData->samples_len) << "samples off of sample_count";
	}

	QPen savedPen = pPainter->pen();

	double size5mm = ( 5.0 * ( pPainter->device()->logicalDpiY() / 25.4 ) );

	double device_dots_per_sec = size5mm * 5;
	double device_dots_per_mm = size5mm / 5.0;

	QPointF *pts = new QPointF[sample_count];

	qreal mV_per_digital_sample = ( qreal ) theEcgData->device_range_mV / ( qreal ) range_per_sample;

	QPen pen_solid( MYCOLOR_DATA, DPM( pPainter, 0.4 ), Qt::SolidLine );
	pPainter->setPen( pen_solid );

	/** For each channel... */
	qreal baseline_offset = STRIPHEIGHT_MM * device_dots_per_mm * 1 / 2;

	int ptsCount = 0;

	for ( i = 0 ; i < sample_count ; i++ ) {
		pts[ptsCount].setX( ( qreal ) i * ( device_dots_per_sec * ECG_DISPLAY_WINDOW_SIZE_SECONDS ) / samples_across_grid );
		pts[ptsCount].setY( ( qreal ) ( ( range_per_sample / 2 - chdata[i] ) * ( device_dots_per_mm * gain_mm_per_mV * mV_per_digital_sample ) + baseline_offset ) );
		ptsCount++;
	}

	pPainter->drawPolyline( pts, ptsCount );

	delete[] pts;

	pPainter->setPen( savedPen );
}
/* }}} */


/** {{{ void Reports::pageHeader( QPainter *pPainter, QPrinter *thePrinter,  QString title )
 *
 * @brief	Prints a nice header on the top of each page and a page number on the bottom of all pages after the first one.
 *
 * Note: a new page is ejected before starting subsequent page.
 *
 */
void Reports::pageHeader( QPainter *pPainter, QString title )
{
	QRect textrect;
	int y = 0;

	qDebug() << "Reports::pageHeader()";

	if ( pageNum > 1 ) {
		newPage();
	}

	int pageWidth = pPainter->device()->width();
	int pageHeight = pPainter->device()->height();

	QPen penLast = pPainter->pen();
	QFont fontLast = pPainter->font();

	QFont fnt( "Helvetica", HORIZONTAL_MARGIN_1MM * 6 );
	pPainter->setFont( fnt );
	pPainter->setPen( QPen( Qt::black ) );

	QSettings settings( QSettings::IniFormat, QSettings::UserScope, MANUFACTURER_NAME, APPLICATION_NAME );

	QImage logo(customer->logo);

	logo = logo.scaled( 250, 100, Qt::KeepAspectRatio );

	pPainter->drawImage( 0, 0, logo );

	if ( pConfigSector ) {
		QFont savedFont = pPainter->font();
		QFont fntHeaderText( "Helvetica", HORIZONTAL_MARGIN_1MM * 2 );
		pPainter->setFont( fntHeaderText );

#define ENDIAN_SWAP( x )  (( (x) >> 24 ) | ( ( (x) << 8 ) & 0x00FF0000 ) | ( ( (x) >> 8 ) & 0x0000FF00 ) | ( (x) << 24 ))

#define REPORTHEADER_ROW( argText1, argText2 ) \
		textrect = pPainter->boundingRect( 0, y, pageWidth/2 - HORIZONTAL_MARGIN_1MM * 2, REPORTHEADER_HEIGHT( pPainter ), Qt::AlignTop | Qt::AlignRight, argText1 ); \
		pPainter->drawText( textrect, Qt::AlignTop | Qt::AlignRight, argText1 ); \
		textrect = pPainter->boundingRect( pageWidth/2, y, pageWidth, REPORTHEADER_HEIGHT( pPainter ), Qt::AlignTop | Qt::AlignLeft, argText2 ); \
		pPainter->drawText( textrect, Qt::AlignTop | Qt::AlignLeft, argText2 ); \
		y += HORIZONTAL_MARGIN_1MM * (3 + 1);

		REPORTHEADER_ROW( tr( "Id:" ), pConfigSector->patient_id );

		if ( pConfigSector->patient_middle_initial.length() < 1 ) {
			pConfigSector->patient_middle_initial = " ";
		}

		if ( pConfigSector->patient_middle_initial[0] == '\0' ) {
			pConfigSector->patient_middle_initial[0] = ' ';
		}

		REPORTHEADER_ROW( tr( "Name:" ), QString( "%1 %2 %3" ).arg( pConfigSector->patient_first_name ).arg( pConfigSector->patient_middle_initial[0] ).arg( pConfigSector->patient_last_name ) );
		REPORTHEADER_ROW( tr( "Physician:" ), pConfigSector->physician_name );
		REPORTHEADER_ROW( tr( "S/N:" ), QString( "%1%2" ).arg( ENDIAN_SWAP( pConfigSector->device_id ), 8, 16, QLatin1Char( '0' ) ).arg( pConfigSector->class_id, 2, 16, QLatin1Char( '0' ) ) );

		pPainter->setFont( savedFont );
	}

	y = logo.height() + VERTICAL_MARGIN_1MM * 1;

	pPainter->setPen( QPen( Qt::gray ) );
	pPainter->drawLine( 0, y, pageWidth, y );

	textrect = pPainter->boundingRect( 0, y - REPORTHEADER_HEIGHT( pPainter ), pageWidth - HORIZONTAL_MARGIN_1MM * 5, REPORTHEADER_HEIGHT( pPainter ), Qt::AlignBottom | Qt::AlignRight, title );
	pPainter->drawText( textrect, Qt::AlignBottom | Qt::AlignRight, title );


	/* {{{ footer */
	pPainter->setPen( QPen( Qt::black ) );
	pPainter->setFont( QFont( "Helvetica" ) );
	QString pageNumStr = tr( "Page %1 of %2" ).arg( pageNum ).arg( pageMax );
	textrect =
		pPainter->boundingRect( TWO_MM_HMARGIN, TWO_MM_VMARGIN, pageWidth - ( 1 * TWO_MM_HMARGIN ), pageHeight - ( 1 * TWO_MM_VMARGIN ),
								Qt::AlignBottom | Qt::AlignLeft, pageNumStr );
	pPainter->drawText( textrect, Qt::AlignBottom | Qt::AlignLeft, pageNumStr );


	QString printDateStr = tr( "Printed on: " ) + QDateTime::currentDateTime().toString( Qt::DefaultLocaleLongDate );
	pPainter->setFont( QFont( "Helvetica", 5 ) );
	textrect =
		pPainter->boundingRect( TWO_MM_HMARGIN, TWO_MM_VMARGIN, pageWidth - ( 4 * HORIZONTAL_MARGIN_1MM ), pageHeight - ( 1 * TWO_MM_VMARGIN ),
								Qt::AlignBottom | Qt::AlignRight, printDateStr );
	pPainter->drawText( textrect, Qt::AlignBottom | Qt::AlignRight, printDateStr );
	/* }}} */

	pPainter->setPen( penLast );
	pPainter->setFont( fontLast );

	/* increment the page count */
	pageNum += 1;
}
/* }}} */



/** {{{ QString getLocalStoragePath( QString subFolder )
*/
QString getLocalStoragePath( QString subFolder )
{
	static QString folderAppData;

	if ( folderAppData.isEmpty() ) {
		QDir pathname;

		// obtain (platform specific) application's data/settings directory
		QSettings settings( QSettings::IniFormat, QSettings::UserScope, MANUFACTURER_NAME, APPLICATION_NAME );
		settings.setValue( "Test/Existence", true );	// make sure that all the path directories exist by making sure *something* is in the settings file

		folderAppData = QDir::cleanPath( QFileInfo( settings.fileName() ).absolutePath() + "/" + "Reports" );

		pathname.mkpath( folderAppData );
		pathname.mkpath( QDir::cleanPath( folderAppData + "/" + "Holter" ) );
		pathname.mkpath( QDir::cleanPath( folderAppData + "/" + "Event" ) );
	}

	return QDir::cleanPath( folderAppData + "/" + subFolder );
}
/* }}} */


/** {{{ void Reports::save( QString fileName )
 */
void Reports::save( QString fileName )
{
	qDebug() << QString( "Reports::save(%1)" ).arg( fileName ).toLatin1().constData();

	QFile filedata( fileName );
	if ( ! filedata.open( QFile::WriteOnly ) ) {
		return;
	}

	QDataStream out( &filedata );

	out << *theEcgData;

}
/* }}} */


/** {{{ void Reports::restore( QString fileName )
 */
void Reports::restore( QString fileName )
{
	qDebug() << QString( "Reports::restore(%1)" ).arg( fileName ).toLatin1().constData();

	QFile filedata( fileName );
	if ( ! filedata.open( QFile::ReadOnly ) ) {
		return;
	}

	QDataStream in( &filedata );

	in >> *theEcgData;
}
/* }}} */



/** {{{ void Reports::generateXmlTo( QString xmlPathName )
 */
void Reports::generateXmlTo( QString xmlPathName )
{
	QTextDocument document;

	QFile filedata( xmlPathName );
	if ( ! filedata.open( QFile::WriteOnly ) ) {
		return;
	}

	QTextStream out( &filedata );
	QMapIterator<QString, QString> i( getMetaData() );

	out << QString( "<?xml version=\"1.0\"?>\n" );
	out << QString( "<Report>\n" );

	while ( i.hasNext() ) {
		i.next();
		QString theKey = i.key();
		theKey.replace( " ", "_" );
		out << QString( "  <%1>%2</%1>\n" ).arg( theKey ).arg( EncodeXML( i.value() ) );
	}

	out << QString( "</Report>\n" );
}
/* }}} */


/** {{{ QString EncodeXML( const QString& encodeMe )
 */
QString EncodeXML( const QString &encodeMe )
{
	QString retval;

	for ( int index = 0 ; index < encodeMe.size() ; index++ ) {
		QChar character( encodeMe.at( index ) );

		switch ( character.unicode() ) {
			case '&':
				retval += "&amp;";
				break;
			case '\'':
				retval += "&apos;";
				break;
			case '"':
				retval += "&quot;";
				break;
			case '<':
				retval += "&lt;";
				break;
			case '>':
				retval += "&gt;";
				break;
			default:
				retval += character;
				break;
		}
	}

	return retval;
}
/* }}} */


/** {{{ QString DecodeXML( const QString& decodeMe )
 */
QString DecodeXML( const QString &decodeMe )
{
	QString retval( decodeMe );

	retval.replace( "&apos;", "'" );
	retval.replace( "&quot;", "\"" );
	retval.replace( "&lt;", "<" );
	retval.replace( "&gt;", ">" );
	retval.replace( "&amp;", "&" );

	return retval;
}
/* }}} */



