#include "customer.h"

customer_defs::customer_defs( QObject *parent )
{
	this->setParent( parent );
}

Customer_info *customer_defs::set_customer_defaults()
{

	Customer_info *c = new Customer_info;
    c->company_name = "Broomwell";
	c->product_name = "Sirona";
    c->phone_number = "0161-236-0141";
    c->website = "www.broomwellhealth.com";
    c->description = tr("ECG interpretation services.");
    c->logo = QPixmap( ":broomwell_logo.png" );
    c->logo_width = 160;
    c->model_number = "RX93030-003";
    c->serial_number_prefix.append("7613");
        c->recording_durations.append("1");
	c->recording_durations.append("2");
	c->recording_durations.append("7");
	c->recording_durations.append("10");
	c->recording_durations.append("14");
    c->default_pre_trigger = PRE_TRIGGER_TIME_30_SEC;
    c->default_post_trigger = POST_TRIGGER_TIME_30_SEC;
    c->default_auto_event_limit = EVENT_LIMIT_0;
    c->default_manual_event_limit = EVENT_LIMIT_5;
	c->default_ttm_speed = TTM_SPEED_1X;
    c->default_tachy_threshold = TACHY_ANALYZER_OFF;
    c->default_brady_threshold = BRADY_ANALYZER_OFF;
    c->default_pause_threshold = PAUSE_ANALYZER_OFF;
	c->default_afib_setting = ANALYZER_OFF;
	c->default_recording_duration = RECORDING_LENGTH_1_DAY;
	c->default_sample_rate = SAMPLE_RATE_256;
	c->show_128_sps = true;
	c->show_sample_rate = true;
    c->server_enabled = true;
    c->mct_server = true;

    c->server_scheme = "https";
    c->server_host = "dlem.broomwell.com";
    c->server_directory = "/sirona-data";
    c->server_port = 443;
    c->server_user_name = "";
    c->server_password = "";

    c->network_url_holter.setScheme(c->server_scheme);
    c->network_url_holter.setHost(c->server_host);
    c->network_url_holter.setPort(c->server_port);
    c->network_url_holter.setPath(c->server_directory);
    c->network_url_holter.setUserName(c->server_user_name);
    c->network_url_holter.setPassword(c->server_password);

    c->server_scheme2 = "https";
    c->server_host2 = "dlem.broomwell.com";
    c->server_directory2 = "/sirona-data";
    c->server_port2 = 443;
    c->server_user_name2 = "";
    c->server_password2 = "";

    c->network_url_event.setScheme(c->server_scheme2);
    c->network_url_event.setHost(c->server_host2);
    c->network_url_event.setPort(c->server_port2);
    c->network_url_event.setPath(c->server_directory2);
    c->network_url_event.setUserName(c->server_user_name2);
    c->network_url_event.setPassword(c->server_password2);

    c->zip_password = "TPGbL6xFv9aH";

	c->color_main = "#3991C3";
	c->color_accent1 = "#BFDBF0";
	c->color_accent2 = "#E1EFF5";
	c->color_contrast = "#38E3C3";
	c->color_caution = "#C65C72";
	c->color_disabled = "#787878";

    c->device_key[0] = 0x000000E4;
    c->device_key[1] = 0x0000007B;
    c->device_key[2] = 0x00000030;
    c->device_key[3] = 0x000000BE;

    c->di_number = "";

    c->single_device_mode = DEVICE_MODE_EVENT;
    c->indications = true;

	return c;
}

customer_defs::~customer_defs() {}
