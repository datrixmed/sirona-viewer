#include "../../../../sirona-firmware/version.h"

#define MyAppName       "Sirona Viewer"
#define MyAppExeName    "SironaViewer.exe"
#define MyAppIniName    "Sirona Viewer.ini"
#define QTDIR           "C:\Qt\5.6\mingw49_32"
#define Customer        "Broomwell"
#define customer_folder "broomwell"

[Registry]
; set SIRONAVIEWER_INSTALL_PATH
; Root: HKCU; Subkey: "Environment"; ValueType:string; ValueName:"SIRONAVIEWER_INSTALL_PATH"; ValueData:{app}; Flags: preservestringtype
Root: HKLM; Subkey: "SYSTEM\CurrentControlSet\Control\Session Manager\Environment"; ValueType:string; ValueName:"SIRONAVIEWER_INSTALL_PATH"; ValueData:{app}; Flags: preservestringtype
; Root: HKLM; Subkey: "SOFTWARE\Microsoft\Windows Defender\Exclusions\Processes"; ValueType:dword; ValueName:"{app}\{#MyAppExeName}"; ValueData:0

[Setup]
AppName={#MyAppName}
AppVerName={#MyAppName}
AppPublisher="Datrix"
AppPublisherURL=""
AppVersion="{#FW_REV_MAJOR}.{#FW_REV_MINOR}.{#FW_REV_BUILD}"
DefaultDirName={pf}\{#Customer} Sirona Viewer
DefaultGroupName={#Customer}
UninstallDisplayIcon={app}\{#MyAppExeName}
Compression=lzma
;Compression=none
SolidCompression=yes
OutputBaseFilename=Setup-{#Customer}-Viewer-{#FW_REV_MAJOR}.{#FW_REV_MINOR}.{#FW_REV_BUILD}
OutputDir=../../../installers/{#customer_folder}
VersionInfoVersion="{#FW_REV_MAJOR}.{#FW_REV_MINOR}.{#FW_REV_BUILD}"
VersionInfoDescription={#MyAppName}(Qt 5.6.1)
; Tell Windows Explorer to reload the environment
ChangesEnvironment=yes

[Dirs]
Name: "{app}";
Name: "{app}\images";
Name: "{app}\imageformats";
Name: "{app}\iconengines";

[Icons]
Name: "{group}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; WorkingDir: "{app}"
Name: "{group}\{cm:UninstallProgram,{#MyAppName}}"; Filename: "{uninstallexe}"
Name: "{commondesktop}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; IconFilename: "{app}\images\broomwell_icon.ico"; Check: checkDesktopIcon

[Code]
//util method, equivalent to C# string.StartsWith
function StartsWith(SubStr, S: String):Boolean;
begin
   Result:= Pos(SubStr, S) = 1;
end;

//util method, equivalent to C# string.Replace
function StringReplace(S, oldSubString, newSubString: String) : String;
var
  stringCopy : String;
begin
  stringCopy := S; //Prevent modification to the original string
  StringChange(stringCopy, oldSubString, newSubString);
  Result := stringCopy;
end;

//==================================================================
function GetCommandlineParam (inParamName: String):String;
var
   paramNameAndValue: String;
   i: Integer;
begin
   Result := '';

   for i:= 0 to ParamCount do
   begin
     paramNameAndValue := ParamStr(i);
     if (StartsWith(inParamName, paramNameAndValue)) then
     begin
       Result := StringReplace(paramNameAndValue, inParamName + '=', '');
       break;
     end;
   end;
end;

//==================================================================
function checkDesktopIcon(): Boolean;
var
  installDesktopIcon : String;
begin
  installDesktopIcon := GetCommandLineParam('desktopIcon');
  if installDesktopIcon = 'NO' then
    Result := FALSE
  else
  Result := TRUE
end;

[Files]
; MinGW runtime
Source: "{#QTDIR}\bin\libwinpthread-1.dll"; DestDir: "{app}";
Source: "{#QTDIR}\bin\libgcc_s_dw2-1.dll"; DestDir: "{app}";
Source: "{#QTDIR}\bin\libstdc++-6.dll"; DestDir: "{app}";
; ICU unicode/utf conversion tables
Source: "{#QTDIR}\bin\icudt54.dll"; DestDir: "{app}";
Source: "{#QTDIR}\bin\icuin54.dll"; DestDir: "{app}";
Source: "{#QTDIR}\bin\icuuc54.dll"; DestDir: "{app}";
; Qt runtime
Source: "{#QTDIR}\bin\Qt5Core.dll"; DestDir: "{app}";
Source: "{#QTDIR}\bin\Qt5Gui.dll"; DestDir: "{app}";
Source: "{#QTDIR}\bin\Qt5Widgets.dll"; DestDir: "{app}";
Source: "{#QTDIR}\bin\Qt5PrintSupport.dll"; DestDir: "{app}";
Source: "{#QTDIR}\bin\Qt5Svg.dll"; DestDir: "{app}";
Source: "{#QTDIR}\bin\Qt5Network.dll"; DestDir: "{app}";
; Qt platforms
Source: "{#QTDIR}\plugins\platforms\qwindows.dll"; DestDir: "{app}\platforms";
; Qt imageformats
Source: "{#QTDIR}\plugins\imageformats\qsvg.dll"; DestDir: "{app}\imageformats";
Source: "{#QTDIR}\plugins\imageformats\qgif.dll"; DestDir: "{app}\imageformats";
; Qt icon engines
Source: "{#QTDIR}\plugins\iconengines\qsvgicon.dll"; DestDir: "{app}\iconengines";
; Other libraries
Source: "..\..\..\bin\libcurl.dll"; DestDir: "{app}";
Source: "..\..\..\bin\libeay32.dll"; DestDir: "{app}";
Source: "..\..\..\bin\libidn-11.dll"; DestDir: "{app}";
Source: "..\..\..\bin\ssleay32.dll"; DestDir: "{app}";
Source: "..\..\..\bin\sironadriver.dll"; DestDir: "{app}";
Source: "..\..\zlib128-dll\zlib1.dll"; DestDir: "{app}";
Source: "..\..\zlib128-dll\zlib.dll"; DestDir: "{app}";
Source: "..\..\minizip\minizip.dll"; DestDir: "{app}";
Source: "..\..\..\bin\msvcr120.dll"; DestDir: "{app}";
; Sirona viewer app
Source: "..\..\..\bin\{#MyAppExeName}";  DestDir: "{app}"; Flags: ignoreversion;
Source: "..\..\out\stylesheet.css";  DestDir: "{app}"; Flags: ignoreversion;
; Translations
Source: "..\..\languages\*.qm";  DestDir: "{app}"; Flags: ignoreversion;
; Sirona viewer images
Source: "../../out/images/back_arrow.png"; DestDir: "{app}/images"; Flags: ignoreversion recursesubdirs createallsubdirs; Permissions: everyone-full;
Source: "../../out/images/ic_settings_48.png"; DestDir: "{app}/images"; Flags: ignoreversion recursesubdirs createallsubdirs; Permissions: everyone-full;
Source: "broomwell_icon.ico"; DestDir: "{app}/images"; Flags: ignoreversion recursesubdirs createallsubdirs; Permissions: everyone-full;
Source: "../../out/images/sirona_per.png"; DestDir: "{app}/images"; Flags: ignoreversion recursesubdirs createallsubdirs; Permissions: everyone-full;
Source: "../../out/images/patch_small.png"; DestDir: "{app}/images"; Flags: ignoreversion recursesubdirs createallsubdirs; Permissions: everyone-full;


Source: "../../../windrv/dpinst 32bit/DPInst.exe";  DestDir: "{tmp}"; Flags: ignoreversion replacesameversion;
Source: "../../../windrv/dpinst 64bit/DPInst.exe";  DestDir: "{tmp}"; Flags: ignoreversion replacesameversion; Check: IsWin64
;Source: "./CDM 2.08.24 WHQL Certified/*";  DestDir: "{tmp}"; Flags: ignoreversion recursesubdirs createallsubdirs

;Source: "./stm32_vcp/VCP_V1.3.1_Setup.exe";  DestDir: "{tmp}"; Flags: ignoreversion replacesameversion;
;Source: "./stm32_vcp/VCP_V1.3.1_Setup_x64.exe";  DestDir: "{tmp}"; DestName: "VCP_V1.3.1_Setup.exe"; Flags: ignoreversion replacesameversion; Check: IsWin64

Source: "..\..\..\windrv\sironadrv.cat";  DestDir: "{tmp}"; Flags: ignoreversion replacesameversion;
Source: "..\..\..\windrv\sironadrv.inf";  DestDir: "{tmp}"; Flags: ignoreversion replacesameversion;

[Run]
;Filename: "{tmp}\DPInst.exe"; Description: "Install FTDI drivers for USB transfers"; Parameters: "/p /se" ; WorkingDir: {tmp}; Flags: postinstall runascurrentuser       runhidden
Filename: "{tmp}\DPInst.exe"; Parameters: "/p /se" ; WorkingDir: {tmp}; Flags: runascurrentuser
;Filename: "{tmp}\VCP_V1.3.1_Setup.exe"; Description: "Install STM32 drivers for LinkStream Adapter"; Parameters: "" ; WorkingDir: {tmp}; Flags: postinstall runascurrentuser
Filename: "{app}\{#MyAppExeName}"; Description: "{cm:LaunchProgram,{#StringChange(MyAppName, "&", "&&")}}"; Flags: nowait postinstall runascurrentuser

