#include "customer.h"

customer_defs::customer_defs( QObject *parent )
{
	this->setParent( parent );
}

Customer_info *customer_defs::set_customer_defaults()
{

	Customer_info *c = new Customer_info;
    c->company_name = "Cardiostaff";
    c->product_name = "cSTAT - W";
    c->phone_number = "800-747-4455";
    c->website = "www.cardiostaff.com";
    c->description = "We take our responsibility to heart\u2122\n\nCardiostaff provides high quality\ncardiac monitoring services\nnationwide. Our lab is staffed 24/7\nand is always available when you\nneed help.";
    c->logo = QPixmap( ":cardiostaff_logo.png" );
    c->logo_width = 83;
    c->model_number = "RX93030-003";
    c->serial_number_prefix.append("7612");
    c->recording_durations.append("1");
    c->recording_durations.append("2");
    c->recording_durations.append("7");
    c->recording_durations.append("10");
    c->recording_durations.append("14");
    c->default_pre_trigger = PRE_TRIGGER_TIME_60_SEC;
    c->default_post_trigger = POST_TRIGGER_TIME_30_SEC;
    c->default_auto_event_limit = EVENT_LIMIT_10;
    c->default_manual_event_limit = EVENT_LIMIT_3;
	c->default_ttm_speed = TTM_SPEED_3X;
    c->default_tachy_threshold = TACHY_THRESHOLD_160;
    c->default_brady_threshold = BRADY_THRESHOLD_40;
    c->default_pause_threshold = PAUSE_THRESHOLD_2_5;
	c->default_afib_setting = ANALYZER_ON;
    c->default_recording_duration = RECORDING_LENGTH_1_DAY;
	c->default_sample_rate = SAMPLE_RATE_256;
    c->show_128_sps = true;
	c->show_sample_rate = true;
    c->server_enabled = true;
    c->mct_server = false;

    c->server_scheme = "ftp";
    c->server_host = "US1003CStf.dvtms.com";
    c->server_directory = "/";
    c->server_port = 63121;
    c->server_user_name = "US1003CStf01Intricon";
    c->server_password = "78hbCALyMjz9WgpUVouy";

    c->network_url_holter.setScheme(c->server_scheme);
    c->network_url_holter.setHost(c->server_host);
    c->network_url_holter.setPort(c->server_port);
    c->network_url_holter.setUserName(c->server_user_name);
    c->network_url_holter.setPassword(c->server_password);

    c->server_scheme2 = c->server_scheme;
    c->server_host2 = c->server_host;
    c->server_directory2 = c->server_directory;
    c->server_port2 = c->server_port;
    c->server_user_name2 = c->server_user_name;
    c->server_password2 = c->server_password;


    c->network_url_event.setScheme(c->server_scheme2);
    c->network_url_event.setHost(c->server_host2);
    c->network_url_event.setPort(c->server_port2);
    c->network_url_event.setUserName(c->server_user_name2);
    c->network_url_event.setPassword(c->server_password2);

    c->zip_password = "BYBndNHEXe58";

	c->color_main = "#3991C3";
	c->color_accent1 = "#BFDBF0";
	c->color_accent2 = "#E1EFF5";
	c->color_contrast = "#38E3C3";
	c->color_caution = "#C65C72";
	c->color_disabled = "#787878";

    c->device_key[0] = 0x00000045;
    c->device_key[1] = 0x0000004A;
    c->device_key[2] = 0x000000F2;
    c->device_key[3] = 0x000000E7;
	
    c->di_number = "00851074007194";

	return c;
}


customer_defs::~customer_defs() {}
