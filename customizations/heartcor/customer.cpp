#include "customer.h"

// #define TESTING

customer_defs::customer_defs( QObject *parent )
{
	this->setParent( parent );
}

Customer_info *customer_defs::set_customer_defaults()
{

	Customer_info *c = new Customer_info;
    c->company_name = "HeartCor Solutions";
    c->product_name = "Sentinel";
#ifdef TESTING
    c->phone_number = "Datrix Test";
#else
    c->phone_number = "800-987-4117";
#endif
    c->website = "www.heartcorsolutions.com";
    c->description = tr("Worldwide ECG Core Lab Services.");
    c->logo = QPixmap( ":heartcor_logo.png" );
    c->logo_width = 140;
    c->serial_number_prefix.append("7614");
        c->recording_durations.append("1");
	c->recording_durations.append("2");
	c->recording_durations.append("7");
	c->recording_durations.append("10");
	c->recording_durations.append("14");
    c->default_pre_trigger = PRE_TRIGGER_TIME_60_SEC;
    c->default_post_trigger = POST_TRIGGER_TIME_60_SEC;
    c->default_auto_event_limit = EVENT_LIMIT_1000;
    c->default_manual_event_limit = EVENT_LIMIT_1000;
	c->default_ttm_speed = TTM_SPEED_3X;
    c->default_tachy_threshold = TACHY_THRESHOLD_160;
	c->default_brady_threshold = BRADY_THRESHOLD_40;
	c->default_pause_threshold = PAUSE_THRESHOLD_3_0;
	c->default_afib_setting = ANALYZER_ON;
	c->default_recording_duration = RECORDING_LENGTH_1_DAY;
	c->default_sample_rate = SAMPLE_RATE_256;
	c->show_128_sps = true;
	c->show_sample_rate = true;

#ifdef TESTING
    c->server_enabled = true;
    c->mct_server = false;

	c->server_scheme = "ftp";
	c->server_host = "sironaecg.com";
    c->server_directory = "/";
	c->server_port = 21;
	c->server_user_name = "genericsirona@sironaecg.com";
	c->server_password = "i4&gmX5iyBOL";

    c->server_scheme2 = "https";
    c->server_host2 = "sironaecg.com";
    c->server_directory2 = "/mct/event.php";
    c->server_port2 = 443;
    c->server_user_name2 = "";
    c->server_password2 = "";
#else
    c->server_enabled = true;
    c->mct_server = false;

	c->server_scheme = "ftp";
    c->server_host = "upload.heartcorsolutions.com";
    c->server_directory = "/FTP Uploads/sirona/";
    c->server_port = 990;
    c->server_user_name = "sirona";
    c->server_password = "$3ndHCS@f1l3";
    c->server_tls = EXPLICIT_TLS;

    c->server_scheme2 = "ftps";
    c->server_host2 = "US1009Hrtc.dvtms.com";
    c->server_directory2 = "/";
    c->server_port2 = 63199;
    c->server_user_name2 = "US1009Hrtc01Intricon";
    c->server_password2 = "hVqbNXcMTJvc8nkibqb8";
    c->server_tls2 = IMPLICIT_TLS;
#endif

    c->network_url_holter.setScheme(c->server_scheme);
    c->network_url_holter.setHost(c->server_host);
    c->network_url_holter.setPort(c->server_port);
    c->network_url_holter.setUserName(c->server_user_name);
    c->network_url_holter.setPassword(c->server_password);

    c->network_url_event.setScheme(c->server_scheme2);
    c->network_url_event.setHost(c->server_host2);
    c->network_url_event.setPort(c->server_port2);
    c->network_url_event.setUserName(c->server_user_name2);
    c->network_url_event.setPassword(c->server_password2);

    c->zip_password = "whG6aXpHzddE";

	c->color_main = "#3991C3";
	c->color_accent1 = "#BFDBF0";
	c->color_accent2 = "#E1EFF5";
	c->color_contrast = "#38E3C3";
	c->color_caution = "#C65C72";
	c->color_disabled = "#787878";

    c->device_key[0] = 0x000000FA;
    c->device_key[1] = 0x00000023;
    c->device_key[2] = 0x0000000D;
    c->device_key[3] = 0x000000D2;

    c->di_number = "00851074007279";

	return c;
}

customer_defs::~customer_defs() {}
