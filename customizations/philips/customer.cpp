#include "customer.h"

customer_defs::customer_defs( QObject *parent )
{
	this->setParent( parent );
}

Customer_info *customer_defs::set_customer_defaults()
{

	Customer_info *c = new Customer_info;
    c->company_name = "Datrix";
	c->product_name = "Sirona";
    c->phone_number = "800-555-1234";
    c->website = "www.DatrixMed.com";
    c->description = tr("Datrix enhances the mobility\nand effectiveness of miniature medical body-worn devices.");
    c->logo = QPixmap( ":Company Logo.png" );
    c->logo_width = 160;
    c->serial_number_prefix.append("76A0");
        c->recording_durations.append("1");
	c->recording_durations.append("2");
	c->recording_durations.append("7");
	c->recording_durations.append("10");
	c->recording_durations.append("14");
    c->mct_recording_durations.append("7");
    c->mct_recording_durations.append("14");
    c->mct_recording_durations.append("21");
    c->mct_recording_durations.append("30");
    c->default_pre_trigger = PRE_TRIGGER_TIME_30_SEC;
    c->default_post_trigger = POST_TRIGGER_TIME_30_SEC;
    c->default_auto_event_limit = EVENT_LIMIT_1000;
    c->default_manual_event_limit = EVENT_LIMIT_1000;
	c->default_ttm_speed = TTM_SPEED_3X;
    c->default_tachy_threshold = TACHY_THRESHOLD_160;
	c->default_brady_threshold = BRADY_THRESHOLD_40;
	c->default_pause_threshold = PAUSE_THRESHOLD_3_0;
	c->default_afib_setting = ANALYZER_ON;
	c->default_recording_duration = RECORDING_LENGTH_1_DAY;
    c->default_mct_recording_duration = RECORDING_LENGTH_21_DAYS;
	c->default_sample_rate = SAMPLE_RATE_256;
	c->show_128_sps = true;
	c->show_sample_rate = true;
    c->review_patient_before_download = false;

#if 1   // Datrix
    c->server_enabled = false;
    c->mct_server = true;
    c->generateResFile = true;

	c->server_scheme = "ftp";
	c->server_host = "sironaecg.com";
    c->server_directory = "/";
	c->server_port = 21;
	c->server_user_name = "genericsirona@sironaecg.com";
	c->server_password = "i4&gmX5iyBOL";

    c->server_scheme2 = "https";
    c->server_host2 = "sironaecg.com";
    c->server_directory2 = "/mct/event.php";
    c->server_port2 = 443;
    c->server_user_name2 = "";
    c->server_password2 = "";
#endif

#if 0   // Techmedic: Ana
    c->server_enabled = true;
    c->mct_server = true;

    c->server_scheme = "https";
    c->server_host = "nl0025anab.dvtms.com";
    c->server_directory = "/sirona-data";
    c->server_port = 61185;
    c->server_user_name = "";
    c->server_password = "";

    c->server_scheme2 = c->server_scheme;
    c->server_host2 = c->server_host;
    c->server_directory2 = c->server_directory;
    c->server_port2 = c->server_port;
    c->server_user_name2 = c->server_user_name;
    c->server_password2 = c->server_password;
#endif

    c->network_url_holter.setScheme(c->server_scheme);
    c->network_url_holter.setHost(c->server_host);
    c->network_url_holter.setPort(c->server_port);
    c->network_url_holter.setPath(c->server_directory);
    c->network_url_holter.setUserName(c->server_user_name);
    c->network_url_holter.setPassword(c->server_password);

    c->network_url_event.setScheme(c->server_scheme2);
    c->network_url_event.setHost(c->server_host2);
    c->network_url_event.setPort(c->server_port2);
    c->network_url_event.setPath(c->server_directory2);
    c->network_url_event.setUserName(c->server_user_name2);
    c->network_url_event.setPassword(c->server_password2);

    c->zip_password = "i4&gmX5iyBOL";

	c->color_main = "#3991C3";
	c->color_accent1 = "#BFDBF0";
	c->color_accent2 = "#E1EFF5";
	c->color_contrast = "#38E3C3";
	c->color_caution = "#C65C72";
	c->color_disabled = "#787878";

    c->device_key[0] = 0x000000CE;
    c->device_key[1] = 0x000000CA;
    c->device_key[2] = 0x000000CA;
    c->device_key[3] = 0x000000FE;

    c->di_number = "00851074007125";

	return c;
}

customer_defs::~customer_defs() {}
