#include "customer.h"

customer_defs::customer_defs( QObject *parent )
{
	this->setParent( parent );
}

Customer_info *customer_defs::set_customer_defaults()
{

	Customer_info *c = new Customer_info;
    c->company_name = "AMI Cardiac Monitoring, Inc.";
    c->product_name = "SironaWireless";
    c->phone_number = "1-800-785-4354";
    c->website = "www.amicardiac.com";
    c->description = "A Medicare-Certified Independent\nDiagnostic Testing Facility";
    c->logo = QPixmap( ":AMI_Logo_Transparent.png" );
    c->logo_width = 83;
    c->model_number = "RX93030-001";
    c->serial_number_prefix.append("4000");
    c->recording_durations.append("1");
    c->recording_durations.append("2");
    c->recording_durations.append("7");
    c->recording_durations.append("10");
    c->default_pre_trigger = PRE_TRIGGER_TIME_45_SEC;
    c->default_post_trigger = POST_TRIGGER_TIME_45_SEC;
    c->default_auto_event_limit = EVENT_LIMIT_10;
    c->default_manual_event_limit = EVENT_LIMIT_3;
	c->default_ttm_speed = TTM_SPEED_3X;
	c->default_tachy_threshold = TACHY_THRESHOLD_140;
    c->default_brady_threshold = BRADY_THRESHOLD_35;
	c->default_pause_threshold = PAUSE_THRESHOLD_3_0;
	c->default_afib_setting = ANALYZER_ON;
    c->default_recording_duration = RECORDING_LENGTH_1_DAY;
	c->default_sample_rate = SAMPLE_RATE_256;
    c->show_128_sps = false;
	c->show_sample_rate = true;
    c->server_enabled = true;
    c->mct_server = false;

    c->server_scheme = "ftp";
    c->server_host = "sirona.amicardiac.com";
    c->server_directory = "/";
    c->server_port = 63121;
    c->server_user_name = "US1005AMI01Intricon";
    c->server_password = "SnVH78ZrllDukO5hRxGM";

    c->network_url_holter.setScheme(c->server_scheme);
    c->network_url_holter.setHost(c->server_host);
    c->network_url_holter.setPort(c->server_port);
    c->network_url_holter.setUserName(c->server_user_name);
    c->network_url_holter.setPassword(c->server_password);

    c->server_scheme2 = c->server_scheme;
    c->server_host2 = c->server_host;
    c->server_directory2 = c->server_directory;
    c->server_port2 = c->server_port;
    c->server_user_name2 = c->server_user_name;
    c->server_password2 = c->server_password;


    c->network_url_event.setScheme(c->server_scheme2);
    c->network_url_event.setHost(c->server_host2);
    c->network_url_event.setPort(c->server_port2);
    c->network_url_event.setUserName(c->server_user_name2);
    c->network_url_event.setPassword(c->server_password2);

    c->zip_password = "q6C3NvPzYYpW";
	c->color_main = "#3991C3";
	c->color_accent1 = "#BFDBF0";
	c->color_accent2 = "#E1EFF5";
	c->color_contrast = "#38E3C3";
	c->color_caution = "#C65C72";
	c->color_disabled = "#787878";

    c->device_key[0] = 0x000000F0;
    c->device_key[1] = 0x00000095;
    c->device_key[2] = 0x0000000C;
    c->device_key[3] = 0x000000B7;
	
	c->di_number = "00851074007255";

	return c;
}


customer_defs::~customer_defs() {}
