#include "customer.h"

customer_defs::customer_defs( QObject *parent )
{
	this->setParent( parent );
}

Customer_info *customer_defs::set_customer_defaults()
{

	Customer_info *c = new Customer_info;
    c->company_name = "Mobilis Health";
	c->product_name = "HealthWatch";
    c->phone_number = "";
    c->website = "www.MobilisHealth.net";
    c->description = tr("Mobilis Health provides advanced monitoring services to physicians and patients, while simplifying the workflow process necessary to complete the cardiac testing.");
    c->logo = QPixmap( ":Company Logo.png" );
    c->logo_width = 160;
    c->serial_number_prefix.append("76A0");
	c->recording_durations.append("1");
	c->recording_durations.append("2");
	c->recording_durations.append("7");
	c->recording_durations.append("10");
	c->recording_durations.append("14");
    c->mct_recording_durations.append("7");
    c->mct_recording_durations.append("14");
    c->mct_recording_durations.append("21");
    c->mct_recording_durations.append("30");
    c->default_pre_trigger = PRE_TRIGGER_TIME_30_SEC;
    c->default_post_trigger = POST_TRIGGER_TIME_30_SEC;
    c->default_auto_event_limit = EVENT_LIMIT_1000;
    c->default_manual_event_limit = EVENT_LIMIT_1000;
	c->default_ttm_speed = TTM_SPEED_3X;
	c->default_tachy_threshold = TACHY_THRESHOLD_160;
	c->default_brady_threshold = BRADY_THRESHOLD_40;
	c->default_pause_threshold = PAUSE_THRESHOLD_3_0;
	c->default_afib_setting = ANALYZER_ON;
	c->default_recording_duration = RECORDING_LENGTH_1_DAY;
    c->default_mct_recording_duration = RECORDING_LENGTH_21_DAYS;
	c->default_sample_rate = SAMPLE_RATE_256;
	c->show_128_sps = true;
	c->show_sample_rate = true;
    c->review_patient_before_download = false;
    c->server_enabled = false;
    c->mct_server = true;

	c->server_scheme = "https";
	c->server_host = "cloudbeatapi.azurewebsites.net";
    c->server_directory = "/wirelessevent/";
	c->server_port = 443;
	c->server_user_name = "sirona";
	c->server_password = "Sirona2016";

    c->network_url_holter.setScheme(c->server_scheme);
    c->network_url_holter.setHost(c->server_host);
    c->network_url_holter.setPort(c->server_port);
    c->network_url_holter.setPath(c->server_directory);
    c->network_url_holter.setUserName(c->server_user_name);
    c->network_url_holter.setPassword(c->server_password);

    c->server_scheme2 = c->server_scheme;
    c->server_host2 = c->server_host;
    c->server_directory2 = c->server_directory;
    c->server_port2 = c->server_port;
    c->server_user_name2 = c->server_user_name;
    c->server_password2 = c->server_password;

    c->network_url_event.setScheme(c->server_scheme2);
    c->network_url_event.setHost(c->server_host2);
    c->network_url_event.setPort(c->server_port2);
    c->network_url_event.setPath(c->server_directory2);
    c->network_url_event.setUserName(c->server_user_name2);
    c->network_url_event.setPassword(c->server_password2);

    c->zip_password = "i4&gmX5iyBOL";

	c->color_main = "#3991C3";
	c->color_accent1 = "#BFDBF0";
	c->color_accent2 = "#E1EFF5";
	c->color_contrast = "#38E3C3";
	c->color_caution = "#C65C72";
	c->color_disabled = "#787878";

    c->device_key[0] = 0x00000029;
    c->device_key[1] = 0x00000050;
    c->device_key[2] = 0x000000BA;
    c->device_key[3] = 0x0000002F;

    c->di_number = "";

	return c;
}

customer_defs::~customer_defs() {}
