
#include <time.h>
#include <stdio.h>
#include <stdint.h>

#include "logdecode.h"

#define LOG_PARAM_0	0x0000
#define LOG_PARAM_1I16 	0x2000
#define LOG_PARAM_2I16	0x4000
#define LOG_PARAM_4I16	0x6000
#define LOG_PARAM_1I32	0x8000
#define LOG_PARAM_2I32	0xA000

#define VAL32BIT(arr,offset)	( *((uint32_t *) &(arr[offset])))
#define VAL16BIT(arr,offset)	( *((uint16_t *) &(arr[offset])))
#define VAL8BIT (arr,offset)	( *((uint8_t *) &(arr[offset])))

#define OFFSET_HEADER_DBG_VER		(0)
#define OFFSET_HEADER_HDR_SIZE		(2)
#define OFFSET_HEADER_NUM_COUNTERS	(4)
#define OFFSET_HEADER_CUR_TICKS		(8)
#define OFFSET_HEADER_CUR_SECS		(12)
#define OFFSET_COUNTERS				(VAL16BIT(header,OFFSET_HEADER_HDR_SIZE))


extern long readFile( char *filename, void *buf );
extern char * decodeType( int msg );
extern char *date( const char *fmt, time_t rawtime );


#define NR_PARAMETERS (sizeof(paramNameString)/sizeof(paramNameString[0]))
#define PARAMETER_MAX_LENGTH (20)

static const char paramNameString[][PARAMETER_MAX_LENGTH + 1] = {
	"CODE_REV",
	"DEV_ID",
	"FW_REV",
	"PRE_TRIGGER",
	"POST_TRIGGER",
	"ERR_CODE",
	"REC_MODE",
	"DOWN_COMP",
	"REC_LENGTH",
	"AUTO_EV_LIMIT",
	"MAN_EV_LIMIT",
	"AUDIO_MUTE",
	"TACHY_ON_FLG",
	"TACHY_THRESH",
	"BRADY_ON_FLG",
	"BRADY_THRESH",
	"PAUSE_ON_FLG",
	"PAUSE_THRESH",
	"ANAL_CH_MASK",
	"BATT_VOLT",
	"TTM_EN_FLG",
	"TTM_3X_FLG",
	"CABLE_ID",
	"SAMP_RATE",
	"PAT_ID",
	"PAT_NAME",
	"PAT_MIDDLE",
	"PAT_LAST",
	"PHY_NAME",
	"TIME_ZONE",
	"DEV_MODE",
	"PROC_STATE",
	"PAT_DOB",
	"AF_ON_FLG",
	"HOLT_START", // PC app up to 2.0.2 used this - use PROC_START_TIME instead
	"COMMENT",
	"PROC_START_TIME",
	"LAST_CABLE", // PC app up to 2.0.2 used this, not used anymore
	"MODEL_NUMBER",
	"MCT_ENABLED",
	"PROC_TIME",
	"ANNOT_SENT",
	"TECH_NAME",
	"FACILITY",
	"DIARY_TIMEOUT",
};



#ifdef STAND_ALONE
int main( int argc, char **argv )
{
	uint8_t dataBuf[1000000];

	long lenData = readFile( argv[1], dataBuf );

	decodeFwLog( (char*) dataBuf, lenData );
}
#endif


QString decodeFwLog( char *data, long lenData )
{
	QString strLog;

	if ( lenData < 16 ) {
		return strLog; /* "File too short for header" */
	}

	/* uint16_t * header = unpack( "vdebugVersion/vheaderSize/vnumCounters/vunused/VcurTicks/VcurSeconds", data ); */
	uint8_t * header = (uint8_t *) data;

	int headerSize = VAL16BIT(header,OFFSET_HEADER_HDR_SIZE);
	int numCounters = VAL16BIT(header,OFFSET_HEADER_NUM_COUNTERS);
	if ( VAL16BIT(header,OFFSET_HEADER_DBG_VER) != 1 || headerSize < 16 || numCounters < 9 ) {
		strLog += QString::asprintf("Error:  Bad Header   %04x %04x %04x \n", VAL16BIT(header,OFFSET_HEADER_DBG_VER), headerSize, numCounters );
		return strLog;	/* Bad Header */
	}

	if ( lenData < headerSize + numCounters * 4 ) {
		strLog += QString::asprintf("Error:  File too short for counters\n");
		return strLog;
	}

	 strLog += QString::asprintf("LOG_CNT_POWER_ON = %d\n",		VAL32BIT( header, OFFSET_COUNTERS + 0 * sizeof(uint32_t)) );
	 strLog += QString::asprintf("LOG_CNT_HOLTER_START = %d\n",	VAL32BIT( header, OFFSET_COUNTERS + 1 * sizeof(uint32_t)) );
	 strLog += QString::asprintf("LOG_CNT_EVENT_START = %d\n",	VAL32BIT( header, OFFSET_COUNTERS + 2 * sizeof(uint32_t)) );
	 strLog += QString::asprintf("LOG_CNT_POST_START = %d\n",	VAL32BIT( header, OFFSET_COUNTERS + 3 * sizeof(uint32_t)) );
	 strLog += QString::asprintf("LOG_CNT_MCT_START = %d\n",		VAL32BIT( header, OFFSET_COUNTERS + 4 * sizeof(uint32_t)) );
	 strLog += QString::asprintf("LOG_CNT_HOLTER_LEN = %d\n",	VAL32BIT( header, OFFSET_COUNTERS + 5 * sizeof(uint32_t)) );
	 strLog += QString::asprintf("LOG_CNT_EVENT_LEN = %d\n",		VAL32BIT( header, OFFSET_COUNTERS + 6 * sizeof(uint32_t)) );
	 strLog += QString::asprintf("LOG_CNT_POST_LEN = %d\n",		VAL32BIT( header, OFFSET_COUNTERS + 7 * sizeof(uint32_t)) );
	 strLog += QString::asprintf("LOG_CNT_MCT_LEN = %d\n",		VAL32BIT( header, OFFSET_COUNTERS + 8 * sizeof(uint32_t)) );


	uint16_t * logdata = (uint16_t *) &(data[headerSize + numCounters * 4]);

	int logsize = lenData - (headerSize + numCounters * 4);
	logsize /= 2;	/* convert to 16bit words */
	logsize -= 1;	/* account for php using a 1-based index */

	while ( logsize >= (4 + headerSize + numCounters * 4) ) {
		int blocksize = (int) ( logdata[logsize - 1] / 2 );
		long _blockTime = (logdata[logsize - 2] << 16 ) + ( long ) logdata[logsize - 3];
		logsize -= 4;

		if ( _blockTime == 0 ) {
			strLog += QString::asprintf("\n-no valid date-\n");
		} else {
			strLog += QString::asprintf("\n%s\n", date( "%Y-%m-%d %H:%M:%S\n", _blockTime ) );
		}

		while ( blocksize >= 3 && logsize >= 3 ) {
			strLog += QString::asprintf( "%10ld: ", ( long )( logdata[logsize] << 16 ) + ( long ) logdata[logsize - 1] );
			int logType = ((long) logdata[logsize - 2] & 0x1FFF);
			strLog += QString::asprintf("%s ", decodeType(logType) );

			int paramType = (logdata[logsize - 2] & 0xE000);

			switch ( paramType ) {
				case  0x0000: /* LOG_PARAM_0 */
					logsize -= 3;
					blocksize -= 3;
					break;
				case 0x2000: /* LOG_PARAM_1I16 */
					if ( blocksize >= 4 && logsize >= 4 ) {
						int val = logdata[logsize - 3];
						strLog += QString::asprintf("%3d  (0x%02x)", val, val );
						if ( logType == 401 ) {
							if ( val < (int) NR_PARAMETERS ) {
								strLog += QString::asprintf(" \"%s\"", paramNameString[val] );
							}
						}
						logsize -= 4;
						blocksize -= 4;
					} else {
						logsize -= blocksize;
						blocksize = 0;
					}
					break;
				case 0x4000: /* LOG_PARAM_2I16 */
					if ( blocksize >= 5 && logsize >= 5 ) {
						strLog += QString::asprintf("%3d %3d  (0x%04x 0x%04x)", logdata[logsize - 3], logdata[logsize - 4] , logdata[logsize - 3], logdata[logsize - 4] );
						logsize -= 5;
						blocksize -= 5;
					} else {
						logsize -= blocksize;
						blocksize = 0;
					}
					break;

				case 0x6000: /* LOG_PARAM_4I16 */
					if ( blocksize >= 7 && logsize >= 7 ) {
						strLog += QString::asprintf("%3d %3d %3d %3d   (0x%04x 0x%04x 0x%04x 0x%04x)", logdata[logsize - 3], logdata[logsize - 4], logdata[logsize - 5], logdata[logsize - 6] , logdata[logsize - 3], logdata[logsize - 4], logdata[logsize - 5], logdata[logsize - 6] );
						logsize -= 7;
						blocksize -= 7;
					} else {
						logsize -= blocksize;
						blocksize = 0;
					}
					break;

				case 0x8000: /* LOG_PARAM_1I32 */
					if ( blocksize >= 5 && logsize >= 5 ) {
						strLog += QString::asprintf("%4ld   (0x%04lx)", (long) ((long)logdata[logsize - 3] << 16) | (long)logdata[logsize - 4] , (long) ((long)logdata[logsize - 3] << 16) | (long)logdata[logsize - 4] );
						logsize -= 5;
						blocksize -= 5;
					} else {
						logsize -= blocksize;
						blocksize = 0;
					}
					break;

				case 0xA000: /* LOG_PARAM_2I32 */
					if ( blocksize >= 7 && logsize >= 7 ) {
						strLog += QString::asprintf("%4ld %4ld   (0x%04lx 0x%04lx)", (long) ((long)logdata[logsize - 3] << 16) | (long)logdata[logsize - 4], (long) ((long)logdata[logsize - 5] << 16) | (long)logdata[logsize - 6] , (long) ((long)logdata[logsize - 3] << 16) | (long)logdata[logsize - 4], (long) ((long)logdata[logsize - 5] << 16) | (long)logdata[logsize - 6] );
						logsize -= 7;
						blocksize -= 7;
					} else {
						logsize -= blocksize;
						blocksize = 0;
					}
					break;

				default:
					strLog += QString::asprintf("\nUnknown param type 0x%04x", paramType );
					return strLog;
			}
			strLog += QString::asprintf("\n");
		}
		logsize -= blocksize;
		blocksize = 0;
	}

	return strLog;
}

long readFile( char *filename, void *buf )
{
	FILE *pFile;
	long lSize;
	size_t result;

	if ( (pFile = fopen(filename,"rb" )) == NULL ) {
		fputs ( "File error", stderr );
		return 0;
	}

	// obtain file size:
	fseek( pFile, 0, SEEK_END );
	lSize = ftell( pFile );
	rewind( pFile );

	// copy the file into the buf:
	result = fread( buf, 1, lSize, pFile );
	if ( (long)result != lSize ) {
		fputs( "Reading error", stderr );
		return 0;
	}

	fclose( pFile );
	return lSize;
}




char *date( const char *fmt, time_t rawtime )
{
	struct tm * timeinfo;
	static char buffer[80];

	timeinfo = localtime( &rawtime );

	strftime( buffer, 80 , fmt, timeinfo );

	return buffer;
}




char * decodeType( int msg )
{
	static char buf[50];

	switch ( msg ) {
		case 101 :
			return (char *) "AT_GOTO_STATE";
		case 102 :
			return (char *) "BLUETOOTH_CONNECT";
		case 103 :
			return (char *) "AT_GOING_TO_SLEEP";
		case 104 :
			return (char *) "DEVICE_ID_FROM_HARDWARE";
		case 105 :
			return (char *) "DEVICE_ID_FROM_CONFIG";
		case 106 :
			return (char *) "CONNECT_TIMEOUT";
		case 107 :
			return (char *) "BLE_CONNECTED";
		case 108 :
			return (char *) "BT_SPP_CONNECTED";
		case 109 :
			return (char *) "BT_UNKNOWN_CONNECTED";
		case 110 :
			return (char *) "BLUETOOTH_DISCONNECT";
		case 111 :
			return (char *) "NO_CONNECT_LAST";
		case 112 :
			return (char *) "REQ_BT_FACTORY_RESET";
		case 201 :
			return (char *) "INVALID_SAMPLING_RATE";
		case 202 :
			return (char *) "SAMPLING_RATE_SET";
		case 203 :
			return (char *) "SHUTDOWN";
		case 301 :
			return (char *) "SPI2_RX_OVERRUN_DURING_RX";
		case 302 :
			return (char *) "SPI2_RX_TIMEOUT_DURING_RX";
		case 303 :
			return (char *) "SPI2_TXE_TIMEOUT_DURING_RX";
		case 304 :
			return (char *) "SPI2_BSY_TIMEOUT_DURING_RX";
		case 305 :
			return (char *) "SPI2_RX_OVERRUN_DURING_TXS";
		case 306 :
			return (char *) "SPI2_RX_TIMEOUT_DURING_TXS";
		case 307 :
			return (char *) "SPI2_TX_OVERRUN_DURING_TXS";
		case 308 :
			return (char *) "SPI2_TX_TIMEOUT_DURING_TXS";
		case 309 :
			return (char *) "SPI2_TXE_TIMEOUT_DURING_TXS";
		case 310 :
			return (char *) "SPI2_BSY_TIMEOUT_DURING_TXS";
		case 311 :
			return (char *) "SPI2_RX_OVERRUN_DURING_TX";
		case 312 :
			return (char *) "SPI2_RX_TIMEOUT_DURING_TX";
		case 313 :
			return (char *) "SPI2_TX_OVERRUN_DURING_TX";
		case 314 :
			return (char *) "SPI2_TX_TIMEOUT_DURING_TX";
		case 315 :
			return (char *) "SPI2_TXE_TIMEOUT_DURING_TX";
		case 316 :
			return (char *) "SPI2_BSY_TIMEOUT_DURING_TX";
		case 401 :
			return (char *) "GET_PARAMETER";
		case 402 :
			return (char *) "SET_PARAMETER";
		case 403 :
			return (char *) "ERROR_READING_CONFIG";
		case 404 :
			return (char *) "HAMMER_PWM_STRING_NOT_FOUND";
		case 405 :
			return (char *) "BAD_CONFIG_CRC";
		case 406 :
			return (char *) "BAD_PRETRIGGER";
		case 407 :
			return (char *) "BAD_POSTTRIGGER";
		case 408 :
			return (char *) "INVALID_RUN_MODE";
		case 409 :
			return (char *) "DEVICE_MODE";
		case 410 :
			return (char *) "BAD_SAMPLE_RATE";
		case 411 :
			return (char *) "WRITING_CONFIG";
		case 412 :
			return (char *) "SET_ERR_CODE";
		case 413 :
			return (char *) "SUP_SET_PROC_STATE";
		case 414 :
			return (char *) "GETTING_BAD_CONF_DATE";
		case 415 :
			return (char *) "GETTING_BAD_PROC_START_TIME";
		case 501 :
			return (char *) "ERROR_INIT_SD";
		case 502 :
			return (char *) "ERROR_READING_SECTOR";
		case 503 :
			return (char *) "ERROR_WRITING_SECTOR";
		case 504 :
			return (char *) "ERROR_FILE_ERASE";
		case 505 :
			return (char *) "ERROR_FILE_READ";
		case 506 :
			return (char *) "ERROR_FILE_WRITE";
		case 507 :
			return (char *) "ERROR_FILE_FLUSH";
		case 508 :
			return (char *) "ERROR_FILE_SIZE";
		case 509 :
			return (char *) "ERROR_FILE_SET_SIZE";
		case 510 :
			return (char *) "LOG_FILE_OVERREAD";
		case 601 :
			return (char *) "FAST_SEND_PRESSED";
		case 602 :
			return (char *) "SLOW_SEND_PRESSED";
		case 603 :
			return (char *) "FAST_RECORD_PRESSED";
		case 604 :
			return (char *) "SLOW_RECORD_PRESSED";
		case 605 :
			return (char *) "RECORD_RELEASED";
		case 606 :
			return (char *) "CRYPTIC_BUTTON_PRESSED";
		case 701 :
			return (char *) "FIRMWARE_CHUNK";
		case 702 :
			return (char *) "CHALLENGE_MISMATCH";
		case 703 :
			return (char *) "CHALLENGE_MATCH";
		case 704 :
			return (char *) "SUP_CHALLENGE_INCORRECT";
		case 705 :
			return (char *) "INVALID_SUP_CMD";
		case 706 :
			return (char *) "SEND_NOK";
		case 707 :
			return (char *) "SET_DEVICE_TIME";
		case 708 :
			return (char *) "SET_TIME_DELTA";
		case 709 :
			return (char *) "SUP_START_PROCEDURE";
		case 710 :
			return (char *) "SUP_END_PROCEDURE";
		case 711 :
			return (char *) "SUP_SOFT_RESET";
		case 712 :
			return (char *) "SUP_RESUME_PROCEDURE";
		case 801 :
			return (char *) "USB_COM_PORT_OPEN";
		case 901 :
			return (char *) "FORMATTING_SD";
		case 902 :
			return (char *) "RTC_PRESCALER_CONFIG_FAILED";
		case 903 :
			return (char *) "RTC_TIME_AT_BOOT";
		case 904 :
			return (char *) "INITIALIZATION_DONE";
		case 905 :
			return (char *) "LOFF_ANNOTATION";
		case 906 :
			return (char *) "LOW_BATTERY";
		case 907 :
			return (char *) "PREREQUISITES_FAILED";
		case 908 :
			return (char *) "RECOVERING_PROCEDURE";
		case 909 :
			return (char *) "REC_PRESSED_WHILE_DONE";
		case 910 :
			return (char *) "USB_CONNECTED";
		case 911 :
			return (char *) "USB_DISCONNECTED";
		case 912 :
			return (char *) "RESET_PLUG";
		case 913 :
			return (char *) "FW_UPDATE_RAW";
		case 914 :
			return (char *) "FW_UPDATE";
		case 915 :
			return (char *) "RESET_WATCHDOG";
		case 916 :
			return (char *) "RESET_OTHER_SOURCE";
		case 917 :
			return (char *) "PROC_STOP_ON_BOOT";
		case 918 :
			return (char *) "15MIN_AUTOSTART";
		case 919 :
			return (char *) "BUTTON_REC_START";
		case 920 :
			return (char *) "STARTUP";
		case 1001 :
			return (char *) "SRAM_GET_STATUS";
		case 1002 :
			return (char *) "SRAM_READ_BYTE";
		case 1003 :
			return (char *) "SRAM_WRITE_PAGE_MODE";
		case 1004 :
			return (char *) "SRAM_BUFFER_TEST";
		case 1005 :
			return (char *) "SRAM_SEQUENTIAL_MODE";
		case 1006 :
			return (char *) "SRAM_SEQUENTIAL_BUFFER_COMP";
		case 1007 :
			return (char *) "SRAM_PASS";
		case 1008 :
			return (char *) "SRAM_FAIL";
		case 1101 :
			return (char *) "SPI1_RX_OVERRUN";
		case 1102 :
			return (char *) "SPI1_RX_TIMEOUT";
		case 1103 :
			return (char *) "NMI";
		case 1104 :
			return (char *) "HARD_FAULT";
		case 1105 :
			return (char *) "MEM_MANAGE_FAULT";
		case 1106 :
			return (char *) "BUS_FAULT";
		case 1107 :
			return (char *) "USAGE_FAULT";
		case 1108 :
			return (char *) "SVC_EXCEPTION";
		case 1109 :
			return (char *) "DEBUG_MONITOR_EXCEPTION";
		case 1110 :
			return (char *) "PEND_SV_EXCEPTION";
		case 1111 :
			return (char *) "TOO_MANY_BAD_SAMPLES";
		case 1201 :
			return (char *) "BAD_DEBUG_LOG_PARAM";
		case 1202 :
			return (char *) "DEBUG_LOG_CLEARED";
		case 1301 :
			return (char *) "ADS_OVERFLOW";
		case 1302 :
			return (char *) "ADS_ERROR_DESTROYING";
		case 1401 :
			return (char *) "FAILED_INIT_ECG_PROCESSING";
		case 1501 :
			return (char *) "AUTO_EVENT_LIMIT_REACHED";
		case 1502 :
			return (char *) "MANUAL_EVENT_LIMIT_REACHED";
		case 1503 :
			return (char *) "CANT_DECREMENT_UNSENT_CNT";
		case 1504 :
			return (char *) "CANT_DECREMENT_UNSENT_MAN";
		case 1505 :
			return (char *) "CANT_DECREMENT_UNSENT_AUTO";
		case 1506 :
			return (char *) "BAD_EVENT_STRUCT";
		case 1601 :
			return (char *) "ANALYSIS_FIFO_OVERFLOW";
		case 1701 :
			return (char *) "SEND_ERROR_MSG";
		case 1801 :
			return (char *) "STARTED_RECORDING";
		case 1802 :
			return (char *) "RESUMED_RECORDING";
		case 1803 :
			return (char *) "CANT_RESUME_PROCEDURE";
		case 1901 :
			return (char *) "DAT_FULL_END";
		case 1902 :
			return (char *) "DAT_WRITE_PROG_OUT_OF_SYNC";
		case 1903 :
			return (char *) "INVALID_LOG_MAP";
		case 1904 :
			return (char *) "CANT_RESUME_DAT";
		case 2001 :
			return (char *) "PUT_ILLEGAL_ANNOTATION";
		case 2002 :
			return (char *) "WFDB_PUT_ILLEGAL_DELTA";
		case 2003 :
			return (char *) "PUT_ANN_SEARCH";
		case 2101 :
			return (char *) "GETTING_BAD_EVENT_TIME";
		case 2201 :
			return (char *) "SD_READ_BLOCK_FAILED";
		case 2202 :
			return (char *) "SD_READ_MULTI_BLKS_FAILED";
		case 2203 :
			return (char *) "SD_WRITE_BLOCK_FAILED";
		case 2204 :
			return (char *) "SD_WRITE_MULTI_BLKS_FAILED";
		case 2205 :
			return (char *) "SD_GO_IDLE_STATE_FAILED";
		default :
			sprintf( buf, "Unknown (%d)", msg );
			return buf;
	}

	return NULL;
}
