#ifndef PROJECT_MACROS_H
#define PROJECT_MACROS_H

#define bswap_32(value) \
    ({register uint32_t ret = (value); \
    __asm__ ("bswap %0": "=r"(ret): "r"(ret): ); \
    ret; });

#endif // PROJECT_MACROS_H
