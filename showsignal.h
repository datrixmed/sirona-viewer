/**
 * @file showsignal.h
*/

#ifndef SHOWSIGNAL
#define SHOWSIGNAL

#include <QFileInfo>
#include <QWidget>

#include "ecgdata.h"
#include "interface.h"

#define VIEWWIDTH		(draw_width)
#define VIEWHEIGHT		(draw_height)

#define STRIPHEIGHT_MM	(100)

#define ECG_DISPLAY_WINDOW_SIZE_SECONDS		(8)

#define CHANNELS_TO_VIEW_CH1	(0x01 << 0)
#define CHANNELS_TO_VIEW_CH2	(0x01 << 1)
#define CHANNELS_TO_VIEW_CH3	(0x01 << 2)
#define CHANNELS_TO_VIEW_ALL	(CHANNELS_TO_VIEW_CH1 | CHANNELS_TO_VIEW_CH2 | CHANNELS_TO_VIEW_CH3)

extern void setWhichChannelsToView( int whichChannelsToView );
extern int whichChannelsToView();
extern int cntViewableChannels( int maxChannels );
extern int isChannelViewable( int chan );




/* {{{ class ShowSignal
   @brief	Displays an ECG signal on the screen
*/
class ShowSignal
	: public QWidget
{
	Q_OBJECT

public:
	ShowSignal( QWidget *parent = 0, EcgData *theEcgData = 0, bool liveEcg = true );
	~ShowSignal();

	QSize minimumSizeHint() const;
	QSize sizeHint() const;

protected:
	void paintEvent( QPaintEvent *event );
	void resizeEvent( QResizeEvent *event );

	float zoom_amount;
	float m_zoom_x;
	float m_zoom_y;

public slots:
	void mousePressEvent( QMouseEvent *event );
	void mouseMoveEvent( QMouseEvent *event );
	void wheelEvent( QWheelEvent *e );
	void keyPressEvent( QKeyEvent *event );

	void smooth_advance();

	void print();
	void printPDF();

	void moveLeft();
	void moveRight();

signals:
	int focusChanged();

public:

	EcgData *m_ecgdata;
	qreal gain_mm_per_mV;

	QString currentFile()
	{
		return curFile;
	}
	QString userFriendlyCurrentFile()
	{
		return strippedName( curFile );
	}
	QString strippedName( const QString &fullFileName )
	{
		return QFileInfo( fullFileName ).fileName();
	}

	void cut()
	{
		return;
	}
	void copy()
	{
		return;
	}
	void paste()
	{
		return;
	}

	long SetPos( long start_time_samps );
	long GetPos()
	{
		return curpos_samples;
	}

	void setPostTrigger( int postTrigger );

private:

	QString curFile;	// used for MDI
	bool isUntitled;	// used for MDI

	bool is_printing;

	bool isLiveEcg;

	QPoint lastPos;
	int xRot;
	int yRot;
	int zRot;

	int m_PostTrigger;

protected:
	void Render( QPainter *dc );
	virtual void ShowGrid( QPainter *dc, int cols, int rows );
	virtual void ShowData( QPainter *dc );
	virtual void ShowAnnotation( QPainter *dc );

	void CreateCableMap();

	long end_time_samps;


private:
	int draw_width, draw_height;
	int draw_xpos, draw_ypos;

	QMap < int, QMap < int, QList<QColor> > >  cableTypeChanColors;

public:

	long curpos_samples;
	QString which_mode;  // used for using class for different modes

	QTimer		*m_timer;
	long	m_time_ends;

	bool	m_testspeed;
	bool	m_test_antialiasing;

};
/* }}} */


#endif // SHOWSIGNAL
