#ifndef CUSTOMER_H
#define CUSTOMER_H

#include <QObject>
#include <QPixmap>
#include <QMap>
#include <QUrl>
#include "interface.h"
#include "utilities.h"


enum tls_type {
	NO_TLS = 0,
	EXPLICIT_TLS = 1,
	IMPLICIT_TLS = 2
};


class CustomerInfo : public QObject
{
	Q_OBJECT

	public:
		CustomerInfo( QObject *parent = NULL );
		~CustomerInfo();

		bool hasConfig( QString encryptedString = "" );
		void load();

	protected:
		QString customerName;

	public:
		QString customCode;
		QString company_name;
		QString product_name;
		QString phone_number;
		QString website;
		QString description;
		QString sv_icon;
		QPixmap logo;
		QString model_number;
		QList<QString> serial_number_prefix;
		qint16 default_pre_trigger;
		qint16 default_post_trigger;
		qint16 default_auto_event_limit;
		qint16 default_manual_event_limit;
		qint16 default_ttm_speed;
		qint16 default_tachy_threshold;
		qint16 default_brady_threshold;
		float default_pause_threshold;
		qint8 default_afib_setting;
		qint32 default_recording_duration;
		qint32 default_mct_recording_duration;
		qint8 show_sample_rate;
		bool review_patient_before_download = false;
		qint16 default_sample_rate;
		bool server_enabled;
		bool mct_server;
		QString server_scheme;
		QString server_host;
		QString server_directory;
		qint32 server_port;
		QString server_user_name;
		QString server_password;
		tls_type server_tls = NO_TLS;
		QUrl network_url_holter;
		QString server_scheme2;
		QString server_host2;
		QString server_directory2;
		qint32 server_port2;
		QString server_user_name2;
		QString server_password2;
		tls_type server_tls2 = NO_TLS;
		QUrl network_url_event;
		QString zip_password;
		QString color_main;
		QString color_accent1;
		QString color_accent2;
		QString color_contrast;
		QString color_caution;
		QString color_disabled;
		int device_key[4];
		int logo_width;
		QList<QString> recording_durations;
		QList<QString> mct_recording_durations;
		bool show_128_sps;
		QString di_number;
		quint8 single_device_mode = 0;
		bool indications = false;
		bool manufacturing = false;
		bool hide_first_name = false;
		bool hide_middle_name = false;
		bool hide_last_name = false;
		bool hide_dob = false;
		bool hide_physician = false;
		bool hide_patient_id = false;
		bool hide_comment = false;
		bool hide_technician = false;
		bool hide_facility = false;
		bool generateResFile = false;
};

#endif // CUSTOMER_H
