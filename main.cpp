/**
 * @file main.cpp
*/


#include <QApplication>
#include <QTranslator>
#include <QPixmap>
#include <QList>

#if defined(Q_OS_WIN)
#include <winsock2.h>
#include "windows.h"
#include <tlhelp32.h>
#endif
#include <signal.h>

#include "Sironaviewerui.h"
#include "mymessagebox.h"
#include "utilities.h"
#include "customer.h"


static void signalHandler( int sig )
{
	switch ( sig ) {
		case SIGILL:
			LOGMSG( "Illegal Instruction" );
			break;
		case SIGFPE:
			LOGMSG( "Floating Point Error" );
			break;
		case SIGSEGV:
			LOGMSG( "Segmentation Fault" );
			break;
		default:
			LOGMSG( "Signal %d caught", sig );
			break;
	}
}

// Check if there is another instance of this application is running
static bool isCurrentApplicationAlreadyRunning( )
{
	int count = false;
#ifdef Q_OS_WIN
	PROCESSENTRY32 entry;
	entry.dwSize = sizeof( PROCESSENTRY32 );

	QString processName = QFileInfo( QApplication::applicationFilePath() ).fileName();

	const wchar_t *we = ( const wchar_t * )processName.constData();

	HANDLE snapshot = CreateToolhelp32Snapshot( TH32CS_SNAPPROCESS, 0 );

	if ( Process32First( snapshot, &entry ) ) {
		do {
			if ( !wcsicmp( entry.szExeFile, we ) ) {
				count++;
			}
		} while ( Process32Next( snapshot, &entry ) );
	}

	CloseHandle( snapshot );
#endif // Q_OS_WIN

	return count > 1;
}

// Registers all signal/slots custom types
static void registerCustomTypes()
{
	qRegisterMetaType<ConfigParams>( "ConfigParams" );
	qRegisterMetaType<EventHeader>( "EventHeader" );
	qRegisterMetaType<QList<QString>>( "QList<QString>" );
	qRegisterMetaType<QList<deviceInfo_t>>( "QList<deviceInfo_t>" );
	qRegisterMetaType<deviceInfo_t>( "deviceInfo_t" );
	qRegisterMetaType<uint32_t>( "uint32_t" );
    qRegisterMetaType<uint16_t>( "uint16_t" );
    qRegisterMetaType<uint8_t>( "uint8_t" );
}

int main( int argc, char *argv[] )
{
	LOGMSG( "Viewer started" );
	signal( SIGILL, signalHandler );
	signal( SIGFPE, signalHandler );
	signal( SIGSEGV, signalHandler );

	QApplication app( argc, argv );

	registerCustomTypes();

	QTranslator myappTranslator;
	myappTranslator.load( "SironaViewer_" + QLocale::system().name() );
	app.installTranslator( &myappTranslator );

	CustomerInfo *pCustomer = new CustomerInfo();

	if ( isCurrentApplicationAlreadyRunning() ) {
		MyMessageBox msgBox;
		msgBox.setText( QObject::tr( "Another instance of this application is already running" ) );
		msgBox.setWindowIcon( QPixmap( pCustomer->sv_icon ) );
		msgBox.setStandardButtons( QMessageBox::Ok );
		msgBox.setAutoClose( true );
		msgBox.setTimeout( 4 ); //Closes after 4 seconds
		msgBox.exec();
		return 0;
	}

	app.setAttribute( Qt::AA_UseHighDpiPixmaps, true );

	SironaViewerUi Sironaviewer( NULL, pCustomer );

	if ( Sironaviewer.isLowDiskSpace() ) {
		return -1;
	}

	Sironaviewer.show();

	int rtn = -1;
	try {
		rtn = app.exec();
	} catch ( ... ) {
		LOGMSG( "Uncaught exception" );
	}
	return rtn;
}


