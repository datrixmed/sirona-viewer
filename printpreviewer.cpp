
#include <QtGui>

#if defined(Q_OS_WIN)
#include <winsock2.h>
#endif

#include "printpreviewer.h"
#include "Sironaviewerui.h"



/** {{{ PdfPrinter::PdfPrinter( QString pathname )
 */
PdfPrinter::PdfPrinter( QString pathname )
{
	qDebug() << "PdfPrinter::PdfPrinter()";
	pathnameReport = pathname;
	ppw = NULL;
	pPrinter = NULL;
	layoutParent = NULL;
	zoomFactor = 1.0;
}
/* }}} */


/** {{{ PdfPrinter::~PdfPrinter()
 */
PdfPrinter::~PdfPrinter()
{
	qDebug() << "PdfPrinter::~PdfPrinter()";

	if ( ppw != NULL ) {
		delete ppw;
	}
	if ( pPrinter != NULL ) {
		delete pPrinter;
	}
}
/* }}} */


/** {{{ PdfPrinter::initPrinter()
 */
void PdfPrinter::initPrinter()
{
	qDebug() << "PdfPrinter::initPrinter()";

	if ( pPrinter ) {
		delete pPrinter;
	}
	/* {{{ setup the PDF printer */
	pPrinter = new QPrinter();
	QString pdfFilename = QDir::cleanPath( QString( "%1/%2.pdf" ).arg( QDir::tempPath() ).arg( REPORT_FILENAME ) );
	if ( ! pathnameReport.isEmpty() ) {
		pdfFilename = pathnameReport;
	}
#ifdef PRINT_DIRECTLY_TO_PRINTER
#else
	pPrinter->setOutputFormat( QPrinter::PdfFormat );
	pPrinter->setOutputFileName( pdfFilename );
#endif
	pPrinter->setPaperSize( QPrinter::Letter );
	pPrinter->setOrientation( QPrinter::Landscape );
	// pPrinter->setPageMargins( 5, 10, 5, 10, QPrinter::Millimeter );
	pPrinter->setPageMargins( 0, 0, 0, 0, QPrinter::Millimeter );
	/* }}} */
}
/* }}} */


/** {{{ PdfPrinter::init()
 */
void PdfPrinter::init()
{
	qDebug() << "PdfPrinter::init()";

	if ( ppw ) {
		disconnect( ppw, SIGNAL( paintRequested( QPrinter * ) ) );
		delete ppw;
	}

	ppw = new QPrintPreviewWidget( pPrinter, this );
	connect( ppw, SIGNAL( paintRequested( QPrinter * ) ), this, SIGNAL( paintRequested( QPrinter * ) ) );

	if ( layoutParent ) {
		layoutParent->insertWidget( 0, ppw );
	}

	zoomFactor = 1.0;

	setFocus();
}
/* }}} */


/** {{{ void PdfPrinter::zoomIn()
 */
void PdfPrinter::zoomIn()
{
	zoomFactor = ppw->zoomFactor() * 1.10;
	if ( zoomFactor >= 10.0 ) {
		zoomFactor = 10.0;
	}
	ppw->setZoomFactor( zoomFactor );
	qDebug() << "zoomFactor =" << zoomFactor;
}
/* }}} */


/** {{{ void PdfPrinter::zoomOut()
 */
void PdfPrinter::zoomOut()
{
	zoomFactor = ppw->zoomFactor() * 0.90;
	if ( zoomFactor <= 0.1 ) {
		zoomFactor = 0.1;
	}
	ppw->setZoomFactor( zoomFactor );
	qDebug() << "zoomFactor =" << zoomFactor;
}
/* }}} */


