#include <QDataStream>
#include <QFile>
#include <stdint.h>
#include <stdio.h>
#include <time.h>
#include "ecglib.h"
#include "utilities.h"


#define ANN_EOF  0
#define ANN_SKIP 59
#define ANN_NUM  60
#define ANN_SUB  61
#define ANN_CHN  62
#define ANN_AUX  63

// From http://physionet.org/physiotools/wfdb/lib/ecgcodes.h
// See also http://www.physionet.org/physiotools/wpg/wpg_32.htm#index-ECG-annotation-code
#define ANN_NORMAL	1	/* normal beat */
#define	ANN_PVC     5	/* premature ventricular contraction */
#define	ANN_FUSION	6	/* fusion of ventricular and normal beat */
#define	ANN_APC     8	/* atrial premature contraction */
#define	ANN_UNKNOWN	13	/* unclassifiable beat */
#define	ANN_NOISE	14	/* signal quality change */
#define ANN_PWAVE	24	/* P-wave peak */
#define	ANN_PACESP	26	/* non-conducted pacer spike */

// Sirona custom annotations
#define ANN_SIGNAL   42     /* change in signal connection (electrode or cable on/off) */
#define ANN_PATIENT  43     /* patient initiated event (patient pressed the "record" button) */
#define ANN_ARRSTART 44     /* arrhythmia start */
#define ANN_ARREND   45     /* arrhythmia end */


static void annotationConvertAtr2Res(char *annotName, ECGLIB_ANNOTATIONSTREAM *annStream)
{
    FILE *fpInput;
    uint32_t sampleNumber = 0;
    uint16_t rawdata16;
    uint8_t rawdatabytes[4];
    bool eofAnn = false;
    uint16_t type = 9999;
//    uint16_t annNum = 0;
//    uint16_t subtyp = 0;
    uint16_t chan = 0;
    uint32_t skipcnt = 0;
    char aux[4096] = {0};

    if ((fpInput = fopen(annotName, "rb")) == NULL) {
        // couldn't open atr file
        return;
    }

    while (!feof(fpInput)) {
        // read 2 bytes at a time
        if (fread(&rawdata16, sizeof(rawdata16), 1, fpInput) != 1) {
            break;
        }

        uint16_t newType = (rawdata16 >>  10) & 0x3F;
        uint16_t newInterval = rawdata16 & 0x3FF;

        switch (newType) {
        case ANN_EOF:
            eofAnn = true;
            break;
        case ANN_SKIP:
            if (fread(rawdatabytes, sizeof(rawdatabytes), 1, fpInput) != 1) {
                eofAnn = true;
                break;
            }
            // interval is in PDP-11 format
            skipcnt += ((unsigned)rawdatabytes[1] << 24) | ((unsigned)rawdatabytes[0] << 16) | ((unsigned)rawdatabytes[3] << 8) | (unsigned)rawdatabytes[2];
            break;
        case ANN_NUM:
//            annNum = newInterval;
            break;
        case ANN_SUB:
//            subtyp = newInterval;
            break;
        case ANN_CHN:
            chan = newInterval;
            break;
        case ANN_AUX:
            // round up to an even number of bytes
            newInterval += (newInterval & 1);
            if (newInterval > sizeof(aux)) {
                // aux is larger than we can handle
                eofAnn = true;
                break;
            }
            if (fread(aux, newInterval, 1, fpInput) != 1) {
                eofAnn = true;
                break;
            }
            break;
        default:
            // process the old annotation before starting the new one
            if (type != 9999) {     // is there an annotation?
                if (type == ANN_PATIENT) {
                    ECGLIB_AnnotationStreamWrite(annStream, sampleNumber, ECGLIB_PATIENT_EVENT, chan, (aux[0] != '\0') ? aux : NULL);
                }
            }
            sampleNumber += newInterval + skipcnt;
            skipcnt = 0;
            type = newType;
//            subtyp = 0;
            aux[0] = 0;
            break;
        }
        if (eofAnn) {
            break;
        }
    }
    // process the last annotation
    if (type != 9999) {             // is there an annotation?
        if (type == ANN_PATIENT) {
            ECGLIB_AnnotationStreamWrite(annStream, sampleNumber, ECGLIB_PATIENT_EVENT, chan, (aux[0] != '\0') ? aux : NULL);
        }
    }

    fclose(fpInput);
}

bool buildRes(char *serialNumber, unsigned channelCount, unsigned sampleRate, uint32_t startTime, QFile *datFile)
{
    ECGLIB_RECORD *record;
    ECGLIB_SIGNALSTREAM *sigStream;
    ECGLIB_ANNOTATIONSTREAM *annStream;
    unsigned channelIndex = 0;
    int16_t sample;
    unsigned short ecgBuffer[3];

    record = ECGLIB_RecordOpen(Utilities::changeExtenstion(datFile->fileName(), "res").toLatin1().data(), ECGLIB_WRITE | ECGLIB_CREAT);
    if (record == NULL) {
        // failed to open the record
        return false;
    }
    sigStream = ECGLIB_SignalStreamCreate(record, "Default", sampleRate * 65536, 10, channelCount,
                                          ECGLIB_CODEC_STOREONLY, 0, 0, (unsigned long)-1, ECGLIB_WRITE | ECGLIB_CREAT);

    if (!datFile->open(QIODevice::ReadOnly)) {
        // couldn't open dat file
        return false;
    }
    QDataStream datStream(datFile);
    datStream.setByteOrder(QDataStream::LittleEndian);
    while (!datFile->atEnd()) {
        quint32 ecgWord;
        datStream >> ecgWord;
        sample = ((ecgWord >> 0) & 0x3FF);
        if (sample >= 512) {
            sample -= 1024;
        }
        ecgBuffer[channelIndex] = (uint16_t)sample + 512;
        if (++channelIndex >= channelCount) {
            ECGLIB_SignalStreamWrite(sigStream, ecgBuffer, 1);
            channelIndex = 0;
        }
        sample = ((ecgWord >> 10) & 0x3FF);
        if (sample >= 512) {
            sample -= 1024;
        }
        ecgBuffer[channelIndex] = (uint16_t)sample + 512;
        if (++channelIndex >= channelCount) {
            ECGLIB_SignalStreamWrite(sigStream, ecgBuffer, 1);
            channelIndex = 0;
        }
        sample = ((ecgWord >> 20) & 0x3FF);
        if (sample >= 512) {
            sample -= 1024;
        }
        ecgBuffer[channelIndex] = (uint16_t)sample + 512;
        if (++channelIndex >= channelCount) {
            ECGLIB_SignalStreamWrite(sigStream, ecgBuffer, 1);
            channelIndex = 0;
        }
    }
    datFile->close();

    annStream = ECGLIB_AnnotationStreamOpen(sigStream, "Info", ECGLIB_WRITE | ECGLIB_CREAT);
    ECGLIB_AnnotationStreamWriteProfileString(annStream, "SignalCreator", "DigiTrak XT", 0);
    ECGLIB_AnnotationStreamWriteProfileString(annStream, "SignalCreatorSN", serialNumber, 0);
    ECGLIB_AnnotationStreamWriteProfileString(annStream, "SignalCreatorFPN", "400-0643-00", 0);
    ECGLIB_AnnotationStreamWriteProfileString(annStream, "SignalCreatorVersion", "C.2", 0);
    ECGLIB_AnnotationStreamWriteProfileString(annStream, "SignalCreatorCFGPN", "400-0644-02", 0);
    ECGLIB_AnnotationStreamWriteProfileString(annStream, "SignalCreatorCFGVersion", "C.2", 0);
    ECGLIB_AnnotationStreamWriteProfileString(annStream, "MinEqualsPhys", "-0.00500", 0);
    ECGLIB_AnnotationStreamWriteProfileString(annStream, "MaxEqualsPhys", "0.00500", 0);
    ECGLIB_AnnotationStreamWriteProfileU32(annStream, "SignalCreationTimeLocal", startTime, 0);
    ECGLIB_AnnotationStreamClose(annStream);

    annStream = ECGLIB_AnnotationStreamOpen(sigStream, "Events", ECGLIB_WRITE | ECGLIB_CREAT);
    annotationConvertAtr2Res(Utilities::changeExtenstion(datFile->fileName(), "atr").toLatin1().data(), annStream);
    ECGLIB_AnnotationStreamClose(annStream);

    ECGLIB_SignalStreamClose(sigStream);
    ECGLIB_RecordClose(record);
    return true;
}
