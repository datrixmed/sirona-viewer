param (
		[string]$IconName = "datrix-sironaViewer.ico"
	  )

$sh = New-Object -ComObject ("WScript.Shell")
$ShortcutDest = $sh.SpecialFolders("Desktop")

$ShortcutName = "Sirona Viewer DAB"
$ExePath = "C:\Program Files (x86)\Datrix Sirona Viewer"
$IconName = $IconName.split('\\|/')[-1]
$IconLocation = "$ExePath\images\$IconName"

$Shortcut = $sh.CreateShortcut( "$ShortCutDest\$ShortcutName.lnk")

$Shortcut.WindowStyle = 1
$Shortcut.Hotkey = "CTRL+SHIFT+S"
$Shortcut.Description = "Shortcut to $ShortcutName"
$Shortcut.TargetPath = "$ExePath\SironaViewer.exe"
$Shortcut.WorkingDirectory = $ExePath
$Shortcut.IconLocation = "$IconLocation, 0"

$Shortcut.Save()
