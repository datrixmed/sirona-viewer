#include <QJsonDocument>
#include <QStorageInfo>
#include <QJsonObject>
#include <QMessageBox>
#include <QDateTime>
#include <QDir>

#include "Sironaviewerui.h"
#include "devicehandler.h"
#include "utilities.h"
#include "zip.h"
#include "iowin32.h"
#include "resbuilder.h"
#include "logdecode.h"
#include "floatingdisplay.h"

#define IDLE_PING_INTERVAL_MS 1000

QList<QString> httpFileList;

quint32 endian_swap( quint32 x )
{
	return ( x >> 24 ) | ( ( x << 8 ) & 0x00FF0000 ) | ( ( x >> 8 ) & 0x0000FF00 ) | ( x << 24 );
}

QString getEventTypeString( quint32 type )
{
	QString ret;

	switch ( type ) {
		case 0:
			ret = "Manual";
			break;
		case 1:
			ret = "Automatic - BRADY";
			break;
		case 2:
			ret = "Automatic - TACHY";
			break;
		case 4:
			ret = "Automatic - PAUSE";
			break;
		case 5:
			ret = "Manual - Asymptomatic";
			break;
		case 6:
			ret = "Manual - Symptomatic";
			break;
		case 16:
			ret = "Automatic - ARRHYTHMIA_AF";
			break;
		case 32:
			ret = "Automatic - ARRHYTHMIA_AF_STOP";
			break;
		default:
			ret = "Unknown";
			break;
	}

	return ret;
}

DeviceHandler::DeviceHandler( QObject *parent ) :
	QObject( parent ),
	m_TerminateStreaming( false ),
	m_TerminateLiveECG( false ),
	m_ConnectionTimer( 0 ),
	m_StopWaitingForStorage( false )
{
	LOGMSG( "DeviceHandler" );
	memset( &devInfo, 0, sizeof( devInfo ) );
}

void DeviceHandler::enumerateBtDevices()
{
	int retVal = SIRONA_DRIVER_NO_ERROR;
	sirona_enum_handle enum_handle = nullptr;
	QList<deviceInfo_t> devices;

	LOGMSG("enumerateBtDevices()");

	retVal = sironadriver_enum_open( &enum_handle, SIRONA_DRIVER_DEVICE_BLUETOOTH );

	if ( retVal == SIRONA_DRIVER_NO_ERROR ) {

		deviceInfo_t device;

		memset( &device, 0, sizeof( device ) );

		while ( ( retVal = sironadriver_enum_next( enum_handle, device.devSerialNumber, device.devPortName, &device.devType ) ) == SIRONA_DRIVER_NO_ERROR ) {
			devices.append( device );
			LOGMSG("enumerateBtDevices()   found device: %s", device.devSerialNumber );
		}

		if ( retVal != SIRONA_DRIVER_NO_MORE_DEVICES ) {
			emit sDeviceHandlerMessage( true, false, tr( "Error [100]: " ) + sironadriver_error_get_string( retVal ) );
		}

		sironadriver_enum_close( enum_handle );
	} else {
		emit sDeviceHandlerMessage( true, false, tr( "Error [101]: " ) + sironadriver_error_get_string( retVal ) );
	}

	emit sDeviceEnumerateBtDevices( devices );
}

void DeviceHandler::connectDevice( deviceInfo_t device )
{
	int retVal = SIRONA_DRIVER_NO_ERROR;
	int tmpType = device.devType;
	if (manufacturing) {            // manufacturing uses the skeleton key so the type is different
		if (tmpType == SIRONA_DRIVER_DEVICE_USB) {
			tmpType = 4;
		} else {
			tmpType = 5;
		}
	}

	devInfo = device;

	retVal = sironadriver_open(tmpType, device.devPortName, &devInfo.handle, device_key);
	if ( retVal != SIRONA_DRIVER_NO_ERROR ) {
		deviceDisconnect( false, retVal );
		return;
	}

	emit sDeviceConnectStatus( true, devInfo.devSerialNumber, device.devType );
}

void DeviceHandler::deviceDisconnect( bool silent, int retVal )
{
	LOGMSG( "deviceDisconnect(%d, %d)", ( int )silent, retVal );
	if ( ( retVal != SIRONA_DRIVER_NO_ERROR ) && ( !silent ) ) {
		emit sDeviceHandlerMessage( false, false, tr( "Error [102]: " ) + sironadriver_error_get_string( retVal ) );
	}

	if ( devInfo.handle ) {
		sironadriver_close( devInfo.handle );
		memset( &devInfo, 0, sizeof( devInfo ) );
	}

	emit sDeviceConnectStatus( false, devInfo.devSerialNumber, 0 );

	stopReconnectTimer();
}


void DeviceHandler::isDeviceConnected()
{
	//LOGMSG( "isDeviceConnected" );
	// Enum USB connection if not connected.

	if ( sironadriver_get_type( devInfo.handle ) != SIRONA_DRIVER_DEVICE_USB ) {
		return;
	}

	int retVal = SIRONA_DRIVER_NO_ERROR;

#define MAX_RETRIES	(5)

	int retries = 0;
	for ( retries = 0 ; retries < MAX_RETRIES ; retries++ ) {
		retVal = sironadriver_device_ping( devInfo.handle );
		if ( retVal == SIRONA_DRIVER_NO_ERROR ) {
			break;
		}
		LOGMSG("Ping Failure: retrying %d out of %d attempts.", retries, MAX_RETRIES );
		QThread::msleep( 10 );
	}
	if ( retries >= MAX_RETRIES ) {
		LOGMSG("Device disconnect due to Ping failure.");
		deviceDisconnect( true, retVal );
	}
}


void DeviceHandler::devicePopulateConfig( ConfigParams devConf )
{
	LOGMSG( "devicePopulateConfig" );
	devConfig = devConf;
}

void DeviceHandler::getRequiredFields()
{
	uint8_t first_name_state;
	uint8_t last_name_state;
	uint8_t dob_state;
	uint8_t physician_state;
	uint8_t patient_id_state;
	uint8_t middle_name_state;
	uint8_t comment_state;
	uint8_t technician_state;
	uint8_t facility_state;

	// ignore errors, since sironadriver_get_patient_field_state will just mark the field as unsupported
	sironadriver_get_patient_field_state(devInfo.handle, (char*)"PAT_NAME", &first_name_state);
	sironadriver_get_patient_field_state(devInfo.handle, (char*)"PAT_LAST", &last_name_state);
	sironadriver_get_patient_field_state(devInfo.handle, (char*)"PAT_DOB", &dob_state);
	sironadriver_get_patient_field_state(devInfo.handle, (char*)"PHY_NAME", &physician_state);
	sironadriver_get_patient_field_state(devInfo.handle, (char*)"PAT_ID", &patient_id_state);
	sironadriver_get_patient_field_state(devInfo.handle, (char*)"PAT_MIDDLE", &middle_name_state);
	sironadriver_get_patient_field_state(devInfo.handle, (char*)"COMMENT", &comment_state);
	sironadriver_get_patient_field_state(devInfo.handle, (char*)"TECH_NAME", &technician_state);
	sironadriver_get_patient_field_state(devInfo.handle, (char*)"FACILITY", &facility_state);

	emit requiredFields(first_name_state, last_name_state, dob_state, physician_state, patient_id_state, middle_name_state, comment_state, technician_state, facility_state);
}

void DeviceHandler::getStatus()
{
	int retval;
	uint8_t procedure_type;
	uint8_t procedure_state;
	uint8_t procedure_leads;
	uint8_t cable_channels;
	uint8_t cable_status;
	uint8_t cable_type;

	if (devInfo.handle != 0) {          // in case a status timer is still running while disconnecting
		retval = sironadriver_get_status(devInfo.handle, &procedure_type, &procedure_state, &procedure_leads, &cable_channels, &cable_status, &cable_type);
		if (retval == SIRONA_DRIVER_NO_ERROR) {
			emit sDeviceStatus(cable_status, cable_type, procedure_state, procedure_type);
		}
	}
}

void DeviceHandler::deviceIsThereRecordedData()
{
	LOGMSG( "deviceIsThereRecordedData" );
	int retVal = SIRONA_DRIVER_NO_ERROR;
	uint32_t proc_time = 0;
	char parameterName[] = "PROC_TIME";

	retVal = sironadriver_parameter_read(devInfo.handle, sizeof(parameterName), parameterName, sizeof(uint32_t), &proc_time);
	// LOGMSG( "sironadriver_parameter_read(devInfo.handle, sizeof(parameterName), %s, sizeof(uint32_t), &proc_time) -> %d", parameterName, retVal );
	if ( retVal != SIRONA_DRIVER_NO_ERROR ) {
		LOGMSG("Device disconnect due to PROC_TIME parameter read failure.");
		deviceDisconnect( false, retVal );
	}

	bool hasData = proc_time > 0;

#ifdef ANTIQUE
	if ( (devConfig.device_mode == PROCTYPE_EVENT) && (deviceEventCount() <= 0) ) {
		hasData = false;
	}
#else
#endif
	emit sDeviceThereIsDataRecorded( hasData );

	resetReconnectTimer();
}


int DeviceHandler::deviceEventCount()
{
	quint32 eventCount = 0;

	int retVal = sironadriver_event_get_count( devInfo.handle, &eventCount );
	if ( retVal != SIRONA_DRIVER_NO_ERROR ) {
		eventCount = 0;
	}

	return eventCount;
}


void DeviceHandler::deviceSaveData( bool saveEvents, QString eventPath, bool saveHolter, QString holterPath )
{
	LOGMSG( "deviceSaveData %d %s %d %s", saveEvents, eventPath.toLatin1().data(), saveHolter, holterPath.toLatin1().data() );
	class guard_storage
	{
	public:
		guard_storage( DeviceHandler *obj ) : obj( obj )
		{
			obj->m_StopWaitingForStorage = false;
		}
		guard_storage( guard_storage const & ) = delete;
		guard_storage &operator=( guard_storage const & ) = delete;
		~guard_storage()
		{
			obj->m_StopWaitingForStorage = false;
		}

		bool available( QString path, quint32 size, QString message )
		{
			while ( !QThread::currentThread()->isInterruptionRequested() && !obj->m_StopWaitingForStorage ) {
				if ( QStorageInfo( path ).bytesAvailable() <= size ) {
					emit obj->sDeviceComputerOutOfSpace( message );
				} else {
					return true;
				}
				QThread::sleep( 1 );
			}
			return false;
		}
	private:
		DeviceHandler *obj;
	} checkWaitStorage { this };

	int retVal = SIRONA_DRIVER_NO_ERROR;
	bool eventSavedWithSuccess = false;
	if ( saveEvents ) {
		quint32 eventCount = 0;

		emit sDeviceDownloadProgress( 0, 0 );

		retVal = sironadriver_event_get_count( devInfo.handle, &eventCount );

		LOGMSG( "deviceSaveData()  total of eventCount = %d ", eventCount );

		if ( retVal == SIRONA_DRIVER_NO_ERROR ) {
			if ( eventCount ) {
				eventSavedWithSuccess = false;
				httpFileList.clear();

				for ( quint32 eventNo = 0; eventNo < eventCount; eventNo++ ) {
					EventHeader eventHeader;
					retVal = deviceEventGetHeader( eventNo, eventHeader );
					if ( retVal != SIRONA_DRIVER_NO_ERROR ) {
						LOGMSG( "deviceSaveData()  eventNo: %d    SironaDriverError = %d", eventNo, retVal );
						break;
					}

					emit sDeviceDownloadProgress( eventNo, eventCount );

					for ( int retry = 0; retry < 3; retry++ ) {
						retVal = eventDownload( eventPath, eventNo, eventHeader, eventCount );
						if ( retVal == SIRONA_DRIVER_NO_ERROR ) {
							eventSavedWithSuccess = true;

							break;
						}

						LOGMSG( "deviceSaveData()  eventNo: %d    trying to download... retry = %d SironaDriverError = %d", eventNo, retry, retVal );
						QThread::msleep( 100 );
					}

					if ( ! eventSavedWithSuccess ) {
						break;
					}
				}

				if ( eventSavedWithSuccess ) {
					emit sDeviceDownloadProgress( eventCount, eventCount );
				} else {
					emit sDeviceHandlerMessage( false, false, tr( "Error [103]: Error happened during event download." ) );
					deviceDisconnect( true, retVal );
				}

			} else {
				//emit sDeviceHandlerMessage( false, false, tr( "Error [104]: There are no Events recorded on the device." ) );
				LOGMSG("There are no Events recorded on the device.");
			}
		} else {
			LOGMSG("Disconnect due to event get count failure.");
			deviceDisconnect( false, retVal );
		}
	}

	bool holterSavedWithSuccess = false;
	if ( saveHolter ) {
		uint32_t procedureTime;
		char parName[] = "PROC_TIME";
		retVal = sironadriver_parameter_read( devInfo.handle, sizeof(parName), parName, sizeof(procedureTime), &procedureTime );
		if ( retVal == SIRONA_DRIVER_NO_ERROR ) {
			LOGMSG( "deviceSaveData()  downloading holter to %s from procedureTime of %ld...", holterPath.toLatin1().data(), (long)procedureTime );
			retVal = holterDownload( holterPath, procedureTime );
			if ( retVal == SIRONA_DRIVER_NO_ERROR ) {
				LOGMSG( "deviceSaveData()  download of holter successful" );
				holterSavedWithSuccess = true;
			}
		} else {
			LOGMSG( "deviceSaveData()  ERROR when trying to download holter.    SironaDriverError = %d", retVal );
			deviceDisconnect( false, retVal );
		}
	}

	emit sDeviceDataSaved ( eventSavedWithSuccess, holterSavedWithSuccess, httpFileList );

	resetReconnectTimer();
}

void DeviceHandler::deviceDeleteData()
{
	LOGMSG( "deviceDeleteData" );
	int retVal = SIRONA_DRIVER_NO_ERROR;
	bool deleteSuccess = false;

	retVal = sironadriver_soft_reset(devInfo.handle);
	if ( retVal == SIRONA_DRIVER_NO_ERROR ) {
		deleteSuccess = true;
	}

	emit sDeviceDataDeleted ( deleteSuccess );

	resetReconnectTimer();
}

void DeviceHandler::deviceParameterRead( QString parName )
{
	LOGMSG( "DeviceHandler::deviceParameterRead(%s)", parName.toLatin1().data() );
	int retVal = SIRONA_DRIVER_NO_ERROR;
	char parValue[20] = {0};
	QByteArray parName_ba = parName.toLocal8Bit();      // when converting from QString to char*, we need to keep the QByteArray in a variable so it stays in memory
	char *parName_cstr = parName_ba.data();

	retVal = sironadriver_parameter_read(devInfo.handle, parName.size(), parName_cstr, sizeof(parValue), parValue);
	if ( retVal == SIRONA_DRIVER_NO_ERROR ) {
		emit sDeviceHandlerMessage( true, true, tr( "INFO: Read parameter command executed with success." ) );
		populateConfigEntry(devConfig, (void*)parName_cstr, sizeof(parValue), parValue);
		emit sDeviceConfiguration( devConfig );
		resetReconnectTimer();
	} else {
		LOGMSG("Disconnect due to parameter read failure.");
		deviceDisconnect( false, retVal );
	}
}

int DeviceHandler::parameterWrite( const QString &parName )
{
	LOGMSG( "DeviceHandler::parameterWrite(%s)", parName.toLatin1().data() );

	int retVal = SIRONA_DRIVER_NO_ERROR;
	QByteArray parName_ba = parName.toLocal8Bit();      // when converting from QString to char*, we need to keep the QByteArray in a variable so it stays in memory
	char *parName_cstr = parName_ba.data();

	if ( parName.compare( "DEV_ID" ) == 0 ) {
		char devSerNum[5] = {0};
		memcpy( devSerNum, &devConfig.device_id, 4 );
		memcpy( devSerNum + 4, &devConfig.class_id, 1 );
		retVal = sironadriver_parameter_write(devInfo.handle, parName.size(), parName_cstr, 5, (void*)devSerNum);
	} else if ( parName.compare( "DEV_MODE" ) == 0 ) {
		retVal = sironadriver_parameter_write(devInfo.handle, parName.size(), parName_cstr, sizeof(quint8), (void*)&devConfig.device_mode);
#ifdef ANTIQUE
	} else if ( parName.compare( "CABLE_ID" ) == 0 ) {
		retVal = sironadriver_parameter_write(devInfo.handle, parName.size(), parName_cstr, sizeof(quint8), (void*)&devConfig.cable_id);
#endif
	} else if ( parName.compare( "PRE_TRIGGER" ) == 0 ) {
		retVal = sironadriver_parameter_write(devInfo.handle, parName.size(), parName_cstr, sizeof(quint16), (void*)&devConfig.pre_trigger);
	} else if ( parName.compare( "POST_TRIGGER" ) == 0 ) {
		retVal = sironadriver_parameter_write(devInfo.handle, parName.size(), parName_cstr, sizeof(quint16), (void*)&devConfig.post_trigger);
	} else if ( parName.compare( "TIME_ZONE" ) == 0 ) {
		retVal = sironadriver_parameter_write(devInfo.handle, parName.size(), parName_cstr, sizeof(quint16), (void*)&devConfig.time_zone);
	} else if ( parName.compare( "DOWN_COMP" ) == 0 ) {
		retVal = sironadriver_parameter_write(devInfo.handle, parName.size(), parName_cstr, sizeof(quint8), (void*)&devConfig.download_complete);
	} else if ( parName.compare( "REC_LENGTH" ) == 0 ) {
		retVal = sironadriver_parameter_write(devInfo.handle, parName.size(), parName_cstr, sizeof(quint16), (void*)&devConfig.recording_len);
	} else if ( parName.compare( "AUTO_EV_LIMIT" ) == 0 ) {
		retVal = sironadriver_parameter_write(devInfo.handle, parName.size(), parName_cstr, sizeof(quint16), (void*)&devConfig.automatic_event_limit);
	} else if ( parName.compare( "MAN_EV_LIMIT" ) == 0 ) {
		retVal = sironadriver_parameter_write(devInfo.handle, parName.size(), parName_cstr, sizeof(quint16), (void*)&devConfig.manual_event_limit);
	} else if ( parName.compare( "AUDIO_MUTE" ) == 0 ) {
		retVal = sironadriver_parameter_write(devInfo.handle, parName.size(), parName_cstr, sizeof(quint8), (void*)&devConfig.audiable_on);
	} else if ( parName.compare( "TACHY_ON_FLG" ) == 0 ) {
		retVal = sironadriver_parameter_write(devInfo.handle, parName.size(), parName_cstr, sizeof(quint8), (void*)&devConfig.analyzer_tachy_on);
	} else if ( parName.compare( "TACHY_THRESH" ) == 0 ) {
		retVal = sironadriver_parameter_write(devInfo.handle, parName.size(), parName_cstr, sizeof(quint16), (void*)&devConfig.analyzer_tachy_threshold);
	} else if ( parName.compare( "BRADY_ON_FLG" ) == 0 ) {
		retVal = sironadriver_parameter_write(devInfo.handle, parName.size(), parName_cstr, sizeof(quint8), (void*)&devConfig.analyzer_brady_on);
	} else if ( parName.compare( "BRADY_THRESH" ) == 0 ) {
		retVal = sironadriver_parameter_write(devInfo.handle, parName.size(), parName_cstr, sizeof(quint16), (void*)&devConfig.analyzer_brady_threshold);
	} else if ( parName.compare( "PAUSE_ON_FLG" ) == 0 ) {
		retVal = sironadriver_parameter_write(devInfo.handle, parName.size(), parName_cstr, sizeof(quint8), (void*)&devConfig.analyzer_pause_on);
	} else if ( parName.compare( "PAUSE_THRESH" ) == 0 ) {
		retVal = sironadriver_parameter_write(devInfo.handle, parName.size(), parName_cstr, sizeof(quint16), (void*)&devConfig.analyzer_pause_threshold);
	} else if ( parName.compare( "AF_ON_FLG" ) == 0 ) {
		retVal = sironadriver_parameter_write(devInfo.handle, parName.size(), parName_cstr, sizeof(quint8), (void*)&devConfig.analyzer_af_on);
	} else if ( parName.compare( "ANAL_CH_MASK" ) == 0 ) {
		retVal = sironadriver_parameter_write(devInfo.handle, parName.size(), parName_cstr, sizeof(quint8), (void*)&devConfig.analyzer_channel_mask);
	} else if ( parName.compare( "TTM_EN_FLG" ) == 0 ) {
		retVal = sironadriver_parameter_write(devInfo.handle, parName.size(), parName_cstr, sizeof(quint8), (void*)&devConfig.ttm_enabled);
	} else if ( parName.compare( "TTM_3X_FLG" ) == 0 ) {
		retVal = sironadriver_parameter_write(devInfo.handle, parName.size(), parName_cstr, sizeof(quint8), (void*)&devConfig.ttm_speedup_on);
	} else if ( parName.compare( "SAMP_RATE" ) == 0 ) {
		retVal = sironadriver_parameter_write(devInfo.handle, parName.size(), parName_cstr, sizeof(quint16), (void*)&devConfig.sample_rate);
	} else if ( parName.compare( "PAT_DOB" ) == 0 ) {
		retVal = sironadriver_parameter_write(devInfo.handle, parName.size(), parName_cstr, sizeof(qint32), (void*)&devConfig.patient_date_of_birth);
	} else if ( parName.compare( "PAT_ID" ) == 0 ) {
		QByteArray ba = devConfig.patient_id.toUtf8();
		retVal = sironadriver_parameter_write(devInfo.handle, parName.size(), parName_cstr, ba.length() + 1, (void*)ba.constData());
	} else if ( parName.compare( "PAT_NAME" ) == 0 ) {
		QByteArray ba = devConfig.patient_first_name.toUtf8();
		retVal = sironadriver_parameter_write(devInfo.handle, parName.size(), parName_cstr, ba.length() + 1, (void*)ba.constData());
	} else if ( parName.compare( "PAT_MIDDLE" ) == 0 ) {
		QByteArray ba = devConfig.patient_middle_initial.toUtf8();
		retVal = sironadriver_parameter_write(devInfo.handle, parName.size(), parName_cstr, ba.length() + 1, (void*)ba.constData());
	} else if ( parName.compare( "PAT_LAST" ) == 0 ) {
		QByteArray ba = devConfig.patient_last_name.toUtf8();
		retVal = sironadriver_parameter_write(devInfo.handle, parName.size(), parName_cstr, ba.length() + 1, (void*)ba.constData());
	} else if ( parName.compare( "PHY_NAME" ) == 0 ) {
		QByteArray ba = devConfig.physician_name.toUtf8();
		retVal = sironadriver_parameter_write(devInfo.handle, parName.size(), parName_cstr, ba.length() + 1, (void*)ba.constData());
	} else if ( parName.compare( "COMMENT" ) == 0 ) {
		QByteArray ba = devConfig.comment.toUtf8();
		retVal = sironadriver_parameter_write(devInfo.handle, parName.size(), parName_cstr, ba.length() + 1, (void*)ba.constData());
	} else if ( parName.compare( "TECH_NAME" ) == 0 ) {
		QByteArray ba = devConfig.technician_name.toUtf8();
		// don't set retVal, since an error just means it isn't supported by the firmware
		sironadriver_parameter_write(devInfo.handle, parName.size(), parName_cstr, ba.length() + 1, (void*)ba.constData());
	} else if ( parName.compare( "FACILITY" ) == 0 ) {
		QByteArray ba = devConfig.facility.toUtf8();
		// don't set retVal, since an error just means it isn't supported by the firmware
		sironadriver_parameter_write(devInfo.handle, parName.size(), parName_cstr, ba.length() + 1, (void*)ba.constData());
	} else if ( parName.compare( "MEDICATION_NOTES" ) == 0 ) {
		// don't set retVal, since an error just means it isn't supported by the firmware
		/* do nothing because this param is only processed locally and not communicated to Sirona */
	} else if ( parName.compare( "DIAGNOSIS" ) == 0 ) {
		// don't set retVal, since an error just means it isn't supported by the firmware
		/* do nothing because this param is only processed locally and not communicated to Sirona */
	} else if ( parName.compare( "DIARY_ENTRIES" ) == 0 ) {
		// don't set retVal, since an error just means it isn't supported by the firmware
		/* do nothing because this param is only processed locally and not communicated to Sirona */
	} else {
		emit sDeviceHandlerMessage(false, false, tr("Error [105]: Invalid parameter or parameter not writable."));
	}

	if ( retVal != SIRONA_DRIVER_NO_ERROR ) {
		LOGMSG("Disconnect due to parameter write failure.");
		deviceDisconnect( false, retVal );
	}

	return retVal;
}

void DeviceHandler::deviceParameterListWrite( ConfigParams devConf, QList<QString> paramNamesList )
{
	LOGMSG( "deviceParameterListWrite" );
	int retVal = SIRONA_DRIVER_NO_ERROR;

	devicePopulateConfig( devConf );

	for ( auto paramName : paramNamesList ) {
		retVal = parameterWrite( paramName );
		if ( retVal != SIRONA_DRIVER_NO_ERROR ) {
			emit sDeviceParameterListWritten ( false );
			resetReconnectTimer();
			return;
		}
	}

	retVal = parameterCommit();
	if ( retVal == SIRONA_DRIVER_NO_ERROR ) {
		emit sDeviceParameterListWritten ( true );
	} else {
		emit sDeviceParameterListWritten ( false );
	}

	resetReconnectTimer();
}

int DeviceHandler::parameterCommit()
{
	LOGMSG( "deviceParameterCommit" );
	int retVal = SIRONA_DRIVER_NO_ERROR;

	retVal = sironadriver_parameter_commit( devInfo.handle );
	if ( retVal == SIRONA_DRIVER_NO_ERROR ) {
		emit sDeviceHandlerMessage( true, true, tr( "INFO: Commit parameters command executed with success." ) );
		emit sDeviceConfiguration( devConfig );
	} else {
		LOGMSG("Disconnect due to parameter commit failure.");
		deviceDisconnect( false, retVal );
	}
	return retVal;
}

void DeviceHandler::deviceParameterReadAll()
{
	LOGMSG( "deviceParameterReadAll" );
	int retVal = SIRONA_DRIVER_NO_ERROR;
	quint32 numberOfParameters = 0;

	retVal = sironadriver_get_number_of_parameters( devInfo.handle, &numberOfParameters );
	if ( retVal == SIRONA_DRIVER_NO_ERROR ) {

		quint32 i = 0;
		for ( i = 0; i < numberOfParameters; i++ ) {

			quint32 nameSize = 0;
			char name[20] = {0};
			quint32 valueSize = 0;
			char value[500] = {0};  // Comment field can be 500 bytes

			memset( name, 0, sizeof( name ) );
			memset( value, 0, sizeof( value ) );

			nameSize = sizeof( name );
			valueSize = sizeof( value );

			retVal = sironadriver_parameter_read_index( devInfo.handle, i, &nameSize, name, &valueSize, value );
			if ( retVal == SIRONA_DRIVER_NO_ERROR ) {
				populateConfigEntry ( devConfig, name, valueSize, value );
				resetReconnectTimer();

			} else {
				LOGMSG("Device disconnect due to parameter read index failure.");
				deviceDisconnect( false, retVal );
				break;
			}
		}

		if ( retVal == SIRONA_DRIVER_NO_ERROR ) {
			LOGMSG(tr("Read all parameters command executed with success.").toLatin1().data());
			LOGMSG(QString("Firmware %1.%2.%3").arg(devConfig.fw_rev & 0xFF).arg(devConfig.fw_rev >> 8).arg(devConfig.svn_rev).toLatin1().data());
			emit sDeviceConfiguration( devConfig );
			resetReconnectTimer();
		}

	} else {
		LOGMSG("Disconnect due to get number of parameters failure.    sironadriver_get_number_of_parameters() -> %d", retVal );
		deviceDisconnect( false, retVal );
	}
}

void DeviceHandler::deviceStopWaitingForStorage()
{
	LOGMSG( "StopWaitingForStorage" );
	m_StopWaitingForStorage = true;
}

int DeviceHandler::deviceEventGetHeader( quint32 eventNo, EventHeader &eventHeader )
{
	int retVal = SIRONA_DRIVER_NO_ERROR;
	uint32_t ev_type;
	uint32_t ev_time;
	uint32_t ev_size;
	uint8_t ev_chan;
	uint8_t ev_sent;

	retVal = sironadriver_get_event_params(devInfo.handle, eventNo, &ev_type, &ev_time, &ev_size, &ev_chan, &ev_sent);
	if (retVal == SIRONA_DRIVER_NO_ERROR) {
		eventHeader.event_type = ev_type;
		eventHeader.creation_time = ev_time;
		eventHeader.sent = ev_sent;
		eventHeader.ev_size = ev_size;
		eventHeader.no_channels = ev_chan;
	} else {
		LOGMSG("Disconnect due to event get header failure.");
		deviceDisconnect( false, retVal );
		return retVal;
	}
	emit sDeviceHandlerMessage( true, true, tr( "INFO: Get Event Header command, for Event nr.%1 executed with success." ).arg( eventNo ) );
	LOGMSG( "deviceEventGetHeader type:%d time:%d size:%d chan:%d sent:%d", ev_type, ev_time, ev_size, ev_chan, ev_sent );
	return retVal;
}

int DeviceHandler::eventDownload( const QString &path, quint32 eventNo, EventHeader eventHeader, quint32 eventCount )
{
	LOGMSG( "eventDownload %s %d %d %d", path.toLatin1().data(), eventNo, eventHeader.creation_time, eventCount );
	int retVal = SIRONA_DRIVER_NO_ERROR;

	if ( eventHeader.ev_size <= 0 ) {
		emit sDeviceHandlerMessage( false, false, tr( "Error [106]: Invalid event size returned by the device.\nCommand execution cannot proceed." ) );
		return retVal;
	}

	if ( eventHeader.sent != EVENT_UNSENT ) {
		LOGMSG("Event already sent (%d, %d)", eventNo, eventHeader.sent);
		return retVal;
	}

	uint32_t receivedChunkSize = 0;
	QByteArray ba( 10*1024, '\0' );
	char *receivedChunkData = ba.data();

	QString datFileName = QDir::cleanPath( QString( "%1\\%2%3_%4_%5.dat" )
		.arg( path )
		.arg( devInfo.devSerialNumber )
		.arg( ( ! devConfig.sitecode.isEmpty() ) ? QString("_%1").arg(devConfig.sitecode) : "" )
		.arg( QString( devConfig.patient_id ).replace( QRegExp( "\\W" ), "_" ) )
		.arg( QDateTime::fromTime_t( eventHeader.creation_time - devConfig.pre_trigger ).toString( "yyyyMMddhhmmss" ))
	);


	QFile eventFile( datFileName );
	QFile annotFile( Utilities::changeExtenstion( datFileName, "atr" ) );

	httpFileList.append( datFileName );

	createHeaFile( devConfig.device_mode, datFileName, &eventHeader );

	if ( !eventFile.open( QIODevice::WriteOnly ) ) {
		emit sDeviceHandlerMessage( false, false, tr( "Error [107]: Couldn't open event file to write to.\nCommand execution cannot proceed." ) );
		return -1;
	}

	retVal = sironadriver_ecg_start_transfer( devInfo.handle, eventNo, 0, 0 );
	if ( retVal != SIRONA_DRIVER_NO_ERROR ) {
		LOGMSG("Error starting event download (%d).", eventNo);
		eventFile.close();
		Utilities::removeAllProcedureFiles(datFileName);
		deviceDisconnect( false, retVal );
		return retVal;
	}

	uint32_t chunkSequenceNumber = BULK_LAST_CHUNK;
	uint32_t seq = 0;
	bool lastChunkReadWithSuccess = false;
	bool commError = false;
	do {
		retVal = sironadriver_bulk_read_data( devInfo.handle,  &seq, &receivedChunkSize, receivedChunkData );
		if ( retVal != SIRONA_DRIVER_NO_ERROR ) {
			commError = true;
		} else if ( seq == BULK_LAST_CHUNK ) {
			lastChunkReadWithSuccess = true;
		} else if ( seq != chunkSequenceNumber + 1 ) {
			commError = true;
			retVal = SIRONA_DRIVER_STREAM_STOPPED;
		}
		if (commError) {
			eventFile.close();
			Utilities::removeAllProcedureFiles(datFileName);
			QThread::msleep( 100 );
			sironadriver_ecg_stop_transfer( devInfo.handle );
			LOGMSG("commError in event download (%d)", eventNo);
			return retVal;
		}
		chunkSequenceNumber++;
		eventFile.write( receivedChunkData, receivedChunkSize );
	} while ( !lastChunkReadWithSuccess );

	eventFile.close();

	if ( !annotFile.open( QIODevice::WriteOnly ) ) {
		Utilities::removeAllProcedureFiles(datFileName);
		emit sDeviceHandlerMessage( false, false, tr( "Error [108]: Couldn't open annotation file to write to.\nCommand execution cannot proceed." ) );
		return -1;
	}


	retVal = sironadriver_event_annotation_start_transfer( devInfo.handle, eventNo);
	if ( retVal != SIRONA_DRIVER_NO_ERROR ) {
		LOGMSG("Disconnect due to error starting annotation transfer.");
		annotFile.close();
		Utilities::removeAllProcedureFiles(datFileName);
		deviceDisconnect( false, retVal );
		return retVal;
	}

	chunkSequenceNumber = BULK_LAST_CHUNK;
	lastChunkReadWithSuccess = false;
	do {
		retVal = sironadriver_bulk_read_data( devInfo.handle, &seq, &receivedChunkSize, receivedChunkData );
		if ( retVal != SIRONA_DRIVER_NO_ERROR ) {
			commError = true;
		} else if ( seq == BULK_LAST_CHUNK ) {
			lastChunkReadWithSuccess = true;
		} else if (seq != chunkSequenceNumber + 1) {
			commError = true;
			retVal = SIRONA_DRIVER_STREAM_STOPPED;
		}

		chunkSequenceNumber++;
		annotFile.write( receivedChunkData, receivedChunkSize );
		if (commError) {
			annotFile.close();
			Utilities::removeAllProcedureFiles(datFileName);
			QThread::msleep( 100 );
			sironadriver_annotation_stop_transfer( devInfo.handle );
			LOGMSG("commError during annotation download (%d)", eventNo);
			return retVal;
		}
	} while ( !lastChunkReadWithSuccess );

	annotFile.close();

	createJsonFile( datFileName, &eventHeader );
	if (generateResFile) {
		createResFile(devInfo.devSerialNumber, eventHeader.creation_time - devConfig.pre_trigger, &eventFile);
	}
	zipEventFiles( datFileName );

	if ( retVal == SIRONA_DRIVER_NO_ERROR ) {
		emit sDeviceHandlerMessage( true, true, tr( "INFO: Event %1 from %2 downloaded with success." ).arg( eventNo + 1 ).arg( eventCount ) );
	}

	return retVal;
}

void DeviceHandler::markFilesSent( qint32 eventNo )
{
	int retVal;

	if ( devConfig.device_mode == PROCTYPE_EVENT || devConfig.device_mode == PROCTYPE_POST ) {
		retVal = sironadriver_event_mark_sent( devInfo.handle, eventNo );
		if ( retVal == SIRONA_DRIVER_NO_ERROR ) {
			emit sDeviceHandlerMessage( true, true, tr( "INFO: Event %1 marked as sent with success." ).arg( eventNo + 1 ) );
		} else {
			emit sDeviceHandlerMessage( true, false, tr( "Error [109]: Event downloaded with success, but failed to mark Event as sent." ) );
		}
	} else if ( devConfig.device_mode == PROCTYPE_HOLTER ) {

		devConfig.download_complete = HOLTER_DOWNLOAD_COMPLETE;
		retVal = parameterWrite( "DOWN_COMP" );
		if ( retVal != SIRONA_DRIVER_NO_ERROR ) {
			LOGMSG("Disconnect due to DOWN_COMP parameter write failure.");
			deviceDisconnect( false, retVal );
			emit sDeviceHandlerMessage( true, false, tr( "Error [110]: Holter downloaded with success, but failed to mark Holter as sent." ) );
		}

		retVal = parameterCommit();
		if ( retVal != SIRONA_DRIVER_NO_ERROR ) {
			LOGMSG("Disconnect due to parameter commit failure.");
			deviceDisconnect( false, retVal );
			emit sDeviceHandlerMessage( true, false, tr( "Error [111]: Holter downloaded with success, but failed to mark Holter as sent." ) );
		} else {
			emit sDeviceHandlerMessage( true, true, tr( "INFO: Holter marked as sent with success." ) );
		}
	}
}

int DeviceHandler::bulkReadAll( QByteArray * data )
{
	uint32_t nextSeqNo = 0;
	uint32_t seqNo = 0;
	uint32_t dataSize;
	char buffer[8192];  // Maximum size of packet received
	int retVal = SIRONA_DRIVER_NO_ERROR;
	while (retVal == SIRONA_DRIVER_NO_ERROR ) {
		retVal = sironadriver_bulk_read_data(devInfo.handle, &seqNo, &dataSize, buffer);
		if ((retVal != SIRONA_DRIVER_NO_ERROR) || (dataSize == 0)) {
			return retVal;
		} else if (seqNo != nextSeqNo) {
			return SIRONA_DRIVER_NETWORK_WRONG_SEQUNCE_NUMBER;
		} else {
			nextSeqNo = seqNo + 1;
			data->append(buffer, dataSize);
		}
	}
	return retVal;
}


void DeviceHandler::logDownload()
{
	int retVal = SIRONA_DRIVER_NO_ERROR;
	int retries = 3;

	QByteArray * dataLog = new QByteArray();


	for ( uint i = 0; i < 100; i++ ) {
		retVal = sironadriver_log_start_transfer(devInfo.handle);
		if ( retVal == SIRONA_DRIVER_NO_ERROR ) {
			retVal = bulkReadAll(dataLog);

			if ( retVal == SIRONA_DRIVER_NO_ERROR ) {
				break;	/* success */
			} else {
				LOGMSG("logDownload() Error bulkReadAll()  retVal: %d", retVal );
				retries--;
				if (retries == 0) {
					emit sDeviceHandlerMessage( false, false, tr( "Error [112]: There was an error during LOG data transfer." ) );
					delete dataLog;
					LOGMSG( "logDownload()   Error: There was an error during LOG data transfer." );
					return;
				} else {
					i--;
					retVal = sironadriver_log_stop_transfer(devInfo.handle);
					if (retVal != SIRONA_DRIVER_NO_ERROR) {
						LOGMSG( "logDownload()   Error: LOG Stop Xfer    SironaDriverError = %d", retVal );
					}
				}
			}
			delete dataLog;
		} else if (retries > 0) {
			retries--;
			i--;
			LOGMSG( "logDownload()  Error in sironadriver_log_start_transfer() = %d   i = %d   retries = %d", retVal, i, retries );
		} else {
			LOGMSG( "logDownload()  Error in sironadriver_log_start_transfer() = %d   i = %d   retries = %d    Giving up", retVal, i, retries );
			return;
		}
	}

	LOGMSG("logDownload() Success.");

	QString strLog = decodeFwLog( dataLog->data(), dataLog->length() );
	emit showFloatingDisplay(strLog);

	delete dataLog;

	return;
}


int DeviceHandler::holterDownload( const QString &path, quint32 procedureTime )
{
	LOGMSG( "holterDownload" );
	int retVal = SIRONA_DRIVER_NO_ERROR;
	int maxChunks = 100;
	quint32 chunkSize = procedureTime / maxChunks;
	int retries = 3;

	if ( procedureTime <= 600 ) {
		chunkSize = procedureTime / 5;
		maxChunks = 5;
	}

	QByteArray ba( chunkSize, '\0' );

	QString datFileName = QDir::cleanPath( QString( "%1\\%2%3_%4_%5.dat" )
			.arg( path )
			.arg( devInfo.devSerialNumber )
			.arg( ( ! devConfig.sitecode.isEmpty() ) ? QString("_%1").arg(devConfig.sitecode) : "" )
			.arg( QString( devConfig.patient_id ).replace( QRegExp( "\\W" ), "_" ) )
			.arg(QDateTime::fromTime_t(devConfig.procedure_start_date).toString("yyyyMMddhhmmss")));

	if ( procedureTime > 0 ) {
		QFile holterFile( datFileName );
		QFile annotFile( Utilities::changeExtenstion( datFileName, "atr" ) );
		httpFileList.clear();
		httpFileList.append( datFileName );

		createHeaFile( PROCTYPE_HOLTER, datFileName, nullptr );
		createJsonFile( datFileName, nullptr );

		if ( !holterFile.open( QIODevice::WriteOnly ) ) {
			emit sDeviceHandlerMessage( false, false, tr( "Error [113]: Couldn't open holter file to write to.\nCommand execution cannot proceed." ) );
			LOGMSG( "holterDownload()   Error: Couldn't open holter file to write to." );
			return -1;
		}

		for ( uint i = 0; i <= (uint) maxChunks ; i++ ) {
			retVal = sironadriver_ecg_start_transfer( devInfo.handle, EVENT_NUMBER_THAT_REFERENCES_HOLTER, i * chunkSize, chunkSize);
			if ( retVal == SIRONA_DRIVER_NO_ERROR ) {
				emit sDeviceDownloadProgress( i, maxChunks );
				QByteArray * data = new QByteArray();
				retVal = bulkReadAll(data);
				if (retVal != SIRONA_DRIVER_NO_ERROR) {
					LOGMSG("Holter bulkReadAll error");
					retries--;
					if (retries == 0) {
						emit sDeviceHandlerMessage( false, false, tr( "Error [114]: There was an error during ECG data transfer." ) );
						holterFile.close();
						delete data;
						LOGMSG( "holterDownload()   Error: There was an error during ECG data transfer." );
						return retVal;
					} else {
						i--;
						retVal = sironadriver_ecg_stop_transfer(devInfo.handle);
						if (retVal != SIRONA_DRIVER_NO_ERROR) {
							LOGMSG( "holterDownload()   Error: ECG Stop Xfer    SironaDriverError = %d", retVal );
						}
					}
				} else {
					LOGMSG("bulkReadAll success!");
					retries = 3;
					holterFile.write(*data);
				}
				delete data;
			} else if (retries > 0) {
				retries--;
				i--;
				LOGMSG( "holterDownload()  SironaDriverError = %d   i = %d   retries = %d", retVal, i, retries );
			} else {
				holterFile.close();
				LOGMSG( "holterDownload()  SironaDriverError = %d   i = %d   retries = %d    Giving up with holterFile.close", retVal, i, retries );
				return retVal;
			}
		}

		holterFile.close();

		if ( !annotFile.open( QIODevice::WriteOnly ) ) {
			LOGMSG( "holterDownload()  Error: Couldn't open annotation file to write to." );
			emit sDeviceHandlerMessage( false, false, tr( "Error [115]: Couldn't open annotation file to write to.\nCommand execution cannot proceed." ) );
			return retVal;
		}

		retVal = sironadriver_annotation_start_transfer( devInfo.handle, EVENT_NUMBER_THAT_REFERENCES_HOLTER, 0, procedureTime );
		if ( retVal == SIRONA_DRIVER_NO_ERROR ) {
			QByteArray * data = new QByteArray();
			retVal = bulkReadAll(data);
			if (retVal != SIRONA_DRIVER_NO_ERROR ) {
				annotFile.close();
				delete data;
				LOGMSG( "holterDownload() bulkReadAll() SironaDriverError = %d", retVal );
				return retVal;
			} else {
				annotFile.write(*data);
			}
			delete data;
		} else {
			LOGMSG( "holterDownload() sironadriver_annotation_start_transfer() SironaDriverError = %d", retVal );
			annotFile.close();
			return retVal;
		}

		annotFile.close();
		if (generateResFile) {
			createResFile(devInfo.devSerialNumber, devConfig.procedure_start_date, &holterFile);
		}
	}

	LOGMSG( "holterDownload() zipEventFiles(%s)...", datFileName.toLatin1().data() );
	zipEventFiles( datFileName );

	emit sDeviceDownloadProgress( procedureTime, procedureTime );
	if ( retVal == SIRONA_DRIVER_NO_ERROR ) {
		emit sDeviceHandlerMessage( true, true, tr( "INFO: Download holter command executed with success." ) );
		LOGMSG( "holterDownload() Download holter command executed with success." );
	}

	LOGMSG( "holterDownload() Done." );
	return retVal;
}

void DeviceHandler::deviceGetBatteryStatus()
{
	LOGMSG( "deviceGetBatteryStatus" );
	int retVal = SIRONA_DRIVER_NO_ERROR;
	quint32 batteryVoltage = 0;

	retVal = sironadriver_get_battery_voltage( devInfo.handle, &batteryVoltage );
	if ( retVal == SIRONA_DRIVER_NO_ERROR ) {
		emit sDeviceHandlerMessage( true, true, tr( "INFO: Get battery status command executed with success." ) );
		emit sDeviceBatteryStatus( batteryVoltage );
		resetReconnectTimer();
	} else {
		LOGMSG("Disconnect due to get battery voltage failure.");
		deviceDisconnect( false, retVal );
	}
}

void DeviceHandler::deviceStartProcedure()
{
	LOGMSG( "deviceStartProcedure" );
	int retVal = SIRONA_DRIVER_NO_ERROR;

	retVal = sironadriver_start_procedure( devInfo.handle );
	if ( retVal == SIRONA_DRIVER_NO_ERROR ) {
		emit sDeviceHandlerMessage( true, true, tr( "INFO: Start Procedure command executed with success." ) );
		emit sDeviceProcedureStarted();
		resetReconnectTimer();
	} else {
		LOGMSG("Disconnect due to start procedure failure.");
		deviceDisconnect( false, retVal );
	}
}

void DeviceHandler::deviceStopProcedure()
{
	LOGMSG( "deviceStopProcedure" );
	int retVal = SIRONA_DRIVER_NO_ERROR;

	retVal = sironadriver_stop_procedure( devInfo.handle );
	if ( retVal == SIRONA_DRIVER_NO_ERROR ) {
		emit sDeviceHandlerMessage( true, true, tr( "INFO: Stop Procedure command executed with success." ) );
		emit sDeviceProcedureStopped();
		resetReconnectTimer();
	} else {
		LOGMSG("Disconnect due to stop procedure command failure.");
		deviceDisconnect( false, retVal );
	}
}

void DeviceHandler::deviceSoftReset()
{
	LOGMSG( "deviceSoftReset()" );
	int retVal = SIRONA_DRIVER_NO_ERROR;

	retVal = sironadriver_soft_reset( devInfo.handle );
	if ( retVal == SIRONA_DRIVER_NO_ERROR ) {
		emit sDeviceHandlerMessage( true, true, tr( "INFO: Soft Reset command executed with success." ) );
		deviceSetTime();
		deviceParameterReadAll();
		resetReconnectTimer();
		emit sDeviceSoftResetComplete();
	} else {
		LOGMSG("Disconnect due to soft reset failure.");
		deviceDisconnect( false, retVal );
	}
}

void DeviceHandler::deviceSetTime()
{
	LOGMSG( "deviceSetTime" );
	int retVal = SIRONA_DRIVER_NO_ERROR;
	quint32 utcTime;
	quint32 timeZone;

	QDateTime currentTime = QDateTime::currentDateTime();
	QDateTime utc = currentTime.toUTC();
	utc.setTimeSpec( Qt::LocalTime );
	currentTime.setUtcOffset( utc.secsTo( currentTime ) );
	utcTime = currentTime.toTime_t();
	timeZone = ( currentTime.utcOffset() / 60 );

	LOGMSG("sironadriver_set_device_time( handle, %ld, %ld )", (long) utcTime, (long) timeZone );
	retVal = sironadriver_set_device_time( devInfo.handle, utcTime, timeZone );
	if ( retVal == SIRONA_DRIVER_NO_ERROR ) {
		devConfig.time_zone = ( quint16 ) ( timeZone & 0xFFFF );
		emit sDeviceHandlerMessage( true, true, tr( "INFO: Successfully sent system time to the device." ) );
		emit sDeviceConfiguration( devConfig );
		resetReconnectTimer();
	} else {
		LOGMSG("Disconnect due to set device time failure.");
		deviceDisconnect( false, retVal );
	}
}

void DeviceHandler::deviceStartLiveECG()
{
	LOGMSG( "deviceStartLiveECG" );
	const int PAYLOAD_SIZE = 54;
	uint32_t packetSize = 0;
	QByteArray data;

	int retVal = SIRONA_DRIVER_NO_ERROR;

	sironadriver_liveECG_bulk_read_data( nullptr, nullptr, nullptr, &packetSize, nullptr );

	if ( packetSize != PAYLOAD_SIZE ) {
		LOGMSG( "Memory corruption might happen. Target buffer too small. Closing connection." );
		retVal = SIRONA_DEVICE_MEMORY_ERROR;
		deviceDisconnect( false, retVal );
		return;
	}

	m_TerminateLiveECG = false;

	retVal = sironadriver_liveECG_start_streaming( devInfo.handle );
	if ( retVal == SIRONA_DRIVER_NO_ERROR ) {
		emit sDeviceStartLiveECG();
	} else {
		LOGMSG("Disconnect due to start live ECG failure");
		deviceDisconnect( false, retVal );
		return;
	}

	data.resize( packetSize );

	while ( ( !m_TerminateLiveECG ) && ( !QThread::currentThread()->isInterruptionRequested() ) ) {
		uint32_t seq = 0;
		uint32_t status = 0;
		uint32_t size = 0;

		retVal = sironadriver_liveECG_bulk_read_data( devInfo.handle, &seq, &status, &size, data.data() );
		if ( retVal != SIRONA_DRIVER_NO_ERROR ) {
			if ( retVal == SIRONA_DRIVER_STREAM_STOPPED ) {
				emit sDeviceStopLiveECG( retVal );
			} else {
				LOGMSG("Disconnect due to liveECG bulk read failure.");
				deviceDisconnect( true, retVal );
			}

			return;
		}

		emit sDeviceLiveECG( seq, status, data );
	}

	retVal = sironadriver_liveECG_stop_streaming( devInfo.handle );
	if ( retVal != SIRONA_DRIVER_NO_ERROR ) {
		LOGMSG("Disconnect due to liveECG stop streaming failure.");
		deviceDisconnect( false, retVal );
		return;
	}

	emit sDeviceStopLiveECG( retVal );

	resetReconnectTimer();
}

void DeviceHandler::deviceStopLiveECG()
{
	LOGMSG( "stopLiveECG" );
	m_TerminateLiveECG = true;
}

void DeviceHandler::timerEvent( QTimerEvent *event )
{
	if ( event->timerId() == m_ConnectionTimer ) {
		isDeviceConnected();
	}
}


#define VALSIZED(val,siz)	( (valueSize == 1) ? *((quint8 *)value) : ((valueSize == 2) ? *((quint16 *)value) : *((quint32 *)value)))

void DeviceHandler::populateConfigEntry( ConfigParams &devConf, void *name, quint32 valueSize, void *value )
{
	// LOGMSG( "DeviceHandler::populateConfigEntry(devConf,%s,%d,value)", name, valueSize );
	QString parName ( (char *) name );

	if ( parName.compare( "CODE_REV" ) == 0 ) {
		devConf.svn_rev = VALSIZED( value, valueSize );
	} else if ( parName.compare( "DEV_ID" ) == 0 ) {
		char *tmp = (char *) value;
		devConf.class_id = *( ( quint8 * ) tmp++ );
		devConf.device_id = *( ( quint32 * ) tmp );
		sprintf( devInfo.devSerialNumber, "%02X%08X", devConf.class_id, devConf.device_id  );
	} else if ( parName.compare( "FW_REV" ) == 0 ) {
		devConf.fw_rev = VALSIZED( value, valueSize );
	} else if ( parName.compare( "PROC_STATE" ) == 0 ) {
		devConf.procedure_state = VALSIZED( value, valueSize );
	} else if ( parName.compare( "PRE_TRIGGER" ) == 0 ) {
		devConf.pre_trigger = VALSIZED( value, valueSize );
	} else if ( parName.compare( "POST_TRIGGER" ) == 0 ) {
		devConf.post_trigger = VALSIZED( value, valueSize );
	} else if ( parName.compare( "ERR_CODE" ) == 0 ) {
		devConf.error_code = VALSIZED( value, valueSize );
	} else if ( parName.compare( "DEV_MODE" ) == 0 ) {
		devConf.device_mode = VALSIZED( value, valueSize );
	} else if ( parName.compare( "REC_MODE" ) == 0 ) {
		devConf.operation_mode = VALSIZED( value, valueSize );
	} else if ( parName.compare( "PROC_START_TIME" ) == 0 ) {
		devConf.procedure_start_date = VALSIZED( value, valueSize );
LOGMSG( "DeviceHandler::populateConfigEntry()  %s = %ld", name, (long) devConf.procedure_start_date );
	} else if ( parName.compare( "TIME_ZONE" ) == 0 ) {
		devConf.time_zone = VALSIZED( value, valueSize );
	} else if ( parName.compare( "DOWN_COMP" ) == 0 ) {
		devConf.download_complete = VALSIZED( value, valueSize );
	} else if ( parName.compare( "REC_LENGTH" ) == 0 ) {
		devConf.recording_len = VALSIZED( value, valueSize );
	} else if ( parName.compare( "AUTO_EV_LIMIT" ) == 0 ) {
		devConf.automatic_event_limit = VALSIZED( value, valueSize );
	} else if ( parName.compare( "MAN_EV_LIMIT" ) == 0 ) {
		devConf.manual_event_limit = VALSIZED( value, valueSize );
	} else if ( parName.compare( "AUDIO_MUTE" ) == 0 ) {
		devConf.audiable_on = VALSIZED( value, valueSize );
	} else if ( parName.compare( "TACHY_ON_FLG" ) == 0 ) {
		devConf.analyzer_tachy_on = VALSIZED( value, valueSize );
	} else if ( parName.compare( "TACHY_THRESH" ) == 0 ) {
		devConf.analyzer_tachy_threshold = VALSIZED( value, valueSize );
	} else if ( parName.compare( "BRADY_ON_FLG" ) == 0 ) {
		devConf.analyzer_brady_on = VALSIZED( value, valueSize );
	} else if ( parName.compare( "BRADY_THRESH" ) == 0 ) {
		devConf.analyzer_brady_threshold = VALSIZED( value, valueSize );
	} else if ( parName.compare( "PAUSE_ON_FLG" ) == 0 ) {
		devConf.analyzer_pause_on = VALSIZED( value, valueSize );
	} else if ( parName.compare( "PAUSE_THRESH" ) == 0 ) {
		devConf.analyzer_pause_threshold = VALSIZED( value, valueSize );
	} else if ( parName.compare( "AF_ON_FLG" ) == 0 ) {
		devConf.analyzer_af_on = VALSIZED( value, valueSize );
	} else if ( parName.compare( "ANAL_CH_MASK" ) == 0 ) {
		devConf.analyzer_channel_mask = VALSIZED( value, valueSize );
	} else if ( parName.compare( "TTM_EN_FLG" ) == 0 ) {
		devConf.ttm_enabled = VALSIZED( value, valueSize );
	} else if ( parName.compare( "TTM_3X_FLG" ) == 0 ) {
		devConf.ttm_speedup_on = VALSIZED( value, valueSize );
	} else if ( parName.compare( "CABLE_ID" ) == 0 ) {
		devConf.cable_id = VALSIZED( value, valueSize );
	} else if ( parName.compare( "SAMP_RATE" ) == 0 ) {
		devConf.sample_rate = VALSIZED( value, valueSize );
	} else if ( parName.compare( "PAT_DOB" ) == 0 ) {
		devConf.patient_date_of_birth = VALSIZED( value, valueSize );
	} else if ( parName.compare( "PAT_ID" ) == 0 ) {
		devConf.patient_id = QString::fromUtf8( (char *) value );
	} else if ( parName.compare( "PAT_NAME" ) == 0 ) {
		devConf.patient_first_name = QString::fromUtf8( (char *) value );
	} else if ( parName.compare( "PAT_MIDDLE" ) == 0 ) {
		devConf.patient_middle_initial = QString::fromUtf8( (char *) value );
	} else if ( parName.compare( "PAT_LAST" ) == 0 ) {
		devConf.patient_last_name = QString::fromUtf8( (char *) value );
	} else if ( parName.compare( "PHY_NAME" ) == 0 ) {
		devConf.physician_name = QString::fromUtf8( (char *) value );
	} else if ( parName.compare( "COMMENT" ) == 0 ) {
		devConf.comment = QString::fromUtf8( (char *) value );
	} else if ( parName.compare( "MODEL_NUMBER" ) == 0 ) {
		devConf.model_number = QString::fromUtf8( (char *)  value );
	} else if ( parName.compare( "TECH_NAME" ) == 0 ) {
		devConf.technician_name = QString::fromUtf8( (char *) value );
	} else if ( parName.compare( "FACILITY" ) == 0 ) {
		devConf.facility = QString::fromUtf8( (char *) value );
	} else if ( parName.compare( "MEDICATION_NOTES" ) == 0 ) {
		devConf.medication_notes = QString::fromUtf8( (char *) value );
	} else if ( parName.compare( "DIAGNOSIS" ) == 0 ) {
		devConf.diagnosis = QString::fromUtf8( (char *) value );
	} else if ( parName.compare( "DIARY_ENTRIES" ) == 0 ) {
		// devConf.diary_entries = QString::fromUtf8( (char *) value );
	} else if ( parName.compare ( "MCT_ENABLED" ) == 0 ) {
		devConf.mct_enabled = VALSIZED( value, valueSize );
	} else if ( parName.compare( "PROC_TIME" ) == 0 ) {
		devConf.procedure_time = VALSIZED( value, valueSize );
	} else if ( parName.compare( "BATT_VOLT" ) == 0 ) {
		LOGMSG("Unused param BATT_VOLT: %ld", (long) VALSIZED( value, valueSize ) );
	} else if ( parName.compare( "HOLT_START" ) == 0 ) {
		LOGMSG("Unused param HOLT_START: %ld", (long) VALSIZED( value, valueSize ) );
	} else if ( parName.compare( "LAST_CABLE" ) == 0 ) {
		LOGMSG("Unused param LAST_CABLE: %ld", (long) VALSIZED( value, valueSize ) );
	} else if ( parName.compare( "ANNOT_SENT" ) == 0 ) {
		LOGMSG("Unused param ANNOT_SENT: %ld", (long) VALSIZED( value, valueSize ) );
	} else if ( parName.compare( "DIARY_TIMEOUT" ) == 0 ) {
		LOGMSG("Unused param DIARY_TIMEOUT: %ld", (long) VALSIZED( value, valueSize ) );
	} else if ( parName.compare( "LAST_REBOOT" ) == 0 ) {
		LOGMSG("Unused param LAST_REBOOT: %ld", (long) VALSIZED( value, valueSize ) );
	} else {
		LOGMSG( "Unprocessed parameter: %s", (char *) name );
	}
}


void DeviceHandler::createHeaFile( quint8 device_mode, QString datFileName, EventHeader *eventHeader )
{
	LOGMSG( "createHeaFile %d %s", device_mode, datFileName.toLatin1().data() );
	QString heaFileName = Utilities::changeExtenstion( datFileName, "hea" );
	QFile heaFile( heaFileName );

	if ( heaFile.open( QIODevice::WriteOnly ) ) {

		QString heaData;
		QString time;

		if ( device_mode == PROCTYPE_EVENT ) {
			QString event_type;

			if ( eventHeader == nullptr ) {
				emit sDeviceHandlerMessage( true, false, tr( "Error [116]: createHeaFile called with nullptr eventHeader." ) );

				return;
			}

			time = QDateTime::fromTime_t( eventHeader->creation_time - devConfig.pre_trigger ).toString( "hh:mm:ss dd/MM/yyyy" );

			switch ( eventHeader->event_type ) {
				case 0:
					event_type = "Manual";
					break;
				case 1:
					event_type = "Automatic - BRADY";
					break;
				case 2:
					event_type = "Automatic - TACHY";
					break;
				case 4:
					event_type = "Automatic - PAUSE";
					break;
				case 5:
					event_type = "Manual - Asymptomatic";
					break;
				case 6:
					event_type = "Manual - Symptomatic";
					break;
				case 16:
					event_type = "Automatic - ARRHYTHMIA_AF";
					break;
				case 32:
					event_type = "Automatic - ARRHYTHMIA_AF_STOP";
					break;
				default:
					event_type = "Unknown";
					break;
			}

			//nameOfFile.replace( QString(" "), QString("_") );
			heaData = QString( "%1 %2 %3 %4 %5\n" )
					  .arg( QFileInfo( QFile( datFileName ) ).baseName() )
					  .arg( ( eventHeader->no_channels + 1 ) )
					  .arg( devConfig.sample_rate )
					  .arg( "0" )
					  .arg( time );

			for ( int c = 0; c < ( eventHeader->no_channels + 1 ); c++ ) {
				heaData += QString( "%1%2 %3 %4 %5 %6\n" )
						   .arg( QFileInfo( QFile( datFileName ) ).baseName() )
						   .arg( ".dat" )
						   .arg( QString::number( ECG_DATA_FORMAT ) )
						   .arg( QString::number( ( double )( ( double )POSIBLE_VALUES_FOR_10_BIT_RESOLUTION / ( double )ECG_SIGNAL_RANGE_10_MV ) ) )
						   .arg( QString::number( ADC_BIT_RESOLUTION_FOR_311_FORMAT ) )
						   .arg( QString::number( ADC_UNITS_OFFSET_TO_MIDDLE_OF_RANGE ) );
			}

			heaData += QString( "%1 %2 %3 %4 %5\n%6 %7 %8 %9\n" )
					   .arg( "# Event" )
					   .arg( eventHeader->event_type )
					   .arg( "at" )
					   .arg( eventHeader->creation_time )
					   .arg( event_type )
					   .arg( "# PRE" )
					   .arg( devConfig.pre_trigger )
					   .arg( "POST" )
					   .arg( devConfig.post_trigger );

		} else if ( device_mode == PROCTYPE_POST ) {
			QString event_type;

			if ( eventHeader == nullptr ) {
				emit sDeviceHandlerMessage( true, false, tr( "Error [117]: createHeaFile called with NULL eventHeader." ) );

				return;
			}

			time = QDateTime::fromTime_t( eventHeader->creation_time - devConfig.pre_trigger ).toString( "hh:mm:ss dd/MM/yyyy" );

			event_type = "Manual";

			heaData = QString( "%1 %2 %3 %4 %5\n" )
					  .arg( QFileInfo( QFile( datFileName ) ).baseName() )
					  .arg( ( eventHeader->no_channels + 1 ) )
					  .arg( devConfig.sample_rate )
					  .arg( "0" )
					  .arg( time );

			for ( int c = 0; c < ( eventHeader->no_channels + 1 ); c++ ) {
				heaData += QString( "%1%2 %3 %4 %5 %6\n" )
						   .arg( QFileInfo( QFile( datFileName ) ).baseName() )
						   .arg( ".dat" )
						   .arg( QString::number( ECG_DATA_FORMAT ) )
						   .arg( QString::number( ( double )( ( double )POSIBLE_VALUES_FOR_10_BIT_RESOLUTION / ( double )ECG_SIGNAL_RANGE_10_MV ) ) )
						   .arg( QString::number( ADC_BIT_RESOLUTION_FOR_311_FORMAT ) )
						   .arg( QString::number( ADC_UNITS_OFFSET_TO_MIDDLE_OF_RANGE ) );
			}

			heaData += QString( "%1 %2 %3 %4 %5\n%6 %7 %8 %9\n" )
					   .arg( "# Post" )
					   .arg( eventHeader->event_type )
					   .arg( "at" )
					   .arg( eventHeader->creation_time )
					   .arg( event_type )
					   .arg( "# PRE" )
					   .arg( devConfig.pre_trigger )
					   .arg( "POST" )
					   .arg( devConfig.post_trigger );

		} else if ( device_mode == PROCTYPE_HOLTER || device_mode == PROCTYPE_MCT  ) {
			quint32 no_channel = 0;
			time = QDateTime::fromTime_t(devConfig.procedure_start_date).toString("hh:mm:ss dd/MM/yyyy");

			switch ( devConfig.operation_mode ) {
				case CHANNEL_1_HOLTER:
				case CHANNEL_1_EVENT:
					no_channel = 1;
					break;

				case CHANNEL_2_HOLTER:
				case CHANNEL_2_PATCH:
				case CHANNEL_2_EVENT:
					no_channel = 2;
					break;

				case CHANNEL_3_HOLTER:
					no_channel = 3;
					break;

				default:
					break;
			}

			heaData = QString( "%1 %2 %3 %4 %5\n" )
					  .arg( QFileInfo( QFile( datFileName ) ).baseName() )
					  .arg( no_channel )
					  .arg( devConfig.sample_rate )
					  .arg( "0" )
					  .arg( time );

			for ( quint32 c = 0; c < no_channel; c++ ) {
				heaData += QString( "%1%2 %3 %4 %5 %6\n" )
						   .arg( QFileInfo( QFile( datFileName ) ).baseName() )
						   .arg( ".dat" )
						   .arg( QString::number( ECG_DATA_FORMAT ) )
						   .arg( QString::number( ( double )( ( double )POSIBLE_VALUES_FOR_10_BIT_RESOLUTION / ( double )ECG_SIGNAL_RANGE_10_MV ) ) )
						   .arg( QString::number( ADC_BIT_RESOLUTION_FOR_311_FORMAT ) )
						   .arg( QString::number( ADC_UNITS_OFFSET_TO_MIDDLE_OF_RANGE ) );
			}

		} else {
			emit sDeviceHandlerMessage( true, false, tr( "Error [118]: Invalid input parameter device mode, couldn't create hea file." ) );
		}

		heaFile.write( heaData.toUtf8() );
		heaFile.close();

	} else {
		emit sDeviceHandlerMessage( true, false, tr( "Error [119]: Couldn't open header file to write to." ) );
	}
}

void DeviceHandler::createJsonFile(  QString datFileName, EventHeader *eventHeader )
{
	LOGMSG( "createJsonFile %s", datFileName.toLatin1().data() );
	QString jsonFileName = Utilities::changeExtenstion( datFileName, "json" );
	QFile jsonFile( jsonFileName );

	if ( ! jsonFile.open( QFile::WriteOnly ) ) {

		emit sDeviceWarning( tr( "File open json file for writing(%1)." ).arg( jsonFile.fileName() ) );

		return;
	}

	QByteArray jsonData;
	QJsonObject json, deviceObject, patientObject, timeObject;
	QDateTime procedureStartTime, stripStartTime, transmitTime, utc;
	QString procedureStartTimeStr, stripStartTimeStr, transmitTimeStr;
	QDate epoch( 1900, 1, 1 );

	procedureStartTime = QDateTime::fromTime_t( devConfig.procedure_start_date );
	procedureStartTime.setUtcOffset( devConfig.time_zone * 60 );
	if ( eventHeader ) {
		stripStartTime = QDateTime::fromTime_t( eventHeader->creation_time - devConfig.pre_trigger );
		stripStartTime.setUtcOffset( devConfig.time_zone * 60 );
	}
	transmitTime = QDateTime::currentDateTime();
	utc = transmitTime.toUTC();
	utc.setTimeSpec( Qt::LocalTime );
	transmitTime.setUtcOffset( utc.secsTo( transmitTime ) );

	procedureStartTimeStr = procedureStartTime.toString( Qt::ISODate );
	if ( eventHeader ) {
		stripStartTimeStr = stripStartTime.toString( Qt::ISODate );
	}
	transmitTimeStr = transmitTime.toString( Qt::ISODate );

	deviceObject.insert( "Device ID", devInfo.devSerialNumber );
	json.insert( "Device", deviceObject );

	patientObject.insert( "Patient ID",  devConfig.patient_id );
	patientObject.insert( "Patient First Name", devConfig.patient_first_name );
	patientObject.insert( "Patient Middle Initial", devConfig.patient_middle_initial );
	patientObject.insert( "Patient Last Name", devConfig.patient_last_name );
	patientObject.insert( "Patient Date of Birth",  epoch.addDays( devConfig.patient_date_of_birth ).toString( "yyyy-MM-dd" ) );
	patientObject.insert( "Physician Name", devConfig.physician_name );
	patientObject.insert( "Technician Name", devConfig.technician_name );
	patientObject.insert( "Facility Name", devConfig.facility );
	patientObject.insert( "Medication Notes", devConfig.medication_notes );
	patientObject.insert( "Diagnosis", devConfig.diagnosis );
	if ( glbCustomCode != "ACT1" ) {
		patientObject.insert( "Comment", devConfig.comment );
	}

	// qDebug() << "createJsonFile()   diary_entries = " << devConfig.diary_entries;
	QJsonArray diaryEntriesObject;
	for ( int i = 0 ; i < devConfig.diary_entries.count() ; i++ ) {
		QJsonObject diaryEntryObject;

		diaryEntryObject["Date"] = devConfig.diary_entries[i]["Date"];
		diaryEntryObject["Event"] = devConfig.diary_entries[i]["Event"];
		diaryEntryObject["Symptoms"] = devConfig.diary_entries[i]["Symptoms"];
		diaryEntryObject["Time"] = devConfig.diary_entries[i]["Time"];

		diaryEntriesObject.append(diaryEntryObject);
	}
	patientObject.insert( "Diary Entries", diaryEntriesObject );

	json.insert( "Patient", patientObject );


	timeObject.insert( "Procedure Start Time", procedureStartTimeStr );
	if ( eventHeader ) {
		timeObject.insert( "Event type", getEventTypeString( eventHeader->event_type ) );
		timeObject.insert( "Strip Start Time", stripStartTimeStr );
		timeObject.insert( "Strip Length", devConfig.pre_trigger + devConfig.post_trigger );
	}
	timeObject.insert( "Transmit Time", transmitTimeStr );
	json.insert( "Time", timeObject );

	jsonData = QJsonDocument( json ).toJson();
	jsonFile.write( jsonData );
	jsonFile.close();
}

// fail to build the res file unless resbuilder.cpp is included in the project (most customers don't need the res builder overhead)
bool buildRes(char *serialNumber, unsigned channelCount, unsigned sampleRate, uint32_t startTime, QFile *datFile) __attribute__((weak));
bool buildRes(char *serialNumber, unsigned channelCount, unsigned sampleRate, uint32_t startTime, QFile *datFile)
{
	Q_UNUSED(serialNumber);
	Q_UNUSED(channelCount);
	Q_UNUSED(sampleRate);
	Q_UNUSED(startTime);
	Q_UNUSED(datFile);
	return false;
}

void DeviceHandler::createResFile(char *serialNumber, uint32_t startTime, QFile *datFile)
{
	unsigned channelCount = 0;

	switch (devConfig.operation_mode) {
		case CHANNEL_1_HOLTER:
		case CHANNEL_1_EVENT:
		case CHANNEL_1_POST:
			channelCount = 1;
			break;

		case CHANNEL_2_HOLTER:
		case CHANNEL_2_PATCH:
		case CHANNEL_2_EVENT:
			channelCount = 2;
			break;

		case CHANNEL_3_HOLTER:
		case CHANNEL_3_EVENT:
			channelCount = 3;
			break;
	}

	if (!buildRes(serialNumber, channelCount, devConfig.sample_rate, startTime, datFile)) {
		emit sDeviceHandlerMessage(false, false, tr("Error [120]: createResFile failed."));
	}
}

void DeviceHandler::createZipFile( QString destinationPath, QList<QString> paths, QString password )
{
	LOGMSG( "createZipFile" );
#ifdef Q_OS_WIN
	QByteArray password_ba;
	char *password_cstr = nullptr;
	if (password != nullptr) {
		password_ba = password.toLocal8Bit();   // when converting from QString to char*, we need to keep the QByteArray in a variable so it stays in memory
		password_cstr = password_ba.data();
	}
	zlib_filefunc64_def ffunc;
	fill_win32_filefunc64W( &ffunc );
	zipFile zf = zipOpen2_64( destinationPath.constData(), APPEND_STATUS_CREATE, nullptr, &ffunc );
	if ( zf != nullptr ) {

		while ( paths.size() ) {

			QString fileName = paths.first();
			paths.removeFirst();
			QFile file( fileName );
			if ( file.open( QFile::ReadOnly ) ) {
				QByteArray fileData;
				long size = file.size();
				zip_fileinfo zfi = { {0, 0, 0, 0, 0, 0}, 0, 0, 0 };
				QByteArray fileShortName_ba = QFileInfo(fileName).fileName().toLocal8Bit();	  // when converting from QString to char*, we need to keep the QByteArray in a variable so it stays in memory
				char *fileShortName = fileShortName_ba.data();
				qulonglong crcValue = crc32(0L, nullptr, 0L);

				for (long count = 0; count < size; count += fileData.size()) {
					fileData = file.read(1000000);
					crcValue = crc32(crcValue, (const Bytef*)fileData.constData(), fileData.size());
				}
				file.reset();

#define S_OK 0
				if (S_OK == zipOpenNewFileInZip4_64(zf, fileShortName, &zfi, nullptr, 0, nullptr, 0, nullptr, Z_DEFLATED, Z_DEFAULT_COMPRESSION,
							0, -MAX_WBITS, DEF_MEM_LEVEL, Z_DEFAULT_STRATEGY, password_cstr, crcValue, 0, 1 << 11, 0)) {
					for (long count = 0; count < size; count += fileData.size()) {
						fileData = file.read(1000000);
						zipWriteInFileInZip(zf, fileData.constData(), fileData.size());
					}
					zipCloseFileInZip(zf);
				}

				file.close();
			}
		}
	}

	zipClose( zf, nullptr );
#else

	// TODO: implement zip file creation for non-Windows platforms

#endif // Q_OS_WIN
}

void DeviceHandler::zipEventFiles( QString datFileName )
{
	LOGMSG( "zipEventFiles" );

	// Make a list of all files to be zipped
	QList<QString> filesToZip;
	if (generateResFile) {
		filesToZip.append(Utilities::changeExtenstion(datFileName, "res"));
	} else {
		filesToZip.append(datFileName);
		filesToZip.append(Utilities::changeExtenstion(datFileName, "hea"));
		filesToZip.append(Utilities::changeExtenstion(datFileName, "atr"));
		filesToZip.append(Utilities::changeExtenstion(datFileName, "json"));
	}

	// Zip all files
	createZipFile ( Utilities::changeExtenstion( datFileName, "zip" ), filesToZip, zip_password );
}


void DeviceHandler::resetReconnectTimer()
{
	stopReconnectTimer();
	m_ConnectionTimer = startTimer( IDLE_PING_INTERVAL_MS );
	//LOGMSG( "Create DeviceHandler::m_ConnectionTimer = %d", m_ConnectionTimer );
}

void DeviceHandler::stopReconnectTimer()
{
	if ( m_ConnectionTimer ) {
		killTimer( m_ConnectionTimer );
		m_ConnectionTimer = 0;
	}
}

void DeviceHandler::setZipPassword( QString zipPassword )
{
	zip_password = zipPassword;
}


