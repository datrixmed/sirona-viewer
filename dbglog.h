
#ifndef DBGLOG_H
#define DBGLOG_H

#include <QtWidgets>
#include <QDebug>
#include <QFile>
#include <QTime>
#include <QRect>


/** {{{ class QQQ : QDebug()
 */
class QQQ
{
    struct Stream {
        Stream(QIODevice *device) : ts(device), ref(1), type(QtDebugMsg), space(true), message_output(false) {}
        Stream(QString *string) : ts(string, QIODevice::WriteOnly), ref(1), type(QtDebugMsg), space(true), message_output(false) {}
        Stream(QtMsgType t) : ts(&buffer, QIODevice::WriteOnly), ref(1), type(t), space(true), message_output(true) {}
        QTextStream ts;
        QString buffer;
        int ref;
        QtMsgType type;
        bool space;
        bool message_output;
    } *stream;
public:
    inline QQQ(QIODevice *device) : stream(new Stream(device)), pFile(0) {}
    inline QQQ(QtMsgType t) : stream(new Stream(t)), pFile(0) {}
    inline QQQ(const QQQ &o):stream(o.stream), pFile(0) { ++stream->ref; }
    inline QQQ &operator=(const QQQ &other);
    inline ~QQQ() {
        if (!--stream->ref) {
            if(stream->message_output) {
                QT_TRY {
                    qt_message_output(stream->type, QMessageLogContext(), stream->buffer.toLatin1().constData());
                } QT_CATCH(std::bad_alloc&) { /* We're out of memory - give up. */ }
            }
            delete stream;
            delete pFile;
        }
    }
    inline QQQ &space() { stream->space = true; stream->ts << ' '; return *this; }
    inline QQQ &nospace() { stream->space = false; return *this; }
    inline QQQ &maybeSpace() { if (stream->space) stream->ts << ' '; return *this; }

    inline QQQ &operator<<(QChar t) { stream->ts << '\'' << t << '\''; return maybeSpace(); }
    inline QQQ &operator<<(bool t) { stream->ts << (t ? "true" : "false"); return maybeSpace(); }
    inline QQQ &operator<<(char t) { stream->ts << t; return maybeSpace(); }
    inline QQQ &operator<<(signed short t) { stream->ts << t; return maybeSpace(); }
    inline QQQ &operator<<(unsigned short t) { stream->ts << t; return maybeSpace(); }
    inline QQQ &operator<<(signed int t) { stream->ts << t; return maybeSpace(); }
    inline QQQ &operator<<(unsigned int t) { stream->ts << t; return maybeSpace(); }
    inline QQQ &operator<<(signed long t) { stream->ts << t; return maybeSpace(); }
    inline QQQ &operator<<(unsigned long t) { stream->ts << t; return maybeSpace(); }
    inline QQQ &operator<<(qint64 t)
        { stream->ts << QString::number(t); return maybeSpace(); }
    inline QQQ &operator<<(quint64 t)
        { stream->ts << QString::number(t); return maybeSpace(); }
    inline QQQ &operator<<(float t) { stream->ts << t; return maybeSpace(); }
    inline QQQ &operator<<(double t) { stream->ts << t; return maybeSpace(); }
    inline QQQ &operator<<(const char* t) { stream->ts << QString::fromLatin1(t); return maybeSpace(); }
//    inline QQQ &operator<<(const QString & t) { stream->ts << t << "\n"; return maybeSpace(); }  //inserts newlines when don't want them. Easy to add "\n" in QQQ statment
    inline QQQ &operator<<(const QString & t) { stream->ts << t ; return maybeSpace(); }
    inline QQQ &operator<<(const QStringRef & t) { return operator<<(t.toString()); }
//    inline QQQ &operator<<(const QLatin1String &t) { stream->ts << t.latin1() << "\n"; return maybeSpace(); } //inserts newlines when don't want them. Easy to add "\n" in QQQ statment
    inline QQQ &operator<<(const QLatin1String &t) { stream->ts << t.latin1(); return maybeSpace(); }
//    inline QQQ &operator<<(const QByteArray & t) { stream->ts  << t << "\n"; return maybeSpace(); } //inserts newlines when don't want them. Easy to add "\n" in QQQ statment
    inline QQQ &operator<<(const QByteArray & t) { stream->ts  << t; return maybeSpace(); }
    inline QQQ &operator<<(const void * t) { stream->ts << t; return maybeSpace(); }

    inline QQQ &operator<<(QTextStreamFunction f) { stream->ts << f; return *this; }
    inline QQQ &operator<<(QTextStreamManipulator m) { stream->ts << m; return *this; } 

    inline QQQ &operator<<(const QRect & r) { stream->ts << "QRect(" << r.x() << "," << r.y() << "  " << r.width() << "x" << r.height() << ")"; return maybeSpace(); }

	QQQ &operator<<( QVector<int> & vectorInts );
	QQQ &operator<<( QVector<float> & vectorFloats );
	QQQ &operator<<( QMap<QString, short> m );
	QQQ &operator<<( QMap<QString, long> m );
	QQQ &operator<<( QHash<QString, short> m );
	QQQ &operator<<( QHash<QString, long> m );

public:
    QQQ( QString filename );

protected:
	QFile *pFile;

};
/* }}} */

#endif
